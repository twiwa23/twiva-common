<?php
include('twiva-config.php');
if(isset($_GET['id']) && isset($_GET['params'])) {
    $id = $_GET['id'];
    $params = $_GET['params'];
    $url = $_GET['url'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Twiva</title>
</head>
<body>
    <input type="hidden" id="id" value="<?php echo $id ?>" >
    <input type="hidden" id="params" value="<?php echo $params ?>" >
    <input type="hidden" id="url" value="<?php echo $url ?>" >
    <span id="err"></span>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/api.js"></script>
    <script>
        $( document ).ready(function() {
            checkId();
        });
        function checkId(){
            var json = {
                'id' : $("#id").val(),
                'params' : $("#params").val()
            }
            checkIdWithToken(json);
        }

        function checkIdWithToken(json) {
            __ajax_http("loginAsAdmin", json, { 'Accept': 'application/json' }, AJAX_CONF.apiType.POST, "POST LOGIN", __success_checkIdWithToken);
        }

        function __success_checkIdWithToken(response) {
            console.log(response);
            if(response.original.failure) {
                if(response.original.failure.status == 422){
                    window.location.href = 'business/bussiness-edit-profile-information.php'
                }
            }
            
            if(response.status == 422) {
                if(response.message) {
                    $("#err").show().css("color","red").html(response.message);
                }
                window.location.href = 'business/bussiness-edit-profile-information.php'
                // return ; 
            }
            if (response.status == false) {
                if(response.error){
                    $("#err").show().css("color","red").html(response.error);
                }
                if (response.message) {
                    $("#err").show().css("color","red").html("User not found");

                }
                else{
                    $("#err").show().css("color","red").html(response.message);

                }
                // window.location.href = 'business/business-edit-profile-information.php'
                return; 
            }

        __save_identity(response);
    }

        function __save_identity(response) {
            // identity = response.data;
            // console.log(response.original.status);
            if(response.original.status == true && response.original.data.type == 1) {
                localStorage.clear();
                STORAGE.set(STORAGE.accesstoken, response.original.token);
                STORAGE.set(STORAGE.userId, response.original.data.id);
                STORAGE.set(STORAGE.name, response.original.data.bussiness_name);
                STORAGE.set(STORAGE.logo, response.original.data.logo);
                STORAGE.set(STORAGE.referal_code, response.original.data.referal_code);
                STORAGE.set(STORAGE.points, response.original.data.points);
                if(response.original.data.is_user_update_to_new_platform == 0) {
                    STORAGE.set(STORAGE.needToUpdatePlatform, btoa(btoa(1)));
                } else {
                    STORAGE.set(STORAGE.needToUpdatePlatform, btoa(btoa(0)));
                }
                setTimeout(function() {
                        // goto(UI_URL.onboarding4);
                        // window.open($("#url").val());
                    // let url = data.data.url.split("?");
                    window.location.href = $("#url").val();
                }, 100);
            } else if(response.original.status == true && response.original.data.type == 2) {
                console.log(response.original.data)
                localStorage.clear();
                localStorage.setItem('_userInfo', JSON.stringify(response.original.data));
                localStorage.setItem('_userToken', response.original.token);
                window.open($("#url").val());

                // window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php`;
            }
            else {
                localStorage.clear();
                $("#err").show().css("color","red").html(response.original.error);
            }
            if(response.original.failure || response.status) {
                $("#err").show().css("color","red").html(response.original.failure.message);
                $("#err").show().css("color","red").html(response.message);
            }
        }
    </script>
</body>
</html>