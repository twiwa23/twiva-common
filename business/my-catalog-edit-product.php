<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Add product</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">



    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <link href="../css/custom-style.css" rel="stylesheet">

</head>



<?php include('common/side_menu.php'); ?>

<!-- page content -->
<div class="right_col add-product-page" role="main">
    <!-- <div class="page-title">My Catalog <span class="fa fa-chevron-right"></span> Edit Product</div> -->

    <!--********** Breadcrumb Start ***********-->
    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="#">My Catalog</a></li>
            <li class="active">Edit Product</li>
        </ul>
    </div>

    <!--**********  Breadcrumb End ***********-->
    <div class="edit-product-wrapper">
        <div id="product-edit">




        </div>
        <div class="catalog-form">

            <div class="form-field multi-select-wrapper" id="size-field">
                <label>Select Size</label>


                <select id="size" class="mul-select-category" multiple="true">


                </select>

            </div>


            <div class="form-field multi-select-wrapper" id="color-field">
                <label>Select Color</label>
                <input type="hidden" id="color" />
                <ul class="color-list" id="colors">
                   
                </ul>
                <!-- <select id="color" class="mul-select-color" multiple="true">


                </select> -->

            </div>
        </div>
        <div id="product-edit2">

        </div>

    </div>

    <button class="btntest1" style="display: none;" id="btntest1">test</button>
</div>

<!-- /page content -->

</div>


<!-- Modal -->
<div class="modal fade" id="productUpdateModal" role="dialog">
    <div class="modal-dialog">

        
        <div class="modal-content">
            <div class="modal-header">
                
            </div>
            <div class="modal-body">
                <div class="title text-center">
                    <h4>
                        Product Updated Successfully
                    </h4>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="c-btn close-btn" id="closebtn" data-dismiss="modal" onclick="window.location.href=APP_URL+'/my-catalog-2.php'">Close</button>
            </div>
        </div>

    </div>
</div>

<!--End Modal -->



<!-- multi select -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- multi select -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="../js/custom.js"></script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<!-- <script type="text/javascript" src="../assets/js/business_category_color_size.js"></script>s -->
<!-- <script type="text/javascript" src="../assets/js/business_fetchproducts.js"></script> -->

<script type="text/javascript" src="../assets/js/edit_products.js"></script>
<script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>


<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>



</script>


<script>
    // function readURL(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#blah').attr('src', e.target.result);
    //             logoUpload1(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#blah").removeClass("hide");
    //     }
    // }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#blah').attr('src', e.target.result);
                        logoUpload1(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#blah").removeClass("hide");
        }
    }

    // function image1(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img1').attr('src', e.target.result);
    //             $('.image-delete1').show();
    //             image1upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img1").removeClass("hide");
    //     }
    // }

    function image1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img1').attr('src', e.target.result);
                        $('.image-delete1').show();
                        image1upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img1").removeClass("hide");
        }
    }

    // function image2(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img2').attr('src', e.target.result);
    //             $('.image-delete2').show();
    //             image2upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img2").removeClass("hide");
    //     }
    // }

    function image2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img2').attr('src', e.target.result);
                        $('.image-delete2').show();
                        image2upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img2").removeClass("hide");
        }
    }

    // function image3(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img3').attr('src', e.target.result);
    //             $('.image-delete3').show();
    //             image3upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img3").removeClass("hide");
    //     }
    // }

    function image3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img3').attr('src', e.target.result);
                        $('.image-delete3').show();
                        image3upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img3").removeClass("hide");
        }
    }

    // function image4(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img4').attr('src', e.target.result);
    //             $('.image-delete4').show();
    //             image4upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img4").removeClass("hide");
    //     }
    // }

    function image4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img4').attr('src', e.target.result);
                        $('.image-delete4').show();
                        image4upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img4").removeClass("hide");
        }
    }

    jQuery(document).ready(function() {
        jQuery('#dtOrderExample').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
        jQuery('.dataTables_length').addClass('bs-select');
    });
    $(function() {
        var id = STORAGE.get(STORAGE.pid);
        //get_category();
        product_details(id);
    });
</script>

<script>
    var specId = 2;

    function addspecs() {
        if (specId <= 7) {
            var div = $("<div class='field-inner' >");
            div.html(GenerateTextbox("", specId));
            $("#specs").append(div);
            specId++;
            if(specId > 7){
                $('#addbtn').hide();
            }
        }
        else{
            $('#addbtn').hide();
        }
    }

    $("#buttonGet").bind("click", function() {
        var values = "";
        $("input[name=CreateTextbox]").each(function() {
            values += $(this).val() + "\n";
        });
        alert(values);
    });
    $("body").on("click", ".remove", function() {
        $(this).closest("div").remove();
        specId--;
        console.log(specId);
        if(specId <= 7){
            $('#addbtn').show();
        }
        
    });

    function GenerateTextbox(value, id) {
        return '    <label>Specification</label> <h5 id="specificationcheck"></h5><input name = "CreateTextbox" id="specification' + id + '" type="text" value = "" /> ' +
            '<input type="button" value="Remove" class="remove" /> </div>'
    }
</script>


<script>
    $(document).ready(function() {

        $(".mul-select-color").select2({
            placeholder: "Select Color", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });
        $(".mul-select-category").select2({
            placeholder: "Select Size", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });

        setTimeout(() => {
            $(".mul-select-sub-category").select2({
                placeholder: "Select SubCategory", //placeholder
                tags: true,
                tokenSeparators: ['/', ',', ';', " "]
            });
        }, 2000);

    })




    document.getElementById('btntest1').onclick = function() {
        // var selected1 = [];
        // for (var option of document.getElementById('color').options) {
        //     if (option.selected) {
        //         selected1.push(option.value);
        //     }
        // }
        // STORAGE.set(STORAGE.colors, selected1);
        var selected = [];
        for (var option of document.getElementById('size').options) {
            if (option.selected) {
                selected.push(option.value);
            }
        }

        STORAGE.set(STORAGE.productsize, selected);
    }
</script>
</body>

</html>