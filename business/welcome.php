<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    <title>Welcome Twiva</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>

<body>

    <header>
        <div class="logo">
            <div class="nav toggle">
                <a id="menu_toggle" href="#" class="d-flex">
                    <img src="../images/icons/Frame 5.svg" alt="Toggle_Menu">
                </a>
            </div>
            <a href="" id="nav-click-old" data-url="">
                <img src="../images/logo/logo-white.svg" />
            </a>

            <!--  <ul class="nav navbar-nav navbar-right">

                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="../images/icons/img.jpg" alt=""> Devon Lane</a>
                </li>
            </ul> -->
        </div>
    </header>
    <div class="account-setup">
        <div class="container">

            <div class="welcome-screen">
                <img src="../images/icons/welcome-icon.png">
                <h3>Welcome to Twiva</h3>
                <p>We will help you increase your sales and grow your business. </p>

                <div class="button-sec">

                    <button onclick="subsstart()" class="m-0">Begin Free Trial</button>
                </div>
            </div>


        </div>

    </div>

    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/business_subscription.js"></script>

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>


    <script>
        $(function() {
            // alert("hello");
        })
    </script>
</body>

</html>