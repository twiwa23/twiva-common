<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">

    <title>Profile Information</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">

   
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
      <!--Mobile View Styling-->
      <link href="../css/mobile-view.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
   
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  
</head>

 <body class="nav-md" onload="login_check()">

    <header>
        <div class="logo logo-toggle">
            <div class="nav toggle">
                <a id="menu_toggle" href="#" class="d-flex">
                    <img  src="../images/icons/Frame 5.svg" alt="Toggle_Menu">
                </a>
            </div>
            <a href="" id="nav-click-olds" data-url="/landing">
                <img src="../images/logo/logo-white.svg" />
            </a>
            <!-- <img src="../images/logo/logo-white.svg"> -->
        </div>

        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <!-- <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="" id="userprofile">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="" id="user-image" alt=""> <span id="user-name" style="color:white"></span>
                                <!-- <span class=" fa fa-angle-down"></span> -->
                               
                            </a>
                           
            
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <!-- <li>
                                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-edit-profile.php">Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li> -->
                                    <li class="header-company-name-wrapper">
                                        <p class="company-name" id="nname">Biffco Enterprises Ltd.</p>
                                        <p class="email" id="eemail">nevaeh.simmons@example.com</p>
                                    </li>
                                    <li ><a onclick="logout_check()" id="logout-btn">Log Out</a></li>
                                </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <!-- <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> -->
                            <a href="notifications.php" class="info-number" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="../images/icons/bell-icon.svg">
                                <span class="badge bg-yellow" id="notification_count">0</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
                                
                            </ul>
                           
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
    </header>



    <div class="container-fluid account-main-info-wrapper">
        <div class="back-link full-width-back-btn">
            <a href="dashboard-2.php" class="page-back-btn"><i class="fa fa-chevron-left"></i> Profile Information</a>
        </div>

        <div class="container">
            <div class="account-info-wrapper business-account">
                <div class="row" id="profile_details">
                  
                </div>
            </div>
        </div>


    </div>
    <div id="wait" style="display: flex;
    position: fixed;
    top: 0%;
    left: 0%;
    align-items: center;
    justify-content: center;
    z-index: 9999999;
    width: 100%;
    height: 100%;
    background: #ffffffa3;"><img src='../images/loader.gif' width="100px" /></div>
            <script>
                    $(document).ready(function () {
                        $(document).ajaxStart(function(){
                            $("#wait").css("display", "flex");
                          });
                          $(document).ajaxComplete(function(){
                            $("#wait").css("display", "none");
                          });
                        getNotifications();
                        getNotificationCount();
                    });
            </script>

 
 <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_profile.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
    <?php include('../twiva-config.php'); ?>
    <script>
        $(function(){
            profile_details();
        })
    </script>
    <script>
        $(function(){
            var name= STORAGE.get(STORAGE.name);
            if(name == '' || name == 'undefined' || name == 'null' || name == null) {
                name = 'User';
            }
            var email= STORAGE.get(STORAGE.email);
            var logo ;
            logo = STORAGE.get(STORAGE.logo);
            var image ;
            image = '<?= IMAGES_URI_PATH ?>/icons/profile.jpeg';
            
            if(logo && logo != "null" && logo != null && logo != 'undefined') {
                image = 'https://api.twiva.co.ke/storage/images/products/'+logo;
            }
            console.log(logo);
            document.getElementById("user-image").src = image;
            
            document.getElementById("user-name").innerHTML = name;
            document.getElementById("nname").innerHTML = name;
            document.getElementById("eemail").innerHTML = email;
        });
                 $(function()
    {
        if (localStorage.getItem(STORAGE.accesstoken) == null) {
            goto(UI_URL.login);
        }
        return false;
    });

                     function logout_check() {
    localStorage.clear();
    login_check();
}
function login_check() {
  
    if (localStorage.getItem(STORAGE.accesstoken) == null) {
            goto(UI_URL.login);
        }
        return false;
    }
    function myprofile()
    {
       goto(UI_URL.onboarding10);
    }
            </script>
<script>
    $(document).on('click', '#nav-click-olds', function(e) {
        var user_id = localStorage.getItem('userId');
        var user_token = STORAGE.get(STORAGE.accesstoken);
        var nav_url = $(this).data('url');
        console.log(nav_url);
        e.preventDefault();
        if(user_id) {
            $.ajax({
                url: "https://home.twiva.co.ke/api/v1/nav-link",
                //url: "http://localhost:8000/api/v1/nav-link",
                headers: {
                    'Accept': 'application/json',
                    // 'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
                    ['Authorization']: "Bearer " + STORAGE.get(STORAGE.accesstoken),
                },
                type: "post",
                data: { user_id: user_id, 'nav_url' : nav_url, user_token : user_token},
                success: function(data){
                    console.log(data);
                    console.log(data.data.url);
                    // window.open(data.data.url);
                    // let url = data.data.url.split("?");
                    window.location.href = data.data.url;
                },
                error : function(data) {
                    console.log(data)
                }
            });
        }
    });


            
</script>
</body>

</html>