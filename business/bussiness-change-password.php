<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Change Password</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">


    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<?php include('common/side_menu.php'); ?>
<!-- /sidebar menu -->

</div>
</div>

<!-- page content -->
<div class="right_col add-product-page" role="main">

    <!--********** Breadcrumb Start ***********-->
    <div class="page-title breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="#">Account Setting</a></li>
            <li class="active">Change Password</li>
        </ul>
    </div>

    <!--**********  Breadcrumb End ***********-->


    <div class="dashboard-inner">




        <div class="bank-setup-form">
            <h3 class="subscription-title">
                Change Password
            </h3>
            <h5 style="display: none;" class=" alert alert-success" role="alert" id="otpsuc"></h5>
            <div class="account-setup">
                <form id="myForm">
                    <div class="account-form">
                        <div class="form-field">
                            <label>Old Password</label>
                            <input type="password" id="current" placeholder="Old Password">
                            <h5 style="display: none;" id="error" class="empty-field-error"></h5>
                        </div>

                        <div class="form-field">
                            <label>New Password</label>
                            <input type="password" id="new" placeholder="New Password">
                            <h5 style="display: none;" id="error1" class="empty-field-error"></h5>
                        </div>
                        <div class="form-field">
                            <label>Confirm Password</label>
                            <input type="password" id="confirm" placeholder="Confirm Password">

                        </div>

                </form>
            </div>
            <div class="button-sec right-align-btn  d-block">
                <button onclick="password_param()">Update</button>
            </div>
        </div>


    </div>


</div>
</div>
<!-- /page content -->
</div>
</div>

<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/change_password.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="../js/custom.js"></script>

</body>

</html>