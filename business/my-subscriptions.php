<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    
    <title>My Subscriptions</title>

    <!-- Bootstrap CSS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">


    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<body class="nav-md">

    <?php include('common/side_menu.php') ?>

    <!-- page content -->
    <div class="right_col add-product-page" role="main">

        <!--********** Breadcrumb Start ***********-->
        <div class="breadcrumb-wrapper">
            <ul class="breadcrumb">
                <li><a href="#">Account Setting</a></li>
                <li class="active">My Subscription</li>
            </ul>
        </div>

        <!--**********  Breadcrumb End ***********-->
        <div class="business-content-wrapper">


            <div class="subscription-date-wrapper" id="detailssubs">

            </div>
            <h3 class="subscription-title">
                Update Subscription
            </h3>

           
            <div class="dashboard-inner" id="subsriptionPlans">
                <div class="subscription-wrapper">
                    <div class="sub-ops-wrapper">
                        <div class="btn-wrapper ">
                            <button id="yearly" class="blk-btn active c-btn">
                                Monthly
                            </button>
                            <button id="month" class="blk-btn c-btn">
                                Annually
                            </button>
                        </div>
                    </div>
                    <div class="sub-des-wrapper">
                        <p>
                            Save up to 10% on annually plan
                        </p>
                    </div>
                </div>

                <div class="dashboard-card-outr" id="subscription">




                </div>

                <div class="dashboard-card-outr" id="subscriptionannualy" style="display: none;">

                    <!-- <h1>hello</h1> -->


                </div>

                <div class="dashboard-card-outr" id="phone" style="display: none;">
                    <div class="dashboard-card subscription-card">
                        <input type="text" />

                    </div>
                </div>



                <!-- <div class="dashboard-card-outr" style="display: none;">
                    <button id="plan-continue" onclick="ask_phone();" class="purple-btn">Continue</button>
                </div> -->
                <h5 id="transaction_error" style="color:red;margin:5px 0;"></h5>
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="input-group mt-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text font-weight-bold">+254</span>
                            </div>
                            <input type="number" class="form-control" id="mpesa_number" placeholder="Enter Your MPESA Number">
                        </div>
                    </div>
                </div>  
                <div class="dashboard-card-outr">
                    <button id="subscriptionbtn" class="purple-btn">Upgrade Subscription Plan </button>
                </div>
            </div>
        </div>

    </div>
    </div>
    <!-- /page content -->
    </div>
    <script>
        $(function() {
            fetch_subscription();
        });

        $('#month').click(function() {
            $("#subscription").hide();
            $("#subscriptionannualy").show();
            $("#yearly").removeClass('active');
            $("#month").addClass('active');
        });
        $('#yearly').click(function() {
            $("#subscription").show();
            $("#subscriptionannualy").hide();

            $("#month").removeClass('active');
            $("#yearly").addClass('active');
        })
    </script>

    <!-- /page content -->



    <!-- Modal -->
    <div class="modal fade" id="upgrade-subscription" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="title">
                        <h4>
                            Update Subscription?
                        </h4>
                    </div>
                    <p>The subscription will be active immidiately and
                        will be charged accordingly in the next cycle.

                    </p>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="c-btn close-btn" data-dismiss="modal">Close</button>
                    <button class="purple-btn c-btn">
                        Update
                    </button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(function() {
            subscriptiondetails();
        })
    </script>
    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/business_subscription.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>


</body>

</html>