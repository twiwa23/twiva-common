<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Subscriptions</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">


    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>

<?php include('common/side_menu.php'); ?>


<!-- /sidebar menu -->


<!-- page content -->
<div class="right_col add-product-page" role="main">

    <!--********** Breadcrumb Start ***********-->
    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="#">Account Setting</a></li>
            <li class="active">Subscription</li>
        </ul>
    </div>
    <div class="alert alert-success" id="successMessage" style="display: none;"></div>

    <!--**********  Breadcrumb End ***********-->
    <div class="business-content-wrapper">


        <h3 class="subscription-title">
            Select your subscription plan
        </h3>
        




        <div class="dashboard-inner">
            <div class="subscription-wrapper">
                <div class="sub-ops-wrapper">
                    <div class="btn-wrapper ">
                        <button id="yearly" class="blk-btn active c-btn">
                            Monthly
                        </button>
                        <button id="month" class="blk-btn c-btn">
                            Annually
                        </button>
                    </div>
                </div>
                <div class="sub-des-wrapper">
                    <p>
                    Save 10% on annual plan
                    </p>
                </div>
            </div>

            <div class="dashboard-card-outr " id="subscription">
            </div>

            <div class="dashboard-card-outr" id="subscriptionannualy" style="display: none;">

                <!-- <h1>hello</h1> -->


            </div>
            <h5 id="transaction_error" style="color:red;margin:5px 0;"></h5>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">+254</span>
                        </div>
                        <input type="number" class="form-control" id="mpesa_number" placeholder="Enter Your MPESA Number">
                    </div>
                </div>
            </div>      
            <div class="dashboard-card-outr">
                <button id="subscriptionbtn" class="purple-btn">Subscribe</button>
            </div>
<style>
#mpesa_number::-webkit-outer-spin-button, #mpesa_number::-webkit-inner-spin-button {
  -webkit-appearance: none;
}
</style>





        </div>
    </div>
    <!-- /page content -->
</div>

<script>
    $(function() {
        fetch_subscription();
    });

    $('#month').click(function() {
        $("#subscription").hide();
        $("#subscriptionannualy").show();
        $("#yearly").removeClass('active');
        $("#month").addClass('active');
    });
    $('#yearly').click(function() {
        $("#subscription").show();
        $("#subscriptionannualy").hide();

        $("#month").removeClass('active');
        $("#yearly").addClass('active');
    })
</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/business_subscription.js"></script>
</body>

</html>