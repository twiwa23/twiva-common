<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Product Detail</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<?php include('common/side_menu.php'); ?>
<!-- /sidebar menu -->



<!-- page content -->
<div class="right_col add-product-page" role="main">
    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="./my-catalog-2.php">My Catalog</a></li>
            <li><a href="./my-catalog-add-product.php">Add Product</a></li>
            <li class="active">Preview</li>
        </ul>
    </div>
    <div class="product-main" id="product-de">














    </div>
    <a data-toggle="modal" data-id="121" title="Add this item" class="open-AddBookDialog" href="#addBookDialog"></a>
    <!-- Modal -->
    <div class="modal fade" id="addBookDialog" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="title">
                        <h4 id="message">

                        </h4>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="button" class="c-btn close-btn" id="closebtn" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- /page content -->
</div>
</div>

<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/business_fetchproducts.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>
    $(function() {
        var id = STORAGE.get(STORAGE.pid);
        product_details(id);
    });




    jQuery(document).ready(function() {
        jQuery('#dtOrderExample').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
        jQuery('.dataTables_length').addClass('bs-select');
    });
</script>



<script>
    jQuery(document).ready(function() {
        jQuery('.minus').click(function() {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        jQuery('.plus').click(function() {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>

</body>

</html>