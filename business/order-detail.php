<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog</title>



    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="https://c-dnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    
</head>
       <?php include('common/side_menu.php');?>

            <!-- page content -->
            <div class="right_col dashboard-page" role="main">
                <!--********** Breadcrumb Start ***********-->
                <div class="breadcrumb-wrapper">
                     <ul class="breadcrumb">
                            <li><a href="order.php">Order Received</a></li>
                            <li class="active">Order Details</li>
                          </ul>
                     </div>
               
                 <!--**********  Breadcrumb End ***********-->
                <div class="business-content-wrapper">
                 <div class="title-wrapper">
                     <h4>
                        Order Detail
                     </h4>
                 </div>

                 <div class="invoice-wrapper" id='print-div'>
                    <div class="invoice-header-wrapper">
                        <div class="invoice-detial-wrapper">
                            <ul>
                                <li class='order-number'>
                                    
                                </li>
                                <li class='order-by'>
                                    
                                </li>
                                <li class='order-date'>
                                    
                                </li>
                                <li class='order-status'>
                                    
                                </li>
                            </ul>
                        </div>
                        <div class="invoice-print-btn-wrapper">
                            <button class="print-btn white-bttn b-none" onclick="PrintDiv();">PRINT</button>
                            <!--<button class="download-btn white-bttn b-none">Download</button>-->
                        </div>
                    </div>

                    <div class="order-itmes-table-wrapper table-wrapper">
                        <table class="table ">
                            <thead>
                              <tr>
                                <th scope="col">Order Items</th>
                                <th scope="col">Price</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Total</th>
                              </tr>
                            </thead>
                            <tbody id="order-items">
                              
                            </tbody>
                          </table>

                          <div class="total-amount-wrapper">
                                <p class="subtotal">
                                    <span>Subtotal</span>
                                    <span class='subTotal-amt'> $50</span>
                                </p>
                                <p class="shipping ">
                                    <span>Shipping </span>
                                    <span class='shipping-fee'> $50</span>
                                </p>
                                <p class="grand-total ">
                                    <span>Grand Total </span>
                                    <span class='total'> $50</span>
                                </p>

                          </div>

                          <div class="address-wrapper">
                              <div class="row">
                                  <div class="col-md-4" >
                                    <div class="address-item payment-status shipping-address" id="shipping-address" >
                                      <p>Payment Method:</p>
                                      <p>Transaction ID: XXXASD123567</p>
                                      <p>Payment Status: Paid</p>
                                      <p>Delivery Status: Delivered</p>
                                    </div>
                                  </div>
                                 <!-- <div class="col-md-4">
                                    <div class="address-item payment-status">
                                      <p>Payment Method:</p>
                                      <p>Transaction ID: XXXASD123567</p>
                                      <p>Payment Status: Paid</p>
                                      <p>Delivery Status: Delivered</p>
                                    </div>
                                  </div> -->
                                  
                              </div>
                          </div>

                          <div class="order-tracking-wrapper">
                            <div class="title-wrapper">
                                <h4>
                                    Order Tracking
                                </h4>
                            </div>
                            <div class="order-tracker-item-wrapper">
                                <ul id="progressbar">
                                    <li class="step0 active " id="step1">Received</li>
                                    <li class="step0 " id="step2">In progress</li>
                                    <li class="step0 " id="step3">dispatch</li>
                                    <li class="step0 " id="step4">Delivered</li>
                                </ul>
                            </div>
                          </div>
                    </div>
                 </div>

                 <div class="btn-wrapper" id= "ButtonDiv">
                     <button class="white-bttn" >
                        Cancel Order
                     </button>
                   
                 </div>
                </div>
            <!-- /page content -->
        </div>


    </div>


  
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
   
   
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/Orders.js"></script>

    <script src="../js/custom.js"></script>
</body>

</html>