<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="./images/icons/fav.png" type="image/x-icon">

    <title>My Catalog</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/mobile-view.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/business_fetchproducts.js"></script>

    <script type="text/javascript" src="../assets/js/business_category_color_size.js"></script>
</head>


<?php include('common/side_menu.php'); ?>



<!-- page content -->
<div class="right_col dashboard-page" role="main">
    <div class="page-title">My Catalog</div>
    <div class="alert alert-success" id="successMessage" style="display: none;"></div>
    <div class="catalog-page">

        <div class="select-top">
            <h3>Catalog</h3>
            <div class="button-catalog">
                <!-- <button class="white-bttn"><img src="../images/icons/upload-cloud.svg"> Bulk Upload</button> -->
                <a href="my-catalog-add-product.php"><button class="purple-btn"> <img src="../images/icons/plus.svg">Add Product</button></a>
            </div>
        </div>


        <div class="select-top select-search product-filter">
            <div class="search-panel">
                <img src="../images/icons/search.svg">
                <input type="text" placeholder="Search here..." id="keywords">
            </div>

            <!--Filter Button-->
            <button class="filter-button" type="button">
                <span>Filter</span>
                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Property 1=Sliders, Property 2=16px.svg" alt="" />
            </button>

            <div class="filter-wrapper">
                <div class="filter-header-wrapper">
                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="" /></span>
                    <h2 class="filter-title">Filter</h2>
                </div>

                <div class="button-catalog">
                    <select id="price" class="price-filter">
                        <option value="">Price Range</option>
                        <option value="0">Low to High</option>
                        <option value="1">High to Low</option>
                    </select>

                    <select id="category">
                        <option value="">Categories: All Categories</option>
                    </select>

                    <div class="form-field multi-select-wrapper">
                        <select class="mul-select-category" multiple="true" id="sub_category">
                            <!-- <option value="Sub 1">Sub 1</option>
                            <option value="Sub 2">Sub 2</option>
                            <option value="Sub 3">Sub 3</option>
                            <option value="Sub 4">Sub 4</option> -->
                        </select>
                    </div>

                </div>

                <div class="common-button">
                    <button class="white-bttn" id="reset_filter">Reset</button>
                    <button class="purple-btn" id="apply_filter">Apply</button>
                </div>
            </div>

        </div>
        <!-- <button class="purple-btn" onclick="Get_Category()">fetch products</button> -->

        <div class="table-responsive">
            <table id="dtOrderExample" class="table " cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Image
                        </th>
                        <th class="th-sm">Product Name
                        </th>
                        <th class="th-sm">Rating
                        </th>
                        <th class="th-sm">Quantity
                        </th>
                        <th class="th-sm">Price
                        </th>
                        <th class="th-sm">Stock
                        </th>
                        <th class="th-sm">Status
                        </th>
                        <th class="th-sm">Actions
                        </th>
                    </tr>
                </thead>
                <tbody id="products">



                </tbody>
            </table>
            <div class="card product-box-inner p-3" style="border:0;" id="nf">
                <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                    <img src="../images/icons/empty.svg" alt="">
                    <h3 style="font-size:20px;">
                    You haven’t uploaded any products to the catalog
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
    <div class="page-selection"></div>
</div>


</div>


<!-- Modal -->
<div class="modal fade" id="addBookDialog" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">


            </div>
            <div class="modal-body">
                <div class="title">
                    <h4>
                        Do you want to delete this product ?
                    </h4>
                </div>
            </div>
            <div class="modal-footer ">
                <button type="button" class="c-btn close-btn" id="closebtn" data-dismiss="modal">No</button>
                <input type="button" class="purple-btn c-btn" name="bookId" id="bookId" onclick="delete_products(this.name)" value="Yes" style="cursor: pointer;">
            </div>
        </div>

    </div>
</div>

<!--End Modal -->



<script src="https://code.jquery.com/jquery-migrate-3.3.2.js" integrity="sha256-BDmtN+79VRrkfamzD16UnAoJP8zMitAz093tvZATdiE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="../js/custom.js"></script>
<script src="../assets/js/influencer_filter.js"></script>
<script src="../assets/js/pagination.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script>
    $(function() {
        get_category();
    });
    $(function() {
        fetch_products();

    });
</script>
<script>
    $(document).ready(function() {
        $("#myBtn").click(function() {
            $("#myModal").modal();
        });
    });
</script>

<script>
    $(document).ready(function() {

        $(".mul-select-category").select2({
            placeholder: "Select Sub Category", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });
    });
</script>
<script>
    $(document).ready(function() {
        var allProducts;
        var page = 1;
        var limit = 12;
        var search = '';
        var sort_by = '';
        var category = '';
        var sub_category = '';
        getProductList();

        function getProductList() {
            let loader = `<div class="not-found text-center w-100">
                                <h1><i class="fa fa-spinner fa-spin"></i></h1>
                            </div>`;
            $(".product-list").html(loader);
            $.ajax({
                url: `<?php echo API_URI_PATH; ?>/fetch-products?page=${page}&limit=${limit}&search=${search}&sort_by=${sort_by}&categories=${category}&sub_categories=${sub_category}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("accesstoken")}`,
                },
                type: "get",
                success: function(data) {
                    console.log(data);
                    if (data.total_results > limit) {
                        let pagination = Pagination(data.total_results, limit, data.current_page)
                        $('.page-selection').html(pagination);
                    } else {
                        $('.page-selection').html('');
                    }
                    setProductsListData(data.data);
                },
                error: function(request, status, error) {
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

        function setProductsListData(products) {
            allProducts = products;
            let appenddata = "";
            if (products.length > 0) {
                $('#nf').hide();
            } else {
                $('#nf').show();
            }
            $('#products').html('');
            for (let index = 0; index < products.length; index++) {
                const product = products[index];
                const productImage = product.product_images.find((obj) => obj.is_cover_pic == 1);
                const image = productImage ? productImage.image : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                // appenddata += `<div class="product-box-inner col-12 col-md-4 col-lg-3">
                //             <div class="product-box-content">
                //                 <img  class="thumb" src="<?php echo $image_base; ?>/${image}" height="216"/>
                //                 <div class="box-content">
                //                     <p>${product.name}</p>
                //                     <h2>KSH ${product.price}</h2>
                //                 </div>
                //                 <div class="product-rating">
                //                     <span class="rating-title">${product.rating ? product.rating.toFixed(1) : '0.0'}</span>
                //                     <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                //                 </div>
                //             </div>
                //         </div>`;
                appenddata += `<tr> 
                            <td> <a href="#addBookDialog" id="${product.id}"  onclick="product_detail(this.id)"><img src="https://api.twiva.co.ke/storage/images/products/${product.product_images[0].image}"></a></td>
                            <td> <a href="#addBookDialog" id="${product.id}"  onclick="product_detail(this.id)">${product.name}</a></td>
                            <td> <a href="#addBookDialog" id="${product.id}"  onclick="product_detail(this.id)">`;
                            if(product.rating!=0){
                                appenddata += `${product.rating}
                                <img src="../images/icons/star.svg"></img>`;
                            }else{
                                appenddata += `NA`;
                            }
                            appenddata += `</a></td>
                            <td> <a href="#addBookDialog" id="${product.id}"  onclick="product_detail(this.id)">${product.stock}</a></td>
                            <td> <a href="#addBookDialog" id="${product.id}"  onclick="product_detail(this.id)">KSH ${product.price}</a></td>
                            <td class="green-txt"><button>${inStock}</button></td>
                            <td class="green-txt"><button>${status}</button></td>
                            <td>
                                <div class="actions">
                                <a href="#addBookDialog" id="${product.id}"  onclick="product_edit(${product.id})"><img src="../images/icons/edit-profile.svg"></a>
                                    <a data-toggle="modal" data-id="${product.id}" title="Delete this item" class="open-AddBookDialog" href="#addBookDialog"><img src="../images/icons/trash-red.svg"></a>
                                </div> 
                            </td>
                        </tr>`;

                       
            }
            $("#products").html(appenddata);

            // $(".product-list").html(appenddata);
        }
        $(document).on('click', '.page-link', function(event) {
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if (pageNumber == 'prev') {
                page = page - 1;
            } else if (pageNumber == 'next') {
                page = page + 1;
            } else {
                page = Number($(this).attr('id'));
            }
            getProductList();
        })
        $('#apply_filter').on('click', function() {
            page = 1;
            if ($('#keywords').val().length >= 1) {
                search = $('#keywords').val();
            } else {
                search = '';
            }
            sort_by = $("#price").val() ?? '';
            category = $("#category").val() ?? '';
            sub_category = $("#sub_category").val() ?? '';
            if (sub_category) {
                sub_category = sub_category.toString();
            }
            console.log(search, sort_by, category, sub_category);
            getProductList();
        });

        $('#reset_filter').on('click', function() {
            page = 1;
            $('#keywords').val('');
            $('#category').val('');
            $('#sub_category').val('');
            $('#price').val('');
            search = '';
            sort_by = '';
            category = '';
            sub_category = '';
            console.log(search, sort_by, category, sub_category);
            getProductList();
        });

        function select2Init() {
            $('.category-select-box').select2({
                placeholder: "Select Sub Category",
            });
        }
        select2Init();



    });
</script>
</body>

</html>