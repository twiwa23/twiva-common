<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    
    <title>Otp Verify</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<body class="p-0">

    <div class="login">


        <div class="container">
            <div class="login-inner">

                <div class="login-left">

                </div>

                <div class="login-right">

                    <div class="login-section">
                        <div class="logo"><img src="../images/logo/logo.svg"></div>

                        <h5 class="login-error alert alert-success" role="alert" id="otpsuc">A code has been sent to your email</h5>
                        <h5 style="display: none;" class="login-error alert alert-danger" role="alert" id="otperr"></h5>
                        <p>Enter the code sent your email</p>
                        <div class="input-field">
                            <label>Enter Code</label>
                            <input id="otp" type="text">
                        </div>



                        <div class="forgot-pass"><a onclick="resendCode1()" style="cursor: pointer;">Resend Code</a></div>
                        <button onclick="verify_otp()">Confirm</button>
                        <!--<div class="no-account">
                            Back to <a href="login.php">Login</a>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
        </div>
    </div>

    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>

</body>

</html>