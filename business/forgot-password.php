<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    <title>Forgot Password</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  
</head>

<body class="p-0">

    <div class="login">

   
        
<div class="container">
    <div class="login-inner">
      
    <div class="login-left">
        <!-- <img src="../images/banner/login.png"> -->
       
    </div>

    <div class="login-right">
       
        <div class="login-section">
            <div class="logo"><img src="../images/logo/logo.svg"></div>
    
            <p>Enter your registered email address to get a reset link.</p>
            <h5 style="display: none;" class="login-error alert alert-danger" role="alert" id="otperr">Otp has been sent to your email</h5>
            <div class="input-field">
                <label>E-mail Address</label>
                <input id="email" type="text" placeholder="willie.jennings@example.com">
                <h5 id="usercheck" class="empty-field-error"></h5>
            </div>

            


            <button id="submit">Confirm</button>
            <div class="no-account">
                Back to <a href="../login.php">Login</a>
            </div>
        </div>
    </div> 
    
</div>
</div>

<div class="footer-login">
    Copyright © 2021 Twiva. All Rights Reserved.
    <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
</div>
</div>

 
 <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_login.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
    <script>
$(document).ready(function(){
    $('#usercheck').hide();

    var user_err = true;

    $('#email').keyup(function(){
        email_check();
        
    });

    function email_check(){
        var email_val = $('#email').val();
        if(email_val.length == ''){
            $('#usercheck').show();
            $('#usercheck').html("Email is required");
            $('#usercheck').focus();
            $('#usercheck').css("color","red");
            user_err = false;
            return false;
        }
        else{
            $('#usercheck').hide();
             if(validateEmail($('#email'))){
                 return true;
             }            
             else{
             $('#usercheck').show();
            $('#usercheck').html("Please enter correct mail address");
            $('#usercheck').focus();
            $('#usercheck').css("color","red");
             }
        }
    }

     $('#submit').click(function(){
         user_err = true;
      
         user_err = email_check();
    
        if((user_err == true) ){
            forget_password();
        }
        else{

            return false;
        }



    })
     });


</script>
 
</body>

</html>