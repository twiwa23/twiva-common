<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Dashboard 2</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>



<?php include('common/side_menu.php'); ?>

<!-- page content -->
<div class="right_col add-product-page" role="main">
    <div class="page-title">Dashboard </div>
    <div class="dashboard-inner dashboard-card-status">
        <div class="dashboard-card-outr">

            <div class="dashboard-card">
                <h3><img src="../images/icons/sale.svg">Sale</h3>
                <div class="card-flex">
                    <span>Total Earning</span>
                    <h3 id="totalEarning">KSH 0</h3>
                </div>
                <div class="card-flex">
                    <span>Earning This Month</span>
                    <h3 id="earningThisMonth">KSH 0</h3>
                </div>

                <div class="card-flex">
                    <span>Product Sold This Month</span>
                    <h3 id="ProductSoldThisMonth">0 Product</h3>
                </div>

                <div class="view-ss"> <a href="best-performing-product.php">View Report <span class="fa fa-arrow-right" style="color: #7b1d42"></span> </a></div>
            </div>


            <div class="dashboard-card" id="subsshow">
                <h3><img src="../images/icons/subscription.svg">Subscription</h3>

                <div class="common-button full-button">
                    <a href="subscriptions.php"> <button class="purple-btn">Get a Subscription</button></a>
                </div>
            </div>

            <div class="dashboard-card" id="catalog">
                <h3><img src="../images/icons/catalog.svg">My catalog</h3>

                <div class="common-button">
                    <a href="my-catalog-add-product.php"><button class="purple-btn">Add Product</button></a>
                    <a href="my-catalog-2.php"><button class="white-bttn">View Catalog</button></a>
                </div>
            </div>
        </div>


        <div class="product-section">
            <div class="product-box" id="selling-products">
            </div>

            <div class="product-box" id="recent-orders">

            </div>
        </div>
    </div>
</div>
<!-- /page content -->
</div>
</div>

<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/custom.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('#dtOrderExample').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
        jQuery('.dataTables_length').addClass('bs-select');
    });
</script>



<script>
    jQuery(document).ready(function() {
        jQuery('.minus').click(function() {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        jQuery('.plus').click(function() {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
    $(function() {
        //     const format1 = "YYYY-MMMM-DD HH:mm:ss"
        // const format2 = "YYYY-MM-DD"

        //     var date1 = new Date("2020-06-24 22:57:36");
        // var date2 = new Date();

        // dateTime1 = moment(date1).format(format1);
        // dateTime2 = moment(date2).format(format2);
        // console.log("dateTime1");

        // console.log(dateTime1);

    })
</script>

<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/business_dashboard.js"></script>

</body>

</html>