<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">

    <title>Reset Password</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  
</head>

<body class="p-0">

    <div class="login">

    
        
<div class="container">
    <div class="login-inner">
      
    <div class="login-left">
        <!-- <img src="../images/banner/login.png"> -->
       
    </div>

    <div class="login-right">
       
        <div class="login-section">
            <div class="logo"><img src="../images/logo/logo.svg"></div>
    
            <p>Please enter a new password and confirm it to reset.</p>
            <h5 id="passwordcheck" class="alert alert-danger" style="display: none;"></h5>
                    <div class="input-field">
                        <label>New Password*</label>
                         <input id="password" type="password" placeholder="********">
                        <div class="closed-eye showPass"></div>
                        <div class="open-eye showPass"></div>                          
                         <h5 id="usercheck" class="empty-field-error"></h5>
                    </div>

                    <div class="input-field">
                        <label>Re-enter Password*</label>
                         <input id="c_password"type="password" placeholder="********">
                         <div class="closed-eye showPass1"></div>
                        <div class="open-eye showPass1"></div> 
                         <h5 id="passcheck" class="empty-field-error"></h5>
                    </div>

            


            <button id="submitb">Confirm</button>
        </div>
    </div> 
    
</div>
</div>

<div class="footer-login">
    Copyright © 2021 Twiva. All Rights Reserved.
    <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
</div>
</div>


 <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_login.js"></script>
 

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
    <script>
        $(document).ready(function(){       
        $('.showPass').on('click', function(){
            var passInput=$("#password");
            if(passInput.attr('type')==='password')
            {
                passInput.attr('type','text');
            }else{
                passInput.attr('type','password');
            }
        })
        })
        $(document).ready(function(){       
        $('.showPass1').on('click', function(){
            var passInput=$("#c_password");
            if(passInput.attr('type')==='password')
            {
                passInput.attr('type','text');
            }else{
                passInput.attr('type','password');
            }
        })
        })
        </script>
        <script>
            // Show Password 
            $(document).ready(function() {
                $('.closed-eye').on('click', function() {
                    $(this).hide();
                    $('.open-eye').show();
                });

                $('.open-eye').on('click', function() {
                    $(this).hide();
                    $('.closed-eye').show();
                });
            });
        </script>
    <script type="text/javascript">
        function check()
        {
        let a = $("#password").val();
         let b = $("#c_password").val();
         if(a.length>7 )
         {
                
          


         if(a == b){
            $("#passwordcheck").hide()
            console.log("yes");
            resetpassword();
         }
         else{
            $("#passwordcheck").show().css("color","red").html("Password and Confirm Password does not match");
         }
     }else{

        $("#passwordcheck").show().css("color","red").html("Password should be minimum 8 characters");
     }
        }
</script>

        <script>
$(document).ready(function(){
    $('#usercheck').hide();
    $('#passcheck').hide();

    var user_err = true;
    var pass_err = true;

    $('#password').keyup(function(){
        email_check();
        
    });

    function email_check(){

        var email_val = $('#password').val();
        if(email_val.length == ''){
            $('#usercheck').show();
            $('#usercheck').html("Password is required");
            $('#usercheck').focus();
            $('#usercheck').css("color","red");
            user_err = false;
            return false;



        }
        else{
            $('#usercheck').hide();

        }

    }

    $('#c_password').keyup(function(){
        pass_check();
        
    });

    function pass_check(){
        var email_val = $('#c_password').val();
        if(email_val.length == ''){
            $('#passcheck').show();
            $('#passcheck').html("Confirm password is required");
            $('#passcheck').focus();
            $('#passcheck').css("color","red");
            user_err = false;
            return false;
        


        }
        else{
            $('#passcheck').hide();

        }

    }

    $('#submitb').click(function(){
         user_err = true;
         pass_err = true;
        email_check();
        pass_check();
        if((user_err == true) && (pass_err == true) ){
            check();
        }
        else{
            return false;
        }



    })

});


    </script>
 
</body>

</html>