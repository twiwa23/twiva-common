<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Add product</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link href="../css/custom-style.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <link href="../css/custom-style.css" rel="stylesheet">
    <script type="text/javascript" src="../assets/js/business_category_color_size.js"></script>

</head>



<?php include('common/side_menu.php'); ?>


<!-- page content -->
<div class="right_col add-product-page" role="main">

    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="./my-catalog-2.php">My Catalog</a></li>
            <li class="active">Add Product</li>
        </ul>
    </div>
    <div class="alert alert-success" id="successMessage" style="display: none;">

    </div>
    <div class="form-title">
        <h4>
            Add Product
        </h4>
    </div>

    <div class="catalog-form">
        <div class="form-field">
            <label>Product Title</label>
            <input id="title" type="text" placeholder="">
            <h5 id="titlecheck"></h5>
        </div>

        <div class="form-field">
            <label>SKU</label>
            <input id="sku" type="text" placeholder="">
        </div>

        <div class="form-field">
            <label>Select Category</label>
            <select id="category" onchange="GetSubCategory()">
                <option value="0">Select Category</option>
            </select>
        </div>
        <!-- ===================== -->

        <button class="btntest1" style="display: none;" id="btntest1">test</button>
        <!-- ========================== -->
        <!-- <div class="form-field">
            <label>Price</label>
            <h5 id="pricecheck"></h5>
            <input id="price"type="number" placeholder=" KSH 400">
        </div> -->
        <div class="form-field" id="sca">
            <label>Select SubCategory</label>
            <select id="sub_category" onchange="Get_sizes()">
            </select>
        </div>
        <script>

        </script>
        <div class="form-field">
            <label for="price">Product Price</label>
            <div class="input-group-prepend product-price">
                <span class="input-group-text">KSH</span>
                <input type="number" class="form-control" id="price">
            </div>
            <h5 id="pricecheck"></h5>
        </div>

        <div class="form-field">
            <label>Stock</label>
            <div class="number">
                <span class="minus">-</span>
                <input id="stock" type="text" value="1">
                <span class="plus">+</span>
            </div>
        </div>



        <div class="form-field multi-select-wrapper" id="sa">
            <label>Select Size</label>


            <select id="size" class="mul-select-size" multiple="true">


            </select>

        </div>


        <!-- <div class="form-field multi-select-wrapper" id="color-field">
            <label>Select Color</label>

            <select id="color" class="mul-select-color" multiple="true">


            </select>

        </div> -->
        <div class="select-color form-field" id="color-field">
            <label for="select-color">Select Color</label>
            <div>
                <input type="hidden" id="color" />
                <ul class="color-list" id="colors">
                   
                </ul>
                <h5 id="color-error" class="empty-field-error"></h5>
            </div>
        </div>

        <div class="form-field-full">
            <label>Description</label>
            <textarea id="description" maxlength="250"></textarea>
            <h5 id="descriptioncheck"></h5>
        </div>

        <div class="form-field-full specification-sec" id="specs">
            <div class="field-inner">
                <label>Specification</label>
                <input id="specification1" type="text" placeholder="">
                <h5 id="specificationcheck"></h5>
            </div>


        </div>
        <button id="addbtn">+ Add Specification</button>

        <div class="form-field-full">
            <label>Add Images (Minimum image should be 500x500 pixels & size should be less than 2 MB)</label>
            <div class="browse-box">
                <div class="browse-box-inner">
                    <label for="image">
                        <input type="file" name="myFile" class="drop-zone__input" id="image" onchange="readURL(this);">
                        <div class="browse-content">
                            <img src="../images/icons/image.svg">
                            <!-- <p>Drag & drop</p> -->
                            <p> Browse Image</p>
                        </div>
                        <!-- uploded image preview  -->
                        <div class="featured-image-preview-wrapper">
                            <img id="blah" alt="">
                        </div>
                        <!-- uploded image preview  -->
                    </label>
                </div>
            </div>
            <span class="custom_error" id="image_error" style="display: none;
    margin-top: 10px;
    margin-left: 180px;
    color: red;
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 250%;">Image size should not be greater than 2MB</span>
            <div class="browse-items">
                <!-- <span>+</span> -->
                <div class="browse-ss position-relative">
                    <label for="image1">
                        <input type="file" id="image1" name="image1" onchange="image1(this)">
                        <span>+</span>
                        <!-- uploded image preview  -->
                        <div class="product-additional-image">
                            <img id="img1" alt="">
                        </div>
                        <!-- uploded image preview  -->
                    </label>
                    <span class="image-delete1" data-name='image1' style="
    display:none;
    width: 16px;
    height: 16px;
    position: absolute;
    right: 4px;
    top: 4px;
    border-radius: 35%;
    z-index: 20;
"><img src="../images/icons/x.svg" alt=""></span>
                </div>
                <div class="browse-ss position-relative">
                    <label for="image2">
                        <input type="file" id="image2" onchange="image2(this)">
                        <span>+</span>
                        <!-- uploded image preview  -->
                        <div class="product-additional-image">
                            <img id="img2" alt="">
                        </div>
                        <!-- uploded image preview  -->
                    </label>
                    <span class="image-delete2" data-name='image2' style="
    display:none;
    width: 16px;
    height: 16px;
    position: absolute;
    right: 4px;
    top: 4px;
    border-radius: 35%;
    z-index: 20;
"><img src="../images/icons/x.svg" alt=""></span>
                </div>

                <div class="browse-ss position-relative">
                    <label for="image3">
                        <input type="file" id="image3" onchange="image3(this)">
                        <span>+</span>
                        <!-- uploded image preview  -->
                        <div class="product-additional-image">
                            <img id="img3" alt="">
                        </div>
                        <!-- uploded image preview  -->
                    </label>
                    <span class="image-delete3" data-name='image3' style="
    display:none;
    width: 16px;
    height: 16px;
    position: absolute;
    right: 4px;
    top: 4px;
    border-radius: 35%;
    z-index: 20;
"><img src="../images/icons/x.svg" alt=""></span>
                </div>
                <div class="browse-ss position-relative">
                    <label for="image4">
                        <input type="file" id="image4" onchange="image4(this)">
                        <span>+</span>
                        <!-- uploded image preview  -->
                        <div class="product-additional-image">
                            <img id="img4" alt="">
                        </div>
                        <!-- uploded image preview  -->
                    </label>
                    <span class="image-delete4" data-name='image4' style="
    display:none;
    width: 16px;
    height: 16px;
    position: absolute;
    right: 4px;
    top: 4px;
    border-radius: 35%;
    z-index: 20;
"><img src="../images/icons/x.svg" alt=""></span>
                </div>
            </div>
        </div>
        <span class="custom_error" id="image_error" style="display:none;
    margin-top: -20px;
    margin-left: 100px;
    color: red;
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 150%;">Image size should not be greater than 2MB</span>
        <div class="form-field-full">
            <div class="common-button">
                <button id="submitbtn1" class="purple-btn">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
</div>
</div>
<!-- multi select -->

<!-- Modal -->
<div class="modal fade" id="productAddModal" role="dialog">
    <div class="modal-dialog">


        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">
                <div class="title text-center">
                    <h4>
                        Please add atleast one photo
                    </h4>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="c-btn close-btn" id="closebtn" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>

<!--End Modal -->

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- multi select -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="../js/custom.js"></script>


<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>
    $("#category").change(function() {
        $("#sub_category").val([]).change();
    });

    
</script>

<script>
    $(document).on("click",".image-delete1",function(){
        var image = $(this).attr('data-name');
        $('#img1').removeAttr('src');
        STORAGE.remove(STORAGE.image1);
        $('.image-delete1').hide();
    });
    $(document).on("click",".image-delete2",function(){
        var image = $(this).attr('data-name');
        $('#img2').removeAttr('src');
        STORAGE.remove(STORAGE.image2);
        $('.image-delete2').hide();
    });
    $(document).on("click",".image-delete3",function(){
        var image = $(this).attr('data-name');
        $('#img3').removeAttr('src');
        STORAGE.remove(STORAGE.image3);
        $('.image-delete3').hide();
    });
    $(document).on("click",".image-delete4",function(){
        var image = $(this).attr('data-name');
        $('#img4').removeAttr('src');
        STORAGE.remove(STORAGE.image4);
        $('.image-delete4').hide();
    });


    // function readURL(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#blah').attr('src', e.target.result);
    //             logoUpload1(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#blah").removeClass("hide");
    //     }
    // }
    

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#blah').attr('src', e.target.result);
                        logoUpload1(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#blah").removeClass("hide");
        }
    }

    // function image1(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img1').attr('src', e.target.result);
    //             $('.image-delete1').show();
    //             image1upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img1").removeClass("hide");
    //     }
    // }

    function image1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img1').attr('src', e.target.result);
                        $('.image-delete1').show();
                        image1upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img1").removeClass("hide");
        }
    }

    // function image2(input) {
    //     if (input.files && input.files[0]) {
    //         var reader = new FileReader();
    //         reader.onload = function(e) {
    //         var size=(input.files[0].size);
    //         if(size < 2000000) {
    //             $("#image_error").hide();
    //             $('#img2').attr('src', e.target.result);
    //             $('.image-delete2').show();
    //             image2upload(input);
    //         }else{
    //             $("#image_error").show();
    //             $("#image_error").html('Image size should be less than 2MB');
    //         }
    //         };
    //         reader.readAsDataURL(input.files[0]);
    //         $("#user").addClass("hide");
    //         $("#img2").removeClass("hide");
    //     }
    // }

    function image2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img2').attr('src', e.target.result);
                        $('.image-delete2').show();
                        image2upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img2").removeClass("hide");
        }
    }

    function image3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img3').attr('src', e.target.result);
                        $('.image-delete3').show();
                        image3upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img3").removeClass("hide");
        }
    }

    function image4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var width, height;
            reader.onload = function(e) {
                var size=(input.files[0].size);
                var img = new Image();
                img.src = e.target.result;
                img.onload = function(){
                    width = this.width;
                    height = this.height;

                    if( width < 500 || height < 500 || size > 2000000){
                        $("#image_error").show();
                        if(size > 2000000){
                            $("#image_error").html('Image size should be less than 2MB');
                        }else{
                            $("#image_error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#image_error").hide();
                        $('#img4').attr('src', e.target.result);
                        $('.image-delete4').show();
                        image4upload(input);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
            $("#user").addClass("hide");
            $("#img4").removeClass("hide");
        }
    }

    jQuery(document).ready(function() {
        jQuery('#dtOrderExample').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
        jQuery('.dataTables_length').addClass('bs-select');
    });
</script>



<script>
    jQuery(document).ready(function() {
        jQuery('.minus').click(function() {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        jQuery('.plus').click(function() {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
    $(function() {
        get_category();
    });
</script>

<script>
    $(document).ready(function() {
        //  $(".mul-select-size").select2({
        //         placeholder: "Select Size", //placeholder
        //         tags: true,
        //         tokenSeparators: ['/',',',';'," "] 
        //     });
        $(".mul-select-color").select2({
            placeholder: "Select Color", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });
        $(".mul-select-size").select2({

            placeholder: "Select Size", //placeholder


            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });

        $(".mul-select-sub-category").select2({
            placeholder: "Select SubCategory", //placeholder
            tags: true,
            tokenSeparators: ['/', ',', ';', " "]
        });   
});

    //     document.getElementById('btntest').onclick = function() {
    //     var selected = [];
    //     for (var option of document.getElementById('size').options)
    //     {
    //         if (option.selected) {
    //             selected.push(option.value);
    //         }
    //     }

    //     STORAGE.set(STORAGE.productsize,selected);
    //     // alert(selected); 
    // }

    
    


    document.getElementById('btntest1').onclick = function() {
        // var selected1 = [];
        // for (var option of document.getElementById('color').options) {
        //     if (option.selected) {
        //         selected1.push(option.value);
        //     }

        // }
        // alert(selected1);
        // STORAGE.set(STORAGE.colors, selected1);
        // alert(selected); 
        var selected = [];
        for (var option of document.getElementById('size').options) {
            if (option.selected) {
                selected.push(option.value);
            }
        }

        STORAGE.set(STORAGE.productsize, selected);
        // alert(selected); 
    }
</script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/business_addproducts.js"></script>

<script>
    $(function() {
        var id = 2;
        $("#addbtn").bind("click", function() {
            if (id <= 7) {
                var div = $("<div class='field-inner' >");
                div.html(GenerateTextbox("", id));
                $("#specs").append(div);
                id++;
                if (id > 7) {
                    $('#addbtn').hide();
                }
            } else {
                $('#addbtn').hide();
            }
        });

        $("#buttonGet").bind("click", function() {
            var values = "";
            $("input[name=CreateTextbox]").each(function() {
                values += $(this).val() + "\n";
            });
            alert(values);
        });

        $("body").on("click", ".remove", function() {
            $(this).closest("div").remove();
            id--;
            if (id <= 7) {
                $('#addbtn').show();
            }
        });


    });

    function GenerateTextbox(value, id) {
        return '    <label>Specification</label> <h5 id="specificationcheck"></h5><input name = "CreateTextbox" id="specification' + id + '" type="text" value = "" /> ' +
            '<input type="button" value="Remove" class="remove" /> </div>'
    }
    STORAGE.remove(STORAGE.image);
    STORAGE.remove(STORAGE.image1);
    STORAGE.remove(STORAGE.image2);
    STORAGE.remove(STORAGE.image3);
    STORAGE.remove(STORAGE.image4);
</script>


<script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>




</body>

</html>