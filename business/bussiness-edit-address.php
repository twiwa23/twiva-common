<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Profile Information</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


   
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
      <!--Mobile View Styling-->
      <link href="../css/mobile-view.css" rel="stylesheet">
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
   
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  
</head>

<body class="nav-md">

    <header>
        <div class="logo">
            <img src="../images/logo/logo-white.svg">
        </div>

        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="../images/icons/img.jpg" alt="">  Devon Lane
                                <!-- <span class=" fa fa-angle-down"></span> -->
                               
                            </a>
                               <ul class="nav navbar-nav navbar-right">

                        <li class="" id="userprofile">
                           
                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                              
                                <li ><a onclick="logout_check()"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="../images/icons/bell.svg">
                                <span class="badge bg-yellow">6</span>
                            </a>
                           
                        </li>

                    </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="../images/icons/bell.svg">
                                <span class="badge bg-yellow">6</span>
                            </a>
                         
                        </li>

                    </ul>
                </nav>
            </div>

        </div>
    </header>



    <div class="container-fluid account-main-info-wrapper">
        <div class="back-link full-width-back-btn">
            <a href="bussiness-profile-information.php" class="page-back-btn"> <i class="fa fa-chevron-left"></i> Profile Information</a>
        </div>

        <div class="container edit-address">
            <div class="account-setup">
            <div class="account-cont">

                <h4>Edit Address</h4>
               
        
          <div class="account-form">
            <div class="form-field">
                <label>Country</label>
               <select id="country" onchange="state1()">
                  
               </select>
            </div>
        
        <div class="form-field">
                <label>State</label>
                 <select id="state" onchange="city1()">
                  
               </select>
            </div>
          
            <div class="form-field">
                <label>City</label>
               <select id="city">
                  
               </select>
            </div>
        
            
        
            <div class="form-field">
                <label>Locality / Area / Street</label>
                <input type="text" id="area" value=""placeholder="74 Wellington Road, Pinner">
            </div>
        
            <div class="form-field">
                <label>Building Number</label>
                <input type="text" id="building" placeholder="HA5 4NH">
            </div>
        </div>
          
          
          <div class="button-sec right-align-btn">
              <button>Update Address</button>
          </div>
            </div>
        </div>
        </div>


    </div>
   

 
 <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>
  <script type="text/javascript" src="../assets/js/business_profile.js"></script>
 <script>
        $(function(){
            profile_details();
        })
    </script>
    <script>
                $(function()
                {
                   var name= STORAGE.get(STORAGE.name);
                   var logo = STORAGE.get(STORAGE.logo);
                    $('#userprofile').append(`
                         <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="https://api.twiva.co.ke/storage/images/products/${logo}" alt="">  ${name}
                                <!-- <span class=" fa fa-angle-down"></span> -->
                               
                            </a>
                        `)
                });
                 $(function()
    {
        if (localStorage.getItem(STORAGE.accesstoken) == null) {
            goto(UI_URL.login);
        }
        return false;
    });

                     function logout_check() {
    localStorage.clear();
    login_check();
}
function login_check() {
  
    if (localStorage.getItem(STORAGE.accesstoken) == null) {
            goto(UI_URL.login);
        }
        return false;
    }
    function myprofile()
    {
       goto(UI_URL.onboarding10);
    }
            </script>
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>

</body>

</html>