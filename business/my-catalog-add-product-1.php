<?php include('header.php');?>


    <div class="">
        <div class="dashboard_container">

            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h4>Menu</h4>
                            <ul class="nav side-menu">
                                <li><a href="dashboard.html"><img
                                            src="../images/icons/dashboard.svg">Dashboard<span
                                            class="fa fa-chevron-down"></span></a>
                                    <!-- <ul class="nav child_menu" style="display: none">
                                        <li><a href="index.html">Dashboard</a>
                                        </li>
                                        <li><a href="index2.html">Dashboard2</a>
                                        </li>
                                        <li><a href="index3.html">Dashboard3</a>
                                        </li>
                                    </ul> -->
                                </li>
                                <li><a href="my-catalog.html"><img src="../images/icons/catalog.svg"> My Catalog <span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="order.html"><img src="../images/icons/order.svg"> Order Received<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="best-performing-product.html"><img src="../images/icons/reports.svg">Reports<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="seller-orders.html"><img src="../images/icons/settings.svg"> Account Setting<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="seller-add-product.html"><img src="../images/icons/refer.svg"> Refer & Earn<span class="fa fa-chevron-down"></span></a> </li>
                              </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                </div>
            </div>

            <!-- page content -->
            <div class="right_col add-product-page" role="main">
               <div class="page-title">My Catalog <span class="fa fa-chevron-right"></span> Add Product</div>
<div class="catalog-form">
   <div class="form-field">
       <label>Product Title</label>
       <input type="text" placeholder="Globex Corporation">
   </div>

   <div class="form-field">
    <label>Select Category</label>
    <div class="dropdown multi-select-options">
        <button type="button" class=" " data-toggle="dropdown">
          Dropdown button
        </button>
        <div class="dropdown-menu">
          <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck1">
              Default checkbox
            </label></li>
          <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck1">
              Default checkbox
            </label></li>
          <li class="check-item" ><input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
            <label class="form-check-label" for="defaultCheck1">
              Default checkbox
            </label></li>
        </div>
      </div>
    
</div>

<div class="form-field">
    <label>Price</label>
    <input type="text" placeholder="$400">
</div>

<div class="form-field">
    <label>Stock</label>
    <div class="number">
        <span class="minus">-</span>
        <input type="text" value="1">
        <span class="plus">+</span>
    </div>
</div>

<div class="form-field">
    <label>Select Size</label>
    <select>
        <option>Small</option>
        <option>Medium</option>
        <option>Large</option>
        <option>X-Large</option>
    </select>
</div>

<div class="form-field">
    <label>Select Color</label>
    <select>
        <option>Red</option>
        <option>Blue</option>
        <option>White</option>
    </select>
</div>

<div class="form-field-full">
    <label>Description</label>
    <textarea>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.</textarea>
</div>

<div class="form-field-full specification-sec">
    <div class="field-inner">
    <label>Specification</label>
    <input type="text" placeholder="Globex Corporation">
    </div>
    <button>+ Add Specification</button>
</div>

<div class="form-field-full">
    <label>Specification</label>
    <div class="browse-box">
        <div class="browse-box-inner">
            <label for="product-featur-img">
    <input type="file" id="product-featur-img">
    <div class="browse-content">
        <img src="../images/icons/image.svg">
        <!-- <p>Drag & drop</p> -->
        <p>Your image here or Browse</p>
    </div>
    <!-- uploded image preview  -->
    <div class="featured-image-preview-wrapper">
        <img src="../images/product-img/Rectangle.png" alt="">
    </div>
    <!-- uploded image preview  -->
    </label>
    
    </div>
    </div>

    <div class="browse-items">
        <span>+</span>
        <div class="browse-ss">
            <label for="media0">
            <input type="file" id="media0" >
            
            <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img src="../images/card-images/member-1.jpg" alt="">
            </div>
             <!-- uploded image preview  -->
        </label>
        </div>
        <div class="browse-ss">
            <label for="media1">
                <input type="file" id="media1" >
               
                <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img src="../images/card-images/member-1.jpg" alt="">
            </div>
             <!-- uploded image preview  -->

            </label>
        </div>
        <div class="browse-ss">
            <label for="media2">
                <input type="file" id="media2" >

               <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img src="../images/card-images/member-1.jpg" alt="">
            </div>
             <!-- uploded image preview  -->

            </label>
        </div>
        <div class="browse-ss">
            <label for="media3">
                <input type="file" id="media3">

              <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img src="../images/card-images/member-1.jpg" alt="">
            </div>
             <!-- uploded image preview  -->
            </label>
        </div>
        <div class="browse-ss">
            <label for="media4">
                <input type="file" id="media4" >

             
            </label>
        </div>
    </div>
</div>
<div class="form-field-full">
    <div class="common-button">
    <button class=" white-bttn">Preview</button>
    <button class="purple-btn">Save</button>
    </div>
</div>
</div>
</div>
<!-- /page content -->
</div>
</div>

<?php include('footer.php');?>

</html>