<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>My Catalog || Add product</title>
    
    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    
    
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  
  <!-- multi select -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <!-- multi select -->

</head>



          <?php include('common/side_menu.php');?>


            <!-- page content -->
            <div class="right_col add-product-page" role="main">
            <div class="alert alert-success" id="successMessage" style="display: none;">
    
  </div>
               <div class="page-title">My Catalog <span class="fa fa-chevron-right"></span> Add Product</div>
				
				<div class="form-title">
					<h4>
						Add Product
					</h4>
				</div>
				
<div class="catalog-form">
   <div class="form-field">
       <label>Product Title</label>
       <input id="title" type="text" placeholder="Globex Corporation">
       <h5 id="titlecheck"></h5>
   </div>

   <div class="form-field">
    <label>Select Category</label>
    <select id="category" onchange="Get_sizes()">
       
    </select>
</div>
<!-- ===================== -->
      <div class="form-field multi-select-wrapper">
            <label>Select Category</label>
            
               
                <select id="test" class="mul-select-category" multiple="true">
                    <option value="Cambodia">Cambodia</option>
                    <option value="Khmer">Khmer</option>
                    <option value="Thiland">Thiland</option>
                    <option value="Koren">Koren</option>
                    <option value="China">China</option>
                    <option value="English">English</option>
                    <option value="USA">USA</option>
                    <option value="Cambodia">Cambodia</option>
                    <option value="Khmer">Khmer</option>
                    <option value="Thiland">Thiland</option>
                    <option value="Koren">Koren</option>
                    <option value="China">China</option>
                    <option value="English">English</option>
                    <option value="USA">USA</option>
                </select>
          
        
        </div>
<!-- ========================== -->
<div class="form-field">
    <label>Price</label>
    <h5 id="pricecheck"></h5>
    <input id="price"type="text" placeholder=" KSH 400">
</div>

<div class="form-field">
    <label>Stock</label>
    <div class="number">
        <span class="minus">-</span>
        <input id="stock" type="text" value="1">
        <span class="plus">+</span>
    </div>
</div>


    <div class="form-field">
    <label>Select Size</label>
    <div class="dropdown multi-select-options">
        <button type="button" class=" " data-toggle="dropdown">
          Choose Size
        </button>
        <div class="dropdown-menu" id="size">
          
        </div>
      </div>
    
</div>


<div class="form-field">
    <label>Select Color</label>
    <div class="dropdown multi-select-options">
        <button type="button" class=" " data-toggle="dropdown">
         Choose Color
        </button>
        <div class="dropdown-menu" id="color">
          
        </div>
      </div>
    
</div>

<div class="form-field-full">
    <label>Description</label>
     <h5 id="descriptioncheck"></h5>
    <textarea id="description"></textarea>
</div>

<div class="form-field-full specification-sec"id="specs">
    <div class="field-inner" >
    <label>Specification</label>
     <h5 id="specificationcheck"></h5>
    <input id="specification"type="text" placeholder="Globex Corporation">
    </div>

    
</div>
<button id="addbtn">+ Add Specification</button>

<div class="form-field-full">
    <label>Specification</label>
    <div class="browse-box">
        <div class="browse-box-inner">
            <label for="image">
    <input type="file" name="myFile" class="drop-zone__input" id="image"  onchange="readURL(this);">
    <div class="browse-content">
        <img src="../images/icons/image.svg">
        <p>Drag & drop</p>
        <p>Your image here or Browse</p>
    </div>
    <!-- uploded image preview  -->
    <div class="featured-image-preview-wrapper">
        <img id="blah" alt="">
    </div>
    <!-- uploded image preview  -->
    </label>
    
    </div>
    </div>

    <div class="browse-items">
        <!-- <span>+</span> -->
        <div class="browse-ss">
            <label for="image1">
            <input type="file" id="image1" name="image1" onchange="image1(this)" >
            <span>+</span>
            <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img1"  alt="">
            </div>
             <!-- uploded image preview  -->
        </label>
        </div>
        <div class="browse-ss">
            <label for="image2">
                <input type="file" id="image2" onchange="image2(this)" >
                <span>+</span>
                <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img2"  alt="">
            </div>
             <!-- uploded image preview  -->

            </label>

        </div>

        <div class="browse-ss">
            <label for="image3">
                <input type="file" id="image3" onchange="image3(this)" >
                <span>+</span>
               <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img3"  alt="">
            </div>
             <!-- uploded image preview  -->

            </label>
        </div>
        <div class="browse-ss">
            <label for="image4">
                <input type="file" id="image4" onchange="image4(this)">
                <span>+</span>
              <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img4"  alt="">
            </div>
             <!-- uploded image preview  -->
            </label>
        </div>
        
    </div>
</div>
<div class="form-field-full">
    <div class="common-button">
    <button id="submitbtn1" class="purple-btn">Save</button>
    </div>
</div>
</div>
</div>
<!-- /page content -->
</div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="../js/custom.js"></script>
    
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
    <script>


   
    </script>
<script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        /*.width(150)
                        .height(200);*/
                };
                   logoUpload1(input);
                reader.readAsDataURL(input.files[0]);
                $("#user").addClass("hide");
                $("#blah").removeClass("hide");
               
            }
        }

        //2ndimage
           function image1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img1')
                        .attr('src', e.target.result)
                        /*.width(150)
                        .height(200);*/
                };
                   image1upload(input);
                reader.readAsDataURL(input.files[0]);
                $("#user").addClass("hide");
                $("#img1").removeClass("hide");
               
            }
        }
        function image2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img2')
                        .attr('src', e.target.result)
                        /*.width(150)
                        .height(200);*/
                };
                   image2upload(input);
                reader.readAsDataURL(input.files[0]);
                $("#user").addClass("hide");
                $("#img2").removeClass("hide");
               
            }
        }
          function image3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img3')
                        .attr('src', e.target.result)
                        /*.width(150)
                        .height(200);*/
                };
                   image3upload(input);
                reader.readAsDataURL(input.files[0]);
                $("#user").addClass("hide");
                $("#img3").removeClass("hide");
               
            }
        }
          function image4(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img4')
                        .attr('src', e.target.result)
                        /*.width(150)
                        .height(200);*/
                };
                   image4upload(input);
                reader.readAsDataURL(input.files[0]);
                $("#user").addClass("hide");
                $("#img4").removeClass("hide");
               
            }
        }
         
    jQuery(document).ready(function () {
        jQuery('#dtOrderExample').DataTable({
"order": [[ 3, "desc" ]]
});
jQuery('.dataTables_length').addClass('bs-select');
});

</script>



<script>
    jQuery(document).ready(function () {
        jQuery('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        jQuery('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
    $(function(){
        get_category();
    });

 let addbutton = document.getElementById("addbtn");
addbutton.addEventListener("click", function() {
  let boxes = document.getElementById("specs");
  let clone = boxes.firstElementChild.cloneNode(true);
  boxes.appendChild(clone);
});

</script>
  <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_addproducts.js"></script>
 <script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>
 <script type="text/javascript" src="../assets/js/business_category_color_size.js"></script>

<script>
     $(document).ready(function(){
         $(".mul-select-size").select2({
                placeholder: "Select Size", //placeholder
                tags: true,
                tokenSeparators: ['/',',',';'," "] 
            });
            $(".mul-select-color").select2({
                placeholder: "Select Color", //placeholder
                tags: true,
                tokenSeparators: ['/',',',';'," "] 
            });
            $(".mul-select-category").select2({
                placeholder: "Select Category", //placeholder
                tags: true,
                tokenSeparators: ['/',',',';'," "] 
            });
        })

    document.getElementById('btntest').onclick = function() {
    var selected = [];
    for (var option of document.getElementById('test').options)
    {
        if (option.selected) {
            selected.push(option.value);
        }
    }

    alert(selected);
}
        
</script>

</body>

</html>
