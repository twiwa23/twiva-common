<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, Title, CSS, Favicon, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="./images/icons/fav.png" type="image/x-icon">

    <title>Reports</title>

    <!-- Bootstrap Core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="https://c-dnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">

    <!-- Custom Styling Plus Plugins -->
    <link href="../css/custom.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<?php include('common/side_menu.php'); ?>

<!-- page content -->
<div class="right_col dashboard-page" role="main">
    <div class="page-title">Reports</div>
    <div class="business-content-wrapper">
        <div class="bussiness-tab-page">
            <div class="tabbing-main">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li class=""><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true" class="active">Best performing products</a>
                        </li>
                        <li class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Least performing product</a>
                        </li>
                        <li class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Yearly & Monthly Sale</a></li>
                        <!-- <li   class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Top Connected Influencer</a></li> -->
                    </ul>

                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in show" id="tab_content1" aria-labelledby="home-tab">

                            <div class="admin-table">
                                <div class="table-responsive">
                                    <table id="" class="table table-bordered " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="th-sm">Image
                                                </th>
                                                <th class="th-sm">Product Name
                                                </th>
                                                <th class="th-sm">Rating
                                                </th>
                                                <th class="th-sm">Quantity
                                                </th>
                                                <th class="th-sm">Price
                                                </th>
                                                <th class="th-sm">Status
                                                </th>
                                                <th class="th-sm">Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="bestPerforming">

                                        </tbody>

                                    </table>
                                </div>


                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <div class="admin-table">
                                <div class="table-responsive">
                                    <table id="" class="table table-bordered " cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="th-sm">Image
                                                </th>
                                                <th class="th-sm">Product Name
                                                </th>
                                                <th class="th-sm">Rating
                                                </th>
                                                <th class="th-sm">Quantity
                                                </th>
                                                <th class="th-sm">Price
                                                </th>
                                                <th class="th-sm">Status
                                                </th>
                                                <th class="th-sm">Actions
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="leastPerforming">

                                        </tbody>

                                    </table>

                                </div>


                            </div>
                        </div>


                        <div role="tabpanel" class="report-tab tab-pane fade" id="tab_content3" aria-labelledby="profile-tab2">
                            <div class="admin-table">
                                <div class="business-dashboard">

                                    <div class="business-graph">
                                        <canvas id="cvs" width="600" height="250">
                                            [No canvas support]
                                        </canvas>
                                    </div>

                                    <!-- <div class="pie-graph">
                                        <img src="../images/card-images/pie.jpg">
                                        <canvas id="piechart">
                                            <h1>Pie Chart</h1>
                                        </canvas>
                                    </div> -->

                                    <div class="revenue-section container-fluid">
                                        <div class="row">
                                            <div class="revenue col-lg-8">

                                            </div>

                                            <div class="sales col-lg-4">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                            </div>
                        </div>

                        <!-- <div role="tabpanel" class="report-tab tab-pane fade" id="tab_content4" aria-labelledby="profile-tab2">
                        <div class="admin-table">
                        <div class="table-responsive">
                            <table id="" class="table table-bordered " cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="th-sm">Image
                                    </th>
                                    <th class="th-sm">influencer
                                    </th>
                                    <th class="th-sm">eshop 
                                    </th>
                                    <th class="th-sm">Product QTY
                                    </th>
                                    <th class="th-sm">SEll</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                                <tr>
                                    <td><img src="../images/icons/business-prof.png"></td>
                                    <td>Arlene McCoy</td>
                                    <td>Wonka Industries</td>
                                    <td>250</td>
                                    <td>25 product sold</td>
                                </tr>

                            </table>
                        </div>
                    </div> -->
                    </div>



                </div>
            </div>
        </div>

        <div class="pagination">
            <div class="col-sm-5">

                <!-- <div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0"><span class="fa fa-chevron-left"></span></a></li><li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0"><span class="fa fa-chevron-right"></span></a></li></ul></div></div> -->
            </div>



        </div>
    </div>
</div>

<!-- /page content -->
</div>


</div>


<script src="../js/bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
<script src="../js/jquery-3.2.1.min.js"></script>

<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/reports.js"></script>

<script src="../assets/js/RGraph.common.core.js"></script>
<script src="../assets/js/RGraph.drawing.background.js"></script>
<script src="../assets/js/RGraph.line.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

<script>
    jQuery(document).ready(function() {
        fetch_reports();
        fetch_reports1();
        fetch_reports2();
        fetch_reports3();
        jQuery('#').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
        jQuery('.dataTables_length').addClass('bs-select');
    });
</script>


<script>
    new RGraph.Drawing.Background({
        id: 'cvs',
        options: {
            marginLeft: 35,
            marginRight: 35,
            marginTop: 35,
            marginBottom: 35,
            backgroundGridVlinesCount: 48,
            backgroundGridHlinesCount: 20,
            backgroundGridColor: '#eee'
        }
    }).draw();

    new RGraph.Line({
        id: 'cvs',
        data: [9, 6, 8, 5, 4, 2, 3],
        options: {
            //xaxisPosition: 'top'
        }
    }).draw().responsive([{
            maxWidth: 900,
            width: 400,
            height: 150,
            options: {
                xaxisLabels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                textSize: 10
            }
        },
        {
            maxWidth: null,
            width: 750,
            height: 250,
            options: {
                xaxisLabels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                textSize: 12
            }
        }
    ]);
</script>
</body>

</html>