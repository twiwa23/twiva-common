<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    <title>Bank Setup</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">


    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">


    <link rel="preconnect" href="https://fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>

<?php include('common/side_menu.php') ?>
<!-- /sidebar menu -->

</div>
</div>

<!-- page content -->
<div class="right_col add-product-page" role="main">

    <!--********** Breadcrumb Start ***********-->
    <div class="page-title breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="#">Account Setting</a></li>
            <li class="active">My Subscription</li>
        </ul>
    </div>

    <!--**********  Breadcrumb End ***********-->





    <div class="dashboard-inner">

        <div class="bank-setup-form">
            <h3 class="subscription-title">
                Where would you like to get paid?
            </h3>
            <h5 style="display: none;" class="login-error alert alert-success" role="alert" id="sucotp"></h5>
            <div class="account-setup">
                <div class="account-form">
                    <div class="form-field">
                        <label>Bank Name</label>
                        <select id="bank">

                        </select>
                        <h5 id="bankerr" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field">
                        <label>Branch Name</label>
                        <input type="text" id="branch_name" placeholder="Branch Name" maxlength="60">
                        <h5 id="branch_error" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field">
                        <label>City</label>
                        <input type="text" id="city" placeholder="City" maxlength="60">
                        <h5 id="city_error" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field">
                        <label>Account Number</label>
                        <input type="text" id="account" placeholder="Account Number" maxlength="30">
                        <h5 id="city_error" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field">
                        <label>Re - Enter Account Number</label>
                        <input type="text" id="reaccount" placeholder="Re - Enter Account Number" maxlength="30">
                        <h5 id="reaccounterr" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field">
                        <label>Account Name</label>
                        <input type="text" id="name" placeholder="Account Name" maxlength="60">
                        <h5 id="nameerr" class="empty-field-error"></h5>
                    </div>
                    <!-- <div class="form-field">
                        <label>IFSC Code</label>
                        <input type="text" id="code" placeholder="IFSC Code" maxlength="20">
                        <h5 id="codeerr" class="empty-field-error"></h5>
                    </div> -->
                </div>
                <div class="button-sec right-align-btn d-block">
                    <button id="setopsubmit">Save</button>
                </div>
            </div>


        </div>


    </div>
</div>
<!-- /page content -->
</div>
</div>

<script>
    $(function() {
        bank_list();
        bank_details();
    })
</script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/change_password.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script src="../js/custom.js"></script>


</body>

</html>