<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Orders</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<?php include('common/side_menu.php'); ?>

<!-- page content -->
<div class="right_col dashboard-page" role="main">
    <div class="page-title">Order Received</div>
    <div class="catalog-page">

        <div class="select-top">
            <h3>Order</h3>
        </div>


        <div class="select-top select-search">
            <div class="search-panel">
                <img src="../images/icons/search.svg">
                <input type="text" placeholder="Search here">
            </div>

            <div class="button-catalog">
                <select>
                    <option>Status : All</option>
                </select>

                <input type="date">

            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered  order-tbl" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="th-sm">Order Id
                        </th>
                        <th class="th-sm">Customer Email
                        </th>
                        <th class="th-sm">Order Date
                        </th>
                        <!-- <th class="th-sm">Delivery Date
                        </th> -->
                        <th class="th-sm">Qty
                        </th>
                        <th class="th-sm">Amount
                        </th>
                        <th class="th-sm">Order Status
                        </th>
                    </tr>
                </thead>
                <tbody id='order-listing'>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <div class="col-sm-5">
                <!-- <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div> -->
                <!-- <div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0"><span class="fa fa-chevron-left"></span></a></li><li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="6" tabindex="0">6</a></li><li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0"><span class="fa fa-chevron-right"></span></a></li></ul></div></div> -->
            </div>



        </div>
    </div>
    <!-- /page content -->
</div>


</div>
<!-- <script>
    var rowCount = $('.table tr').length;
    if (rowCount > 1) {
        document.getElementById('nf').style.display = 'none';
    }
    // else{
    //     document.getElementById('nf').style.display = 'block';
    // }
</script> -->

<script src="../js/bootstrap.min.js"></script>
<script src="../js/custom.js"></script>
<script src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/api.js"></script>
<script type="text/javascript" src="../assets/js/Orders.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
<script>

</script>
</body>

</html>