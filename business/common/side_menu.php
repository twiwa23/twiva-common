<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">

    <title>Twiva</title>

    <!-- Bootstrap core CSS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="../css/animate.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet" />
    <link href="../css/style.css" rel="stylesheet" />
    <link href="../css/mobile-view.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
</head>

<body class="nav-md business-page-content-wrapper" onload="login_check()">
    <header>
        <div class="logo logo-toggle">
            <div class="nav toggle">
                <a id="menu_toggle" href="#" class="d-flex">
                    <img src="../images/icons/Frame 5.svg" alt="Toggle_Menu">
                </a>
            </div>
            <a href="" id="nav-click-old" data-url="/landing">
                <img src="../images/logo/logo-white.svg" />
            </a>
        </div>

        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <!-- <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div> -->

                    <ul class="nav navbar-nav navbar-right">

                        <li class="" id="userprofile">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="" id="user-image" alt="" /> <span id="user-name" style="color: white;"></span>
                                <!-- <span class=" fa fa-angle-down"></span> -->
                            </a>

                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <!-- <li>
                                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-edit-profile.php">Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li> -->
                                <li class="header-company-name-wrapper">
                                    <p class="company-name" id="nname">Biffco Enterprises Ltd.</p>
                                    <p class="email" id="eemail">nevaeh.simmons@example.com</p>
                                </li>
                                <li><a onclick="logout_check()" id="logout-btn">Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <!-- <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> -->
                            <a href="notifications.php" class="info-number" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="../images/icons/bell-icon.svg" />
                                <!-- <span class="badge bg-yellow" id="notification_count">0</span> -->
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu"></ul>
                        </li>

                        <li class="message-wrapper">
                            <a href="#"><img src="../images/icons/message-square.svg" alt=""></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div class="">
        <div class="dashboard_container">
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <button class="menu-close-button"><img src="http://localhost/twiva-common/images/icons/x.svg" alt="" /></button>

                        <!-- <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="" id="user-image" alt="" /> <span id="user-name" style="color: white;">Username</span>
                                <span class=" fa fa-angle-down"></span>
                            </a> -->

                        <div class="menu_section">
                            <h4>Menu</h4>
                            <ul class="nav side-menu">
                                <li>
                                    <a href="dashboard-2.php">
                                        <img src="../images/icons/dashboard.svg" />Dashboard
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </li>
                                <li class="sidebar-dropdown">
                                    <a><img src="../images/icons/hire.svg" />Hire<span class="fa fa-chevron-down"></span></a>
                                    <div class="sidebar-submenu">
                                        <ul class="account-setting-dropdown-wrapper sidebar-dropdown">
                                            <li class="drop-down-itme" id="nav-click-old" data-url="/influencers-list">
                                                <a href="#">
                                                    Influencers
                                                </a>
                                            </li>
                                            <li class="drop-down-itme" id="nav-click-old" data-url="/creaters">
                                                <a href="#">
                                                    Designers
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="#" id="nav-click-old" data-url="/posts">
                                        <img src="../images/image.svg" />Posts
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#" id="nav-click-old" data-url="/gig-list">
                                        <img src="../images/icons/twitch.svg" />Twitter Trends
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="my-catalog-2.php"><img src="../images/icons/catalog.svg" />My Catalog <span class="fa fa-chevron-down"></span></a>
                                </li>
                                <li>
                                    <a href="order.php"><img src="../images/icons/order.svg" />Order Received <span class="fa fa-chevron-down"></span></a>
                                </li>

                                <li>
                                    <a href="best-performing-product.php"><img src="../images/icons/reports.svg" />Reports <span class="fa fa-chevron-down"></span></a>
                                </li>

                                <!-- <li><a href="bussiness-profile-information.php"><img src="../images/icons/settings.svg"> Account Setting<span class="fa fa-chevron-down"></span></a></li> -->
                                <li class="sidebar-dropdown">
                                    <a><img src="../images/icons/settings.svg" />Account Settings<span class="fa fa-chevron-down"></span></a>
                                    <div class="sidebar-submenu">
                                        <ul class="account-setting-dropdown-wrapper sidebar-dropdown">
                                            <li class="drop-down-itme active">
                                                <a href="bussiness-profile-information.php">
                                                    Profile Settings
                                                </a>
                                            </li>
                                            <li class="drop-down-itme">
                                                <a href="my-subscriptions.php">
                                                    My Subscription
                                                </a>
                                            </li>
                                            <li class="drop-down-itme">
                                                <a href="bussiness-bank-setup.php">
                                                    Bank Setup
                                                </a>
                                            </li>
                                            <li class="drop-down-itme">
                                                <a href="bussiness-change-password.php">
                                                    Change Password
                                                </a>
                                            </li>

                                            <li class="drop-down-itme">
                                                <a href="earnings.php">
                                                    My Earning
                                                </a>
                                            </li>
                                            <li class="drop-down-itme">
                                                <a href="#">
                                                    Payment History
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <a href="refer-earn.php"><img src="../images/icons/refer.svg" />Refer & Earn <span class="fa fa-chevron-down"></span></a>
                                </li>
                                <li>
                                    <a onclick="logout_check()" href="#" id="logout-btn"><img src="../images/icons/logout-sidebar.svg" />Log Out <span class="fa fa-chevron-down"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <div id="wait" style="display: flex;
    position: fixed;
    top: 0%;
    left: 0%;
    align-items: center;
    justify-content: center;
    z-index: 9999999;
    width: 100%;
    height: 100%;
    background: #ffffffa3;"><img src='../images/loader.gif' width="100px" /></div>
            <script>
                    $(document).ready(function () {
                        $(document).ajaxStart(function(){
                            $("#wait").css("display", "flex");
                          });
                          $(document).ajaxComplete(function(){
                            $("#wait").css("display", "none");
                          });
                        getNotifications();
                        getNotificationCount();
                    });
            </script>
            <div class="backdrop"></div>
            <?php include('../twiva-config.php'); ?>
            <script>
                $(function() {
                    var name = STORAGE.get(STORAGE.name);
                    if (name == "" || name == "undefined" || name == "null" || name == null) {
                        name = "User";
                    }
                    var email = STORAGE.get(STORAGE.email);
                    var logo;
                    logo = STORAGE.get(STORAGE.logo);
                    var image;
                    image = "<?= IMAGES_URI_PATH ?>/icons/profile.jpeg";

                    if (logo && logo != "null" && logo != null && logo != "undefined") {
                        image = "https://api.twiva.co.ke/storage/images/products/" + logo;
                    }
                    document.getElementById("user-image").src = image;
                    document.getElementById("user-name").innerHTML = name;
                    document.getElementById("nname").innerHTML = name;
                    document.getElementById("eemail").innerHTML = email;

                    //     $('#userprofile').append(`
                    //      <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    //             <img src="${logo}" alt="">  ${name}
                    //             <!-- <span class=" fa fa-angle-down"></span> -->

                    //         </a>
                    //     `)
                });
                $(function() {
                    if (localStorage.getItem(STORAGE.accesstoken) == null) {
                        goto(UI_URL.login);
                    }
                    return false;
                });

                function logout_check() {
                    localStorage.clear();
                    login_check();
                }

                function login_check() {
                    if (localStorage.getItem(STORAGE.accesstoken) == null) {
                        goto(UI_URL.login);
                    }
                    return false;
                }

                function myprofile() {
                    goto(UI_URL.onboarding10);
                }
            </script>

            <script>
                $(document).ready(function() {
                    getNotifications();
                    getNotificationCount();
                });

                function getNotifications() {
                    __ajaxregister_http("business/notifications/listing", "", headers(), AJAX_CONF.apiType.GET, "", __success_getNotifications);
                }

                function __success_getNotifications(response) {
                    console.log(response);
                    if (response.success == true) {
                        let notifications = "";

                        response.data.forEach((element) => {
                            const dateTimeAgo = moment(element.created_at).fromNow();
                            notifications += `<li id="markAsRead" data-id="${element.id}" data-request_id="${element.request_id}">
                    <a>
                        <span class="image">
                        </span>
                        <span>
                                <span class="time">${dateTimeAgo}</span>
                                </span>
                                <span class="message">
                            ${element.description} 
                        </span>
                    </a>
                </li>`;
                        });
                        if (response.data.length >= 1) {
                            notifications += `<li>
                    <div class="text-center">
                        <a href="notifications.php">
                            <strong>See All Notifications</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </li>`;
                        } else {
                            notifications += `<li>
                    <div class="text-center">
                        <a href="">
                            <strong>No Notifications</strong>
                        </a>
                    </div>
                </li>`;
                        }
                        $(".msg_list").append(notifications);
                    }
                }

                $(document).on("click", "#markAsRead", function(event) {
                    event.preventDefault();
                    let order_id = $(this).attr("data-request_id");
                    var json = {
                        notification_id: $(this).attr("data-id"),
                    };
                    markAsRead(json);
                    window.location.href = "order-detail.php?order_id=" + btoa(btoa(order_id));
                });

                function markAsRead(json) {
                    __ajaxregister_http("/business/notifications/mark-read", json, headers(), AJAX_CONF.apiType.PUT, "", __success_markAsRead);
                }

                function __success_markAsRead(response) {
                    console.log(response);
                }

                function getNotificationCount() {
                    __ajaxregister_http("business/notifications/unread-count", "", headers(), AJAX_CONF.apiType.GET, "", __success_getNotificationCount);
                }

                function __success_getNotificationCount(response) {
                    console.log(response);
                    if (response.status == true) {
                        $("#notification_count").html(response.unread_count);
                    }
                }
            </script>

            <script>
                $(document).on("click", "#nav-click-old", function(e) {
                    var user_id = localStorage.getItem("userId");
                    var user_token = STORAGE.get(STORAGE.accesstoken);
                    var nav_url = $(this).data("url");
                    console.log(nav_url);
                    e.preventDefault();
                    if (user_id) {
                        $.ajax({
                            //url: "https://twiva.co.ke/api/v1/nav-link",
                            url: "https://home.twiva.co.ke/api/v1/nav-link",
                            //url: "http://127.0.0.1:8000/api/v1/nav-link",
                            headers: {
                                Accept: "application/json",
                                // 'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
                                ["Authorization"]: "Bearer " + STORAGE.get(STORAGE.accesstoken),
                            },
                            type: "post",
                            data: {
                                user_id: user_id,
                                nav_url: nav_url,
                                user_token: user_token
                            },
                            success: function(data) {
                                console.log(data);
                                console.log(data.data.url);
                                // window.open(data.data.url);
                                // let url = data.data.url.split("?");
                                window.location.href = data.data.url;
                            },
                            error: function(data) {
                                console.log(data);
                            },
                        });
                    }
                });
            </script>
            <script>
                function checkId() {
                    var json = {
                        id: $("#id").val(),
                        params: $("#params").val(),
                    };
                    checkIdWithToken(json);
                }

                function checkIdWithToken(json) {
                    __ajax_http("loginAsAdmin", json, {
                        Accept: "application/json"
                    }, AJAX_CONF.apiType.POST, "POST LOGIN", __success_checkIdWithToken);
                }

                function __success_checkIdWithToken(response) {
                    if (response.status == 422) {
                        if (response.message) {
                            $("#err").show().css("color", "red").html(response.message);
                        }
                        return;
                    }
                    if (response.status == false) {
                        if (response.error) {
                            $("#err").show().css("color", "red").html(response.error);
                        }
                        if (response.message) {
                            $("#err").show().css("color", "red").html("User not found");
                        } else {
                            $("#err").show().css("color", "red").html(response.message);
                        }
                        return;
                    }

                    __save_identity(response);
                }

                function __save_identity(response) {
                    // identity = response.data;
                    // console.log(response.original.status);
                    if (response.original.status == true && response.original.data.type == 1) {
                        localStorage.clear();
                        STORAGE.set(STORAGE.accesstoken, response.original.token);
                        STORAGE.set(STORAGE.userId, response.original.data.id);
                        STORAGE.set(STORAGE.name, response.original.data.bussiness_name);
                        STORAGE.set(STORAGE.logo, response.original.data.logo);
                        STORAGE.set(STORAGE.referal_code, response.original.data.referal_code);
                        STORAGE.set(STORAGE.points, response.original.data.points);
                        // setTimeout(function() {
                        //         goto(UI_URL.onboarding4);
                        // }, 100);
                    } else if (response.original.status == true && response.original.data.type == 2) {
                        console.log(response.original.data);
                        localStorage.clear();
                        localStorage.setItem("_userInfo", JSON.stringify(response.original.data));
                        localStorage.setItem("_userToken", response.original.token);
                        // window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-dashboard.php`;
                    } else {
                        localStorage.clear();
                        $("#err").show().css("color", "red").html(response.original.error);
                    }
                    if (response.original.failure || response.status) {
                        $("#err").show().css("color", "red").html(response.original.failure.message);
                        $("#err").show().css("color", "red").html(response.message);
                    }
                }

                $(document).ready(function() {
                    // console.log("clicked");
                    var user_check = STORAGE.get(STORAGE.needToUpdatePlatform);
                    if (atob(atob(user_check)) == 1) {
                        window.location.href = "bussiness-edit-profile-information.php";
                    }
                });
                $(document).ready(function() {
                    $("#back-button").click(function() {
                        window.location.href = "<?= $global_link; ?>";
                    });
                });
            </script>