<?php require_once('../twiva-config.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">
    <title>Sign Up</title>

    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/mobile-view.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>

<body class="p-0">
	

    <div class="login pt-5">

        <!-- <div class="back-link">
            <a href="login.php"> <i class='fas fa-chevron-left'></i> Sign Up</a>
        </div> -->
        <div class="container-fluid m-0">
            <div class="back-button">
                <button id="back-button"><img src="../images/icons/chevron-left-white.svg" alt="">Back</button>
            </div>
        </div>
        
<div class="container" id="step">
    <div class="login-inner">
      
    <div class="login-left">
        <!-- <img src="../images/banner/login.png"> -->
       
    </div>
    <div class="login-right">
       
        <div class="login-section">
            <div class="logo"><img src="../images/logo/logo.svg"></div>
    
            <div class="input-field">
                <label>E-mail Address</label>
                <input type="text" id="email" placeholder="willie.jennings@example.com">
                <h5 id="usercheck" class="empty-field-error"></h5>
            </div>

            <div class="input-field">
                <label>Password</label>
                <input id="password" type="password" placeholder="********">
                <div class="closed-eye showPass"></div>
                <div class="open-eye showPass"></div>
                <h5 id="passcheck" class="empty-field-error"></h5>
            </div>

            <div class="input-field">
                <label>Referral Code</label>
                <input type="text" id="referral"placeholder="Enter referral code...">
            </div>


             <button class="red-bttn" id="submitbtn" >Register Here</button>
            <div class="no-account">
                Already have an account? <a href="../login.php">Login</a>
            </div>
        </div>
    </div> 
</div>
</div>

<div class="footer-login">
    Copyright © 2021 Twiva. All Rights Reserved.
    <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
</div>
</div>

</body> 
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>

    <script>
            // Show Password 
            $(document).ready(function() {
                $('.closed-eye').on('click', function() {
                    $(this).hide();
                    $('.open-eye').show();
                });

                $('.open-eye').on('click', function() {
                    $(this).hide();
                    $('.closed-eye').show();
                });
            });
        </script>
        <script>
        $(document).ready(function(){       
        $('.showPass').on('click', function(){
            var passInput=$("#password");
            if(passInput.attr('type')==='password')
            {
                passInput.attr('type','text');
            }else{
                passInput.attr('type','password');
            }
        })
        })
        </script>
<script>

$(document).ready(function() {
        $("#back-button").click(function() {
            window.location.href = '<?= $global_link; ?>'
        });
    });
   
    </script>
 <script type="text/javascript" src="../assets/js/api.js"></script>
 <script type="text/javascript" src="../assets/js/business_registration.js"></script>

</html>