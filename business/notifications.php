<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Notifications</title>



    <!-- Bootstrap core CSS -->

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- <script src="https://c-dnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">

</head>
<?php include('common/side_menu.php'); ?>

<div class="right_col dashboard-page" role="main" id="payment-history">
    <!--********** Breadcrumb Start ***********-->
    <div class="page-title">
        <a href="#"> Notifications</a>
        <!-- <ul class="breadcrumb">
                    <li><a href="#">Account Setting</a></li>
                    <li class="active">Payment History</li>
                </ul> -->
    </div>

    <!--**********  Breadcrumb End ***********-->

    <div class="earning-paymen-wrapper no-data-details m-4">
        <div class="history-earing-list-wrapper earing-list">
        </div>

        <div class="page-selection"></div>
    </div>
    <!-- /page content -->
</div>


</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

<script src="../assets/js/pagination.js"></script>
<script src="../js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../assets/js/api.js"></script>

<script src="../js/custom.js"></script>

<!-- <script type="text/javascript" src="../assets/js/api.js"></script> -->
<!-- <script type="text/javascript" src="../assets/js/Orders.js"></script> -->

<!-- <script src="../js/custom.js"></script> -->
<script>
    $(document).ready(function() {
        var page = 1;
        var limit = 12;
        getNotifications();

        function getNotifications() {
            $('.loader').removeClass('d-none');
            $.ajax({
                url: `<?php echo API_URI_PATH; ?>/business/notifications/listing?page=${page}&limit=${limit}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("accesstoken")}`,
                },
                type: "get",
                success: function(data) {
                    if (data.total_results > limit) {
                        let pagination = Pagination(data.total_results, limit, data.current_page);
                        $('.page-selection').html(pagination);
                    } else {
                        $('.page-selection').html('');
                    }
                    setNotification(data.data);
                },
                error: function(request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

        function setNotification(notifications) {
            console.log(notifications, 'notifications');
            let appenddata = "";
            $('.earning-list').addClass('w-100')
            if (notifications.length > 0) {
                for (let index = 0; index < notifications.length; index++) {

                    const notification = notifications[index];
                    console.log(notification);
                    let description = notification.description;
                    if (!notification.description) {
                        description = notification.message;
                    }
                    appenddata += `<div class="earning-item-wrapper notification-item" data-id="${notification.id}" data-request_id="${notification.request_id}">
                        <div class="item">
                            <div class="status-wrapper">
                                <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/bell.svg" alt="" />
                                   ${!notification.is_read ? '<span class="badge bg-yellow"></span>' : ''} 
                                </span>
                                <span>
                                    ${description}
                                </span>
                            </div>
                            <div class="date-wrapper">
                                <span>${moment(notification.created_at).format('DD-MM-YYYY h:mm a')}</span>
                            </div>
                        </div>
                    </div>`;
                }
            } else {
                appenddata = `<div class="card product-box-inner p-3">
                            <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                            <img src="../images/icons/empty.svg" alt="">
                                <h3>
                                You don't have any new notifications
                                </h3>
                            </div>
                        </div>`
            }
            $(".earing-list").html(appenddata);
            $('.loader').addClass('d-none');
        }
        $(document).on('click', '.page-link', function(event) {
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if (pageNumber == 'prev') {
                page = page - 1;
            } else if (pageNumber == 'next') {
                page = page + 1;
            } else {
                page = Number($(this).attr('id'));
            }
            getNotifications();
        })
        $(document).on('click', '.notification-item', function(event) {
            event.preventDefault();
            $('.loader').removeClass('d-none');
            let order_id = $(this).attr('data-request_id');
            $.ajax({
                url: "<?php echo API_URI_PATH; ?>/business/notifications/mark-read",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('accesstoken')}`
                },
                dataType: "json",
                data: {
                    notification_id: $(this).attr('data-id')
                },
                type: "put",
                success: function(data) {
                    $('.loader').addClass('d-none');
                    window.location.href = "order-detail.php?order_id=" + btoa(btoa(order_id));
                },
                error: function(request, status, error) {
                    $('.loader').addClass('d-none');
                    window.location.href = "/order.php";
                },
            });
        })
    })
</script>
</body>

</html>