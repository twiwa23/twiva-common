<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">

    <title>Refer & Earn</title>
    <!-- Bootstrap CSS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/mobile-view.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
</head>

<body class="nav-md">

    <?php include('common/side_menu.php') ?>

    <!-- page content -->
    <div class="right_col add-product-page" role="main">
        <div class="page-title">Refer & Earn </div>

        <div class="refer-content-box-wrapper">
            <div class="refer-code-wrapper">
                <div class="refer-item text-center">
                    <img src="../images/icons/gift-box.svg" alt="">
                </div>
                <div class="content text-center">
                    <p>
                        Get rewards for your referrals.
                    </p>
                </div>
                <div class="refer-code">
                    <span class="d-none">I trust you are well. I am using Twiva app, and I thought you would really like it. You can use this code:</span> <h5 id='reffer_code' class="text-uppercase"></h5><span class="d-none"> to sign up after you download the app. You can download the app here: https://play.google.com/store/apps/details?id=com.twiva.ke</span>
                </div>
                <div class="text-center">
                    <button class="refer-code-copy-btn" onclick="CopyCode();">
                        Tap to copy
                    </button>
                </div>
                <div class="refer-note mt-4">
                    <p class="text-center">Earn 2000 points for every business that you refer</p>
                    <p class="text-center">Earn 20 points for every influencer you refer</p>
                    <p class="text-center"> 2000 points = 1000 Ksh </p>
                </div>
                <div class="wait-c">
                    <p>
                        But wait, there's more...
                    </p>
                </div>
                <div class="refer-note">
                    <p class="text-center"> There's always points waiting to be collected. </p>
                    <p class="text-center">
                        Check back often to see new promotions.
                    </p>
                </div>

                <div class="total-point">
                    <p class="text-center"> You have </p>
                    <p class="text-center" id="referral_points">
                        0 Points
                    </p>
                </div>
                <center>
                    <h5 class="login-error alert alert-success" role="alert" id="success" style="display:none;margin-top: 20px;text-align:center;"></h5>
                    <h5 class="login-error alert alert-danger" role="alert" id="error" style="display:none; margin-top: 20px;text-align:center;"></h5>
                </center>
                <input type="hidden" id="points">
                <div class="button-sec  text-center d-flex justify-content-center text-center m-4">
                    <button type="submit" class="red-btn w-100" id="my-form">Redeem</button>
                </div>

            </div>
        </div>

    </div>
    <!-- /page content -->
    </div>
    </div>
    <script>
        $(function() {
            $("#my-form").click(function() {
                var points = $("#points").val();
                console.log(points);
                if (points <= 0) {
                    $('#error').show();
                    $('#error').append('Points should be greater than zero');
                } else {
                    var json = {
                        "points": points
                    }
                    redeem(json);

                    function redeem(json) {
                        __ajax_httpproduct("influencer/redeem-referral", json, headers(), AJAX_CONF.apiType.POST, "", __success_product);
                    }

                    function __success_product(response) {
                        $('#success').show();
                        $('#success').append(response.message);
                    }
                }
            });
        });
    </script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/Orders.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>

    <script>
        $("#reffer_code").html(STORAGE.get(STORAGE.referal_code));
        if (STORAGE.get(STORAGE.points)) {
            $('#referral_points').html(STORAGE.get(STORAGE.points) + '  Points');
            $("#points").val(STORAGE.get(STORAGE.points));
        }
        // function CopyCode(){
        //     var temp = $("<input>");
        //     $("body").append(temp);
        //     temp.val($('#reffer_code').val()).select();
        //     let _this = $(this);
        //     document.execCommand("copy");
        //     _this.text('Copied!').addClass('text-success')
        // }
        $(document).ready(function() {
                    $('.refer-code-copy-btn').on('click', function() {
                        var temp = $("<input>");
                        $("body").append(temp);
                        temp.val($('.refer-code').text()).select();
                        let _this = $(this);
                        document.execCommand("copy");
                        _this.text('Copied!').addClass('text-success');
                    });
                });
    </script>
<script src="../../assets/js/influencer_filter.js"></script>
</body>

</html>