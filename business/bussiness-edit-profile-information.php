<?php include('../twiva-config.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="../images/icons/fav.png" type="image/x-icon">

    <title>Profile Information</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="../css/animate.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />

    <!-- Custom styling plus plugins -->
    <link href="../css/custom.css" rel="stylesheet" />
    <link href="../css/style.css" rel="stylesheet" />
    <!--Mobile View Styling-->
    <link href="../css/mobile-view.css" rel="stylesheet" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body class="nav-md" onload="login_check()">
    <header>
        <div class="logo logo-toggle" id="nav-click-old" data-url="/landing">
            <div class="nav toggle">
                <a id="menu_toggle" class="d-flex">
                    <img src="../images/icons/Frame 5.svg" alt="Toggle_Menu">
                </a>
            </div>

            <a href="">
                <img src="../images/logo/logo-white.svg" />
            </a>
        </div>

        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <!-- <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div> -->

                    <ul class="nav navbar-nav navbar-right">
                        <li class="" id="userprofile">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="" id="user-image" alt="" /> <span id="user-name" style="color: white;"></span>
                                <!-- <span class=" fa fa-angle-down"></span> -->
                            </a>

                            <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                <!-- <li>
                                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-edit-profile.php">Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li> -->
                                <li class="header-company-name-wrapper">
                                    <p class="company-name" id="nname">Biffco Enterprises Ltd.</p>
                                    <p class="email" id="eemail">nevaeh.simmons@example.com</p>
                                </li>
                                <li><a onclick="logout_check()" id="logout-btn">Log Out</a></li>
                            </ul>
                        </li>

                        <li role="presentation" class="dropdown">
                            <!-- <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> -->
                            <a href="notifications.php" class="info-number" aria-expanded="false">
                                <!-- <i class="fa fa-envelope-o"></i> -->
                                <img src="../images/icons/bell-icon.svg" />
                                <span class="badge bg-yellow" id="notification_count">0</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu"></ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div id="step1">
        <div class="container-fluid account-main-info-wrapper">
            <div class="back-link full-width-back-btn profile-info-back-btn">
                <a href="bussiness-profile-information.php" class="page-back-btn"> <i class="fa fa-chevron-left"></i> Profile Information</a>
            </div>

            <div class="container">
                <div class="account-setup">
                    <div class="account-cont">
                        <h4>Edit Basic Information</h4>

                        <div class="browse-pic">
                            <div class="drop-zone" id="image"></div>
                            <h5 id="avtar-error" class="empty-field-error"></h5>
                            <!-- <h3>Biffco Enterprises Ltd.</h3> -->
                        </div>
                        <div class="account-form">
                            <input type="hidden" id="logo_name">
                            <div class="form-field">
                                <label>Business Name</label>
                                <input type="text" id="business_name" placeholder="" />
                                <h5 id="business_name-error" class="empty-field-error"></h5>
                            </div>

                            <div class="form-field">
                                <label>Contact Person Name</label>
                                <input type="text" id="contact_name" placeholder="" />
                                <h5 id="contact_name-error" class="empty-field-error"></h5>
                            </div>

                            <div class="form-field">
                                <label>E-mail Address</label>
                                <input type="text" id="comp_email" placeholder="" />
                                <h5 id="comp_email-error" class="empty-field-error"></h5>
                            </div>

                            <div class="form-field">
                                <label>Business Phone Number</label>
                                <input type="number" id="business_number" placeholder="" />
                                <h5 id="business_number-error" class="empty-field-error"></h5>
                            </div>

                            <div class="form-field">
                                <label>Description</label>
                                <textarea id="description" minlength="20" maxlength="250"> </textarea>
                                <h5 id="description-error" class="empty-field-error"></h5>
                            </div>
                        </div>

                        <div class="button-sec right-align-btn">
                            <button onclick="step()">Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="step2" style="display: none;">
        <div class="container-fluid account-main-info-wrapper">
            <div class="back-link full-width-back-btn">
                <a onclick="back()" class="page-back-btn"> <i class="fa fa-chevron-left"></i> Profile Information</a>
            </div>

            <div class="container edit-address">
                <div class="account-setup">
                    <div class="account-cont">
                        <h4>Edit Address</h4>

                        <div class="account-form">
                            <div class="form-field">
                                <label>Country</label>
                                <select id="country" onchange="state1()"> </select>
                            </div>

                            <div class="form-field">
                                <label>County</label>
                                <select id="state" onchange="city1()"> </select>
                            </div>

                            <div class="form-field">
                                <label>City</label>
                                <select id="city"> </select>
                            </div>

                            <div class="form-field">
                                <label>Locality / Area / Street</label>
                                <input type="text" id="area" value="" placeholder="" />
                            </div>

                            <div class="form-field">
                                <label>Building Number</label>
                                <input type="text" id="building" placeholder="" />
                            </div>
                        </div>

                        <div class="button-sec right-align-btn">
                            <button onclick="update_profile()">Update profile</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="wait" style="display: flex;
    position: fixed;
    top: 0%;
    left: 0%;
    align-items: center;
    justify-content: center;
    z-index: 9999999;
    width: 100%;
    height: 100%;
    background: #ffffffa3;"><img src='../images/loader.gif' width="100px" /></div>
            <script>
                    $(document).ready(function () {
                        $(document).ajaxStart(function(){
                            $("#wait").css("display", "flex");
                          });
                          $(document).ajaxComplete(function(){
                            $("#wait").css("display", "none");
                          });
                        getNotifications();
                        getNotificationCount();
                    });
            </script>
    <script type="text/javascript" src="../assets/js/api.js"></script>
    <script type="text/javascript" src="../assets/js/business_accountsetup.js"></script>

    <script type="text/javascript" src="../assets/js/business_profile.js"></script>
    <script>
        $(function() {
            country();
            profile_business();
        });

        function step() {
            if (!isFormValid()) {
                return false;
            }
            profile_details();
            $("#country").val(country_id);
            $("#state").val(state_id);
            $("#city").val(city_id);
            $("#step1").hide(), $("#step2").show();
        }

        function back() {
            $("#step2").hide(), $("#step1").show();
        }
        /**
         * @isFormValid - Form validation
         */
        function isFormValid() {
            let isValid = true;
            var profile_image = $("#logo_name").val();
            var business_name = $("#business_name").val();
            var contact_name = $("#contact_name").val();
            var comp_email = $("#comp_email").val();
            var business_number = $("#business_number").val();
            var description = $("#description").val();
            if (profile_image == "") {
                isValid = false;
                $("#avtar-error").show();
                $("#avtar-error").html("Please select your profile picture");
                $("#avtar-error").css("color", "red");
            } else {
                $("#avtar-error").hide();
            }
            if (business_name == "") {
                isValid = false;
                $("#business_name-error").show();
                $("#business_name-error").html("Please enter your name");
                $("#business_name-error").css("color", "red");
            } else {
                $("#business_name-error").hide();
            }
            if (contact_name == "") {
                isValid = false;
                $("#contact_name-error").show();
                $("#contact_name-error").html("Please enter your contact name");
                $("#contact_name-error").css("color", "red");
            } else {
                $("#contact_name-error").hide();
            }
            if (comp_email == "") {
                isValid = false;
                $("#comp_email-error").show();
                $("#comp_email-error").html("Please enter your email");
                $("#comp_email-error").css("color", "red");
            } else {
                $("#comp_email-error").hide();
            }
            if (business_number == "") {
                isValid = false;
                $("#business_number-error").show();
                $("#business_number-error").html("Please enter your business number");
                $("#business_number-error").css("color", "red");
            } else {
                $("#business_number-error").hide();
            }
            if (description == "") {
                isValid = false;
                $("#description-error").show();
                $("#description-error").html("Please enter your description");
                $("#description-error").css("color", "red");
            } else {
                $("#description-error").hide();
            }
            return isValid;
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>

    <script>
        $(function() {
            var name = STORAGE.get(STORAGE.name);
            if (name == "" || name == "undefined" || name == "null" || name == null) {
                name = "User";
            }
            var email = STORAGE.get(STORAGE.email);
            var logo;
            logo = STORAGE.get(STORAGE.logo);
            var image;
            image = "<?= IMAGES_URI_PATH ?>/icons/profile.jpeg";

            if (logo && logo != "null" && logo != null && logo != "undefined") {
                image = "https://api.twiva.co.ke/storage/images/products/" + logo;
            }
            document.getElementById("user-image").src = image;

            document.getElementById("user-name").innerHTML = name;
            document.getElementById("nname").innerHTML = name;
            document.getElementById("eemail").innerHTML = email;
        });
        $(function() {
            if (localStorage.getItem(STORAGE.accesstoken) == null) {
                goto(UI_URL.login);
            }
            return false;
        });

        function logout_check() {
            localStorage.clear();
            login_check();
        }

        function login_check() {
            if (localStorage.getItem(STORAGE.accesstoken) == null) {
                goto(UI_URL.login);
            }
            return false;
        }

        function myprofile() {
            goto(UI_URL.onboarding10);
        }

        function error_profile_business(response) {
            console.log(response.responseJSON.message);
            if (response.responseJSON.message == "No company found") {
                let image_path = "<?php echo IMAGES_URI_PATH ?>/icons/profile.jpeg";

                $("#image").append(`
                <label class="drop-zone__prompt" for="drop-imageUpload"><img id="blah" src="${image_path}">
                    <input type="file" name="myFile" class="drop-zone__input" id="drop-imageUpload"  onchange="readURL(this);">
                </label>
            `);
            }
        }
    </script>
    <script>
        $(document).on("click", "#nav-click-old", function(e) {
            var user_id = localStorage.getItem("userId");
            var user_token = STORAGE.get(STORAGE.accesstoken);
            var nav_url = $(this).data("url");
            console.log(nav_url);
            e.preventDefault();
            if (user_id) {
                $.ajax({
                    url: "https://home.twiva.co.ke/api/v1/nav-link",
                    // url: "http://localhost:8000/api/v1/nav-link",
                    headers: {
                        Accept: "application/json",
                        // 'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
                        ["Authorization"]: "Bearer " + STORAGE.get(STORAGE.accesstoken),
                    },
                    type: "post",
                    data: {
                        user_id: user_id,
                        nav_url: nav_url,
                        user_token: user_token
                    },
                    success: function(data) {
                        console.log(data);
                        console.log(data.data.url);
                        // window.open(data.data.url);
                        // let url = data.data.url.split("?");
                        window.location.href = data.data.url;
                    },
                    error: function(data) {
                        console.log(data);
                    },
                });
            }
        });

        $(document).ready(function() {
            let user_update = atob(atob(localStorage.getItem("needToUpdatePlatform")));
            if (user_update == 1) {
                $(".profile-info-back-btn").hide();
            } else {
                $(".profile-info-back-btn").show();
            }
        });
    </script>
</body>

</html>