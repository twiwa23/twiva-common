<?php include "influencer/layouts/Auth/header.php"; ?>
    <div class="container">
        <div class="login-inner">
            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
            </div>

            <div class="login-right">
                <div class="login-section">
                    <div class="logo"><img src="images/logo/logo.svg" /></div>
                    <h3>Sign UP</h3>
                    <p>Are you a Business or an Influencer?</p>
                    <button>I’m a Business</button>
                    <a href="influencer/register.php"><button class="white-bttn">I’m an Influencer</button></a>
                </div>
            </div>
        </div>
    </div>
<?php include "influencer/layouts/Auth/footer.php"; ?>