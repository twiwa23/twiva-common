<?php require_once('./twiva-config.php'); ?>
<!DOCTYPE html>
<html lang="en" class="influencer-pages-content-wrapper">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Twiva</title>
    <link rel="shortcut icon" href="./images/icons/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous" />
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- Custom styling plus plugins -->
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/animate.min.css" rel="stylesheet">
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom.css" rel="stylesheet">
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/style.css" rel="stylesheet">
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/mobile-view.css" rel="stylesheet" />
    <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom-style.css" rel="stylesheet" />
</head>

<body class="p-0">
    <div class="login pt-5">
        <!-- <div class="back-link">
                <a href="#"> <i class="fas fa-chevron-left"></i> Sign Up</a>
            </div> -->

        <div class="container-fluid m-0">
            <div class="back-button">
                <button id="back-button"><img src="./images/icons/chevron-left-white.svg" alt="">Back</button>
            </div>
        </div>
        
        <div class="container">
            <div class="login-inner">

                <div class="login-left">
                    <!-- <img src="images/banner/login.png"> -->
                </div>
                <div class="login-right">
                    <div class="login-section">
                        <form id="login-form" method="post">
                            <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg" /></div>
                            <div class="input-field">
                                <h5 class="login-error alert alert-danger d-none" role="alert" id="errorMessage"></h5>
                                <!-- <h5 class="login-error alert alert-success" role="alert" id="emailerr">Login Successfully</h5> -->
                                <label>E-mail Address</label>
                                <input id="email" type="text" placeholder="Email" />
                                <h5 id="usercheck" class="empty-field-error"></h5>
                            </div>
                            <div class="input-field">
                                <label>Password</label>
                                <input id="password" type="password" placeholder="*******" />
                                <div class="closed-eye showPass"></div>
                                <div class="open-eye showPass"></div>
                                <h5 id="passcheck" class="empty-field-error"></h5>
                            </div>
                            <div class="forgot-pass"><a href="business/forgot-password.php">Forgot Password?</a></div>
                            <button type="submit" id="submitbbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Login</button>
                            <div class="no-account">Don’t have an account? <a href="<?php echo INFLUENCER_AUTH_URI_PATH; ?>/signup.php">Sign Up</a></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php include INFLUENCER_DIRECTORY . "/footer/footer-copyright.php"; ?>
        <?php include INFLUENCER_DIRECTORY . "/footer/footer-auth.php"; ?>
        <script type="text/javascript" src="../assets/js/api.js"></script>
        <script type="text/javascript" src="../assets/js/business_login.js"></script>
        <script>
        $(document).ready(function(){       
        $('.showPass').on('click', function(){
            var passInput=$("#password");
            if(passInput.attr('type')==='password')
            {
                passInput.attr('type','text');
            }else{
                passInput.attr('type','password');
            }
        })
        })
        </script>
        <script>
            $(document).ready(function() {
                function isFormValid() {
                    let isValid = true;
                    let email = $("#email").val();
                    let password = $("#password").val();

                    if (email == "") {
                        isValid = false;
                        $("#usercheck").show();
                        $("#usercheck").html("Please Enter your email");
                        $("#usercheck").css("color", "red");
                    } else {
                        if (!ValidateEmail(email)) {
                            isValid = false;
                            $("#usercheck").show();
                            $("#usercheck").html("Please Enter a valid email");
                            $("#usercheck").css("color", "red");
                        } else {
                            $("#usercheck").hide();
                        }
                    }
                    if (password == "") {
                        isValid = false;
                        $("#passcheck").show();
                        $("#passcheck").html("Please Enter your password");
                        $("#passcheck").css("color", "red");
                    } else {
                        $("#passcheck").hide();
                    }
                    return isValid;
                }

                function ValidateEmail(mail) {
                    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)) {
                        return true;
                    }
                    return false
                }
                $("#submitbbtn").click(function(event) {
                    event.preventDefault();
                    let _this = $(this);
                    if (!isFormValid()) {
                        return false;
                    }
                    // Loader Start
                    _this.attr("disabled", true);
                    _this.find("i").removeClass("d-none");
                    $("#errorMessage").addClass('d-none');
                    var email = $("#email").val();
                    var password = $("#password").val();
                    // Loader End
                    $.ajax({
                        url: "<?php echo API_URI_PATH; ?>/login/influencer",
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                        },
                        type: "post",
                        data: {
                            email: email,
                            password: password,
                            device_type: "3",
                            device_token: "web_token"
                        },
                        success: function(data) {
                            _this.attr("disabled", false);
                            _this.find("i").addClass("d-none");
                            if (data.status == true) {
                                if (!data.data && data.otp) {
                                    localStorage.setItem("email", data.email);
                                    window.location.href = "<?php echo INFLUENCER_AUTH_URI_PATH; ?>/otp_verify.php";
                                }
                                if(data.is_verified==0 && data.old_data.type==1){
                                    localStorage.setItem("email", data.email);
                                    window.location.href = "<?php echo BUSINESS_AUTH_URI_PATH; ?>/verify_emailotp.php"; 
                                }
                                if (!data.data && data.is_verified==0) {
                                    localStorage.setItem("email", data.email);
                                    window.location.href = "<?php echo INFLUENCER_AUTH_URI_PATH; ?>/otp_verify.php";
                                }
                                if (data.data) {
                                    if (data.data.type == 2) {
                                        localStorage.setItem('_userInfo', JSON.stringify(data.data));
                                        localStorage.setItem('_userToken', data.token);
                                        if (data.needToUpdatePlatform == 1) {
                                            window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-basic-information.php`;
                                        }else{
                                        window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-dashboard.php`;
                                        }
                                    }
                                    if (data.data.type == 1) {
                                        __save_identity(data);
                                        STORAGE.set(STORAGE.needToUpdatePlatform, btoa(btoa(data.needToUpdatePlatform)));

                                        if (data.needToUpdatePlatform == 1) {
                                            window.location.href = `business/business-edit-profile-information.php`;
                                        } else {
                                            window.location.href = `business/dashboard-2.php`
                                        }
                                        //localStorage.setItem('_userInfo', JSON.stringify(data.data));
                                        // localStorage.setItem('_userToken', data.token);
                                    }
                                }
                            }
                            // if (data.status == true && data.data.type == 2) {
                            //     localStorage.setItem('_userInfo', JSON.stringify(data.data));
                            //     localStorage.setItem('_userToken', data.token);
                            //     window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-dashboard.php`;
                            // }
                            // else if(data.status == true && data.data.type == 1) {
                            //     __save_identity(data);
                            //     //localStorage.setItem('_userInfo', JSON.stringify(data.data));
                            //    // localStorage.setItem('_userToken', data.token);
                            //     window.location.href = `business/dashboard-2.php`
                            // }
                        },
                        error: function(request, status, error) {
                            console.log(request.responseJSON);
                            if (request.responseJSON.message == 'The selected email is invalid.') {
                                request.responseJSON.message = "Invalid Email";
                            }

                            if (request.responseJSON.error) {
                                request.responseJSON.message = request.responseJSON.error;
                            }
                            _this.attr("disabled", false);
                            _this.find("i").addClass("d-none");
                            $("#errorMessage").removeClass('d-none');
                            $("#errorMessage").html(request.responseJSON.message);
                            $("#errorMessage").css("color", "red");
                        },
                    });
                })
            })
            $(document).ready(function() {
                $("#back-button").click(function() {
                    window.location.href = '<?= $global_link; ?>'
                });
            });
        </script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>

        <script>
            // Show Password 
            $(document).ready(function() {
                $('.closed-eye').on('click', function() {
                    $(this).hide();
                    $('.open-eye').show();
                });

                $('.open-eye').on('click', function() {
                    $(this).hide();
                    $('.closed-eye').show();
                });
            });
        </script>
</body>

</html>