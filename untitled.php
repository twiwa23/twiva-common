<?php require_once('./twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-auth.php"; ?>
<div class="container">
    <div class="login-inner">

        <div class="login-left">
            <!-- <img src="images/banner/login.png"> -->
        </div>
        <div class="login-right">
            <div class="login-section">
                <form id="login-form" method="post" action="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php">
                    <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg" /></div>
                    <div class="input-field">
                        <h5 class="login-error alert alert-danger d-none" role="alert" id="errorMessage"></h5>
                        <!-- <h5 class="login-error alert alert-success" role="alert" id="emailerr">Login Successfully</h5> -->
                        <label>E-mail Address</label>
                        <input id="email" type="text" placeholder="Email" />
                        <h5 id="usercheck" class="empty-field-error"></h5>
                    </div>
                    <div class="input-field">
                        <label>Password</label>
                        <input id="password" type="password" placeholder="*******" />
                        <h5 id="passcheck" class="empty-field-error"></h5>
                    </div>
                    <div class="forgot-pass"><a href="forgot-password.html">Forgot Password?</a></div>
                    <button type="submit" id="submitbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Login</button>
                    <div class="no-account">Don’t have an account? <a href="<?php echo INFLUENCER_AUTH_URI_PATH ; ?>/signup.php">Sign Up</a></div>
                </form>

      
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-copyright.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
<script type="text/javascript" src="/assets/js/api.js"></script>
 <script type="text/javascript" src="/assets/js/business_login.js"></script>

<script>
    $(document).ready(function () {
        function isFormValid() {
            let isValid = true;
            let email = $("#email").val();
            let password = $("#password").val();

            if (email == "") {
                isValid = false;
                $("#usercheck").show();
                $("#usercheck").html("Please Enter your email");
                $("#usercheck").css("color", "red");
            } else {
                $("#usercheck").hide();
            }
            if (password == "") {
                isValid = false;
                $("#passcheck").show();
                $("#passcheck").html("Please Enter your password");
                $("#passcheck").css("color", "red");
            } else {
                $("#passcheck").hide();
            }
            return isValid;
        }
        $("#submitbtn").click( function(event) {
            event.preventDefault();
            let _this = $(this);
            if(!isFormValid()){
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            $("#errorMessage").addClass('d-none');
            var email = $("#email").val();
            var password = $("#password").val();
            // Loader End
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/login/influencer",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                type: "post",
                data: { 
                    email: email,
                    password: password,
                    device_type: "3",
                    device_token: "web_token"
                },
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true && data.data.type == 2) {
                        localStorage.setItem('_userInfo', JSON.stringify(data.data));
                        localStorage.setItem('_userToken', data.token);
                        window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php`
                    }
                    else if(data.status == true && data.data.type == 1) {
                        __save_identity(response);
                        //localStorage.setItem('_userInfo', JSON.stringify(data.data));
                       // localStorage.setItem('_userToken', data.token);
                        window.location.href = `business/dashboard-2.php`
                    }
                },
                error: function (request, status, error) {
                    console.log(request.responseJSON);
                    if(request.responseJSON.message=='The selected email is invalid.'){
                        request.responseJSON.message = "Invalid Email";
                    }

                    if(request.responseJSON.error){
                        request.responseJSON.message = request.responseJSON.error;
                    }
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#errorMessage").removeClass('d-none');
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                },
            });
        })
    })
</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>

