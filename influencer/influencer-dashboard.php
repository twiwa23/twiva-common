
    <?php include 'header.php';?>
    
    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Menu-->
            <div class="left_col">
                <div class="scroll-view">
                    <div class="clearfix"></div>

                    <!-- Sidebar Menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <button class="menu-close-button"><img src="../images/icons/x.svg" alt=""></button>
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="../images/icons/img.jpg" alt="">  Devon Lane
                            <!-- <span class=" fa fa-angle-down"></span> -->
                        </a>
                        <div class="menu_section">
                            <h4>Menu</h4>
                            <ul class="nav side-menu">
                                <li><a href="dashboard.html"><img
                                            src="../images/icons/dashboard.svg">Dashboard<span
                                            class="fa fa-chevron-down"></span></a>
                                </li>
                                <li><a href="my-catalog.html"><img src="../images/icons/catalog.svg"> My Catalog <span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="order.html"><img src="../images/icons/order.svg"> Order Received<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="best-performing-product.html"><img src="../images/icons/reports.svg">Reports<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="seller-orders.html"><img src="../images/icons/settings.svg"> Account Setting<span class="fa fa-chevron-down"></span></a></li>
                                <li><a href="seller-add-product.html"><img src="../images/icons/refer.svg"> Refer & Earn<span class="fa fa-chevron-down"></span></a> </li>
                              </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                </div>
            </div>

            

            <!-- Page Content -->
            <div class="right_col add-product-page">
                <div class="page-title">Dashboard </div>
                <div class="dashboard-inner">
                    <div class="dashboard-card-outr">

                        <div class="dashboard-card">
                            <h3><img src="../images/icons/noun_Money Bag_3762816 1.svg">Earnings</h3>
                            <div class="card-flex">
                                <span>Total Earning</span>
                                <h3>$ 450 .00</h3>
                            </div>
                            <div class="card-flex">
                                <span>Earning This Month</span>
                                <h3>$ 450 .00</h3>
                            </div>
                        </div>

                        <div class="dashboard-card">
                            <h3><img src="../images/icons/noun_Store_2954518 1.svg">My eShop</h3>
                            <div class="card-flex">
                                <!-- <h3>120 Products</h3> -->
                            </div>
                            <div class="common-button">
                                <button class="purple-btn">Add Product</button>
                                <button class="white-bttn">View eShop</button>
                            </div>
                        </div>

                        <div class="dashboard-card" id="card-social">
                            <img src="../images/icons/noun_link_1655590 1.svg">
                            <div class="card-flex">
                                <p>Social Selling</p>
                                <h3>Not Connected</h3>
                            </div>
                            <div class="common-button full-button">
                                <a href="#">Connect Social Accounts 
                                    <span><img src="../images/icons/chevron-right.svg" alt=""></span>
                                </a>
                            </div>
                        </div>

                        
                    </div>
                
                    <!--Product Section-->
                    <div class="product-section">
                        <div class="product-box row">
                            <div class="col-12">
                                <h2>Top 5 Selling Product</h2>                                
                            </div>
                            <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="../images/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>$22.99</h2>
                                        <span>No of Sale: 14</span>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="../images/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>


                            <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="../images/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>$22.99</h2>
                                        <span>No of Sale: 14</span>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="../images/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>


                            <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="../images/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>$22.99</h2>
                                        <span>No of Sale: 14</span>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="../images/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="../images/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>$22.99</h2>
                                        <span>No of Sale: 14</span>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="../images/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="../images/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>$22.99</h2>
                                        <span>No of Sale: 14</span>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="../images/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="product-box">
                            <h2>Recent Orders</h2>
                            
                            <div class="product-box-inner">
                                <img src="../images/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="../images/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="../images/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="../images/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="../images/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>
    <?php include 'footer.php';?>

   