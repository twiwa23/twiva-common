<!DOCTYPE html>
<html lang="en" class="influencer-pages-content-wrapper">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="<?php echo IMAGES_URI_PATH; ?>/icons/fav.png" type="image/x-icon">
        <title>Dashboard</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/animate.min.css" rel="stylesheet" />
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
        <!-- <link href="<?php echo STYLESHEET_URI_PATH; ?>/splide.min.css" rel="stylesheet" /> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" crossorigin="anonymous" />
        <!-- Select 2 -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css"/>
        <!-- Select 2 -->
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/mobile-view.css" rel="stylesheet">
        <!-- Custom styling plus plugins -->
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/starrr.css" rel="stylesheet" />
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom.css" rel="stylesheet" />
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/style.css" rel="stylesheet" />
        <link href="<?php echo STYLESHEET_URI_PATH; ?>/custom-style.css" rel="stylesheet" />
    </head>

    <body class="dashboard-page-wrapper influencer-dashboard-page nav-md <?php if(isset($class)){ echo $class; } ?> influencer-page-content-wrapper">
        
        <div class="loader d-none">
            <i class="fa fa-spinner fa-spin"></i>
        </div>
        <!--Main Header Start-->
        <header>
            <!--Main Header Logo-->
            <div class="logo">
                <div class="nav toggle">
                    <a id="menu_toggle" href="#" class="d-flex">
                        <img  src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 5.svg" alt="Toggle_Menu">
                    </a>
                </div>
                <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo-white.svg" /></a>
            </div>
            <!--Main Header Top Nav Menu-->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <!--Nav Link-->
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-expanded="false">
                                  <div class="user-icon-profile-wrapper"> <img class="user-profile-pic" src="<?php echo IMAGES_URI_PATH; ?>/icons/profile.jpeg" alt="" /> </div>
                                    <span class="user-name"></span>
                                    <!-- <span class=" fa fa-angle-down"></span> -->
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <!-- <li>
                                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-edit-profile.php">Profile</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li> -->
                                    <li class="header-company-name-wrapper">
                                        <p class="company-name"></p>
                                        <p class="email"></p>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" id="logout-btn">Log Out</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-notifications.php" class="info-number notification-bell" >
                                    <!-- <i class="fa fa-envelope-o"></i> -->
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/bell.svg" />
                                </a>
                            </li>

                            <li class="message-wrapper">
                                <a href="#"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/message-square.svg"></a>
                            </li>

                            <!-- <li>
                                <a href="#">
                                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/search-outline.svg" alt="Search" /></span>
                                </a>
                            </li> -->
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <!--Main Header End-->

        <!--Mobile Backdrop-->
        <div class="backdrop"></div>