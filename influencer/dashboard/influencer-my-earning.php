<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>

<!--Main Section Start-->
<div class="" id="main-earning-section">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- Page Content -->
        <div class="right_col add-product-page">
            
            <!--********** Breadcrumb Start ***********-->
            <div class="breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#">Account Settings</a></li>
                    <li class="active">My Earnings</li>
                </ul>
            </div>

            <!--**********  Breadcrumb End ***********-->
            <div class="dashboard-inner">
                <!--Product Section-->
                <div class="product-section">
                    <div class="product-box row">
                        <div class="col-12 total-earning-heading">
                            <h1 class="total-earning">0.00</h1>
                            <h2>Total Earning</h2>
                        </div>
                        <div class="earning-list"></div>
                    </div>
                    <div class="page-selection"></div>
                    <!-- <div class="product-box">
                            <h2>Recent Orders</h2>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>$22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                        </div> -->
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        var page = 1;
        var limit = 12;
        getEarning();
        function getEarning() {
            $('.loader').removeClass('d-none');
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/influencer/earning-history?page=${page}&limit=${limit}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    $('.total-earning').text(`KSH ${((data.total_earnings*7)/100).toFixed(2)}`)
                    if(data.total_results > limit){
                        let pagination = Pagination(data.total_results, limit, data.current_page);
                        $('.page-selection').html(pagination);
                    }else{
                        $('.page-selection').html('');
                    }
                    setEarning(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }
        function setEarning(earnings) {
            let appenddata = "";
            $('.earning-list').addClass('w-100')
            if(earnings.length > 0){
                for (let index = 0; index < earnings.length; index++) {
                    const product = earnings[index];
                    const productImage = product.product_images.find((obj) => obj.is_cover_pic == 1);
                    const image = productImage ? productImage.image_path : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                    appenddata += `<div class="product-box-inner col-12 col-sm-6 col-md-4">
                                        <div class="product-box-content">
                                            <img src="<?php echo $image_base; ?>/${image}" />
                                            <div class="box-content">
                                                <p style="height:unset !important;">${product.prduct_details.product_title}</p>
                                                <h2>KSH ${product.prduct_details.product_price}</h2>
                                                <span class="commission-label">Comission amount: <span class="commission-amount">KSH ${((product.prduct_details.product_price * 7)/100).toFixed(2)}</span></span>
                                            </div>`
                                            if(product.rating>0){
                                            appenddata += ` <div class="product-rating">
                                                <span class="rating-title">${product.rating ? product.rating.toFixed(1) : '0.0'}</span>
                                                <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                            </div>`
                                            }
                                            appenddata += `</div>
                                    </div>`;
                }
            }else{
                appenddata = `<div class="card product-box-inner p-3 not-found text-center d-block h-100">
                            <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                            <img src="../../images/icons/empty.svg" alt="">
                                <h3>
                               
                                You don’t have any sales at this moment 
                                </h3>
                            </div>
                        </div>`
            }
            
            $(".earning-list").html(appenddata);
            $('.loader').addClass('d-none');
        }
        $(document).on('click', '.page-link', function(event){
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if(pageNumber == 'prev'){
                page = page - 1;
            }else if(pageNumber == 'next'){
                page = page + 1;
            }else{
                page = Number($(this).attr('id'));
            }
            getEarning();
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
