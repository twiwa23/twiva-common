<?php $class = "influencer-eshop-page"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">My eShop</div>
            <div class="dashboard-inner">
                <div class="loader text-center w-100 d-none">
                    <h1><i class="fa fa-spinner fa-spin"></i></h1>
                </div>
                <!--Product Banner-->
                <div class="page-inner-section product-banner jumbotron pt-0">
                    <img class="banner-image" src="<?php echo IMAGES_URI_PATH; ?>/product-img/1banner.png" alt="Banner" />
                    <div class="banner-section">
                        <div class="banner-eshop-logo-wrapper">
                           <img class="logo-image" src="<?php echo IMAGES_URI_PATH; ?>/icons/profile.jpeg" alt="" />
                        </div>
                        
                        <div class="banner-content">
                            <h3 class="eShop-title"></h3>
                            <p class="eShop-desc">
                                
                            </p>
                            <div class="common-button">
                                <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/add-eShop-products.php"><button class="white-bttn">Add Products</button></a>
                                <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/edit-eShop.php"><button class="white-bttn">Edit eShop</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-success connect-now-sec">
                        <div class="">
                        <p class="alert-content w-100 pr-4">
                        Share the products directly to your Facebook and Instagram shops. Follow a few simple steps to connect
                        </p>
                        </div>
                        <div class="">
                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-connect-now.php"><button class="alert-button m-0">Connect Now</button></a>

                        </div>
                       
                    </div>
                </div>

                <!--Product Section-->
                <div class="page-inner-section product-section custom-d">
                    <div class="product-box row product-list influencer-eshop"></div>

                    <!-- <div class="product-box">
                            <h2>Recent Orders</h2>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                        </div> -->
                </div>
            </div>
            <div class="page-selection"></div>
        </div>
        <!-- /page content -->
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        localStorage.removeItem('selectedProduct')
        var shopConnectNow = localStorage.getItem('_shopConnectNow')
        if(shopConnectNow && shopConnectNow == 1){
            // $('.connect-now-sec').remove();
        }
        var allProducts;
        var allSelectedProducts = [];
        var shopId = '';
        var page = 1;
        var limit = 12;
        geteShopDetail();
        /**
        * Get eShop Detail from api
        */
        function geteShopDetail() {
            $('.page-inner-section').addClass('d-none');
            $('.loader').removeClass('d-none');
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/shops/list",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    setShopData(data.data[0]);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }


        /**
        * Set data of eShop
        */
        function setShopData(eShopData) {
            shopId = eShopData.id;
            let coverImage = `<?php echo $image_base; ?>/${eShopData.shop_cover_image_path}`
            let logoImage = `<?php echo $image_base; ?>/${eShopData.shop_logo_path}`
            $('.banner-image').attr('src',coverImage)
            $('.logo-image').attr('src',logoImage)
            $('.eShop-title').text(eShopData.shop_name)
            $('.eShop-desc').text(eShopData.shop_description)
            $('.page-inner-section').removeClass('d-none')
            $('.loader').addClass('d-none');
            getProductList();
        }

        /**
        * Get product from api
         */
        function getProductList() {
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/influencer/shops/view/products/${shopId}?page=${page}&limit=${limit}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    if(data.shop_product_details.total > limit){
                        let pagination = Pagination(data.shop_product_details.total, limit,data.shop_product_details.current_page);
                        $('.page-selection').html(pagination);
                    }else{
                        $('.page-selection').html('');
                    }
                    setProductsListData(data.shop_product_details.data);
                },
                error: function (request, status, error) {
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

        /**
        * Set products to product list 
        */
        function setProductsListData(products) {
            allProducts = products;
            let appenddata = "";
            for (let index = 0; index < allProducts.length; index++) {
                const product = allProducts[index];
                const productImage = product.influencer_product_images.find((obj) => obj.is_cover_pic == 1);
                const image = productImage ? productImage.image_path : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                appenddata += `<div class="product-box-inner col-12 col-sm-6 col-md-4 col-lg-3">
                           
                                <div class="product-box-content">
                                <div class="eshop-product-thum-wrapper">
                                    <img class="thumb" src="<?php echo $image_base; ?>/${image}" height="216"/>
                                   </div>
                                    <div class="box-content">
                                        <p>${product.product_title}</p>
                                        <h2>KSH ${product.product_price}</h2>
                                        <div class="p-btn">
                                            <a  href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/eShop-product-detail.php/?request=${product.product_id}&request1=${shopId}"> Personlise </a>
                                        </div>
                                    </div>`;
                                    if(product.product_details.rating!=0){
                                        appenddata += `<div class="product-rating">
                                        <span class="rating-title">${product.product_details && product.product_details.rating ? product.product_details.rating.toFixed(1) : '0.0'}</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                    </div>`;
                                    }
                                    appenddata += `</div>
                          
                        </div>`;
            }
            $(".product-list").html(appenddata);
        }
        $(document).on('click', '.page-link', function(event){
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if(pageNumber == 'prev'){
                page = page - 1;
            }else if(pageNumber == 'next'){
                page = page + 1;
            }else{
                page = Number($(this).attr('id'));
            }
            getProductList();
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>

