<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">
                <div class="back-link">
                    <a href="#">Basic Information</a>
                </div>
            </div>
            <div class="dashboard-inner">
                <div class="account-setup" id="basic-info-content"> 
                   

                    <div class="container">
                        

                        <div class="account-cont">
                            <div class="profile-heading">
                                <a href="#">Edit Basic Information</a>
                            </div>

                            <div class="browse-pic">
                                <div class="drop-zone image-preview-sec">
                                    <label class="drop-zone__prompt" for="drop-zone__input">
                                        <img class="image-preview" src="<?php echo IMAGES_URI_PATH; ?>/card-images/browse.png" />
                                        <i class="fa fa-spinner fa-spin d-none image-loader-icon"></i>
                                        <input accept="image/*" type="file" name="myFile" class="drop-zone__input" id="drop-zone__input" />
                                        <!-- uploded image preview  -->
                                        <div class="user-profile-preview-wrapper d-none">
                                            <img src="" alt="" />
                                        </div>
                                    </label>
                                </div>
                                <h5 id="avtar-error" class="empty-field-error"></h5>
                            </div>

                            <div class="account-form">
                                
                                <div class="form-field">
                                    <label>Full Name</label>
                                    <input type="text" placeholder="Ronald Richards" id="fullName" required/>
                                    <h5 id="fullName-error" class="empty-field-error"></h5>
                                </div>
                                <div class="form-field date-picker-field">
                                    <label>Date Of Birth</label>
                                    <input type="text" class="input-date datepicker" id="dob" autocomplete="off"/>
                                    <h5 id="dob-error" class="empty-field-error"></h5>
                                    <span id="dob-error2"></span>
                                </div>

                                <div class="form-input-field">
                                    <label for="gender" class="mb-3">Gender</label>
                                    <div class="gender-input d-flex" id="gender">
                                        <div class="mr-4">
                                            <input type="radio" id="male" name="gender" value="1" checked=""/>
                                            <label for="male">Male</label>
                                        </div>
                                        <div class="mr-4">
                                            <input type="radio" id="female" name="gender" value="2" />
                                            <label for="female">Female</label>
                                        </div>
                                        <div class="mr-4">
                                            <input type="radio" id="others" name="gender" value="3" />
                                            <label for="other">Other</label>
                                        </div>
                                    </div>
                                    <h5 id="gender-error" class="empty-field-error"></h5>
                                </div>
                                
                                <div class="form-input-field cus-select" >
                                    <div class="select-box">
                                        <label>Interests</label>
                                        <select id="interest" class="js-example-basic-multiple w-100 interest-select-box" multiple="multiple" required></select>
                                        <h5 id="interest-error" class="empty-field-error"></h5>
                                    </div>
                                </div>

                                
                                <div class="form-field">
                                    <label>Description</label>
                                    <textarea id="description" required maxlength="250"></textarea>
                                    <h5 id="description-error" class="empty-field-error"></h5>
                                </div>
                                <input type="hidden" name="avtar" id="avtar" value="" />
                            </div>

                            <span id="error"></span>
                            <div class="button-sec right-align-btn d-block">
                                <button type="submit" id="submitbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        var interestList;
        getData();
        async function getData(){
            await getInterests();
            await getProfileData();
        }
        $('.datepicker').datepicker({  maxDate: new Date()});
        function getProfileData(){
            $('.loader').removeClass('d-none');
            $('.image-preview').addClass('d-none')
            $('.user-profile-preview-wrapper').addClass('d-none');
            $('.image-loader-icon').removeClass('d-none')
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/details",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function (data) {
                    setProfileData(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->",request.responseJSON);
                },
            });
        }

        function setProfileData(userInfo){
            if(userInfo.influencer_details){
                var profile_image = $("#avtar").val(userInfo.influencer_details.profile_image);
                var name = $("#fullName").val(userInfo.influencer_details.name);
                var dob = $("#dob").val(new Date(userInfo.influencer_details.dob).toLocaleDateString("en-US"));
                if(userInfo.influencer_details.gender == 1){
                    $("#male").attr('checked', true);
                }else if(userInfo.influencer_details.gender == 2){
                    $("#female").attr('checked', true);
                }else{
                    $("#other").attr('checked', true);
                }
                var gender = $("#gender").val(userInfo.influencer_details.gender);
                var description = $("#description").val(userInfo.influencer_details.description);
                $('.geneder').text(`${userInfo.influencer_details.gender == 1 ? 'Male' : userInfo.influencer_details.gender == 2 ? 'Female' : 'Other'}`);
                let interest = [];
                let allInterest = userInfo.influencer_details.interests;
                let appenddata = "";
                for (let index = 0; index < interestList.length; index++) {
                    const element = interestList[index];
                    let isExist = allInterest.findIndex(x => x.id == element.id);
                    if(isExist == -1){
                        appenddata += `<option value="${element.id}">${element.name}</option>`;
                    }else{
                        appenddata += `<option selected="" value="${element.id}">${element.name}</option>`;
                    }
                    $(".interest-select-box").html(appenddata);
                }
                $('.interest').text(interest.join(', '));
                $('.description').text(userInfo.influencer_details.description);
                if(userInfo.influencer_details && userInfo.influencer_details.profile_image != ''){
                    let picture = `<?php echo $image_base; ?>${userInfo.influencer_details.profile_image}`
                    $('.image-preview').addClass('d-none')
                    $('.user-profile-preview-wrapper').removeClass('d-none');
                    $('.user-profile-preview-wrapper img').attr('src',picture)
                    $('.image-loader-icon').addClass('d-none')
                }
                select2Init();
                $('.loader').addClass('d-none');
            }else{
                $('.loader').addClass('d-none');
                var name = $("#fullName").val(userInfo.name);
                if(userInfo.influencer_detail){
                var dob = $("#dob").val(new Date(userInfo.influencer_detail.dob).toLocaleDateString("en-US"));
                $('.geneder').text(`${userInfo.influencer_detail.gender == 1 ? 'Male' : userInfo.influencer_detail.gender == 2 ? 'Female' : 'Other'}`);
                }
                $('.image-preview').addClass('d-block');
                $('.user-profile-preview-wrapper').removeClass('d-block');
                $('.image-loader-icon').addClass('d-none');
                let appenddata = "";
                for (let index = 0; index < interestList.length; index++) {
                    const element = interestList[index];
                    appenddata += `<option value="${element.id}">${element.name}</option>`;
                    $(".interest-select-box").html(appenddata);
                }
            select2Init();
            $('.loader').addClass('d-none');
            }
           
        }
        function getInterests(params) {
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/interests",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                type: "get",
                success: function (data) {
                    interestList = data.categories
                },
                error: function (request, status, error) {
                    let appenddata = `<option value="">No Interests</option>`;
                    $(".interest-select-box").html(appenddata);
                },
            });
            
        }
        function select2Init() {
            $('.js-example-basic-multiple').select2({
                placeholder: "Select interests",
            });
        }
        $("#drop-zone__input").change(function () {
            var file_data = $("#drop-zone__input").prop("files")[0];
            if(file_data){
                $('#submitbtn').attr('disabled', true)
                $('.image-preview').addClass('d-none')
                $('.user-profile-preview-wrapper').addClass('d-none');
                $('.image-loader-icon').removeClass('d-none')
            }
            var form_data = new FormData();
            form_data.append("image", file_data);
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/upload-image",
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: "post",
                success: function (data) {
                    let imageData = JSON.parse(data);
                    let imagePreview = "<?php echo $image_base; ?>"+imageData.image;
                    $("#avtar").val(imageData.image);
                    $('#submitbtn').attr('disabled', false)
                    $('.image-preview').addClass('d-none')
                    $('.user-profile-preview-wrapper').removeClass('d-none');
                    $('.user-profile-preview-wrapper img').attr('src',imagePreview)
                    $('.image-loader-icon').addClass('d-none')
                },
            });
        });

         /**
         * @isFormValid - Form validation
         */
        function isFormValid() {
            let isValid = true;
            var profile_image = $("#avtar").val();
            var name = $("#fullName").val();
            var dob = $("#dob").val();
            var gender = $("#gender").val();
            var gender = $('input[name="gender"]:checked').val();
            var description = $("#description").val();
            var interest = $("#interest").val()
            if (profile_image == "") {
                isValid = false;
                $("#avtar-error").show();
                $("#avtar-error").html("Please select your profile picture");
                $("#avtar-error").css("color", "red");
            } else {
                $("#avtar-error").hide();
            }
            if (name == "") {
                isValid = false;
                $("#fullName-error").show();
                $("#fullName-error").html("Please enter your full name");
                $("#fullName-error").css("color", "red");
            } else {
                $("#fullName-error").hide();
            }
            if (dob == "") {
                isValid = false;
                $("#dob-error").show();
                $("#dob-error").html("Please enter your date of birth");
                $("#dob-error").css("color", "red");
            } else {
                $("#dob-error").hide();
            }
            if (!interest.length) {
                isValid = false;
                $("#interest-error").show();
                $("#interest-error").html("Please select your interest");
                $("#interest-error").css("color", "red");
            } else {
                $("#interest-error").hide();
            }
            if (!gender || gender == "") {
                isValid = false;
                $("#gender-error").show();
                $("#gender-error").html("Please select your gender");
                $("#gender-error").css("color", "red");
            } else {
                $("#gender-error").hide();
            }
            return isValid;
        }
        $("#submitbtn").click(function () {
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            //Loader End
            var profile_image = $("#avtar").val();
            var name = $("#fullName").val();
            var dob = $("#dob").val();
            var gender = $("#gender").val();
            var gender = $('input[name="gender"]:checked').val();
            var description = $("#description").val();
            var interests = $("#interest").val().join(',');
            var param = { 
                profile_image: profile_image, 
                name: name, 
                dob: dob, 
                gender: gender, 
                description: description, 
                interests: interests
            }
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/edit/profile",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                dataType: "json",
                data: param,
                type: "post",
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-edit-profile.php";
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    console.log(request.responseJSON.error.dob);
                    $("#description-error").html(request.responseJSON.error.description).css("color", "red");
                    $("#dob-error2").html(request.responseJSON.error.dob).css("color", "red");
                    $("#gender-error").html(request.responseJSON.error.gender).css("color", "red");
                    $("#interest-error").html(request.responseJSON.error.interests).css("color", "red");
                },
            });
        });
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
