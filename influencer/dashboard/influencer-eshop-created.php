<?php $class = "bg-white"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
  <div class="account-setup"> 
        <div class="container">
            <div class="welcome-screen">
                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/welcome-icon.png">
                <h3>eShop Created</h3>
                <div class="button-sec">
                    <button class="m-0"><a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php">Continue</a></button>
                </div>
            </div>
        </div>
    </div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
