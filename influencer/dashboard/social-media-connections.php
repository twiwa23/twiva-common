
<?php require_once('../../twiva-config.php');
require_once('../../Facebook/config.php');
$helper = $facebook->getRedirectLoginHelper();
?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>

<?php
  // $permissions = array('email',"publish_pages","public_profile", "pages_read_engagement" , "pages_show_list", "manage_pages","publish_to_groups"); // Optional permissions
  $permissions = array('email',"publish_pages","public_profile", "pages_read_engagement" , "pages_show_list", "manage_pages","publish_to_groups"); // Optional permissions
    $loginUrl = $helper->getLoginUrl('http://localhost//APPARRANT/TwivaCommon/influencer/dashboard/social-media-connections.php', $permissions);
  if(isset($_GET['code'])){
      $_SESSION['FBRLH_state']=$_GET['state'];
      try {
        $accessToken = $helper->getAccessToken();
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      if (!isset($accessToken)) {
        if ($helper->getError()) {
          header('HTTP/1.0 401 Unauthorized');
          echo "Error: " . $helper->getError() . "\n";
          echo "Error Code: " . $helper->getErrorCode() . "\n";
          echo "Error Reason: " . $helper->getErrorReason() . "\n";
          echo "Error Description: " . $helper->getErrorDescription() . "\n";
        } else {
          header('HTTP/1.0 400 Bad Request');
          echo 'Bad request';
        }
        exit;
      }
      $facebook->setDefaultAccessToken($accessToken);
      $response = $facebook->get('/me?fields=id,name,email', $accessToken);

      $userDetail = $response->getGraphUser();
      // print_r($userDetail);
// $token = $helper->getAccessToken();
// print_r($token);
// $url = 'https://www.facebook.com/logout.php?next=' . 'http://localhost//APPARRANT/TwivaCommon/influencer/dashboard/social-media-connections.php' .
// '&access_token='.$token;
// session_destroy();
  }
?>

  <!--Main Section Start-->
  <div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
  </div>


    <div class="login" id="main-wrappper">

        <div class="back-link">
            <a href="#"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""> Social Media Connection</a>
        </div>
       
        <div class="account-connections" id="account-icons">
        <h5 class="text-success" id="message" style="margin-left: 10px;display:none;">Connected Successfully</h5>
            <ul class="account-list">
                <li class="account-list-item">
                    <a href="" class="disconnected">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 7135.svg" alt="">
                        <p>Twitter</p>
                    </a>
                    <p>Connect Now</p>
                </li>
                <li class="account-list-item">
                    <a href="" class="disconnected">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/facebook-app-symbol 1.svg" alt="">
                        <p>Facebook</p>
                    </a>
                    <p class="not-connect-facebook"><a href="<?php echo htmlspecialchars($loginUrl); ?>">Connect Now</a></p>
                    <p class="active connected-fb d-none">Connected</p>
                </li>
            </ul>

            <ul class="account-list">
                <li class="account-list-item">
                    <a href="" class="disconnected">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/logos_youtube-icon.svg" alt="">
                        <p>Youtube</p>
                    </a>
                    <p>Connect Now</p>
                    <!-- <p class="active">Connected</p> -->
                </li>
                <li class="account-list-item">
                    <a href="" class="disconnected">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 7131.svg" alt="">
                        <p>Instagram</p>
                    </a>
                    <p>Connect Now</p>
                    <!-- <p class="active">Connected</p> -->
                </li>

            </ul>
        </div>

        <div class="footer-login">
            Copyright © 2021 Twiva. All Rights Reserved.
            <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
        </div>
    </div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<style>
  .not-connect-facebook a{
    font-family: 'Source Sans Pro' !important;
    font-size: 16px !important;
    font-style: normal;
    font-weight: 600;
    line-height: 24px;
    color: #7A0D41 !important;
  }
</style>
<script>
      $.ajax({
        url: "<?php echo API_URI_PATH ; ?>/influencer/details",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
        },
        dataType: "json",
        type: "get",
        success: function (data) {
          console.log(data);
          if(data.status){
            let social = data.data.social_connection_details;
            let isExistFb = social.findIndex(element => element.social_network_type == 1);
            // if(isExistFb != -1){
            //   $('.not-connect-facebook').addClass('d-none')
            //   $('.connected-fb').removeClass('d-none')
            // }
          }
        }
      });
</script>
<script>
   
    $(document).ready(function () {
      
      let url = new URL(location.href);
      url.searchParams.delete('code');
      let userId = "<?php echo $userDetail['id'];?>";
      let accessToken = "<?php echo $accessToken;?>";
      if(userId){
          let post = {
              "social_network_type": "1",
              "social_id": userId,
              "request_token": accessToken,
              "details":null
          };
          $.ajax({
            url: "<?php echo API_URI_PATH ; ?>/influencer/social/login",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
            },
            dataType: "json",
            data: post,
            type: "post",
            success: function (data) {
              alert('Facebook connected successfully');
              $("#message").show();
              //window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/social-media-connections.php";
            },
            error: function (request, status, error) {
                _this.attr("disabled", false);
                $(".schedule-post").attr("disabled", false);
                _this.find("i").addClass("d-none");
            },
        });
      }
    })
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
