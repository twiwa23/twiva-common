<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="payment-history">
            <!--********** Breadcrumb Start ***********-->
            <div class="page-title breadcrumb-wrapper">
                <a href="#"> Notifications</a>
                <!-- <ul class="breadcrumb">
                    <li><a href="#">Account Setting</a></li>
                    <li class="active">Payment History</li>
                </ul> -->
            </div>

            <!--**********  Breadcrumb End ***********-->

            <div class="earning-paymen-wrapper no-data-details m-4">
                <div class="history-earing-list-wrapper earing-list">
                </div>
                
                <div class="page-selection"></div>
            </div>
            <!-- /page content -->
        </div>
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        var page = 1;
        var limit = 12;
        getNotifications();
        function getNotifications() {
            $('.loader').removeClass('d-none');
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/business/notifications/listing?page=${page}&limit=${limit}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    if(data.total_results > limit){
                        let pagination = Pagination(data.total_results, limit, data.current_page);
                        $('.page-selection').html(pagination);
                    }else{
                        $('.page-selection').html('');
                    }
                    setNotification(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }
        function setNotification(notifications) {
            console.log(notifications,'notifications');
            let appenddata = "";
            $('.earning-list').addClass('w-100')
            if(notifications.length > 0){
                for (let index = 0; index < notifications.length; index++) {
                    const notification = notifications[index];
                    appenddata += `<div class="earning-item-wrapper notification-item" data-id="${notification.id}">
                        <div class="item">
                            <div class="status-wrapper">
                                <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/bell.svg" alt="" />
                                   ${!notification.is_read ? '<span class="badge bg-yellow"></span>' : ''} 
                                </span>
                                <span>
                                    ${notification.description}
                                </span>
                            </div>
                            <div class="date-wrapper">
                                <span>${moment(notification.created_at).format('DD-MM-YYYY h:mm a')}</span>
                            </div>
                        </div>
                    </div>`;
                }
            }else{
                appenddata = `<div class="card product-box-inner p-3">
                            <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                            <img src="../../images/icons/empty.svg" alt="">
                                <h3>
                                You don't have any new notifications
                                </h3>
                            </div>
                        </div>`
            }
            $(".earing-list").html(appenddata);
            $('.loader').addClass('d-none');
        }
        $(document).on('click', '.page-link', function(event){
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if(pageNumber == 'prev'){
                page = page - 1;
            }else if(pageNumber == 'next'){
                page = page + 1;
            }else{
                page = Number($(this).attr('id'));
            }
            getNotifications();
        })
        $(document).on('click', '.notification-item', function(event){
            event.preventDefault();
            $('.loader').removeClass('d-none');
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/business/notifications/mark-read",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                dataType: "json",
                data: {
                    notification_id: $(this).attr('data-id')
                },
                type: "put",
                success: function (data) {
                    $('.loader').addClass('d-none');
                    window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-my-earning.php";
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-my-earning.php";
                },
            });
        })
    })
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
