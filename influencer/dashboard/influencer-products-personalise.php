<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="dashboard-inner">
                <!--Product Banner-->
                <div class="product-banner">
                    <!--Banner Carousel-->
                    <div id="banner-carousel" class="carousel slide" data-ride="carousel">
                        <div class="banner-details">
                            <h3>Create Shop</h3>
							<p>Step 2 of 3 
								<span class="step-dot active"></span> 
								<span class="step-dot active"></span>
								<span class="step-dot"></span> 
							</p>
                        </div>
                        <!-- Carousel Indicators -->
                    </div>
                </div>

                <!--Product Selection-->
                <div class="product-selection pr-5">
                    <div class="title">
                        <h4>Personalise Products</h4>
                        <p>Edit product details to fit your audience</p>
                    </div>
                </div>
                <!--Product Section-->
                <div class="product-section mt-2">
                    <div class="product-box row product-list">
                        <div class="product-box-inner col-6 col-md-4 col-lg-3">
                            <div class="product-box-content">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg" />
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <button class="white-bttn">Select</button>
                                </div>
                                <div class="product-rating">
                                    <span class="rating-title">5.0</span>
                                    <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
	<div class="selected-product-footer-wrapper container-fluid ">
		<div class="seleced-product-btn-wrapper"> 
				<div class="btn-wrapper">
					<div class="no-of-select-product">
						<h4 class="total-count">
							24 Product Selected
						</h4>
					</div>
					<div class="next-btn-wrapper">
                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop-branding.php">
                            <button>
                                Next Step
                            </button>
                        </a>
					</div>
				</div>	
		</div>
	</div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function () {
        var allProducts;
        var allSelectedProducts = [];
        getSelectedProductList();
        function getSelectedProductList() {
            let loader = `<div class="not-found text-center w-100">
                                <h1><i class="fa fa-spinner fa-spin"></i></h1>
                            </div>`;
            $(".product-list").html(loader);
            allSelectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            $('.total-count').text(`${allSelectedProducts.length} Product Selected`)
            setProductsListData(allSelectedProducts);
        }

        function setProductsListData(products) {
            allProducts = products;
            allSelectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            let appenddata = "";
            for (let index = 0; index < allSelectedProducts.length; index++) {
                const product = allSelectedProducts[index];
                const productImage = product.product_images.find((obj) => obj.is_cover_pic == 1);
                const image = productImage ? productImage.image : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                appenddata += `<div class="product-box-inner  col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product-box-content">
                                <a class="remove-items" href="javascript:void(0);" data-id="${product.id}"><i class="fa fa-times text-white"></i></a>
                                <img class="thumb" src="<?php echo $image_base; ?>/${image}" height="216"/>
                                <div class="box-content">
                                    <p>${product.name}</p>
                                    <h2>KSH ${product.price}</h2>
                                    <button class="white-bttn selected-product" data-id="${product.id}">Personalise</button>
                                </div>`;
                                if(product.rating!=0){
                                    appenddata += `<div class="product-rating">
                                    <span class="rating-title">${product && product.rating ? product.rating.toFixed(1) : '0.0'}</span>
                                    <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                </div>`;
                                }
                                appenddata += `</div>
                        </div>`;
            }
            $(".product-list").html(appenddata);
        }
        $(document).on('click','.remove-items',function(){
            let _this = $(this);
            let id = _this.attr('data-id');
            let index = allSelectedProducts.findIndex(product => product.id == id);
            allSelectedProducts.splice(index, 1);
            $('.total-count').text(`${allSelectedProducts.length} Product Selected`)
            localStorage.setItem('selectedProduct', JSON.stringify(allSelectedProducts));
            _this.closest('.product-box-inner').remove();
            if(allSelectedProducts.length == 0){
                window.history.back()
            }
        })
        $(document).on('click','.selected-product',function(){
            let _this = $(this);
            let id = _this.attr('data-id');
            let product = allSelectedProducts.find(product => product.id == id);
            localStorage.setItem('_productDetail', JSON.stringify(product));
            window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-personalise-product.php`;
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
