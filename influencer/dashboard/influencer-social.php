
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
  
  <!--Main Section Start-->
  <div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- Page Content -->
        <div class="right_col add-product-page" id="post">
            <div class="page-title">Dashboard <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt=""> Social Selling</div>
            <div class="dashboard-inner">
            
                <!--Product Section-->
                <div class="product-section">
                    <div class="toggle-link">
                        <ul class="nav">
                            <li class="nav-item toggle-link-active">
                              <a class="nav-link selling-tab" href="#" data-id="scheduled">Scheduled Post</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link selling-tab" href="#" data-id="history">History</a>
                            </li>
                        </ul>  
                        <div class="common-button">
                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/social-media-connections.php"><button class="white-bttn">Manage Accounts</button></a>
                            <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-post-story.php"><button class="purple-btn">Schedule Stories</button></a>
                        </div>                             
                    </div>

                    <div class="selling-list product-box row" id="scheduled"></div>
                    <div class="page-selection px-3"></div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
    


<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function () {
        var allPosts;
        var page = 1;
        var limit = 12;
        var is_schedule = 1;
        getPostList();
        $('.selling-tab').on('click', function(){
            page = 1;
            $('.nav-item').removeClass('toggle-link-active');
            $(this).closest('.nav-item').addClass('toggle-link-active');
            var activeTab = $(this).attr('data-id');
            if(activeTab == 'history'){
                is_schedule = 0;
            }else{
                is_schedule = 1;
            }
            getPostList()
        })

        /**
        * Get product from api
         */
        function getPostList() {
            $('.loader').removeClass('d-none');
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/influencer/stories/history?page=${page}&limit=${limit}&is_schedule=${is_schedule}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    if(data.total_results > limit){
                        let pagination = Pagination(data.total_results, limit,data.current_page);
                        $('.page-selection').html(pagination);
                    }else{
                        $('.page-selection').html('');
                    }
                    setPostListData(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

         /**
        * Set products to product list 
        */
        function setPostListData(posts) {
            allPosts = posts;
            let appenddata = "";
            if(allPosts.length > 0){
                for (let index = 0; index < allPosts.length; index++) {
                    const product = allPosts[index];
                    appenddata += ` <div class="product-box-inner col-12 col-sm-6 col-md-4">
                                <div class="product-box-content">
                                    <img src="<?php echo $image_base; ?>/${product.image}">
                                    <div class="box-content">
                                        <p>${product.product_details ? product.product_details.product_title : ''}</p>
                                        <h2>KSH ${product.product_details ? product.product_details.product_price : '0.00'}</h2>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">${product.product_details && product.product_details.rating ? product.product_details.rating.toFixed(1) : '0.0'}</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>`;
                }
            }else{
                appenddata += ` <div class="card col-12 col-md-12 mt-2 p-4">
                         
                        <div class="not-found d-block h-100 text-center">
                        <img src="../../images/icons/empty.svg" alt="">
                            <h4>You don’t have any scheduled posts at the moment</h4>
                        </div>
                    </div>`
            }
            
            $(".selling-list").html(appenddata);
            $('.loader').addClass('d-none');
        }
        $(document).on('click', '.page-link', function(event){
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if(pageNumber == 'prev'){
                page = page - 1;
            }else if(pageNumber == 'next'){
                page = page + 1;
            }else{
                page = Number($(this).attr('id'));
            }
            getPostList();
        })
    })
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
