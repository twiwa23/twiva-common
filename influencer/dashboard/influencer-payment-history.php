<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY . "/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY . "/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col dashboard-page" role="main" id="payment-history">
            <!--********** Breadcrumb Start ***********-->
            <div class="breadcrumb-wrapper">
                <!-- <a href="#"> <i class="fa fa-chevron-left"></i> Payment</a> -->
                <ul class="breadcrumb">
                    <li><a href="#">Account Settings</a></li>
                    <li class="active">Payout</li>
                </ul>
            </div>

            <!--**********  Breadcrumb End ***********-->

            <div class="earning-paymen-wrapper no-data-details m-4">
                <div class="earning-edit-btn-wrapper">
                    <div class="select-top">
                        <h3>Payment</h3>
                    </div>
                    <!-- <div class="btn-wrapper">
                            <a href="#">Edit Bank account</a>
                        </div> -->
                </div>

                <div class="upcoming-earing-list-wrapper earing-list">
                    <div class="not-found text-center d-block h-100">
                        <img src="../../images/icons/empty.svg" alt="">
                        <h3>No upcoming payout</h3>
                    </div>
                    <!-- <h4>
                            Upcoming 
                        </h4>
                        <div class="earning-item-wrapper">
                            <div class="item">
                               <div class="status-wrapper">
                                 <span class="active">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 29.svg" alt="">
                                 </span>
                                 <span>
                                    Upcoming
                                 </span>  
                               </div> 
                               <div class="date-wrapper">
                                   <span>31 Dec,2020</span>
                               </div>
                               <div class="amount-wrapper">
                                   <span>
                                       + $80.00
                                    </span>
                               </div>
                            </div>
                        </div> -->
                </div>

                <!-- <div class="history-earing-list-wrapper earing-list">
                        <h4>
                            History 
                        </h4>
                        <div class="earning-item-wrapper">
                            <div class="item">
                               <div class="status-wrapper">
                                 <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 29 (1).svg" alt="">
                                 </span>
                                 <span>
                                     Received
                                 </span>  
                               </div> 
                               <div class="date-wrapper">
                                   <span>31 Dec,2020</span>
                               </div>
                               <div class="amount-wrapper">
                                   <span>
                                       + $80.00
                                    </span>
                               </div>
                            </div>
                            
                             
                        </div>
                        <div class="earning-item-wrapper">
                            <div class="item">
                               <div class="status-wrapper">
                                 <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 29 (1).svg" alt="">
                                 </span>
                                 <span>
                                     Received
                                 </span>  
                               </div> 
                               <div class="date-wrapper">
                                   <span>31 Dec,2020</span>
                               </div>
                               <div class="amount-wrapper">
                                   <span>
                                       + $80.00
                                    </span>
                               </div>
                            </div>
                            
                             
                        </div>
                        <div class="earning-item-wrapper">
                            <div class="item">
                               <div class="status-wrapper">
                                 <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 29 (1).svg" alt="">
                                 </span>
                                 <span>
                                     Received
                                 </span>  
                               </div> 
                               <div class="date-wrapper">
                                   <span>31 Dec,2020</span>
                               </div>
                               <div class="amount-wrapper">
                                   <span>
                                       + $80.00
                                    </span>
                               </div>
                            </div>
                            
                             
                        </div>
                        <div class="earning-item-wrapper paid-earning">
                            <div class="item">
                               <div class="status-wrapper">
                                 <span class="">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 29 (1).svg" alt="">
                                 </span>
                                 <span>
                                    Paid
                                 </span>  
                               </div> 
                               <div class="date-wrapper">
                                   <span>31 Dec,2020</span>
                               </div>
                               <div class="amount-wrapper">
                                   <span>
                                       - $80.00
                                    </span>
                               </div>
                            </div>
                            
                             
                        </div>
                        
                    </div> -->
            </div>

            <!-- /page content -->
        </div>
    </div>

    <?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard-script.php"; ?>
    <?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard.php"; ?>
</div>