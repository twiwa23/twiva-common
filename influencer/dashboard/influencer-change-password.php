<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col add-product-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#">Account Settings</a></li>
                    <li class="active">Change Password</li>
                </ul>
            </div>

            <!--**********  Breadcrumb End ***********-->

            <div class="dashboard-inner">
                <div class="bank-setup-form">
                    <h3 class="subscription-title">
                        Change Password
                    </h3>

                    <div class="account-setup mb-3">
                        <form>
                            <div class="account-form input-fields">
                                <h5 class="alert alert-danger d-none w-100" role="alert" id="errorMessage"></h5>
                                <h5 class="alert alert-success d-none w-100" role="alert" id="successMessage"></h5>
                                <div class="form-field">
                                    <label>Old Password</label>
                                    <input type="password" placeholder="Old Password" id="oldPassword" />
                                    <h5 id="oldPassword-error" class="empty-field-error m-0"></h5>
                                </div>

                                <div class="form-field">
                                    <label>New Password</label>
                                    <input type="password" placeholder="New Password" id="newPassword" />
                                    <h5 id="newPassword-error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>Confirm Password</label>
                                    <input type="password" placeholder="Confirm Password" id="confirmPassword" />
                                    <h5 id="confirmPassword-error" class="empty-field-error m-0"></h5>
                                </div>
                            </div>
                            <div class="button-sec right-align-btn d-block">
                                <button id="submitbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>  Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>

<script>
    $(document).ready(function () {
        function isFormValid() {
            let isValid = true;
            let oldPassword = $("#oldPassword").val();
            let newPassword = $("#newPassword").val();
            let confirmPassword = $("#confirmPassword").val();

            if (oldPassword == "") {
                isValid = false;
                $("#oldPassword-error").show();
                $("#oldPassword-error").html("Please Enter your old password");
                $("#oldPassword-error").css("color", "red");
            } else {
                $("#oldPassword-error").hide();
            }
            if (newPassword == "") {
                isValid = false;
                $("#newPassword-error").show();
                $("#newPassword-error").html("Please Enter your new password");
                $("#newPassword-error").css("color", "red");
            } else {
                if (newPassword.length < 8) {
                    isValid = false;
                    $("#newPassword-error").show();
                    $("#newPassword-error").html("Password should be minimum 8 characters");
                    $("#newPassword-error").css("color", "red");
                }else{
                    $("#newPassword-error").hide();
                }
            }
            if (confirmPassword == "") {
                isValid = false;
                $("#confirmPassword-error").show();
                $("#confirmPassword-error").html("Please Enter your confirm password");
                $("#confirmPassword-error").css("color", "red");
            } else {
                $("#confirmPassword-error").hide();
            }
            if (newPassword != "" && confirmPassword != "") {
                if(newPassword != confirmPassword){
                    isValid = false;
                    $("#confirmPassword-error").show();
                    $("#confirmPassword-error").html("Passwords did not match");
                    $("#confirmPassword-error").css("color", "red");
                }
            }
            return isValid;
        }
        $("#submitbtn").click(function (event) {
            event.preventDefault();
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            $("#errorMessage").addClass("d-none");
            $("#successMessage").addClass("d-none");
            let oldPassword = $("#oldPassword").val();
            let newPassword = $("#newPassword").val();
            let confirmPassword = $("#confirmPassword").val();
            // Loader End
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/change/password",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "post",
                data: {
                    current_password: oldPassword,
                    new_password: newPassword,
                    confirm_password: confirmPassword
                },
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        $("#successMessage").removeClass("d-none");
                        $("#successMessage").html(data.message);
                        $("#oldPassword").val('');
                        $("#newPassword").val('');
                        $("#confirmPassword").val('');
                        setTimeout(() => {
                            $("#successMessage").addClass("d-none");
                        }, 3000);
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#errorMessage").removeClass("d-none");
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                    setTimeout(() => {
                        $("#errorMessage").addClass("d-none");
                    }, 3000);
                },
            });
        });
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
