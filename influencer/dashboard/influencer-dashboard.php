<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">Dashboard</div>
            <div class="dashboard-inner dashboard-card-status">
                <div class="dashboard-card-outr">
                    <div class="dashboard-card">
                        <h3><img src="<?php echo IMAGES_URI_PATH; ?>/icons/noun_Money Bag_3762816 1.svg" />Earnings</h3>
                        <div class="card-flex">
                            <span>Total Earning</span>
                            <h3>KSH  <span class="total-earning ml-2">0.00</span></h3>
                        </div>
                        <div class="card-flex">
                            <span>Earning This Month</span>
                            <h3>KSH  <span class="earning-of-month ml-2">0.00</span></h3>
                        </div>
                    </div>

                    <div class="dashboard-card is-eShop-exist">
                        <div class="card-flex">
                            <h3><img src="<?php echo IMAGES_URI_PATH; ?>/icons/noun_Store_2954518 1.svg" />My eShop</h3>
                            <h3 class="eShop-product-count">0 Products</h3>
                        </div>
                        <div class="common-button">
                            <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/add-eShop-products.php" style="width: 48%;"><button class="purple-btn">Add Product</button></a>
                            <button class="white-bttn " style="max-width: 48%;"><a class="text-red w-100" href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php">View eShop</a></button>
                        </div>
                    </div>

                    <div class="dashboard-card d-none is-eShop-not-exist" id="card-eShop">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/noun_Store_2954518 2.svg" />
                        <div class="card-flex">
                            <p>My eShop</p>
                            <h3>Not Connected</h3>
                        </div>
                        <div class="common-button full-button">
                            <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-select-product.php">
                                Create an eShop
                                <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" /></span>
                            </a>
                        </div>
                    </div>
                    <div class="dashboard-card" id="card-social">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/noun_link_1655590 1.svg" />
                        <div class="card-flex">
                            <p>Social Selling</p>
                            <h3 style="color: #262626;">Not Connected</h3>
                        </div>
                        <div class="common-button full-button">
                            <!--href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/social-media-connections.php"-->
                            <a>
                                Connect Social Accounts
                                <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right-black.svg" alt="" /></span>
                            </a>
                        </div>
                    </div>
                </div>

                <!--Product Section-->
				<div class="c-title-wrapper">
					<h5>
					Top 5 Selling Product
					</h5>
				</div>
                <div class="product-section">

                    <div class="product-box row pt-0 top_selling_product" style="margin:0;"></div>

                    <!-- <div class="product-box">
                    <h2>Recent Orders</h2>
                    
                    <div class="product-box-inner">
                        <img src="../images/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    
                    <div class="product-box-inner">
                        <img src="../images/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    
                    <div class="product-box-inner">
                        <img src="../images/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    <div class="product-box-inner">
                        <img src="../images/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    <div class="product-box-inner">
                        <img src="../images/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                </div> -->
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
	
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        getDashboardData();
        var allDashboardData;
        function getDashboardData(){
            $('.loader').removeClass('d-none')
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/dashboard",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function (data) {
                    allDashboardData = data.data; 
                    setDashBoardData(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none')
                    console.log("Error: ->",request.responseJSON);
                },
            });
        }
        function setDashBoardData(dashboardData){
            if(dashboardData.shop_details){
                localStorage.setItem('_shopId',dashboardData.shop_details.id)
            }
            if(dashboardData.user.influencer_details){
                localStorage.setItem('_shopConnectNow',dashboardData.user.influencer_details.is_shop_listed)
                localStorage.setItem('_userReferalCode',dashboardData.user.referal_code)
            }
            $('.total-earning').text(((dashboardData.total_earnings*7)/100).toFixed(2));
            $('.earning-of-month').text(((dashboardData.earning_this_month*7)/100).toFixed(2));
            $('.eShop-product-count').text(`${dashboardData.shop_products_count} Products`);
            localStorage.setItem('_isShopCreated', dashboardData.is_shop_created)
            localStorage.setItem('_copyConnectNow', dashboardData.eshop_products_export)
            let eShopLink = '';
            let isShopCreated = localStorage.getItem('_isShopCreated')
            if(isShopCreated == 1){
                eShopLink = '<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php'
            }else{
                eShopLink = '<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-select-product.php'
            }
            $('.eshop-link').attr('href',eShopLink);
            if(!dashboardData.is_shop_created){
                $('.is-eShop-exist').addClass('d-none');
                $('.is-eShop-not-exist').removeClass('d-none');
            }
            let appenddata = "";
            if(dashboardData.top_five_selling_products.length){
                for (var i = 0; i < dashboardData.top_five_selling_products.length; i++) {
                    const product = dashboardData.top_five_selling_products[i];
                    console.log(product,'product');
                    let productImage = product.influencer_product_images.find((obj) => obj.is_cover_pic == 1);
                    let image = productImage ? `<?php echo $image_base; ?>/${productImage.image_path}` : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                    appenddata += `<div class="product-box-inner col-6 col-md-4 product-item" data-id="${product.product_id}">
                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-product.php/?request=${product.product_id}&request1=${product.shop_id}">    
                    <div class="product-box-content">
                            <img src="${image}" />
                            <div class="box-content">
                                <p>${product.product_title}</p>
                                <h2>KSH ${product.product_price}</h2>
                                <span>No of Sale: ${product.total_sale}</span>
                            </div>`;
                            if(product.product_details.rating!=0){
                            appenddata += `<div class="product-rating">
                                <span class="rating-title">${product.product_details && product.product_details.rating ? product.product_details.rating.toFixed(1) : '0.0'}</span>
                                <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                            </div>`;
                            }
                        appenddata += `</div></a>
                    </div>`;
                }
            }else{
                appenddata += `<div class="product-box-inner col-12 col-md-12 no-data-details p-5" style="box-shadow: 0px 4px 6px rgb(0 0 0 / 10%);border-radius: 8px;">
                        <div class="not-found">
                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/empty.svg" alt=""> 
                            You don’t have any sales at the moment
                        </div>
                    </div>`;
            }
            $('.top_selling_product').html(appenddata);
            $('.loader').addClass('d-none')
        }
        // $(document).on('click', '.product-item', function(){
        //     let id = $(this).attr('data-id');
        //     let product = allDashboardData.top_five_selling_products.find((obj) => obj.id == id)
        //     localStorage.setItem('_productDetail', JSON.stringify(product))
        //     window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-product.php"
        // })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
