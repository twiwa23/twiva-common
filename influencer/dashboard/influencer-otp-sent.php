<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="login" id="mobile-verification">
    <div class="back-link">
        <a href="#"> <i class="fas fa-chevron-left"></i> Phone Verification</a>
    </div>

    <div class="container pt-5" id="otp-sent-page">
        <div class="login-inner">
            <div class="login-left">
                <!-- <img src="<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->
            </div>

            <div class="login-right">
                <h5 class="alert alert-danger d-none w-100 mt-5 mb-0" role="alert" id="errorMessage" style="margin: 2rem 0 0 0 !important;"></h5>
                <h5 class="alert alert-success d-none w-100 mt-5 mb-0" role="alert" id="successMessage" style="margin: 2rem 0 0 0 !important;"></h5>
                            
                <div class="login-section position-relative" style="margin-top: 3rem !important;">
                    
                    <div class="loader-otp d-none">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                    <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg" /></div>

                    <p>Please enter a new password and confirm it to reset.</p>
                    <h5 id="passwordcheck" style="display: none;"></h5>

                    <div class="input-field">
                        <label class="mobile-number"></label>

                        <div class="digit-group otp-input text-center">
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-1" name="digit-1" data-next="digit-2" />
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                            <input type="text" required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="digit-6" name="digit-6" data-previous="digit-5" />
                        </div>
                        <h5 id="otp-error" class="empty-field-error"></h5>
                        <div class="no-account">Did not receive the code? <a class="resend-otp" href="javascript:void(0);">Resend OTP</a></div>
                    </div>
                    <div class="bottom-button">
                        <button id="verificationBtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Verify</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function () {
        var verificationData = localStorage.getItem("_verificationData");
        if (verificationData) {
            verificationData = JSON.parse(verificationData);
            $('.mobile-number').text(`We have sent you an SMS on +${verificationData.code}-${verificationData.phone} with 6 digit verification code.`)
            console.log(verificationData,'lll');
            if(verificationData.otp){
                let otp = verificationData.otp.toString();
                $("#digit-1").val(otp.charAt(0));
                $("#digit-2").val(otp.charAt(1));
                $("#digit-3").val(otp.charAt(2));
                $("#digit-4").val(otp.charAt(3));
                $("#digit-5").val(otp.charAt(4));
                $("#digit-6").val(otp.charAt(5));
            }
            
        }
        function isFormValid(){
            let isValid = true;
            let digit1 = $("#digit-1").val();
            let digit2 = $("#digit-2").val();
            let digit3 = $("#digit-3").val();
            let digit4 = $("#digit-4").val();
            let digit5 = $("#digit-5").val();
            let digit6 = $("#digit-6").val();
            if(digit1 == '' || digit2 == '' || digit3 == '' || digit4 == '' || digit5 == '' || digit6 == ''){
                isValid = false;
                $("#otp-error").show();
                $("#otp-error").html("Please enter a valid otp");
                $("#otp-error").css("color", "red");
            }else{
                $("#otp-error").hide();
            }
            return isValid;
        }
        $(document).on("click", "#verificationBtn", function (event) {
            event.preventDefault();
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            $("#errorMessage").addClass("d-none");
            $("#successMessage").addClass("d-none");
            let digit1 = $("#digit-1").val();
            let digit2 = $("#digit-2").val();
            let digit3 = $("#digit-3").val();
            let digit4 = $("#digit-4").val();
            let digit5 = $("#digit-5").val();
            let digit6 = $("#digit-6").val();
            let otp = `${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}`;
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/phone/verify-otp",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "post",
                data: {
                    otp: otp
                },
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-mobile-verification.php`
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#errorMessage").removeClass("d-none");
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                    setTimeout(() => {
                        $("#errorMessage").addClass("d-none");
                    }, 3000);
                },
            });
        });
        $('.resend-otp').on('click', function(){
            $('.loader').removeClass('d-none');
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/phone/resend-otp",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "post",
                success: function (data) {
                    verificationData.otp = data.otp;
                    verificationData = JSON.stringify(verificationData);
                    localStorage.setItem('_verificationData',verificationData)
                    $('.loader').addClass('d-none');
                    let otp = data.otp.toString();
                    $("#digit-1").val(otp.charAt(0));
                    $("#digit-2").val(otp.charAt(1));
                    $("#digit-3").val(otp.charAt(2));
                    $("#digit-4").val(otp.charAt(3));
                    $("#digit-5").val(otp.charAt(4));
                    $("#digit-6").val(otp.charAt(5));
                    $("#successMessage").removeClass("d-none");
                    $("#successMessage").html(data.message);
                    setTimeout(() => {
                        $("#successMessage").addClass("d-none");
                    }, 3000);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    $("#errorMessage").removeClass("d-none");
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                    setTimeout(() => {
                        $("#errorMessage").addClass("d-none");
                    }, 3000);
                },
            });
        });
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
