<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col add-product-page" role="main">
            <div class="page-title">Connect Now</div>


            <div class="social-share-item-wrapper p-5">
                <h4 class="text-center">Connect your eshop to Facebook and Instagram shops</h4>
                <div class="content text-center">
                    <p>Connect your Twiva eShop with your Social Media accounts </p>
                    <p class="mt-2"></p>
                </div>
                <div class="content text-center mt-4">
                    <p class="mb-4">Follow the instructions on the video to connect your Twiva eShop with social media shops so that we can automate the listing of products on your social media accounts so that you can sell more. Please contact us if you need help with this.</p>
                    <p class="mt-2"></p>
                </div>

                <div class="video-wrapper">
                    <video class="w-100" controls poster="../../assets/videos/thumbnail.jpg" >
                        <source src="../../assets/videos/how_to_connect_twiva_eShop_with_social_media_shops.mp4" type="video/mp4">
                    </video>
                </div>

                <div class="refer-content-box-wrapper">
                    <div class="refer-code-wrapper">
                        <div class="">
                            <p class="mt-5 text-info link-url text-center">
                                TWIVA123
                            </p>
                        </div>
                        <div class="text-center">
                            
                            <button class="refer-code-copy-btn">
                                Tap to copy
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function () {
        let url = localStorage.getItem('_copyConnectNow')
        $('#link-url').val(url)
        $('.link-url').text(url)
        $('.refer-code-copy-btn').on('click', function(){
            var temp = $("<input>");
            $("body").append(temp);
            temp.val($('.link-url').text()).select();
            let _this = $(this);
            document.execCommand("copy");
            _this.text('Copied!').addClass('text-success')
        })
    })
    
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
