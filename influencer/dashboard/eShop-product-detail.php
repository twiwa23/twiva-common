<?php $class = "influencer-eshop-page product-detail-wrapper"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page" role="main">
            <div class="page-title">
                <a onclick="window.history.back()">
                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="" /></span>
                    Product Detail
                </a>
            </div>
            <div class="loader text-center w-100 d-none">
                <h1><i class="fa fa-spinner fa-spin"></i></h1>
            </div>
            <div class="product-main d-none px-4 influencer-product-detail">
                <div class="product-detail-cont">
                    <div id="primary-slider" class="splide">
                        <div class="splide__track">
                            <ul class="splide__list main-slide">
                                <li class="splide__slide">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png" />
                                </li>
                                <li class="splide__slide">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png" />
                                </li>
                                <li class="splide__slide">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="secondary-slider" class="splide">
                        <div class="splide__track">
                            <ul class="splide__list thumb-slides"></ul>
                        </div>
                    </div>
                    <!-- <div class="action-button mt-5">
                        <button class="white-bttn delete-product mr-3">Delete</button>
                        <button class="purple-btn edit-product">Edit</button>
                    </div> -->
                </div>

                <div class="product-specification">
                    <h2 id="product-title"></h2>
                    <h3 id="product-price"><label style="width: 73px;">Price: </label><b></b></h3>

                    <div class="spec-row">
                        <label>Size:</label>
                        <div id="product-size"></div>
                    </div>

                    <div class="spec-row align-items-start">
                        <label>Color:</label>
                        <div id="product-color" class="flex-wrap"></div>
                    </div>

                    <div class="prod-desc">
                        <label>Description</label>
                        <p id="description"></p>
                    </div>

                    <div class="prod-spec">
                        <label>Specification</label>
                        <div id="specification"></div>
                    </div>

                    <div class="prod-spec">
                        <label>Review & Rating</label>
                        <div class="review-list">
                        </div>
                    </div>

                    <!-- <div class="prod-link">
                            <a href="#">View all 8 review</a>
                        </div> -->
                </div>
                <!-- /page content -->
            </div>
        </div>
    </div>

    <?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
    <script>
        $(document).ready(function () {
            
            const params = new URLSearchParams(window.location.search);
            if (params.has("request") && params.has("request1")) {
                getProductDetail(params.get("request"), params.get("request1"));
            }
            /**
             * Get product from api
             */
            function getProductDetail(productID, shopID) {
                $(".loader").removeClass("d-none");
                $.ajax({
                    url: "<?php echo API_URI_PATH ; ?>/influencer/products/details",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                    },
                    type: "post",
                    data: { shop_id: shopID, product_id: productID },
                    success: function (data) {
                        setProductsListData(data.data);
                    },
                    error: function (request, status, error) {
                        $(".loader").addClass("d-none");
                        console.log("Error: ->", request.responseJSON);
                    },
                });
            }
            function setProductsListData(productDetail) {
                localStorage.setItem("_productDetailEdit", JSON.stringify(productDetail));
                if (productDetail) {
                    $("#product-title").text(productDetail.product_title);
                    $("#product-price b").text(`KSH ${productDetail.product_price}`);
                    var productSize = "";
                    productDetail.product_details.size_details.length ? productDetail.product_details.size_details.map((size) => (productSize += `<span>${size.name}</span>`)) : (productSize += `<span>N/A</span>`);
                    $("#product-size").html(productSize);
                    var productColor = "";
                    productDetail.product_details.color_details.length
                        ? productDetail.product_details.color_details.map((color) => (productColor += `<div style="width: 40px; height: 40px; background: #${color.hexaval}; margin: 0 13px 13px 0;"></div>`))
                        : (productColor += `<span>N/A</span>`);
                    $("#product-color").html(productColor);
                    $("#description").text(productDetail.product_description);
                    $("#specification").html(productDetail.specification);
                    var appendSliderData = "";
                    productDetail.influencer_product_images.length
                        ? productDetail.influencer_product_images.map((image) => {
                              if (image.image_path) {
                                  let classIsCover = image.is_cover_pic ? "coverImage" : "";
                                  appendSliderData += `<li class="splide__slide slider_slide ${classIsCover}">
                                                <img src="<?php echo $image_base; ?>/${image.image_path}">
                                            </li>`;
                              }
                          })
                        : null;
                    $(".main-slide").html(appendSliderData);
                    $(".thumb-slides").html(appendSliderData);
                    $(".loader").addClass("d-none");
                    $(".product-main").removeClass("d-none");
                    initSlider();
                    let ratings = productDetail.product_reviews_and_ratings;
                    console.log(ratings,'ratings');
                    let ratingAppend = '';
                    if(ratings && ratings.length > 0){
                        for (let index = 0; index < ratings.length; index++) {
                            const rating = ratings[index];
                            ratingAppend += `
                            <div class="review-item border-bottom pb-2">
                                <div class="review-author">
                                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/verified.svg" /></span> ${rating.buyer_name ? rating.buyer_name.name : '-'}
                                </div>
                                <div class="date d-flex"><div class="my-rating-4 mr-2" data-rating="${rating.rating}"></div> ${moment(rating.created_at).format('MMM YYYY')}</div>
                                <p>${rating.review}</p>
                            </div>
                            `
                        }
                        $('.review-list').html(ratingAppend);
                        $(".my-rating-4").starRating({
                            totalStars: 5,
                            starShape: 'rounded',
                            starSize: 15,
                            readOnly: true,
                            emptyColor: 'lightgray',
                            hoverColor: '#f7b844',
                            activeColor: '#f7b844',
                            useGradient: false
                        });
                    }else{
                        ratingAppend += ` <div class="card col-12 col-md-12 border border-0">
                            <div class="not-found">
                                <h4>No Review Found!</h4>
                            </div>
                        </div>`
                        $('.review-list').html(ratingAppend);
                    }
                }
            }
            function initSlider() {
                var secondarySlider = new Splide("#secondary-slider", {
                    fixedWidth: 100,
                    height: 60,
                    gap: 10,
                    cover: true,
                    isNavigation: true,
                    focus: "center",
                    breakpoints: {
                        "600": {
                            fixedWidth: 66,
                            height: 40,
                        },
                    },
                }).mount();

                var primarySlider = new Splide("#primary-slider", {
                    type: "fade",
                    heightRatio: 0.5,
                    pagination: false,
                    arrows: false,
                    cover: true,
                }); // do not call mount() here.

                primarySlider.sync(secondarySlider).mount();
            }

            $(".edit-product").on("click", function () {
                window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/edit-eShop-product.php";
            });
            $(".delete-product").on("click", function () {
                let detail = JSON.parse(localStorage.getItem("_productDetailEdit"));
                swal(
                    {
                        title: "Are you sure you want to delete this product?",
                        // text: "You want to delete this product!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        $.ajax({
                            url: "<?php echo API_URI_PATH ; ?>/influencer/products/delete",
                            headers: {
                                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                                Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                            },
                            type: "post",
                            data: { id: detail.id },
                            success: function (data) {
                                window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php`;
                            },
                            error: function (request, status, error) {
                                console.log("Error: ->", request.responseJSON);
                            },
                        });
                    }
                );
            });
        });
    </script>
    <?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
    <div class="selected-product-footer-wrapper container-fluid">
		<div class="seleced-product-btn-wrapper"> 
				<div class="btn-wrapper">
                    <div class="preview-btn-wrapper">
                    <button class=" delete-product">
							    Delete
						    </button>
                    </div>
               
				    <div class="next-btn-wrapper">
                    <button class="edit-product ml-1">
							Edit
					 </button>
                    </div>

				</div>	
		</div>
	</div>
</div>
