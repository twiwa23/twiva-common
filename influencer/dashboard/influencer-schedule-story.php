<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>

<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>

<div class="account-setup" id="schedule-story">
    <div class="back-link">
        <a href="#">
            Dashboard <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" /> Social Selling <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" /> Post Story
            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" />
            Schedule Story
        </a>
    </div>
    <div class="container">
        <h1>Schedule Story</h1>
        <div class="account-cont">
            <div class="account-form">
                <div class="form-field">
                    <label>Time</label>
                    <input type="time" id="time" placeholder="5:30 PM" />
                    <h5 id="time-error" class="empty-field-error"></h5>
                </div>

                <div class="form-field date-picker-field">
                    <label>Start Date</label>
                    <input type="text" class="input-date datepicker" id="start-date"/>
                    <h5 id="start-date-error" class="empty-field-error"></h5>
                </div>
            </div>

            <div id="schedule-input-form">
                <div class="form-input-field">
                    <label for="">Repeat</label>
                    <div class="radio-input">
                        <div>
                            <input id="daily" type="radio" checked name="repeat" class="schedule-repeat" value="1"/>
                            <label for="daily">Daily</label>
                        </div>
                        <div>
                            <input id="repeat" type="radio" name="repeat" class="schedule-repeat" value="2"/>
                            <label for="repeat">On</label>
                        </div>
                    </div>

                    <div class="form-field m-0 d-none days-field">
                        <label>Select Days</label>
                        <div class="dropdown multi-select-options">
                            <select id="days" class="js-example-basic-multiple w-100 days-select-box" multiple>
                                <option disabled>Select days in week</option>
                                <option value="sunday">Sunday</option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                            </select>
                        </div>
                        <h5 id="days-error" class="empty-field-error"></h5>
                    </div>
                </div>

                <div class="form-input-field">
                    <label for="">End After</label>
                    <div class="radio-input">
                        <div>
                            <input id="enddate" checked type="radio" name="endAfter" class="end-after" value="1"/>
                            <label for="enddate">End Date</label>
                        </div>
                        <div>
                            <input id="stories" type="radio" name="endAfter" class="end-after" value="2"/>
                            <label for="stories">Stories</label>
                        </div>
                    </div>
                    <div class="form-field mx-0 date-picker-field end-date">
                        <label>End Date</label>
                        <input type="text" class="input-date datepicker2" id="end-date"/>
                        <h5 id="end-date-error" class="empty-field-error"></h5>
                    </div>
                    <div class="form-field mx-0 number-field d-none">
                        <label>Select Number</label>
                        <div class="dropdown multi-select-options">
                            <select class="js-example-basic-multiple w-100 number-select-box" id="number"> </select>
                        </div>
                        <h5 id="number-error" class="empty-field-error"></h5>
                    </div>
                </div>
            </div>

            <div class="button-sec">
                <button class="submit-schedule"><i class="fa fa-spinner fa-spin mr-2 d-none text-white"> </i>Schedule</button>
            </div>
        </div>
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            change: function (e) {
                updateEndDate()
            } 
        });
        var numbers = "<option disabled selected>Select Number</option>";
        for (let index = 1; index <= 100; index++) {
            numbers += `<option value="${index}">${index}</option>`;
        }
        $(".number-select-box").html(numbers);
        select2Init();
        function updateEndDate() {
            $(".datepicker2").datepicker({minDate: new Date($('#start-date').val()),format: 'yyyy-mm-dd' });
        }
        $('.schedule-repeat').on('click change', function(e) {
            if(e.target.value == 2){
                $('.days-field').removeClass('d-none');
                select2Init()
            }else{
                $('.days-field').addClass('d-none');
            }
        });
        $('.end-after').on('click change', function(e) {
            if(e.target.value == 2){
                $('.number-field').removeClass('d-none');
                $('.end-date').addClass('d-none');
                select2Init()
            }else{
                $('.number-field').addClass('d-none');
                $('.end-date').removeClass('d-none');
            }
        });
        function select2Init() {
            $('.js-example-basic-multiple').select2({
                placeholder: "Select days in week",
            });
        }
        /**
        * @isFormValid - Form validation
        */
        function isFormValid() {
            let isValid = true;
            let time = $("#time").val();
            let startDate = $("#start-date").val();
            let endDate = $("#end-date").val();
            let repeat = $("input[name='repeat']:checked").val();
            let endAfter = $("input[name='endAfter']:checked").val();
            let days = $("#days").val();
            let number = $("#number").val();
            if (!time || time == "") {
                isValid = false;
                $("#time-error").show();
                $("#time-error").html("Please select a start time");
                $("#time-error").css("color", "red");
            } else {
                $("#time-error").hide();
            }
            if (!startDate || startDate == "") {
                isValid = false;
                $("#start-date-error").show();
                $("#start-date-error").html("Please select a start date");
                $("#start-date-error").css("color", "red");
            } else {
                $("#start-date-error").hide();
            }
            if (!startDate || startDate == "") {
                isValid = false;
                $("#start-date-error").show();
                $("#start-date-error").html("Please select a start date");
                $("#start-date-error").css("color", "red");
            } else {
                $("#start-date-error").hide();
            }
            if(repeat == 2){
                if(days.length == 0){
                    isValid = false;
                    $("#days-error").show();
                    $("#days-error").html("Please select a days");
                    $("#days-error").css("color", "red");
                }else{
                    $("#days-error").hide();
                }
            }
            if(endAfter == 1){
                if(!endDate || endDate == ""){
                    isValid = false;
                    $("#end-date-error").show();
                    $("#end-date-error").html("Please select a end date");
                    $("#end-date-error").css("color", "red");
                }else{
                    $("#end-date-error").hide();
                }
            }else{
                if(!number || number == ""){
                    isValid = false;
                    $("#number-error").show();
                    $("#number-error").html("Please select a number");
                    $("#number-error").css("color", "red");
                }else{
                    $("#number-error").hide();
                }
            }
            return isValid;
        }
        $('.submit-schedule').on('click', function () {
            if(!isFormValid()){
                return false;
            }
            let _this = $(this);
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            let time = $("#time").val();
            let startDate = $("#start-date").val();
            let endDate = $("#end-date").val();
            let repeat = $("input[name='repeat']:checked").val();
            let endAfter = $("input[name='endAfter']:checked").val();
            let days = $("#days").val();
            let number = $("#number").val();
            let postData = JSON.parse(localStorage.getItem('_postStory'));
            let post = {
                start_date: startDate,
                start_time : time,
                repeat_type : repeat,
                end_after_type : endAfter,
                is_schedule : 1,
                repeated_days : repeat == 2 ? days.join(',') : null,
                end_date: endAfter == 1 ? endDate : null,
                number_of_time: endAfter == 2 ? number : 0
            }
            let params = {...postData, ...post};
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/post/story",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                dataType: "json",
                data: params,
                type: "post",
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    localStorage.removeItem('_postStory')
                    if (data.status == true) {
                        window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-social.php";
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                },
            });
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
