<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title mb-0">
                <div class="back-link">
                    <a href="#">Edit Profile</a>
                </div>
            </div>
            <div class="dashboard-inner">
                <div class="login pt-5" id="account-settings">
                    <div class="container">
                        <div class="account-info-wrapper mt-0">
                            <div class="row">
                                <div class="col-md-12 loader-wrapper d-none"></div>
                                <div class="col-md-12 user-info-wrapper">
                                    <din class="user-info">

                                    <div class="row">
                                        <div class="col-md-6 border-right d-flex align-items-center" >
                                        <din class="profile-wrapper content-wrapper d-flex">
                                            <div>
                                                <img class="profile-picture" src="<?php echo IMAGES_URI_PATH; ?>/icons/profile.jpeg" alt="" />
                                                <!-- <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/profile-camera.svg" alt=""></span>  -->
                                            </div>
                                            <div class="company-info text-left">
                                                <h4 class="name"></h4>
                                                <p class="email"></p>
                                            </div>
                                           </din>
                                          
                                        </div>
                                        <div class="col-md-6 pl-4">
                                      <div class="content-wrapper">
                                            <div class="contact-wrapper">
                                                <!-- <p><span>Contact Person:</span><span> </span></p> -->
                                                <p><span>Date of Birth</span><span class="dob"></span></p>
                                                <p><span>Gender</span><span class="gender"> </span></p>
                                                <p><span>Interest</span><span class="interest"></span></p>
                                            </div>
                                            <div class="about-wrapper">
                                                <p class="description"></p>
                                            </div>
                                        </div>
                                        <div class="edit-btn-wrapper pull-right">
                                            <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-basic-information.php">
                                                Edit Information
                                            </a>
                                        </div>
                                      </div>
                                     
                                    </div>
                                      
                                        
                                        
                                    </din>
                                </div>
                                   

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        getProfileData();
        function getProfileData(){
            $('.loader').removeClass('d-none');
            $('.loader-wrapper').removeClass('d-none');
            $('.user-info-wrapper').addClass('d-none');
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/details",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function (data) {
                    setProfileData(data.data);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->",request.responseJSON);
                },
            });
        }

        function setProfileData(userInfo){
            $('.loader-wrapper').addClass('d-none');
            $('.user-info-wrapper').removeClass('d-none');
            $('.loader-wrapper').html('');
            if(userInfo.influencer_details){
                $('.name').text(userInfo.influencer_details.name);
                $('.email').text(userInfo.email);
                $('.dob').text(userInfo.influencer_details.dob);
                $('.gender').text(`${userInfo.influencer_details.gender == '1' ? 'Male' : userInfo.influencer_details.gender == '2' ? 'Female' : 'Other'}`);
                let interest = [];
                let allInterest = userInfo.influencer_details.interests;
                for (let index = 0; index < allInterest.length; index++) {
                    const element = allInterest[index];
                    interest.push(element.name)
                }
                $('.interest').text(interest.join(', '));
                $('.description').text(userInfo.influencer_details.description);
                if(userInfo.influencer_details && userInfo.influencer_details.profile_image != '' && userInfo.influencer_details.profile_image != null){
                    let picture = `<?php echo $image_base; ?>${userInfo.influencer_details.profile_image}`
                    $('.profile-picture').attr('src', picture);
                }
            }else{
                $('.loader').addClass('d-none');
                $('.name').text(userInfo.name);
                $('.email').text(userInfo.email);
                $('.dob').text(userInfo.influencer_detail.dob);
                $('.gender').text(`${userInfo.influencer_detail.gender == '1' ? 'Male' : userInfo.influencer_detail.gender == '2' ? 'Female' : 'Other'}`);              
            }
            $('.loader').addClass('d-none');
        }
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
