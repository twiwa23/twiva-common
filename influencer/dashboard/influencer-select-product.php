<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="dashboard-inner">
                <!--Product Banner-->
                <div class="product-banner">
                    <!--Banner Carousel-->
                    <div id="banner-carousel" class="carousel slide" data-ride="carousel">
                        <div class="banner-details">
                            <h3>Create Shop</h3>
							<p>Step 1 of 3 
								<span class="step-dot active"></span> 
								<span class="step-dot"></span>
								<span class="step-dot"></span> 
							</p>
                        </div>
                        <!-- Carousel Indicators -->
                       

                        <!-- Wrapper for slides -->
                        
                        <!-- Left and right controls -->
                        <!-- <a class="carousel-control-prev" href="#banner-carousel" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#banner-carousel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a> -->
                    </div>
                </div>

                <!--Product Selection-->
                <div class="product-selection">
                    <div class="title">
                        <h4>Select Products</h4>
                        <p>Select products for your eShop</p>
                    </div>

                    <div class="product-form">
                        <div class="product-filter">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/search.svg" alt="" /></span>
                                </div>
                                <input type="text" class="form-control search-bar" placeholder="Search here.." id="keywords"/>
                            </div>

                            <!--Filter Button-->
                            <button class="filter-button" type="button">
                                <span>Filter</span>
                                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Property 1=Sliders, Property 2=16px.svg" alt="" />
                            </button>

                            <div class="filter-wrapper">
                                <div class="filter-header-wrapper">
                                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt="" /></span>
                                    <h2 class="filter-title">Filter</h2>
                                </div>
                                
                                <div class="button-catalog">
                                    <select id="price" class="price-filter">
                                        <option value="">Price Range</option>
                                        <option value="0">Low to High</option>
                                        <option value="1">High to Low</option>
                                    </select>

                                    <select id="category">
                                        <option value="">Categories: All Categories</option>
                                    </select>


                                    <div class="form-field multi-select-wrapper">      
                                        <select class="mul-select-category" multiple="true" id="sub_category">
                                                <!-- <option value="Sub 1">Sub 1</option>
                                                <option value="Sub 2">Sub 2</option>
                                                <option value="Sub 3">Sub 3</option>
                                                <option value="Sub 4">Sub 4</option> -->
                                        </select>
                                    </div>
                                </div>

                                <div class="common-button">
                                    <button class="white-bttn" id="reset_filter">Reset</button>
                                    <button class="purple-btn" id="apply_filter">Apply</button>
                                </div>

                            </div>

                            
                        </div>
                    </div>
                </div>

                <!--Product Section-->
                <div class="product-section mt-2">
                    <div class="product-box row product-list">
                        <div class="product-box-inner col-6 col-md-4 col-lg-3">
                            <div class="product-box-content">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg" />
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <button class="white-bttn">Select</button>
                                </div>
                                <div class="product-rating">
                                    <span class="rating-title">5.0</span>
                                    <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Page Selection-->
            <div class="page-selection"></div>
        </div>
        <!-- /page content -->
    </div>
	<div class="selected-product-footer-wrapper container-fluid d-none">
		<div class="seleced-product-btn-wrapper"> 
				<div class="btn-wrapper">
					<div class="no-of-select-product">
						<h4 class="total-count">
							0 Product Selected
						</h4>
					</div>
					<div class="next-btn-wrapper">
						<a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-products-personalise.php">
                            <button>
							Next Step
						    </button>
                        </a>
					</div>
				</div>	
		</div>
	</div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script src="../../assets/js/api.js"></script>
<script src="../../assets/js/influencer_filter.js"></script>
<script>
    $(document).ready(function () {
        get_category();
    });
</script>
<script>
    $(document).ready(function () {
        localStorage.removeItem('selectedProduct')
        var allProducts;
        var allSelectedProducts = [];
        var page = 1;
        var limit = 12;
        var search = '';
        var sort_by = '';
        var category = '';
        var sub_category = '';
        getProductList();
        function getProductList() {
            let loader = `<div class="not-found text-center w-100">
                                <h1><i class="fa fa-spinner fa-spin"></i></h1>
                            </div>`;
            $(".product-list").html(loader);
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/influencer/products/list?page=${page}&limit=${limit}&search=${search}&sort_by=${sort_by}&categories=${category}&sub_categories=${sub_category}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    if(data.total_results > limit){
                        let pagination = Pagination(data.total_results, limit,data.current_page);
                        $('.page-selection').html(pagination);
                    }else{
                        $('.page-selection').html('');
                    }
                    setProductsListData(data.data);
                },
                error: function (request, status, error) {
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

        function setProductsListData(products) {
            allProducts = products;
            allSelectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            let appenddata = "";
            if(products.length > 0){
                for (let index = 0; index < products.length; index++) {
                    const product = products[index];
                    const productImage = product.product_images.find((obj) => obj.is_cover_pic == 1);
                    const selectedProduct = allSelectedProducts.findIndex((obj) => obj.id == product.id);
                    const image = productImage ? productImage.image : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                    appenddata += `<div class="product-box-inner col-12 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img  class="thumb" src="<?php echo $image_base; ?>/${image}" height="216"/>
                                    <div class="box-content">
                                        <p>${product.name}</p>
                                        <h2>KSH ${product.price}</h2>
                                        <button class="white-bttn select-product  ${selectedProduct != -1 ? 'active' : ''}" data-id="${product.id}">Select</button>
                                    </div>`;
                                    if(product.rating!=0){
                                    appenddata += `<div class="product-rating" >
                                        <span class="rating-title">${product && product.rating ? product.rating.toFixed(1) : '0.0'}</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                    </div>`;
                                    }                                   
                                    appenddata += `</div>
                            </div>`;
                }

            }else{
                appenddata = `<div class="col-12 col-md-12 col-lg-12 text-center p-5">
                                <img src="../../images/icons/empty.svg" alt="">
                                <h3 style="font-size:20px;">
                                    No Products Found! 
                                </h3>
                            </div>`
            }
            $(".product-list").html(appenddata);
        }
        $(document).on('click','.select-product',function(){
            let _this = $(this);
            let id = _this.attr('data-id');
            let index = allSelectedProducts.findIndex(product => product.id == id);
            if(index == -1){
                _this.addClass('active');
                _this.text('Selected');
                const productDetail = allProducts.find((obj) => obj.id == id);
                allSelectedProducts.push(productDetail);
            }else{
                _this.removeClass('active');
                _this.text('Select');
                allSelectedProducts.splice(index, 1)
            }
            $('.total-count').text(`${allSelectedProducts.length} Product Selected`)
            localStorage.setItem('selectedProduct', JSON.stringify(allSelectedProducts))
            if(allSelectedProducts.length > 0){
                $('.selected-product-footer-wrapper').removeClass('d-none');
            }else{
                $('.selected-product-footer-wrapper').addClass('d-none');
            }
        })
        $(document).on('click', '.page-link', function(event){
            event.preventDefault();
            let pageNumber = $(this).attr('id');
            if(pageNumber == 'prev'){
                page = page - 1;
            }else if(pageNumber == 'next'){
                page = page + 1;
            }else{
                page = Number($(this).attr('id'));
            }
            getProductList();
        })
        // $('input.search-bar').on('keyup',function() { 
        //     if($(this).val().length >= 3){
        //         page = 1;
        //         search = $(this).val();
        //         getProductList();
        //     }else if($(this).val().length == 0){
        //         page = 1;
        //         search = '';
        //         getProductList();
        //     }
        // });
        $('#apply_filter').on('click',function() { 
            page = 1;
            if($('#keywords').val().length >= 1){
                search = $('#keywords').val();
            } else {
                search = '';
            }
            sort_by = $("#price").val() ?? '';
            category = $("#category").val() ?? '';
            sub_category = $("#sub_category").val() ?? '';
            if(sub_category) {
                sub_category = sub_category.toString();
            }
            console.log(search, sort_by, category, sub_category);
            getProductList();
        });
        $('#reset_filter').on('click',function() { 
            page = 1;
            $('#keywords').val('');
            $('#category').val('');
            $('#sub_category').val('');
            $('#price').val('');
            search = '';
            sort_by = '';
            category = '';
            sub_category = '';
            console.log(search, sort_by, category, sub_category);
            getProductList();
        });
    });
</script>

<script>
        $(document).ready(function(){
            $(".mul-select-category").select2({
                placeholder: "Select Sub Category", //placeholder
                tags: true,
                tokenSeparators: ['/',',',';'," "] 
            });
        });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
