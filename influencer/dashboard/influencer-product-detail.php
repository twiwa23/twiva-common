<?php $class = "influencer-eshop-page product-detail-wrapper"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page" role="main">
                <div class="page-title">
                    <a onclick="window.history.back()" >
                        <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""></span>
                        Product Detail
                    </a>
                </div>

                <div class="product-main">
                    <div class="product-detail-cont">
                        <div id="primary-slider" class="splide">
                            <div class="splide__track">
                                <ul class="splide__list main-slide">
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="secondary-slider" class="splide">
                            <div class="splide__track">
                                <ul class="splide__list thumb-slides">
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="product-specification">
                    <h2 id="product-title"></h2>
                    <h3 id="product-price"><label style="width: 73px;">Price: </label><b></b></h3>
                    
                    <div class="spec-row">
                        <label>Size:</label>
                        <div id="product-size">
                        </div>
                    </div>
                    
                    <div class="spec-row align-items-start">
                        <label>Color:</label>
                        <div id="product-color" class="flex-wrap">
                        </div>
                    </div>
                    
                    <div class="prod-desc">
                        <label>Description</label>
                        <p id="description"> </p>
                    </div>
                    
                    <div class="prod-spec">
                        <label>Specification</label>
                        <div id="specification"></div>
                    </div>
                    
                    <div class="prod-spec">
                        <label>Review & Rating</label>
                        <div class="review-list">
                        </div>
                    </div>

                    <!-- <div class="prod-link">
                        <a href="#">View all 8 review</a>
                    </div> -->

                </div>               
                <!-- /page content -->
            </div>   
        </div>
    </div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
        $(document).ready(function(){
            var allSelectedProducts = [];
            var productDetail = localStorage.getItem('_productDetailForPreview');
            if(productDetail){
                productDetail = JSON.parse(productDetail);
                $('#product-title').text(productDetail.name);   
                $('#product-price b').text(`KSH ${productDetail.price}`); 
                var productSize = '';
                productDetail.size_details.map((size) => (
                    productSize += `<span>${size.name}</span>`
                ))
                $('#product-size').html(productSize);  
                var productColor = '';
                productDetail.color_details.map((color) => (
                    productColor += `<div style="width: 40px; height: 40px; background: #${color.hexaval}; margin: 0 13px 13px 0;"></div>`
                ))
                $('#product-color').html(productColor);
                $('#description').text(productDetail.description);  
                $('#specification').html(productDetail.specification);  
                var appendSliderData = "";
                productDetail.product_images.length 
                ? productDetail.product_images.map((image) => {
                    if(image.image){
                        let classIsCover = image.is_cover_pic ? 'coverImage' : '';
                        appendSliderData += `<li class="splide__slide slider_slide ${classIsCover}">
                                                <img src="<?php echo $image_base; ?>/${image.image}">
                                            </li>`;
                    }
                })
                :
                null;
                $('.main-slide').html(appendSliderData);
                $('.thumb-slides').html(appendSliderData);
                initSlider();
                let ratings = productDetail.product_reviews_and_ratings;
                let ratingAppend = '';
                if(ratings && ratings.length > 0){
                    for (let index = 0; index < ratings.length; index++) {
                        const rating = ratings[index];
                        ratingAppend += `
                        <div class="review-item border-bottom pb-2">
                            <div class="review-author">
                                <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/verified.svg" /></span> ${rating.buyer_name ? rating.buyer_name.name : '-'}
                            </div>
                            <div class="date d-flex"><div class="my-rating-4 mr-2" data-rating="${rating.rating}"></div> ${moment(rating.created_at).format('MMM YYYY')}</div>
                            <p>${rating.review}</p>
                        </div>
                        `
                    }
                    $('.review-list').html(ratingAppend);
                    $(".my-rating-4").starRating({
                        totalStars: 5,
                        starShape: 'rounded',
                        starSize: 15,
                        readOnly: true,
                        emptyColor: 'lightgray',
                        hoverColor: '#f7b844',
                        activeColor: '#f7b844',
                        useGradient: false
                    });
                }else{
                    ratingAppend += ` <div class="card col-12 col-md-12 border border-0">
                        <div class="not-found">
                            <h4>No Review Found!</h4>
                        </div>
                    </div>`
                    $('.review-list').html(ratingAppend);
                }
            }
            function initSlider() {
                var secondarySlider = new Splide( '#secondary-slider', {
                    fixedWidth  : 100,
                    height      : 60,
                    gap         : 10,
                    cover       : true,
                    isNavigation: true,
                    focus       : 'center',
                    breakpoints : {
                        '600': {
                            fixedWidth: 66,
                            height    : 40,
                        }
                    },
                } ).mount();
                
                var primarySlider = new Splide( '#primary-slider', {
                    type       : 'fade',
                    heightRatio: 0.5,
                    pagination : false,
                    arrows     : false,
                    cover      : true,
                } ); // do not call mount() here.
                
                primarySlider.sync( secondarySlider ).mount();
            }
        });
    </script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
