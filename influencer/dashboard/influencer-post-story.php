<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<?php
  use Facebook\Facebook;
  use Facebook\Exceptions\FacebookResponseException;
  use Facebook\Exceptions\FacebookSDKException;
  $accessToken = "EAAHGG5NHYDoBAAwCaGXU6iVCYJWbxsFZCpWW5o9ZATh9QldnEA6WoSZCg2PLz7JxyVZC64ZCS6Sjb6aXvW8DZB547bO8mG4lTPnlmWlUf8BzTRs8yyp9XGwUn1x6oioSGhEyZCTrFOUeTIeO8LK8tbI1S3ZBtb0Q7CZAe0Rd4ZBG5wJgZDZD";
  $_SESSION['facebook_access_token'] = $accessToken;
  if(isset($accessToken)){
      if(isset($_SESSION['facebook_access_token'])){
          $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
      }

      //FB post content
      $message = 'Test message from CodexWorld.com website';
      $title = 'Post From Website';
      $link = 'http://www.google.com/';
      $description = 'CodexWorld is a programming blog.';
      $picture = 'https://i.picsum.photos/id/870/200/300.jpg?blur=2&grayscale&hmac=ujRymp644uYVjdKJM7kyLDSsrqNSMVRPnGU99cKl6Vs';

      $attachment = array(
          'message' => $message,
          'name' => $title,
          'link' => $link,
          'description' => $description,
          'picture'=>$picture,
      );
      try{
          // Post to Facebook
          $fb->post('/me/feed', $attachment, $accessToken);

          // Display post submission status
          echo 'The post was published successfully to the Facebook timeline.';
      }catch(FacebookResponseException $e){
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
      }catch(FacebookSDKException $e){
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
      }
  }else{
      // Get Facebook login URL
      $fbLoginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);

      // Redirect to Facebook login page
      echo '<a href="'.$fbLoginURL.'"><img src="fb-btn.png" /></a>';
  }
?>
<!--Main Section Start-->
<div id="post-story">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>
        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">
                <a href=""> Dashboard <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" /> Social Selling <img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="" /> Post Story</a>
            </div>

            <div class="personalise-product">
                <div class="create-eshop-wrapper">
                    <!-- <div class="eshop-logo-wrapper">
                            <label for="logo-feature-img">
                                <input type="file" id="logo-feature-img">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 7120.png" alt="">
                                <h3 class="image-title">eShop Logo</h3>
                                <p class="image-size">image size 2000 X 1200</p>
                            </label>
                        </div> -->

                    <div class="eshop-details-wrapper">
                        <div class="form-field">
                            <label>Select Product</label>
                            <div class="dropdown multi-select-options">
                                <input type="hidden" name="" id="products">
                                <!-- <select id="products" class="js-example-basic-multiple w-100 product-select-box">
                                    <option disabled selected>Select Product</option>
                                </select> -->
                                <button class="select-story-btn" data-toggle="modal" data-target="#productMmodal">Select Product</button>
                            </div>
                            <h5 id="products-error" class="empty-field-error"></h5>
                        </div>

                        <div class="product-section selected-product">
                            <div class="no-product-selected">No product selected</div>
                        </div>

                        <div class="product-images">
                            <label for="">Select Image to Post</label>
                            <input type="hidden" id="selected-image" />
                            <ul class="product-image-list">
                                <li>
                                    <div class="no-product-selected py-3 px-4 w-100 font-weight-bold">?</div>
                                </li>
                            </ul>
                            <h5 id="selected-image-error" class="empty-field-error"></h5>
                        </div>

                        <div class="form-field">
                            <label>Platform</label>
                            <div class="dropdown multi-select-options">
                                <select id="platform" class="js-example-basic-multiple w-100 platform-select-box">
                                    <option disabled selected>Select Platform</option>
                                    <option value="1">Facebook</option>
                                    <option value="2">Twitter</option>
                                    <option value="3">Instagram</option>
                                    <option value="4">Youtube</option>
                                </select>
                            </div>
                            <h5 id="platform-error" class="empty-field-error"></h5>
                        </div>

                        <div class="form-field-full">
                            <label>Caption</label>
                            <textarea id="caption" placeholder="Enter caption here..."></textarea>
                            <h5 id="caption-error" class="empty-field-error"></h5>
                        </div>

                        <div class="select-color form-group">
                            <label for="select-color">Select Color</label>
                            <input type="hidden" id="color" />
                            <ul class="color-list">
                                <li class="color-list-item" style="background-color: #fee2e2;" data-color="#FEE2E2">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #fca5a5;" data-color="#FCA5A5">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #fdba74;" data-color="#FDBA74">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #fb923c;" data-color="#FB923C">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #fde047;" data-color="#FDE047">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #eab308;" data-color="#EAB308">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #86efac;" data-color="#86EFAC">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #22c55e;" data-color="#22C55E">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #14532d;" data-color="#14532D">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #60a5fa;" data-color="#60A5FA">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #c084fc;" data-color="#C084FC">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #1e3a8a;" data-color="#1E3A8A">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #f472b6;" data-color="#F472B6">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                                <li class="color-list-item" style="background-color: #4c1d95;" data-color="#4C1D95">
                                    <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                            <h5 id="color-error" class="empty-field-error"></h5>
                        </div>

                        <!-- <div class="common-button">
                                <button class="white-bttn">Preview</button>
                            </div> -->
                    </div>
                </div>

                <div id="post-button">
                    <button class="white-bttn schedule-post"><i class="fa fa-spinner fa-spin mr-2 d-none text-white"> </i>Schedule</button>
                    <button class="purple-btn submit-post"><i class="fa fa-spinner fa-spin mr-2 d-none text-white"> </i>Post Now</button>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<!-- Image Modal Start -->
<div id="ImgModal" class="image-modal">
    <!-- The Close Button -->
    <span class="close">&times;</span>

    <!-- Modal Content (The Image) -->
    <img class="image-modal-content" id="img" />
</div>
<!-- Image Modal End -->

<!-- The Modal -->
<div class="modal fade" id="productMmodal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-product-wrapper">
                <div class="row products-list">
                    <!-- <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                    <button type="button" class="added-product d-none"></button>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/product-default-img.png" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">shshsjsis sudhs do ththyuui did she yuui uiii niko u uvu8uvu</h4>
                                    <h5 class="price">KSH 1250</h5>
                                    <span class="product-no-sale">No of Sale: 0</span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer-wrapper">
                <div class="btn-wrapper">
                    <div class="confirm-btn-wrapper">
                        <button class="continue-btn modal-btn">Continue</button>
                    </div>
                    <div class="close-btn-wrapper">
                        <button class="close-btn modal-btn" data-dismiss="modal">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
      console.log(localStorage,'localStorage');
        var allProducts;
        var shopId = localStorage.getItem("_shopId");
        var page = 1;
        var limit = 100000;
        getProductList();
        $(".js-example-basic-multiple").select2();
        /**
         * Get product from api
         */
        function getProductList() {
            $.ajax({
                url: `<?php echo API_URI_PATH ; ?>/influencer/shops/view/products/${shopId}?page=${page}&limit=${limit}`,
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    setProductsListData(data.shop_product_details.data);
                },
                error: function (request, status, error) {
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }
        /**
         * Set products to product list
         */
        function setProductsListData(products) {
            allProducts = products;
            let appenddata = "";
            for (let index = 0; index < allProducts.length; index++) {
                const product = allProducts[index];
                const productImage = product.influencer_product_images.find((obj) => obj.is_cover_pic == 1);
                const image = productImage ? productImage.image_path : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;

                appenddata += `
                    <div class="col-md-12 col-xl-4 col-lg-4 col-sm-12 product-wrapper" data-id="${product.id}">
                        <a href="#" class="d-block">
                            <div class="product-itme">
                                <div class="product-img-wrapper">
                                    <div class="img-wrapper">
                                        <img src="<?php echo $image_base; ?>/${image}" alt="" />
                                    </div>
                                    <div class="rating-wrapper">
                                        <span class="rating-title">3.5</span>
                                        <span class="rating-star">
                                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" />
                                        </span>
                                    </div>
                                </div>
                                <div class="product-detail-wrapper">
                                    <h4 class="product-name">${product.product_title}</h4>
                                    <h5 class="price">KSH ${product.product_price}</h5>
                                    <span class="product-no-sale">No of Sale: ${product.total_sale}</span>
                                    <button type="button" class="added-product d-none"></button>
                                </div>
                            </div>
                        </a>
                    </div>
                `;
                // appenddata += `<option value="${product.id}">${product.product_title}</option>`;
            }
            $(".products-list").html('');
            $(".products-list").html(appenddata);
            // $(".product-select-box").html(appenddata);
        }

        $(".color-list-item").on("click", function (event) {
            event.preventDefault();
            $(".color-list-item").removeClass("selected-color");
            $("#color").val($(this).attr("data-color"));
            $(this).addClass("selected-color");
        });
        $(document).on("click", ".product-wrapper" ,function () {
            $('.product-wrapper').removeClass('selected');
            $('.product-wrapper .added-product').addClass('d-none');
            $(this).find('.added-product').removeClass('d-none');
            $(this).addClass('selected');
        });
        $(document).on("click", ".continue-btn" ,function () {
            $('#products').val($('.product-wrapper.selected').attr('data-id'))
            setSingleProduct($('.product-wrapper.selected').attr('data-id'));
            $('#productMmodal').modal('hide')
        });
        // $("#products").on("change", function () {
        //     console.log($(this).val(), "id");
        //     setSingleProduct($(this).val());
        // });
        function setSingleProduct(id) {
            const productDetail = allProducts.find((obj) => obj.id == id);
            const productImage = productDetail.influencer_product_images.find((obj) => obj.is_cover_pic == 1);
            const image = productImage ? productImage.image_path : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
            let selectedProduct = ` <div class="product-box row selected-product">
                                    <div class="product-box-inner col-12 col-sm-12 col-md-12">
                                        <div class="product-box-content">
                                            <img src="<?php echo $image_base; ?>/${image}">
                                            <div class="box-content ml-4">
                                                <p>${productDetail.product_title}</p>
                                                <h2>KSH ${productDetail.product_price}</h2>
                                            </div>
                                            <div class="product-rating">
                                                <span class="rating-title">${productDetail.product_details && productDetail.product_details.rating ? productDetail.product_details.rating.toFixed(1) : "0.0"}</span>
                                                <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>`;
            $(".selected-product").html(selectedProduct);
            let images = "";
            for (let index = 0; index < productDetail.influencer_product_images.length; index++) {
                const element = productDetail.influencer_product_images[index];
                if (element.image_path) {
                    images += `<li class="product-image-list-item" data-image="${element.image_path}">
                                <i class="fa fa-check" aria-hidden="true"></i>
                                <img src="<?php echo $image_base; ?>/${element.image_path}" alt="" class="Img-List">
                            </li>`;
                }
            }
            $(".product-image-list").html(images);
            $(".product-image-list-item:nth-child(1)").click();
        }

        $(document).on("click", ".product-image-list-item", function () {
            console.log("Hello1");
            $("#selected-image").val($(this).attr("data-image"));
            $(".product-image-list-item").find("i").css("display", "none");
            $(this).find("i").css("display", "flex");
            // $('#ImgModal').css('display','block');
            // $('#ImgModal img').attr('src', $(this).find('img').attr('src'));
        });

        $(".submit-post").on("click", function () {
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            } else {
                _this.attr("disabled", true);
                $(".schedule-post").attr("disabled", true);
                _this.find("i").removeClass("d-none");
                let products = $("#products").val();
                let selectedImage = $("#selected-image").val();
                let platform = $("#platform").val();
                let caption = $("#caption").val();
                let color = $("#color").val();
                let post = {
                    product_id: products,
                    platform: platform,
                    image: selectedImage,
                    color: color,
                    caption: caption,
                };

                $.ajax({
                    url: "<?php echo API_URI_PATH ; ?>/influencer/post/story",
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                    },
                    dataType: "json",
                    data: post,
                    type: "post",
                    success: function (data) {
                        _this.attr("disabled", false);
                        _this.find("i").addClass("d-none");
                        if (data.status == true) {
                            window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-social.php";
                        }
                    },
                    error: function (request, status, error) {
                        _this.attr("disabled", false);
                        $(".schedule-post").attr("disabled", false);
                        _this.find("i").addClass("d-none");
                    },
                });
            }
        });
        /**
         * @isFormValid - Form validation
         */
        function isFormValid() {
            let isValid = true;
            let products = $("#products").val();
            let selectedImage = $("#selected-image").val();
            let platform = $("#platform").val();
            let caption = $("#caption").val();
            let color = $("#color").val();
            if (!products || products == "") {
                isValid = false;
                $("#products-error").show();
                $("#selected-image-error").show();
                $("#products-error").html("Please select a product");
                $("#products-error").css("color", "red");
                $("#selected-image-error").html("Please select a product");
                $("#selected-image-error").css("color", "red");
            } else {
                $("#products-error").hide();
            }
            if (products) {
                if (!selectedImage || selectedImage == "") {
                    isValid = false;
                    $("#selected-image-error").show();
                    $("#selected-image-error").html("Please select a product image");
                    $("#selected-image-error").css("color", "red");
                } else {
                    $("#selected-image-error").hide();
                }
            }
            if (!platform || platform == "") {
                isValid = false;
                $("#platform-error").show();
                $("#platform-error").html("Please select a platform");
                $("#platform-error").css("color", "red");
            } else {
                $("#platform-error").hide();
            }
            if (!caption || caption == "") {
                isValid = false;
                $("#caption-error").show();
                $("#caption-error").html("Please enter a caption");
                $("#caption-error").css("color", "red");
            } else {
                $("#caption-error").hide();
            }
            if (!color || color == "") {
                isValid = false;
                $("#color-error").show();
                $("#color-error").html("Please select a color");
                $("#color-error").css("color", "red");
            } else {
                $("#color-error").hide();
            }
            return isValid;
        }
        $(".schedule-post").on("click", function () {
            if (!isFormValid()) {
                return false;
            } else {
                let products = $("#products").val();
                let selectedImage = $("#selected-image").val();
                let platform = $("#platform").val();
                let caption = $("#caption").val();
                let color = $("#color").val();
                let post = {
                    product_id: products,
                    platform: platform,
                    image: selectedImage,
                    color: color,
                    caption: caption,
                };
                localStorage.setItem("_postStory", JSON.stringify(post));
                window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-schedule-story.php";
            }
        });
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
