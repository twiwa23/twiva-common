<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">Selected Products</div>
            <div class="dashboard-inner">
                <!--Product Selection-->
                <div class="product-selection pr-5">
                    <div class="title">
                        <h4>Selected Products</h4>
                        <p>Selected products of your eShop</p>
                    </div>

                    <!-- <div class="product-form product-list-btn">
                        <button class="btn-custom-primary" data-shop_id="" id="submit-selected-product"><i class="fa fa-spinner fa-spin text-white mr-2 d-none"></i> Save</button>
                    </div> -->
                </div>

                <!--Product Section-->
                <div class="product-section mt-2">
                    <div class="product-box row product-list">
                        <div class="product-box-inner col-6 col-md-4 col-lg-3">
                            <div class="product-box-content">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg" />
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <button class="white-bttn">Select</button>
                                </div>
                                <div class="product-rating">
                                    <span class="rating-title">5.0</span>
                                    <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="product-box">
                    <h2>Recent Orders</h2>
                    
                    <div class="product-box-inner">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    
                    <div class="product-box-inner">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    
                    <div class="product-box-inner">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    <div class="product-box-inner">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                    <div class="product-box-inner">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                        <div class="box-content">
                            <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                            <h2>KSH 22.99</h2>
                            <span>No of Sale: 14</span>
                        </div>
                    </div>
                    
                </div> -->
                </div>
            </div>

            <!--Page Selection-->
            <!-- <div class="page-selection">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link left-arrow" href="#" disabled><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Left.svg" alt="" /></a>
                    </li>
                    <li class="page-item"><a class="page-link page-link-active" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                    <li class="page-item"><a class="page-link" href="#">7</a></li>
                    <li class="page-item"><a class="page-link" href="#">8</a></li>
                    <li class="page-item"><a class="page-link" href="#">9</a></li>
                    <li class="page-item"><a class="page-link" href="#">10</a></li>
                    <li class="page-item">
                        <a class="page-link right-arrow" href="#"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Right.svg" alt="" /></a>
                    </li>
                </ul>
            </div> -->
        </div>
        <!-- /page content -->
    </div>
    <div class="selected-product-footer-wrapper container-fluid">
		<div class="seleced-product-btn-wrapper"> 
				<div class="btn-wrapper">
					<div class="no-of-select-product">
						<h4 class="total-count">
							0 Product Selected
						</h4>
					</div>
					<div class="next-btn-wrapper">
						<button class="btn-custom-primary" data-shop_id="" id="submit-selected-product">
							Save <i class="fa fa-spinner fa-spin text-white mr-2 d-none"></i> 
                        </button>
					</div>
				</div>	
		</div>
	</div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function () {
        var allProducts;
        var allSelectedProducts = [];
        getSelectedProductList();
        geteShopDetail();
        localStorage.removeItem('_productDetailForPreview')
        function getSelectedProductList() {
            let loader = `<div class="not-found text-center w-100">
                                <h1><i class="fa fa-spinner fa-spin"></i></h1>
                            </div>`;
            $(".product-list").html(loader);
            allSelectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            $('.total-count').text(`${allSelectedProducts.length} Product Selected`)
            setProductsListData(allSelectedProducts);
        }
        function geteShopDetail() {
            $('.page-inner-section').addClass('d-none');
            $('.loader').removeClass('d-none');
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/shops/list",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    $('.loader').addClass('d-none');
                    $('#submit-selected-product').attr('data-shop_id',data.data[0].id);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }
        function setProductsListData(products) {
            allProducts = products;
            allSelectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            let appenddata = "";
            for (let index = 0; index < allSelectedProducts.length; index++) {
                const product = allSelectedProducts[index];
                const productImage = product.product_images.find((obj) => obj.is_cover_pic == 1);
                const image = productImage ? productImage.image : `<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg`;
                appenddata += `<div class="product-box-inner  col-12 col-sm-6 col-md-4 col-lg-3">
                            <div class="product-box-content">
                                <a class="remove-items" href="javascript:void(0);" data-id="${product.id}"><i class="fa fa-times text-white"></i></a>
                                <img class="thumb" src="<?php echo $image_base; ?>/${image}" height="216"/>
                                <div class="box-content">
                                    <p>${product.name}</p>
                                    <h2>KSH ${product.price}</h2>
                                    <button class="white-bttn selected-product" data-id="${product.id}">Personalise</button>
                                </div>`;
                                if(product.rating!=0){
                                appenddata +=`<div class="product-rating">
                                    <span class="rating-title">${product.rating ? product.rating.toFixed(1) : '0.0'}</span>
                                    <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt="" /></span>
                                </div>`;
                                }
                                appenddata +=`</div>
                        </div>`;
            }
            $(".product-list").html(appenddata);
        }
        $(document).on('click','.remove-items',function(){
            let _this = $(this);
            let id = _this.attr('data-id');
            let index = allSelectedProducts.findIndex(product => product.id == id);
            allSelectedProducts.splice(index, 1);
            $('.total-count').text(`${allSelectedProducts.length} Product Selected`)
            localStorage.setItem('selectedProduct', JSON.stringify(allSelectedProducts));
            _this.closest('.product-box-inner').remove();
            if(allSelectedProducts.length == 0){
                window.history.back()
            }
        })
        $(document).on('click','.selected-product',function(){
            let _this = $(this);
            let id = _this.attr('data-id');
            let product = allSelectedProducts.find(product => product.id == id);
            localStorage.setItem('_productDetail', JSON.stringify(product));
            window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-personalise-product.php`;
        })
        $(document).on('click','#submit-selected-product',function(){
            let _this = $(this);
            let shop_id = $(this).attr('data-shop_id');
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            let products = [];
            let selectedProducts = localStorage.getItem('selectedProduct') ? JSON.parse(localStorage.getItem('selectedProduct')) : [];
            for (let index = 0; index < selectedProducts.length; index++) {
                const product = selectedProducts[index];
                let productImagesFormat = [];
                for (let indexInner = 0; indexInner < product.product_images.length; indexInner++) {
                    const productImage = product.product_images[indexInner];
                    productImagesFormat.push({image: productImage.image, is_cover_pic: productImage.is_cover_pic})
                }
                let productInfo = {
                    product_id :  product.id,
                    product_images: productImagesFormat,
                    product_title : product.name,
                    product_price : product.price,
                    product_description : product.description
                }
                products.push(productInfo)
            }
            const finalObject = {
                shop_id,
                products
            }
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/products/add",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                dataType: "json",
                data: finalObject,
                type: "post",
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        localStorage.removeItem('_productDetail');
                        localStorage.removeItem('selectedProduct');
                        window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php";
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                },
            });
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
