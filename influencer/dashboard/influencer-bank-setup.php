<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col add-product-page" role="main">
            <!--********** Breadcrumb Start ***********-->
            <div class="breadcrumb-wrapper">
                <ul class="breadcrumb">
                    <li><a href="#">Account Setting</a></li>
                    <li class="active">Bank Setup</li>
                </ul>
            </div>

            <!--**********  Breadcrumb End ***********-->

            <div class="dashboard-inner">
                <div class="bank-setup-form">
                    <h3 class="subscription-title">
                        Where would you like to get paid?
                    </h3>
                    <form>
                        <div class="account-setup">
                            <div class="account-form input-fields">
                                <h5 class="alert alert-danger d-none w-100" role="alert" id="errorMessage"></h5>
                                <h5 class="alert alert-success d-none w-100" role="alert" id="successMessage"></h5>
                                
                                <div class="form-field">
                                    <label>Bank Name</label>
                                    <select class="banksName " id="bank_name">
                                        <option>Bank Name</option>
                                    </select>
                                    <h5 id="bank_name-error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>Branch Name</label>
                                    <input type="text" id="branch_name" placeholder="Branch Name" maxlength="60">
                                    <h5 id="branch_error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>City</label>
                                    <input type="text" id="city" placeholder="City" maxlength="60">
                                    <h5 id="city_error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>Account Number</label>
                                    <input id="account_number" type="text" placeholder="Account Number"/>
                                    <h5 id="account_number-error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>Re - Enter Account Number</label>
                                    <input id="re_account_number" type="text" placeholder="Re - Enter Account Number" />
                                    <h5 id="re_account_number-error" class="empty-field-error m-0"></h5>
                                </div>
                                <div class="form-field">
                                    <label>Recipient Name</label>
                                    <input id="recipient_name" type="text" placeholder="Recipient Name" />
                                    <h5 id="recipient_name-error" class="empty-field-error m-0"></h5>
                                </div>
                                <!-- <div class="form-field">
                                    <label>IFSC Code</label>
                                    <input id="ifsc" type="text" placeholder="IFSC Code" />
                                    <h5 id="ifsc-error" class="empty-field-error"></h5>
                                </div> -->
                            </div>
                            <div class="button-sec right-align-btn d-block">
                                <button id="save-account" type="submit"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>   Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        var banksList;
        var banksDetail;
        getBanckDetail();
        $('.loader').removeClass('d-none')
        $('.bank-setup-form').css({cursor: 'inherit', opacity: .7})
        $('.bank-setup-form').css({cursor: 'no-drop'})
        $('.bank-setup-form *').css({cursor: 'no-drop'})
        function getBanckDetail() {
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/bank/details",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function (data) {
                    $('#save-account').html('<i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>   Update')
                    banksDetail = data.data
                    getBanckList(banksDetail);
                },
                error: function (request, status, error) {
                    getBanckList({});
                    $('.loader').addClass('d-none')
                    $('.bank-setup-form').css({cursor: 'inherit', opacity: 1})
                    $('.bank-setup-form *').css({cursor: 'inherit'})
                    $('#save-account').html('<i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>   Save')
                    console.log(request.responseJSON.message);
                },
            });

        }
        function getBanckList(banksDetail) {
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/bank/list",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function (data) {
                    banksList = data.data
                    setData(banksList,banksDetail);
                },
                error: function (request, status, error) {
                    console.log(request.responseJSON.message,'ssssss');
                },
            });

        }
        function setData(banksListDate,banksDetail) {
            let appenddata = "";
            for (var i = 0; i < banksListDate.length; i++) {
                if(banksDetail.bank_id && (banksDetail.bank_id == banksListDate[i].id)){
                    appenddata += `<option selected="" value="${banksListDate[i].id}">${banksListDate[i].bank_name}</option>`;
                }else{
                    appenddata += `<option value="${banksListDate[i].id}">${banksListDate[i].bank_name}</option>`;
                }
            }
            $("#account_number").val(banksDetail.account_number);
            $("#re_account_number").val(banksDetail.account_number);
            $("#recipient_name").val(banksDetail.recipient_name);
            //$("#ifsc").val(banksDetail.ifsc_code);
            $("#branch_name").val(banksDetail.branch_name);
            $("#city").val(banksDetail.city);
            $("#bank_name").html(appenddata);
            $('.bank-setup-form').css({cursor: 'inherit', opacity: 1})
            $('.bank-setup-form *').css({cursor: 'inherit'})
            $('.loader').addClass('d-none')
        }
        
        function isFormValid() {
            let isValid = true;
            let bank_name = $("#bank_name").val();
            let account_number = $("#account_number").val();
            let re_account_number = $("#re_account_number").val();
            let recipient_name = $("#recipient_name").val();
            let ifsc = 'NA';//$("#ifsc").val();
            let branch_name = $("#branch_name").val();
            let city = $("#city").val();
            if(branch_name == ""){
                isValid = false;
                $("#branch_error").show();
                $("#branch_error").html("Please enter branch name");
                $("#branch_error").css("color", "red");
            } else {
                $("#branch_error").hide();
            }

            if(city == ""){
                isValid = false;
                $("#city_error").show();
                $("#city_error").html("Please enter city");
                $("#city_error").css("color", "red");
            } else {
                $("#city_error").hide();
            }

            if (bank_name == "") {
                isValid = false;
                $("#bank_name-error").show();
                $("#bank_name-error").html("Please select your bank");
                $("#bank_name-error").css("color", "red");
            } else {
                $("#bank_name-error").hide();
            }
            if (account_number == "") {
                isValid = false;
                $("#account_number-error").show();
                $("#account_number-error").html("Please enter your account number");
                $("#account_number-error").css("color", "red");
            } else {
                if (account_number.length < 8) {
                    isValid = false;
                    $("#account_number-error").show();
                    $("#account_number-error").html("Please enter a valid account number");
                    $("#account_number-error").css("color", "red");
                }else{
                    $("#account_number-error").hide();
                }
            }
            if (re_account_number == "") {
                isValid = false;
                $("#re_account_number-error").show();
                $("#re_account_number-error").html("Please confirm your account number");
                $("#re_account_number-error").css("color", "red");
            } else {
                $("#re_account_number-error").hide();
            }
            if (account_number != "" && re_account_number != "") {
                if(account_number != re_account_number){
                    isValid = false;
                    $("#re_account_number-error").show();
                    $("#re_account_number-error").html("Account number did not match");
                    $("#re_account_number-error").css("color", "red");
                }
            }
            if (recipient_name == "") {
                isValid = false;
                $("#recipient_name-error").show();
                $("#recipient_name-error").html("Please enter recipient name");
                $("#recipient_name-error").css("color", "red");
            } else {
                $("#recipient_name-error").hide();
            }
            if (ifsc == "") {
                isValid = false;
                $("#ifsc-error").show();
                $("#ifsc-error").html("Please enter your IFSC code");
                $("#ifsc-error").css("color", "red");
            } else {
                $("#ifsc-error").hide();
            }
            return isValid;
        }

        $('#save-account').on('click', function(event){
            event.preventDefault();
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            $("#errorMessage").addClass("d-none");
            $("#successMessage").addClass("d-none");
            let bank_name = $("#bank_name").val();
            let account_number = $("#account_number").val();
            let re_account_number = $("#re_account_number").val();
            let recipient_name = $("#recipient_name").val();
            let ifsc = 'NA';//$("#ifsc").val();
            let branch_name = $("#branch_name").val();
            let city = $("#city").val();
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/bank/setup",
                headers: { 
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "post",
                data: {
                    bank_id: bank_name,
                    account_number: account_number,
                    recipient_name: recipient_name,
                    ifsc_code: ifsc,
                    branch_name: branch_name,
                    city: city,
                },
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        $("#successMessage").removeClass("d-none");
                        $("#successMessage").html(data.message);
                        setTimeout(() => {
                            $("#successMessage").addClass("d-none");
                        }, 3000);
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#errorMessage").removeClass("d-none");
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                },
            });
        })
    });
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
