<?php $class = "influencer-select-product"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<div class="">
    <div class="dashboard_container">
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!-- page content -->
        <div class="right_col add-product-page" role="main">
            <div class="page-title">Refer & Earn</div>

            <div class="refer-content-box-wrapper">
                <div class="refer-code-wrapper">
                    <div class="refer-item text-center">
                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/gift-box.svg" alt="" />
                    </div>
                    <div class="content text-center">
                        <p>
                        Get rewarded for your referrals
                        </p>
                    </div>
                    <p class="refer-code-content d-none">
                        I trust you are well. I am using Twiva app, and I thought you would really like it. You can use this code: <span id="refer-conde-id"></span> to sign up after you download the app. You can download the app here: https://play.google.com/store/apps/details?id=com.twiva.ke</p>
                    <div class="refer-code">
                      <h5 class="text-uppercase">
                      </h5>
                    </div>
                    <div class="text-center">
                        <button class="refer-code-copy-btn">
                            Tap to copy
                        </button>
                    </div>
                    <div class="refer-note mt-2">
                        <p class="text-center">Earn 2000 points for every business that you refer</p>
                        <p class="text-center">Earn 20 points for every influencer you refer</p>
                        <p class="text-center"> 2000 points = 1000 Ksh </p>
                    </div>
                    <div class="wait-c">
                        <p>
                             But wait, there's more...
                        </p>
                    </div>
                    <div class="refer-note">
                        <p class="text-center"> There's always points waiting to be collected. </p>
                        <p class="text-center">
                        Check back often to see new promotions.
                        </p>
                    </div>

                    <div class="total-point">
                        <p class="text-center"> You have </p>
                        <p class="text-center" id="referral_points">
                        0 Points
                        </p>
                    </div>
                    <center>
                    <h5 class="login-error alert alert-success" role="alert" id="success" style="display:none;margin-top: 20px;text-align:center;"></h5>
                    <h5 class="login-error alert alert-danger" role="alert" id="error" style="display:none; margin-top: 20px;text-align:center;"></h5>
                    </center>
                    <input type="hidden" id="points">
                    <div class="button-sec  text-center d-flex justify-content-center text-center m-4">
                    <button type="submit" class="red-btn w-100 m-0" id="my-form">Redeem</button>
                </div>


                </div>
            </div>
            <script>
        $(function() {
            $("#my-form").click(function() {
                var points = $("#points").val();
                if (points <= 0) {
                    $('#error').show();
                    $('#error').append('Points should be greater than zero');
                } else {
                    var json = {
                        "points": points
                    }
                    redeem(json);

                    function redeem(json) {
                        __ajax_httpproduct("influencer/redeem-referral", json, headers(), AJAX_CONF.apiType.POST, "", __success_product);
                    }

                    function __success_product(response) {
                        $('#success').show();
                        $('#success').append(response.message);
                    }
                }
            });
        });
    </script>
            <!-- <div class="social-share-item-wrapper">
                <h4>
                    Share Via
                </h4>

                <div class="social-share-item">
                    <a href="#" class="item">
                        <span class="copy-link"> </span>
                        <span>
                            Copy Code
                        </span>
                    </a>
                    <a href="#" class="item">
                        <span class="email">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                        <span>
                            Email
                        </span>
                    </a>
                    <a href="#" class="item">
                        <span class="facebook">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </span>
                        <span>
                            Facebook
                        </span>
                    </a>

                    <a href="#" class="item">
                        <span class="twitter">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </span>
                        <span>
                            Tweet
                        </span>
                    </a>

                    <a href="#" class="item">
                        <span class="linked">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                        </span>
                        <span>
                            Linked In
                        </span>
                    </a>
                </div>
            </div> -->
        </div>
        <!-- /page content -->
    </div>
</div>

<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    // $(document).ready(function(){
    //     var user = localStorage.getItem('_userInfo');
    //     var parsedUser = JSON.parse(user)
    //     $('#referral_points').html( parsedUser.points + '  Points');
        
    //     var referalCode = localStorage.getItem('_userReferalCode');
    //     $('.refer-code h5').text(referalCode);
    //     $('.refer-code-content #refer-conde-id').text(referalCode);
    //     var referalCodeContent = $('.refer-code-content').text();
    //     $('.refer-code-copy-btn').on('click', function(){
    //         let _this = $(this);
    //         navigator.clipboard.writeText(referalCodeContent).then(function() {
    //             _this.text('Copied!').addClass('text-success')
    //         }, function(err) {
    //             console.error('Async: Could not copy text: ', err);
    //         });
    //     })
    // });

    $(document).ready(function(){
        var user = localStorage.getItem('_userInfo');
        var parsedUser = JSON.parse(user)
        $('#referral_points').html( parsedUser.points + '  Points');
        $("#points").val(parsedUser.points);
        var referalCode = localStorage.getItem('_userReferalCode');
        $('.refer-code h5').text(referalCode);
        $('.refer-code-content #refer-conde-id').text(referalCode);
        var referalCodeContent = $('.refer-code-content').text();
        $('.refer-code-copy-btn').on('click', function() {
            var temp = $("<input>");
            $("body").append(temp);
            temp.val($('.refer-code-content').text()).select();
            let _this = $(this);
            document.execCommand("copy");
            _this.text('Copied!').addClass('text-success');
        });
    });

</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
