<?php $class = "influencer-eshop-page personalise-product-wrapper"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
    <!--Main Section Start-->
    <div class="">
        <div class="dashboard_container">
            <!--Left Column-->
            <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

            <!--Right Column-->
            <!-- Page Content -->
            <div class="right_col add-product-page">
                <div class="page-title">
                    <a href="">
                        <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""></span>
                        Preview
                    </a>
                </div>
                <div class="dashboard-inner">

                    <!--Product Banner-->
                    <div class="product-banner jumbotron">
                        <img class="banner-image" src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 7125.png" alt="Banner">
                        <div class="banner-section">
                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/Ellipse 123.svg" alt="">
                            <div class="banner-content">
                                <h3>Barone LLC.</h3>
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iusto, enim officia expedita 
                                    exercitationem illum ratione quo autem, placeat labore magnam dolores suscipit earum 
                                    optio, mollitia excepturi recusandae corporis nostrum voluptates.
                                </p>
                                <!-- <div class="common-button">
                                    <button class="white-bttn">Add Products</button>
                                    <button class="white-bttn">Edit eShop</button>
                                </div> -->
                            </div>
                        </div>

                        <!-- <div class="alert alert-success">
                            <p class="alert-content">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et saepe animi, porro inventore incidunt neque consequuntur expedita dolor voluptas adipisci ipsum suscipit repudiandae ut praesentium laudantium eaque laboriosam similique aliquam.</p>
                            <button class="alert-button">Connect Now</button>
                        </div> -->
                    </div>


                    <!--Product Section-->
                    <div class="product-section">
                        <div class="product-box row">
                        

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>


                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>


                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product-box-inner col-6 col-md-4 col-lg-3">
                                <div class="product-box-content">
                                    <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                    <div class="box-content">
                                        <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                        <h2>KSH 22.99</h2>
                                        <button class="white-bttn">Select</button>
                                    </div>
                                    <div class="product-rating">
                                        <span class="rating-title">5.0</span>
                                        <span class="rating-star"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/star.svg" alt=""></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="product-box">
                            <h2>Recent Orders</h2>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                            <div class="product-box-inner">
                                <img src="<?php echo IMAGES_URI_PATH; ?>/card-images/product.jpg">
                                <div class="box-content">
                                    <p>Canon EOS 80D 24.2MP Digital SLR Camera</p>
                                    <h2>KSH 22.99</h2>
                                    <span>No of Sale: 14</span>
                                </div>
                            </div>
                            
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
