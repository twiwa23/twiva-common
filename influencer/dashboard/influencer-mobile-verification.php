<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY . "/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Menu-->
        <?php include INFLUENCER_DIRECTORY . "/sidebar/sidebar-dashboard.php"; ?>
    </div>
</div>


<div class="container">


    <!-- <input id="phone" type="tel" name="phone" /> -->


</div>

<div class="login" id="mobile-verification">

    <!-- <div class="back-link">
            <a href="#">Account Setting<img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-right.svg" alt="">Phone Verification</a>
        </div> -->

    <!--********** Breadcrumb Start ***********-->
    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href="#">Account Settings</a></li>
            <li class="active">Phone Verification</li>
        </ul>
    </div>

    <!--**********  Breadcrumb End ***********-->

    <div class="container">
        <div class="login-inner">

            <div class="login-left">
                <!-- <img src="<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->
            </div>
            <h5 class="alert alert-danger d-none w-100 mt-5 mb-0 ml-0" role="alert" id="errorMessage"></h5>
            <h5 class="alert alert-success d-none w-100 mt-5 mb-0 ml-0" role="alert" id="successMessage"></h5>

            <div class="login-right" id="verificationFormSection">
                <!-- <div class="login-section">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                        <p>Please enter a new password and confirm it to reset.</p>
                        <h5 id="passwordcheck" style="display: none;"></h5>
                        <label>Please verify your phone number</label>
                        <div class="input-field">
                            <div class="mobile-number-wrapper d-flex">
                                <select class="country-code">
                                    <option value="+1">+1</option>
                                </select> 

                               <input type="tel" name="phone" type="tel" class="mobile-no-input " placeholder="Enter your Mobile number">
                            </div>
                           
                            <div class="bottom-button">
                                <button>Continue</button>
                            </div> 
                        </div>                              
                        
                    </div> -->
                <div class="login-section" style="margin-top: 3rem !important;">
                    <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                    <p>Please enter a new password and confirm it to reset.</p>
                    <h5 id="passwordcheck" style="display: none;"></h5>
                    <label class="mb-4">Please Verify your Phone Number</label>
                    <div class="input-field flex-column align-items-end mb-2">
                        <div class="mobile-number-wrapper d-flex mb-3">
                            <select class="country-code">
                                <option value="+1">+1</option>
                            </select>

                            <input id="tel" name="phone" type="tel" class="mobile-no-input " value="" autocomplete="off" maxlength="10">
                        </div>

                        <div class="bottom-button w-100 d-flex align-items-center justify-content-between">
                            <!-- <button>Verify</button> -->
                            <a href="#" class="no-change" id="cancle-btn">Cancel</a>
                            <button id="verificationBtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Click to Verify</button>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </div>


    <div class="footer-login">
        Copyright © 2021 Twiva. All Rights Reserved.
        <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
    </div>
</div>

<?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function() {
        var counryCodeData;
        var userDetail;
        getUserDetail();
        $('.loader').removeClass('d-none')
        $('.login-section').css({
            cursor: 'inherit',
            opacity: .7
        })
        $('.login-section').css({
            cursor: 'no-drop'
        })
        $('.login-section *').css({
            cursor: 'no-drop'
        })

        function getUserDetail() {
            $.ajax({
                url: "<?php echo API_URI_PATH; ?>/influencer/details",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function(data) {
                    userDetail = data.data;
                    $("#tel").val(userDetail.phone_number);
                    getCountryCodeList(userDetail);
                },
                error: function(request, status, error) {
                    getCountryCodeList({});
                    $('.loader').addClass('d-none')
                    $('.login-section').css({
                        cursor: 'inherit',
                        opacity: 1
                    })
                    $('.login-section *').css({
                        cursor: 'inherit'
                    })
                    $('#save-account').html('<i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>   Save')
                    console.log(request.responseJSON.message);
                },
            });

        }

        function getCountryCodeList(userDetail) {
            $.ajax({
                url: "<?php echo API_URI_PATH; ?>/influencer/country-code/list",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "get",
                success: function(data) {
                    counryCodeData = data.data;
                    setData(data.data, userDetail);
                },
                error: function(request, status, error) {
                    console.log(request.responseJSON.message, 'ssssss');
                },
            });

        }
        //${counryCodeData[i].country_code}
        function setData(counryCodeData, userDetail) {
            let verifactionForm = '';
            if (!userDetail.is_phone_verified) {
                let appenddata = "";
                for (var i = 0; i < counryCodeData.length; i++) {
                    appenddata += `<option value="${counryCodeData[i].phone_code}">  +${counryCodeData[i].phone_code}</option>`;
                }
                verifactionForm += `
                    <div class="login-section" style="margin-top: 3rem !important;">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                        <p>Please enter a new password and confirm it to reset.</p>
                        <h5 id="passwordcheck" style="display: none;"></h5>
                        <label>Please Verify your Phone Number</label>
                        <div class="input-field flex-column align-items-end mb-2">
                            <div class="mobile-number-wrapper d-flex mb-3">
                                <select class="country-code" id="country_code">
                                    ${appenddata}
                                </select> 

                                <input id="phone_number" type="tel" name="phone" type="tel" class="mobile-no-input " value="" autocomplete="off" maxlength="10">
                            </div>
                            
                            <div class="bottom-button w-100 d-flex align-items-center justify-content-end">
                                <button id="verificationBtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Click to Verify</button>
                            </div> 
                        </div>   
                        <h5 id="phone_number-error" class="empty-field-error text-center"></h5>                                                     
                    </div>
                `;
            } else {
                let appenddata = "";
                for (var i = 0; i < counryCodeData.length; i++) {
                    if (userDetail.phone_code == counryCodeData[i].phone_code) {
                        appenddata += `<option selected="" value="${counryCodeData[i].phone_code}">+${counryCodeData[i].phone_code}</option>`;
                    } else {
                        appenddata += `<option value="${counryCodeData[i].phone_code}">+${counryCodeData[i].phone_code}</option>`;
                    }
                }
                verifactionForm += `
                    <div class="login-section" style="margin-top: 3rem !important;">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                        <p>Please enter a new password and confirm it to reset.</p>
                        <h5 id="passwordcheck" style="display: none;"></h5>
                        <label>Please Verify your Phone Number</label>
                        <div class="input-field flex-column align-items-end mb-2">
                            <div class="mobile-number-wrapper d-flex mb-3">
                                <select disabled class="country-code" id="country_code">
                                    ${appenddata}
                                </select> 

                                <input disabled value="${userDetail.phone_number}" id="phone_number" type="tel" name="phone" type="tel" class="mobile-no-input " autocomplete="off" maxlength="10">
                                
                            </div>
                            
                            <div class="bottom-button w-100 d-flex align-items-center justify-content-between">
                                <a href="#" class="no-change" id="changeMobileNumber">Change Number</a>
                                <button class="verified-btn text-success"><i class="fa fa-check-circle mr-2" style="color: #28a745; font-size: 16px;"></i>Verified</button>
                            </div> 

                        </div>    
                        
                    </div>
                `;
            }
            $('#verificationFormSection').html(verifactionForm);
            let appenddata = "";
            for (var i = 0; i < counryCodeData.length; i++) {
                if (userDetail.bank_id && (userDetail.bank_id == counryCodeData[i].id)) {
                    appenddata += `<option selected="" value="${counryCodeData[i].id}">${counryCodeData[i].bank_name}</option>`;
                } else {
                    appenddata += `<option value="${counryCodeData[i].id}">${counryCodeData[i].bank_name}</option>`;
                }
            }
            // userDetail.is_phone_verified
            $('.login-section').css({
                cursor: 'inherit',
                opacity: 1
            })
            $('.login-section *').css({
                cursor: 'inherit'
            })
            $('.login-section label').addClass('mb-4')
            $('.loader').addClass('d-none')
        }

        function isFormValid() {
            let isValid = true;
            let phone_number = $("#phone_number").val();
            let country_code = $("#country_code").val();

            if (phone_number == "") {
                isValid = false;
                $("#phone_number-error").show();
                $("#phone_number-error").html("Please Enter your Phone Number");
                $("#phone_number-error").css("color", "red");
            } else {
                $("#phone_number-error").hide();
            }
            return isValid;
        }

        $(document).on('click', '#changeMobileNumber', function(event) {
            let appenddata = "";
            let verifactionForm = '';
            for (var i = 0; i < counryCodeData.length; i++) {
                appenddata += `<option value="${counryCodeData[i].phone_code}">+${counryCodeData[i].phone_code}</option>`;
            }
            verifactionForm += `
                <div class="login-section" style="margin-top: 3rem !important;">
                    <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                    <p>Please enter a new password and confirm it to reset.</p>
                    <h5 id="passwordcheck" style="display: none;"></h5>
                    <label class="mb-4">Please Verify your Phone Number</label>
                    <div class="input-field flex-column align-items-end mb-2">
                        <div class="mobile-number-wrapper d-flex mb-3">
                            <select class="country-code" id="country_code">
                                ${appenddata}
                            </select> 

                            <input id="phone_number" type="tel" name="phone" type="tel" class="mobile-no-input" autocomplete="off" maxlength="10">
                        </div>
                        
                        <div class="bottom-button w-100 d-flex align-items-center justify-content-between">
                            <a href="#" class="no-change" id="cancle-btn">Cancel</a>
                            <button id="verificationBtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>  Click to Verify</button>
                        </div> 
                    </div>   
                    
                    <h5 id="phone_number-error" class="empty-field-error text-center"></h5>                                                     
                </div>
            `;
            $('#verificationFormSection').html(verifactionForm);
        })
        $(document).on('click', '#cancle-btn', function(event) {
            let verifactionForm = '';
            if (userDetail.is_phone_verified) {
                let appenddata = "";
                for (var i = 0; i < counryCodeData.length; i++) {
                    if (userDetail.phone_code == counryCodeData[i].phone_code) {
                        appenddata += `<option selected="" value="${counryCodeData[i].phone_code}">+${counryCodeData[i].phone_code}</option>`;
                    } else {
                        appenddata += `<option value="${counryCodeData[i].phone_code}">+${counryCodeData[i].phone_code}</option>`;
                    }
                }
                verifactionForm += `
                    <div class="login-section" style="margin-top: 3rem !important;">
                        <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"></div>
                        <p>Please enter a new password and confirm it to reset.</p>
                        <h5 id="passwordcheck" style="display: none;"></h5>
                        <label class="mb-4">Please Verify your Phone Number</label>
                        <div class="input-field flex-column align-items-end mb-2">
                            <div class="mobile-number-wrapper d-flex mb-3">
                                <select disabled class="country-code" id="country_code">
                                    ${appenddata}
                                </select> 

                                <input disabled value="${userDetail.phone_number}" id="phone_number" type="tel" name="phone" type="tel" class="mobile-no-input " autocomplete="off" maxlength="10">
                                
                            </div>
                            
                            <div class="bottom-button w-100 d-flex align-items-center justify-content-between">
                                <a href="#" class="no-change" id="changeMobileNumber">Change Number</a>
                                <button class="verified-btn text-success"><i class="fa fa-check-circle mr-2" style="color: #28a745; font-size: 16px;"></i>Verified</button>
                            </div> 

                        </div>    
                        
                    </div>
                `;
                $('#verificationFormSection').html(verifactionForm);
            }
        })
        $(document).on('click', '#verificationBtn', function(event) {
            event.preventDefault();
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            $("#errorMessage").addClass("d-none");
            $("#successMessage").addClass("d-none");
            let phone_number = $("#phone_number").val();
            let country_code = $("#country_code").val();
            $.ajax({
                url: "<?php echo API_URI_PATH; ?>/influencer/phone/send-otp",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                },
                type: "post",
                data: {
                    phone_number: phone_number,
                    phone_code: country_code
                },
                success: function(data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        let verificationData = {
                            otp: data.otp,
                            phone: phone_number,
                            code: country_code
                        }
                        localStorage.setItem('_verificationData', JSON.stringify(verificationData));
                        $("#successMessage").removeClass("d-none");
                        $("#successMessage").html(data.message);
                        window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-otp-sent.php`
                    }
                },
                error: function(request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#errorMessage").removeClass("d-none");
                    $("#errorMessage").html(request.responseJSON.message);
                    $("#errorMessage").css("color", "red");
                    setTimeout(() => {
                        $("#errorMessage").addClass("d-none");
                    }, 3000);
                },
            });
        })
        $(document).on('keypress', '.mobile-no-input', validateNumber)

        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode === 8 || event.keyCode === 46) {
                return true;
            } else if (key < 48 || key > 57) {
                return false;
            } else {
                return true;
            }
        };

    });
</script>

<?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard.php"; ?>