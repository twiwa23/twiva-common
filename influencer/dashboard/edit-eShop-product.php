<?php $class = "influencer-eshop-page personalise-product-wrapper"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY . "/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY . "/sidebar/sidebar-dashboard.php"; ?>
        <!-- Page Content -->
        <div class="right_col add-product-page edit-eshop-product-wrapper">
            <div class="page-title">
                <a class="back-btn" onclick="window.history.back()">
                    <span><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left.svg" alt=""></span>
                    Edit Product
                </a>
            </div>
            <div class="px-4">
                <div class="personalise-product px-4">
                    <div class="product-details">
                        <form action="">
                            <div class="form-group">
                                <label for="product-title">Product Title</label>
                                <input type="text" class="form-control" id="product-title">
                                <h5 id="title-error" class="empty-field-error"></h5>
                            </div>

                            <div class="input-group">
                                <label for="price">Product Price</label>
                                <div class="input-group-prepend product-price w-100">
                                    <span class="input-group-text">KSH</span>
                                    <input type="text" class="form-control" id="product-price" disabled>
                                </div>
                                <h5 id="price-error" class="empty-field-error"></h5>
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" rows="5" ></textarea>
                                <h5 id="desc-error" class="empty-field-error"></h5>
                            </div>

                            <div class="common-button mt-5">
                                <button class="purple-btn submit-perdonalise"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i>Save</button>
                                <!-- <button class="white-bttn product-preview">Preview</button> -->
                            </div>
                        </form>
                    </div>
                    <div class="product-images">
                        <label for="">Add Images</label>
                        <div id="primary-slider" class="splide">
                            <div class="splide__track">
                                <ul class="splide__list main-slide">
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                    <li class="splide__slide">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 1490.png">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="secondary-slider" class="splide">
                            <div class="splide__track">
                                <ul class="splide__list thumb-slides">
                                </ul>
                            </div>
                        </div>
                        <h5 id="error"></h5>
                    </div>
                </div>
            </div>

        </div>
        <!-- /page content -->
    </div>
</div>

<?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard-script.php"; ?>


<script>
    $(document).ready(function() {
        var allSelectedProducts = [];
        var productDetail = localStorage.getItem('_productDetailEdit');
        if (productDetail) {
            productDetail = JSON.parse(productDetail);
            let backLink = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/eShop-product-detail.php/?request=${productDetail.product_id}&request1=${productDetail.shop_id}`;
            $('.back-btn').attr('href', backLink);
            $('#product-title').val(productDetail.product_title);
            $('#product-price').val(productDetail.product_price);
            $('#description').val(productDetail.product_description);
            var appendSliderData = "";
            productDetail.influencer_product_images.length ?
                productDetail.influencer_product_images.map((image) => {
                    if (image.image_path) {
                        let classIsCover = image.is_cover_pic ? 'coverImage' : '';
                        appendSliderData += `<li class="splide__slide slider_slide ${classIsCover}">
                                                <img src="<?php echo $image_base; ?>/${image.image_path}">`;
                                                if(image.is_cover_pic!=1){
                                                appendSliderData += `<span class="removeImg" data-image="${image.image_path}"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/x.svg"></span style="display:none;">`;
                                                }
                                                appendSliderData += `</li>`;
                    }
                }) :
                null;
            var sliderList = ` <li class="product-image-list-item mr-2" >
                                            <label for="add-image">
                                                <input type="file" id="add-image" accept="image/*" style="display: none;">
                                                <img height="60" width="75" src="<?php echo IMAGES_URI_PATH; ?>/product-img/Frame 7087.png" alt="">
                                            </label>
                                        </li>`;
            sliderList += appendSliderData;
            $('.main-slide').html(appendSliderData);
            $('.thumb-slides').html(sliderList);
            initSlider();
        }

        var _URL = window.URL || window.webkitURL;
        var width,height;
        $('#add-image').on('change', function() {
            var file_data, img;
            if ((file_data = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file_data);
                img.onload = function () {
                    var fileSize = Math.round(file_data.size / 1024);
                     width = this.width;
                     height = this.height;
                     if(width<500 || height<500 || fileSize>2000){
                        $("#error").show();
                        if(fileSize>2000){
                            $("#error").html('Image size should be maximum 2 MB');
                        }else{
                            $("#error").html('Minimum image should be 500x500 pixels');
                        }
                    }
                    else{
                        $("#error").hide();
                    // var file_data = $(this).prop("files")[0];
                    if (file_data) {
                        $('.product-images').css('opacity', '.2');
                        $('.product-images').css('cursor', 'progress');
                        $('.product-image-list-item').css('cursor', 'not-allowed');
                        $('.submit-perdonalise').attr('disabled', true);
                    }
                    var form_data = new FormData();
                    form_data.append("image", file_data);
                    $.ajax({
                        url: "<?php echo API_URI_PATH; ?>/upload-image",
                        dataType: "text",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: "post",
                        success: function(data) {
                            
                            let imageData = JSON.parse(data);
                            let imagePreview = "<?php echo $image_base; ?>" + imageData.image;
                            let slide = `<li class="splide__slide slider_slide">
                                                    <img src="<?php echo $image_base; ?>/${imageData.image}">
                                                    <span class="removeImg" data-image="${imageData.image}"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/x.svg"></span>
                                                </li>`;
                            sliderList += slide;
                            appendSliderData += slide;
                            $('.main-slide').html(appendSliderData);
                            $('.thumb-slides').html(sliderList);
                            let productImage = productDetail.influencer_product_images;
                            /*$('.removeImg').on('click', function(){
                                    $(this).parent('.slider_slide').remove();
                                    var image = $(this).data("image");
                                    var index = productDetail.influencer_product_images.findIndex(function(e) {
                                    return e.image_path == image;
                                    })
                                    var sli_id = index+1;
                                    $("#primary-slider-slide0"+sli_id).remove();
                                    $("#primary-slider-slide01").addClass('is-active is-visible');
                                    productDetail.influencer_product_images.splice(index, 1);
                            });*/
                            initSlider();
                            $('.product-images').css('opacity', '1');
                            $('.product-images').css('cursor', 'default');
                            $('.product-image-list-item').css('cursor', 'default');
                            $('.submit-perdonalise').attr('disabled', false);
                            productImage.push({
                                is_cover_pic: 0,
                                image_path: imageData.image
                            })
                            productDetail.influencer_product_images = productImage;
                        },
                    });
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });
        //Vikash delete image
        /*let productImage = productDetail.influencer_product_images;
        $('.removeImg').on('click', function(){
                $(this).parent('.slider_slide').remove();
                var image = $(this).data("image");
                var index = productDetail.influencer_product_images.findIndex(function(e) {
                return e.image_path == image;
                })
                var sli_id = index+1;
                $("#primary-slider-slide0"+sli_id).remove();
                $("#primary-slider-list li:first").addClass('is-active is-visible');
                productDetail.influencer_product_images.splice(index, 1);
        });*/
        // $(document).on('click', '.slider_slide', function(){
        //     let coverPic = productDetail.influencer_product_images[$(this).index()-1];
        //     let allProductImages = productDetail.influencer_product_images;
        //     for (let index = 0; index < allProductImages.length; index++) {
        //         if(index == ($(this).index()-1)){
        //             allProductImages[index].is_cover_pic = 1;
        //         }else{
        //             allProductImages[index].is_cover_pic = 0;
        //         } 
        //     }
        //     productDetail.influencer_product_images = allProductImages;
        // });
        $('.submit-perdonalise').on('click', function(event) {
            event.preventDefault();
            if (!isFormValid()) {
                return false;
            }
            let _this = $(this);
            localStorage.setItem('_productDetailEdit', JSON.stringify(productDetail));
            $(this).attr('disabled', true);
            $(this).find('i').removeClass('d-none');
            var title = $('#product-title').val();
            var price = $('#product-price').val();
            var description = $('#description').val();
            productDetail.product_title = title;
            productDetail.product_price = price;
            productDetail.product_description = description;
            let productData = {
                shop_id: productDetail.shop_id,
                product_id: productDetail.product_id,
                product_title: productDetail.product_title,
                product_price: productDetail.product_price,
                product_description: productDetail.product_description

            };
            let productImagesFormat = [];
            for (let index = 0; index < productDetail.influencer_product_images.length; index++) {
                const productImage = productDetail.influencer_product_images[index];
                productImagesFormat.push({
                    image: productImage.image_path,
                    is_cover_pic: productImage.is_cover_pic
                })
            }
            productData.product_images = productImagesFormat;
            $.ajax({
                url: "<?php echo API_URI_PATH; ?>/influencer/products/personalise/edit",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                data: productData,
                type: "post",
                success: function(data) {
                    _this.attr('disabled', false);
                    _this.find('i').addClass('d-none');
                    window.history.back();
                    // window.location.href = `<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/eShop-product-detail.php/?request=${productData.product_id}&request1=${productData.shop_id}`
                },
            });
        });
        $('.product-preview').on('click', function(event) {
            event.preventDefault();
            window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH; ?>/influencer-product-detail.php";
        })

        function initSlider() {
            var secondarySlider = new Splide('#secondary-slider', {
                fixedWidth: 100,
                height: 60,
                gap: 10,
                cover: true,
                isNavigation: true,
                focus: 'center',
                breakpoints: {
                    '600': {
                        fixedWidth: 66,
                        height: 40,
                    }
                },
            }).mount();

            var primarySlider = new Splide('#primary-slider', {
                type: 'fade',
                heightRatio: 0.5,
                pagination: false,
                arrows: false,
                cover: true,
            }); // do not call mount() here.

            primarySlider.sync(secondarySlider).mount();
        }

        function isFormValid() {
            let isValid = true;
            var title = $('#product-title').val();
            var price = $('#product-price').val();
            var description = $('#description').val();
            if (title == "") {
                isValid = false;
                $("#title-error").show();
                $("#title-error").html("Please enter product title");
                $("#title-error").css("color", "red");
            } 
            else if (title.length <3) {
                isValid = false;
                $("#title-error").show();
                $("#title-error").html("Please fill the title between 3-70 characters limit");
                $("#title-error").css("color", "red");
            } 
            else if (title.length > 70) {
                isValid = false;
                $("#title-error").show();
                $("#title-error").html("Please fill the title between 3-70 characters limit");
                $("#title-error").css("color", "red");
            } 
            else {
                $("#title-error").hide();
            }
            if (price == "") {
                isValid = false;
                $("#price-error").show();
                $("#price-error").html("Please enter product price");
                $("#price-error").css("color", "red");
            } else {
                $("#price-error").hide();
            }
             if (description == "") {
                isValid = false;
                $("#desc-error").show();
                $("#desc-error").html("Please enter product description ");
                $("#desc-error").css("color", "red");
            }
            else if (description.length < 20){
                isValid = false;
                $('#desc-error').show();
                $('#desc-error').html("Please fill the description between 20-250 characters limit");
                $('#desc-error').focus();
                $('#desc-error').css("color","red");
            } 
            else if (description.length > 250){
                isValid = false;
                $('#desc-error').show();
                $('#desc-error').html("Please fill the description between 20-250 characters limit");
                $('#desc-error').focus();
                $('#desc-error').css("color","red");
            }else {
              
                $("#desc-error").hide();
            }
           
            return isValid;
        }
    })

    // function description_check(){
    //     var description_val = $('#description').val();
    //     if(description_val.length <= 250 && description_val.length >= 20){
    //         $('#desc-error').hide();
    //     }
    //     else{
    //         $('#desc-error').show();
    //         $('#desc-error').html("Please fill the description between 20-250 characters limit");
    //         $('#desc-error').focus();
    //         $('#desc-error').css("color","red");
    //         description_err = false;
    //         return false;
    //     }
    // }

    // $('#description').keyup(function(){
    //     description_check();
        
    // });

</script>
<?php include INFLUENCER_DIRECTORY . "/footer/footer-dashboard.php"; ?>