<?php $class = "influencer-eshop-page personalise-product-wrapper eshop-branding-wrapper"; ?>
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-dashboard.php"; ?>
<!--Main Section Start-->
<div class="">
    <div class="dashboard_container">
        <!--Left Column-->
        <?php include INFLUENCER_DIRECTORY."/sidebar/sidebar-dashboard.php"; ?>

        <!--Right Column-->
        <!-- Page Content -->
        <div class="right_col add-product-page">
            <div class="page-title">
                Edit eShop
            </div>

            <div class="product-selection px-4 d-flex brand-header">
                <div class="title">
                    <h4>Brand Your eShop</h4>
                    <p>Define your eShop's unique brand</p>
                </div>
                <!-- <div class="product-form product-list-btn">
                    <button class="btn-custom-primary publish-eshop" id="submit-selected-product"><i class="fa fa-spinner fa-spin text-white mr-2 d-none"></i> Publish</button>
                </div> -->
            </div>

            <div class="personalise-product">
                <div class="form-field-full">
                    <div class="browse-box">
                        <label for="product-featur-img">
                            <input type="file" id="product-featur-img" class="d-none" />
                            <input type="hidden" id="cover-image" />
                            <input type="hidden" id="shop_id" />
                            <div class="browse-box-inner">
                                <div class="browse-content overflow_hidden">
                                    <div class="cover-loader d-none">
                                        <i class="fa fa-spinner fa-spin image-loader-icon"></i>
                                    </div>
                                    <div class="browse-content-inner text-center">
                                        <img src="<?php echo IMAGES_URI_PATH; ?>/icons/image.svg" />
                                        <p>Cover Image</p>
                                        <p>Select cover image here</p>
                                    </div>
                                    <div class="cover-image-preview w-100 d-none">
                                        <img class="w-100" src="https://lh3.googleusercontent.com/proxy/mAQjFqfZ2FXxCgrJ7ug9ATnKMlHI7hPsLD840Isa4wF2CCVCjVPWGvImXOUoeyf3xDGubbjNyFj_AVRlLySfuJvkvQ00JlDGHqsQDRzHmeaTn3jH37FMlw" alt="" />
                                    </div>
                                </div>
                                <!-- uploded image preview  -->
                            </div>
                        </label>
                        <h5 id="cover-error" class="empty-field-error text-center "></h5>
                    </div>
                </div>

                <div class="create-eshop-wrapper mb-5">
                    <div class="eshop-logo-wrapper">
                        <div class="logo-preview">
                            <input type="file" id="logo-feature-img" class="d-none" />
                            <input type="hidden" id="logo-image"  />
                            <label for="logo-feature-img">
                                <div class="logo-loader d-none">
                                    <i class="fa fa-spinner fa-spin image-loader-icon"></i>
                                </div>
                                <img class="default-image" src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 7120.png" alt="" />
                                <div class="user-profile-preview-wrapper d-none">
                                    <img class="w-100" src="<?php echo IMAGES_URI_PATH; ?>/icons/img.jpg" alt="" />
                                </div>
                                <h3 class="image-title">eShop Logo</h3>
                                <p class="image-size">image size 2000 X 1200</p>
                            </label>
                        </div>
                        <h5 id="logo-error" class="empty-field-error text-center mt-5"></h5>
                    </div>

                    <div class="eshop-details-wrapper">
                        <form action="">

                            <div class="form-group">
                                <label for="eshop-name">eShop Name</label>
                                <div class="">
                                    <input type="text" class="form-control" id="eshop-name" />
                                    <h5 id="eShop-name-error" class="empty-field-error"></h5>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <div class="">
                                    <textarea id="description" rows="10" maxlength="500"></textarea>
                                    <h5 id="description-error" class="empty-field-error"></h5>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="font-style" style="display: none;">Font Style</label>
                                <div class="">
                                    <select class="form-control" id="font-style" hidden>
                                        <option value="" selected>Select Font Style</option>
                                        <option value="1">Anton</option>
                                        <option value="2">Source Sans Pro Regular</option>
                                        <option value="3">Notoserif Bold</option>
                                        <option value="4">Ubuntu Regular</option>
                                        <option value="5">Type Wr</option>
                                        <option value="6">Traffolight</option>
                                        <option value="7">Dancing Script Regular</option>
                                        <option value="8">Dancing Script Bold</option>
                                        <option value="9">Cabold Comic Demo</option>
                                        <option value="10">Assassin</option>
                                        <option value="11">Aramis Italic</option>
                                    </select>
                                    <h5 id="font-style-error" class="empty-field-error"></h5>
                                </div>
                            </div>

                            <div class="select-color form-group">
                                <label for="select-color">Select Color</label>
                                <div>
                                    <input type="hidden" id="color" />
                                    <ul class="color-list">
                                        <li class="color-list-item" style="background-color: #fee2e2;" data-color="#fee2e2">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #fca5a5;" data-color="#fca5a5">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #fdba74;" data-color="#fdba74">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #fb923c;" data-color="#fb923c">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #fde047;" data-color="#fde047">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #eab308;" data-color="#eab308">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #86efac;" data-color="#86efac">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #22c55e;" data-color="#22c55e">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #14532d;" data-color="#14532d">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #60a5fa;" data-color="#60a5fa">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #c084fc;" data-color="#c084fc">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #1e3a8a;" data-color="#1e3a8a">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #f472b6;" data-color="#f472b6">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #4c1d95;" data-color="#4c1d95">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #78350f;" data-color="#78350f">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="color-list-item" style="background-color: #000000;" data-color="#000000">
                                            <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                                        </li>
                                    </ul>
                                    <h5 id="color-error" class="empty-field-error"></h5>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->
    </div>
    <div class="selected-product-footer-wrapper container-fluid ">
		<div class="seleced-product-btn-wrapper"> 
				<div class="btn-wrapper">
					<!-- <div class="preview-btn-wrapper">
						<button>
							Preview
						</button>
					</div> -->
					<div class="next-btn-wrapper">
						<button class="publish-eshop">
                        <i class="fa fa-spinner fa-spin mr-2 d-none text-white"> </i>Publish
						</button>
					</div>
				</div>	
		</div>
	</div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard-script.php"; ?>
<script>
    $(document).ready(function(){
        geteShopDetail();
        /**
        * Get eShop Detail from api
        */
        function geteShopDetail() {
            $('.page-inner-section').addClass('d-none');
            $('.loader').removeClass('d-none');
            $('.logo-loader').removeClass('d-none');
            $('.default-image').addClass('d-none')
            $('.browse-content-inner').addClass('d-none')
            $('.cover-image-preview').addClass('d-none');
            $('.cover-loader').removeClass('d-none')
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/influencer/shops/list",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                    Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                },
                type: "get",
                success: function (data) {
                    seteShopData(data.data[0]);
                },
                error: function (request, status, error) {
                    $('.loader').addClass('d-none');
                    console.log("Error: ->", request.responseJSON);
                },
            });
        }

        function seteShopData(shopData){
            let imagePreview = "<?php echo $image_base; ?>"+shopData.shop_logo_path;
            let imagePreviewCover = "<?php echo $image_base; ?>"+shopData.shop_cover_image_path;
            $('.user-profile-preview-wrapper').removeClass('d-none');
            $('.user-profile-preview-wrapper img').attr('src',imagePreview)
            $('.logo-loader').addClass('d-none');
            $("#shop_id").val(shopData.id);
            var logo = $("#logo-image").val(shopData.shop_logo_path);
            var cover = $("#cover-image").val(shopData.shop_cover_image_path);
            var eshopName = $("#eshop-name").val(shopData.shop_name);
            // var fontStyle = $("#font-style").val(shopData.shop_font);
            var description = $("#description").val(shopData.shop_description);
            var color = $("#color").val(shopData.shop_color);
            $('.color-list-item[data-color="'+shopData.shop_color+'"]').addClass('selected-color');
            $('.cover-image-preview').removeClass('d-none');
            $('.cover-image-preview img').attr('src',imagePreviewCover)
            $('.cover-loader').addClass('d-none')
            $('.loader').addClass('d-none');
        }
        $("#logo-feature-img").change(function () {
            var file_data = $("#logo-feature-img").prop("files")[0];
            if(file_data){
                $('.publish-eshop').attr('disabled', true)
                $('.default-image').addClass('d-none')
                $('.user-profile-preview-wrapper').addClass('d-none');
                $('.logo-loader').removeClass('d-none')
            }
            var form_data = new FormData();
            form_data.append("image", file_data);
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/upload-image",
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: "post",
                success: function (data) {
                    let imageData = JSON.parse(data);
                    let imagePreview = "<?php echo $image_base; ?>"+imageData.image;
                    $("#logo-image").val(imageData.image);
                    $('.publish-eshop').attr('disabled', false)
                    $('.image-preview').addClass('d-none')
                    $('.user-profile-preview-wrapper').removeClass('d-none');
                    $('.user-profile-preview-wrapper img').attr('src',imagePreview)
                    $('.logo-loader').addClass('d-none')
                },
            });
        });
        $("#product-featur-img").change(function () {
            var file_data = $("#product-featur-img").prop("files")[0];
            if(file_data){
                $('.publish-eshop').attr('disabled', true)
                $('.browse-content-inner').addClass('d-none')
                $('.cover-image-preview').addClass('d-none');
                $('.cover-loader').removeClass('d-none')
            }
            var form_data = new FormData();
            form_data.append("image", file_data);
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/upload-image",
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: "post",
                success: function (data) {
                    let imageData = JSON.parse(data);
                    let imagePreview = "<?php echo $image_base; ?>"+imageData.image;
                    $("#cover-image").val(imageData.image);
                    $('.publish-eshop').attr('disabled', false)
                    $('.browse-content-inner').addClass('d-none')
                    $('.cover-image-preview').removeClass('d-none');
                    $('.cover-image-preview img').attr('src',imagePreview)
                    $('.cover-loader').addClass('d-none')
                    $('.browse-box-inner').addClass('opacity-1')
                    $('.browse-content').addClass('opacity-1')
                },
            });
        });
        $('.color-list-item').on('click', function(event){
            event.preventDefault();
            $('.color-list-item').removeClass('selected-color');
            $('#color').val($(this).attr('data-color'));
            $(this).addClass('selected-color');
        })
        $('.publish-eshop').on('click', function(event){
            event.preventDefault();
            let _this = $(this);
            if(!isFormValid()){
                return false;
            }else{
                _this.attr("disabled", true);
                _this.find("i").removeClass("d-none");
                var shop_id = $("#shop_id").val();
                var logo = $("#logo-image").val();
                var cover = $("#cover-image").val();
                var eshopName = $("#eshop-name").val();
                // var fontStyle = $("#font-style").val();
                var fontStyle = 1;
                var description = $("#description").val();
                var color = $("#color").val();
                let shop = {
                    shop_id: shop_id,
                    shop_name : eshopName,
                    shop_cover_image_path : cover,
                    shop_logo_path : logo,
                    shop_description : description,
                    shop_font : fontStyle,
                    shop_color : color
                }
                
                $.ajax({
                    url: "<?php echo API_URI_PATH ; ?>/influencer/shops/edit",
                    headers: { 
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                        "Authorization": `Bearer ${localStorage.getItem('_userToken')}`
                    },
                    dataType: "json",
                    data: shop,
                    type: "post",
                    success: function (data) {
                        _this.attr("disabled", false);
                        _this.find("i").addClass("d-none");
                        if (data.status == true) {
                            window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php";
                        }
                    },
                    error: function (request, status, error) {
                        _this.attr("disabled", false);
                        _this.find("i").addClass("d-none");
                    },
                });
            }
        })
        /**
        * @isFormValid - Form validation
        */
        function isFormValid() {
            let isValid = true;
            var logo = $("#logo-image").val();
            var cover = $("#cover-image").val();
            var eshopName = $("#eshop-name").val();
            // var fontStyle = $("#font-style").val();
            var description = $("#description").val();
            var color = $("#color").val();
            if (logo == "") {
                isValid = false;
                $("#logo-error").show();
                $("#logo-error").html("Please select eShop logo");
                $("#logo-error").css("color", "red");
            } else {
                $("#logo-error").hide();
            }
            if (cover == "") {
                isValid = false;
                $("#cover-error").show();
                $("#cover-error").html("Please enter eShop cover picture");
                $("#cover-error").css("color", "red");
            } else {
                $("#cover-error").hide();
            }
            if (eshopName == "") {
                isValid = false;
                $("#eShop-name-error").show();
                $("#eShop-name-error").html("Please enter your eShop name");
                $("#eShop-name-error").css("color", "red");
            } else {
                $("#eShop-name-error").hide();
            }
            // if (fontStyle == "") {
            //     isValid = false;
            //     $("#font-style-error").show();
            //     $("#font-style-error").html("Please select your eShop font style");
            //     $("#font-style-error").css("color", "red");
            // } else {
            //     $("#font-style-error").hide();
            // }
            if (description == "") {
                isValid = false;
                $("#description-error").show();
                $("#description-error").html("Please select your eShop description");
                $("#description-error").css("color", "red");
            } else {
                $("#description-error").hide();
            }
            if (color == "") {
                isValid = false;
                $("#color-error").show();
                $("#color-error").html("Please select your eShop color");
                $("#color-error").css("color", "red");
            } else {
                $("#color-error").hide();
            }
            return isValid;
        }
    })
</script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-dashboard.php"; ?>
