<!-- <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Otp Verify</title>


    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <link href="../css/custom.css" rel="stylesheet">
    
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  
</head>

<body class="p-0">

    <div class="login"> -->
<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-auth.php"; ?>


<div class="container">
    <div class="login-inner">
        <div class="login-left">
            <!-- <img src="../images/banner/login.png"> -->
        </div>
        <div class="login-right">
            <div class="login-section">
                <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg" /></div>
                <h5 class="login-error alert alert-danger" role="alert" id="otperr" style="display:none;">A code has been sent to your email</h5>
                <h5 class="login-error alert alert-success" role="alert" id="otpsuc" style="display:none;">A code has been sent to your email</h5>
                <p>Enter code sent to your email</p>
                <div class="input-field">
                    <label>Enter Code</label>
                    <input name="otp" id="otp" type="text" value="" />
                    <input name="email" id="email" type="hidden" value="" />
                    <h5 id="otp-error" class="empty-field-error"></h5>
                </div>
                <div class="forgot-pass"><a id="resendCode" style="cursor: pointer;">Resend Code</a></div>
                <button id="submitOtp"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Confirm</button>
                <div class="no-account">Back to <a href="<?php echo base_path ; ?>/login.php">Login</a></div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="footer-login">
    Copyright © 2021 Twiva. All Rights Reserved.
    <a href="https://twiva.co.ke/terms-conditions">Terms & Conditions</a>
</div> -->
<?php include INFLUENCER_DIRECTORY."/footer/footer-copyright.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
<!-- </div>

 
 <script type="text/javascript" src="api/api.js"></script>
 <script type="text/javascript" src="api/login.service.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
 
</body>

</html> -->
<script>
    $(document).ready(function () {
        var email = localStorage.getItem("email");
        if (!email) {
            window.location.href = "<?php echo $base; ?>/login.php";
        }
        localStorage.removeItem("email");
        $("#email").val(email);
        /**
         * @isFormValid - Form validation
         */
        function isFormValid() {
            let isValid = true;
            var otpVal = $("#otp").val();

            if (otpVal == "") {
                isValid = false;
                $("#otp-error").show();
                $("#otp-error").html("Please enter your code");
                $("#otp-error").css("color", "red");
            } else {
                $("#otp-error").hide();
            }
            return isValid;
        }
        $("#submitOtp").click(function () {
            if (!isFormValid()) {
                return false;
            }
            let _this = $(this);
            var otp = $("#otp").val();
            var email = $("#email").val();
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            //Loader End
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/verify/otp",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                dataType: "json",
                data: { email: email, otp: otp, device_token: "web_device_token", device_type: 3 },
                type: "post",
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        localStorage.setItem('_userInfo', JSON.stringify(data.data));
                        localStorage.setItem('_userToken', data.token);
                        window.location.href = "<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php";
                    } else {
                        window.location.href = "<?php echo base_path ; ?>/login.php";
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    $("#otp-error").show();
                    $("#otp-error").html(request.responseJSON.message);
                    $("#otp-error").css("color", "red");
                },
            });
        });  
        
        $("#resendCode").click(function () {
            $.ajax({
                        url: "<?php echo API_URI_PATH ; ?>/resend/otp",
                        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                        dataType: "json",
                        data: { email: email},
                        type: "post",
                        success: function (response) {
                            if(response.status==true){
                                $("#otpsuc").show();
                                $("#otp-error").hide();
                            }else{
                                $("#otperr").html(response.message);
                                $("#otperr").show();
                            }
                        }
            });
        });
    });
        
</script>

