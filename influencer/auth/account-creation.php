<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-auth.php"; ?>
<header>
    <!--Main Header Logo-->
    <div class="logo">
        <div class="nav toggle">
            <a id="menu_toggle"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/Frame 5.svg" alt="Toggle_Menu" /></a>
        </div>
        <a href="/"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo-white.svg" /></a>
    </div>
</header>
<div class="account-setup pt-2" style="background-color: #F8F8F8;">
    <div class="container">
        <div class="back-link">
            <a href="#" class="pl-0">Create your Account</a>
        </div>

        <div class="account-cont">
            <!-- <div class="setup-progress">
                    <div class="progress-txt">
                        <a class="active"><span>1</span>Basic Information</a>
                        <a><span>2</span>Address</a>
                    </div>
                </div> -->

            <div class="browse-pic">
                <div class="drop-zone image-preview-sec">
                    <label class="drop-zone__prompt" for="drop-zone__input">
                        <img class="image-preview" src="<?php echo IMAGES_URI_PATH; ?>/card-images/browse.png" />
                        <i class="fa fa-spinner fa-spin d-none image-loader-icon"></i>
                        <input accept="image/*" type="file" name="myFile" class="drop-zone__input" id="drop-zone__input" />
                        <!-- uploded image preview  -->
                        <div class="user-profile-preview-wrapper d-none">
                            <img src="<?php echo IMAGES_URI_PATH; ?>/icons/img.jpg" alt="" />
                        </div>
                    </label>
                </div>
                <h5 id="avtar-error" class="empty-field-error"></h5>
                <h3>Profile Picture</h3>
            </div>

            <div class="account-form">
                <div class="form-field">
                    <label>Full Name</label>
                    <input type="text" placeholder="" id="fullName" />
                    <h5 id="fullName-error" class="empty-field-error"></h5>
                </div>

                <!-- <div class="form-field">
                        <label>Contact Person Name</label>
                        <input type="text" placeholder="Marvin McKinney">
                    </div>

                    <div class="form-field">
                        <label>E-mail  Address</label>
                        <input type="text" placeholder="nevaeh.simmons@example.com">
                    </div> -->

                <!-- <div class="form-field">
                        <label>Business Phone Number</label>
                        <input type="text" placeholder="(209) 555-0104">
                    </div> -->
                <div class="form-field date-picker-field">
                    <label>Date Of Birth</label>
                    <input type="text" class="input-date datepicker" id="dob" autocomplete="off"/>
                    <h5 id="dob-error" class="empty-field-error"></h5>
                    <span id="dob-error2"></span>
                </div>

                <div class="form-input-field mb-3">
                    <label for="gender" class="mb-3">Gender</label>
                    <div class="gender-input d-flex" id="gender">
                        <div class="mr-4">
                            <input type="radio" id="male" name="gender" value="1" checked=""/>
                            <label for="male">Male</label>
                        </div>
                        <div class="mr-4">
                            <input type="radio" id="female" name="gender" value="2" />
                            <label for="female">Female</label>
                        </div>
                        <div class="mr-4">
                            <input type="radio" id="others" name="gender" value="3" />
                            <label for="other">Other</label>
                        </div>
                    </div>
                    <h5 id="gender-error" class="empty-field-error"></h5>
                </div>
                
                <div class="form-input-field">
                    <div class="select-box">
                        <label>Interests</label>
                        <select id="interest" class="js-example-basic-multiple w-100 interest-select-box" multiple="multiple"></select>
                        <h5 id="interest-error" class="empty-field-error"></h5>
                    </div>
                </div>
                <div class="form-field">
                    <label>Description</label>
                    <textarea id="description" maxlength="350"></textarea>
                    <h5 id="description-error" class="empty-field-error"></h5>
                </div>
                <input type="hidden" name="email" id="email" value="" />
                <input type="hidden" name="password" id="password" value="" />
                <input type="hidden" name="avtar" id="avtar" value="" />
            </div>
            <div class="button-sec right-align-btn">
                <!-- <button type="submit" id="submitbtn"><a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php">Register</a></button> -->
                <button type="submit" id="submitbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Register</button>
            </div>
        </div>
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var email = localStorage.getItem("email");
        var password = localStorage.getItem("password");
        getInterests();
        if (!email || !password) {
            window.location.href = "<?php echo INFLUENCER_AUTH_URI_PATH; ?>/register.php";
        }
        $("#email").val(email);
        $("#password").val(password);
        localStorage.removeItem("email");
        localStorage.removeItem("password");
        $('.datepicker').datepicker({  maxDate: new Date()});
        /**
         * @isFormValid - Form validation
         */
        function isFormValid() {
            let isValid = true;
            var profile_image = $("#avtar").val();
            var name = $("#fullName").val();
            var dob = $("#dob").val();
            var gender = $("#gender").val();
            var gender = $('input[name="gender"]:checked').val();
            var description = $("#description").val();
            var interest = $("#interest").val()
            if (profile_image == "") {
                isValid = false;
                $("#avtar-error").show();
                $("#avtar-error").html("Please select your profile picture");
                $("#avtar-error").css("color", "red");
            } else {
                $("#avtar-error").hide();
            }
            if (name == "") {
                isValid = false;
                $("#fullName-error").show();
                $("#fullName-error").html("Please enter your full name");
                $("#fullName-error").css("color", "red");
            } else {
                $("#fullName-error").hide();
            }
            if (dob == "") {
                isValid = false;
                $("#dob-error").show();
                $("#dob-error").html("Please enter your date of birth");
                $("#dob-error").css("color", "red");
            } else {
                $("#dob-error").hide();
            }
            if (!interest.length) {
                isValid = false;
                $("#interest-error").show();
                $("#interest-error").html("Please select your interest");
                $("#interest-error").css("color", "red");
            } else {
                $("#interest-error").hide();
            }
            if (!gender || gender == "") {
                isValid = false;
                $("#gender-error").show();
                $("#gender-error").html("Please select your gender");
                $("#gender-error").css("color", "red");
            } else {
                $("#gender-error").hide();
            }
            return isValid;
        }

        $("#drop-zone__input").change(function () {
            var file_data = $("#drop-zone__input").prop("files")[0];
            if(file_data){
                $('#submitbtn').attr('disabled', true)
                $('.image-preview').addClass('d-none')
                $('.user-profile-preview-wrapper').addClass('d-none');
                $('.image-loader-icon').removeClass('d-none')
            }
            var form_data = new FormData();
            form_data.append("image", file_data);
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/upload-image",
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: "post",
                success: function (data) {
                    let imageData = JSON.parse(data);
                    let imagePreview = "<?php echo $image_base; ?>"+imageData.image;
                    $("#avtar").val(imageData.image);
                    $('#submitbtn').attr('disabled', false)
                    $('.image-preview').addClass('d-none')
                    $('.user-profile-preview-wrapper').removeClass('d-none');
                    $('.user-profile-preview-wrapper img').attr('src',imagePreview)
                    $('.image-loader-icon').addClass('d-none')
                },
            });
        });

        $("#submitbtn").click(function () {
            let _this = $(this);
            if (!isFormValid()) {
                return false;
            }
            // Loader Start
            _this.attr("disabled", true);
            _this.find("i").removeClass("d-none");
            //Loader End
            var profile_image = $("#avtar").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var type = 2;
            var name = $("#fullName").val();
            var dob = $("#dob").val();
            var gender = $("#gender").val();
            var gender = $('input[name="gender"]:checked').val();
            var description = $("#description").val();
            var interests = $("#interest").val().join(',');
            var referal_code = "";
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/register/influencer",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                dataType: "json",
                data: { profile_image: profile_image, email: email, password: password, type: type, name: name, dob: dob, gender: gender, description: description, interests: interests, referal_code: referal_code },
                type: "post",
                success: function (data) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    if (data.status == true) {
                        localStorage.setItem("email", data.email);
                        window.location.href = "<?php echo INFLUENCER_AUTH_URI_PATH ; ?>/otp_verify.php";
                    }
                },
                error: function (request, status, error) {
                    _this.attr("disabled", false);
                    _this.find("i").addClass("d-none");
                    // $("#description-error").html(request.responseJSON.message).css("color", "red");
                    $("#dob-error2").html(request.responseJSON.message).css("color", "red");
                    // $("#gender-error").html(request.responseJSON.message).css("color", "red");
                    // $("#interest-error").html(request.responseJSON.message).css("color", "red");
                },
            });
        });
        function getInterests(params) {
            $.ajax({
                url: "<?php echo API_URI_PATH ; ?>/interests",
                headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                type: "get",
                success: function (data) {
                    let appenddata = "";
                    for (var i = 0; i < data.categories.length; i++) {
                        appenddata += `<option value="${data.categories[i].id}">${data.categories[i].name}</option>`;
                    }
                    $(".interest-select-box").html(appenddata);
                },
                error: function (request, status, error) {
                    let appenddata = `<option value="">No Interests</option>`;
                    $(".interest-select-box").html(appenddata);
                },
            });
            select2Init();
        }
        function select2Init() {
            $('.js-example-basic-multiple').select2({
                placeholder: "Select interests",
            });
        }
    });
</script>
