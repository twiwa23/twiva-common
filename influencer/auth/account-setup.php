<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Account Setup</title>
 
    <!-- Bootstrap core CSS -->

    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css">
    <!-- Custom styling plus plugins -->
    <link href="../../css/custom.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>

    <header>
        <div class="logo">
            <img src="../images/logo/logo-white.svg">
        </div>
    </header>
    <div class="account-setup"> 
<div class="container">
    <div class="back-link">
        <a href="#"> <i class='fas fa-chevron-left'></i> Account Setup</a>
    </div>

       
  <div class="account-cont">

      <div class="setup-progress">
          <div class="progress-txt">
              <a id="s1" class="active"><span>1</span>Basic Information</a>
              <a id="s2"class=""><span>2</span>Address</a>
          </div>
      </div>
       <div id="step1">
<div class="browse-pic">
    <div>
        <span class="drop-zone__prompt"><img src="../images/card-images/browse.png"/></span>
        <input class="btn btn-primary" type="file" id="logo"name="myFile" >
      </div>
      <h3>Business logo</h3>
      <p>image size 2000 x 1200</p>
</div>
<div class="account-form">
    <div class="form-field">
        <label>Business Name</label>
        <input id="business" type="text" placeholder="Globex Corporation">
             <h5 id="businesscheck">hello</h5>
    </div>
    <div class="form-field">
        <label>Contact Person Name</label>
        <input type="text" id="name" placeholder="Marvin McKinney">
             <h5 id="namecheck"></h5>
    </div>

    <div class="form-field">
        <label>E-mail  Address</label>
        <input type="text" id="email" placeholder="nevaeh.simmons@example.com">
             <h5 id="emailcheck"></h5>
    </div>

    <div class="form-field">
        <label>Business Phone Number</label>
        <input type="text" id="mobile"placeholder="(209) 555-0104">
             <h5 id="mobilecheck"></h5>
    </div>

    <div class="form-field">
        <label>Description</label>
        <textarea id="description"></textarea>
             <h5 id="descriptioncheck"></h5>
    </div>
</div>


<div class="button-sec right-align-btn">
    <button id="submitbtn">Next</button>
</div>
  </div>
</div>

</div>

<div id="step2" style="display: none;">
    <div class="container">
         <div class="account-form" style="background-color: #fff;">
    <div class="form-field">
        <label>Country</label>
        <h5 id="countrycheck"></h5>
       <select id="country">
           <option value="1">Mozambique</option>
       </select>
    </div>

    <div class="form-field">
        <label>Pincode</label>
        <input type="text" id="pincode" placeholder="2178">
         <h5 id="pincodecheck"></h5>
    </div>

    <div class="form-field">
        <label>City</label>
          <h5 id="citycheck"></h5>
        <select id="city">
           <option value="1">Delhi</option>
       </select>
      
    </div>

    <div class="form-field">
        <label>State</label>
         <h5 id="statecheck"></h5>
        <select id="state">
           <option value="1">Delhi</option>
       </select>
        
    </div>

    <div class="form-field">
        <label>Locality / Area / Street</label>
          <h5 id="areacheck"></h5>
        <input type="text" id="area" placeholder="74 Wellington Road, Pinner">
    </div>

    <div class="form-field">
        <label>Building Number</label>
        <h5 id="buildingcheck"></h5>
        <input type="text" id="building" placeholder="HA5 4NH">
    </div>
</div>


<div class="button-sec right-align-btn">
    <button onclick="AccountSetup()">Next</a></button>
</div>
  </div>
</div>
</div>

</div>


</div>

 

 </div>
</body>

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>
<script>
   
    $(document).ready(function(){
    $('#businesscheck').hide();
    $('#namecheck').hide();
    $('#emailcheck').hide();
    $('#mobilecheck').hide();
    $('#countrycheck').hide();
    $('#pincodecheck').hide();
    $('#citycheck').hide();
    $('#statecheck').hide();
    $('#areacheck').hide();
    $('#buildingcheck').hide();

    var business_err = true;
    var name_err = true;
    var email_err = true;
    var mobile_err = true;
    var description_err = true;
    var country_err = true;
    var pincode_err = true;
    var city_err = true;
    var area_err = true;
    var building_err = true;

    $('#business').keyup(function(){
        business_check();
        
    });
//Business name validation

    function business_check(){
        var business_val = $('#business').val();
        if(business_val.length == ''){
            $('#businesscheck').show();
            $('#businesscheck').html("Please fill the business name");
            $('#businesscheck').focus();
            $('#businesscheck').css("color","red");
            business_err = false;
            return false;



        }
        else{
            $('#businesscheck').hide();

        }

    }
//name validation
         $('#name').keyup(function(){
        name_check();
        
    });

    function name_check(){
        var business_val = $('#name').val();
        if(business_val.length == ''){
            $('#namecheck').show();
            $('#namecheck').html("Please fill the name");
            $('#namecheck').focus();
            $('#namecheck').css("color","red");
            name_err = false;
            return false;



        }
        else{
            $('#namecheck').hide();

        }

    }



        //email validation
         $('#email').keyup(function(){
        email_check();
        
    });

    function email_check(){
        var email_val = $('#email').val();
        if(email_val.length == ''){
            $('#emailcheck').show();
            $('#emailcheck').html("Please fill the email");
            $('#emailcheck').focus();
            $('#emailcheck').css("color","red");
            email_err = false;
            return false;



        }
        else{
            $('#emailcheck').hide();

        }

    }


                //mobile validation
         $('#mobile').keyup(function(){
        mobile_check();
        
    });

    function mobile_check(){
        var mobile_val = $('#mobile').val();
        if(mobile_val.length == ''){
            $('#mobilecheck').show();
            $('#mobilecheck').html("Please fill the mobile number");
            $('#mobilecheck').focus();
            $('#mobilecheck').css("color","red");
            mobile_err = false;
            return false;



        }
        else{
            $('#mobilecheck').hide();

        }

    }


                 //description validation
         $('#description').keyup(function(){
        description_check();
        
    });

    function description_check(){
        var description_val = $('#description').val();
        if(description_val.length == ''){
            $('#descriptioncheck').show();
            $('#descriptioncheck').html("Please fill the description");
            $('#descriptioncheck').focus();
            $('#descriptioncheck').css("color","red");
            description_err = false;
            return false;



        }
        else{
            $('#descriptioncheck').hide();

        }

    }


    $('#country').keyup(function(){
        country_check();
        
    });
//Business name validation

    function country_check(){
        var country_val = $('#country').val();
        if(country_val.length == ''){
            $('#countrycheck').show();
            $('#countrycheck').html("Please fill the country name");
            $('#countrycheck').focus();
            $('#countrycheck').css("color","red");
            country_err = false;
            return false;



        }
        else{
            $('#countrycheck').hide();

        }

    }


     $('#pincode').keyup(function(){
        pincode_check();
        
    });
//Business name validation

    function pincode_check(){
        var pincode_val = $('#pincode').val();
        if(pincode_val.length == ''){
            $('#pincodecheck').show();
            $('#pincodecheck').html("Please fill the pincode name");
            $('#pincodecheck').focus();
            $('#pincodecheck').css("color","red");
            pincode_err = false;
            return false;



        }
        else{
            $('#pincodecheck').hide();

        }

    }

     $('#area').keyup(function(){
        area_check();
        
    });
//Business name validation

    function area_check(){
        var area_val = $('#area').val();
        if(area_val.length == ''){
            $('#areacheck').show();
            $('#areacheck').html("Please fill the area name");
            $('#areacheck').focus();
            $('#areacheck').css("color","red");
            area_err = false;
            return false;



        }
        else{
            $('#areacheck').hide();

        }

    }


     $('#building').keyup(function(){
        building_check();
        
    });
//Business name validation

    function building_check(){
        var building_val = $('#building').val();
        if(building_val.length == ''){
            $('#buildingcheck').show();
            $('#buildingcheck').html("Please fill the building number");
            $('#buildingcheck').focus();
            $('#buildingcheck').css("color","red");
            building_err = false;
            return false;



        }
        else{
            $('#buildingcheck').hide();

        }

    }

    $('#submitbtn').click(function(){
         $("#s1").removeClass('active');
         $("#s2").addClass('active');
        
            
         business_err = true;
         name_err = true;
         email_err = true;
         mobile_err = true;
         description_err = true;
         country_err = true;
         pincode_err = true;
         area_err = true;
         building_err = true;

            business_check();
            name_check();
            email_check();
            mobile_check();
            description_check();
            country_check();
            pincode_check();
            area_check();
            building_check();
            if((business_err == true) && (name_err == true) && (email_err == true) && (mobile_err == true) && (description_err == true)  ){
         $("#step1").hide();
         $("#step2").show();
            }
            else{
                return false;
            }



    })


});

   
    </script>
 <script type="text/javascript" src="api/api.js"></script>
 <script type="text/javascript" src="api/login.service.js"></script>

</html>
