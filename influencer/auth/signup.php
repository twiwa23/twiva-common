<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-auth.php"; ?>
    <div class="container-fluid m-0">
        <div class="back-button">
            <button id="back-button" onclick="window.history.go(-1); return false;"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/chevron-left-white.svg" alt="">Back</button>
        </div>
    </div>

    

    <div class="container signup-page">
        <div class="login-inner">
            <div class="login-left">
                <!-- <img src="../images/banner/login.png"> -->
                
            </div>

            <div class="login-right">
            <div class="container signup-heading mb-5">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                            <h1 class="text-center">Market And Sell on Twiva Social Commerce Platform</h1>
                            <p class="text-center">The Only Influencer Marketing Hub That Sells Your Products and Services Through Thousands of Influencers</p>
                        </div>
                    </div>
                </div>
                <div class="login-section" style="width:100%; padding: 30px 65px; ">
                    <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg"/></div>

                    <div class="signup-section">
                        <h3>Sign Up</h3>
                        <p class="mb-2">Are you a Business or an Influencer?</p>
                        <a href="<?php echo BUSINESS_AUTH_URI_PATH ; ?>/signup.php" class="d-block mb-4 mt-4 "><button class="white-bttn ">I’m a Business</button></a>
                        <a href="<?php echo INFLUENCER_AUTH_URI_PATH ; ?>/register.php" class="d-block"><button class="white-bttn">I’m an Influencer</button></a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#back-button").click(function() {
                window.location.href = '<?= $global_link; ?>'
            });
        });
    </script>
<?php include INFLUENCER_DIRECTORY."/footer/footer-copyright.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
