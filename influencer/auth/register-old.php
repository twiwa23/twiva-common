<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Sign Up</title>

        <!-- Bootstrap core CSS -->

        <link href="../css/bootstrap.min.css" rel="stylesheet" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link href="../css/animate.min.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" />
        <!-- Custom styling plus plugins -->
        <link href="../css/custom.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet" />
        <link href="../css/style.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap" rel="stylesheet" />
    </head>

    <body class="p-0">
        <div class="login">
            <!-- <div class="back-link">
            <a href="login.php"> <i class='fas fa-chevron-left'></i> Sign Up</a>
        </div> -->

            <div class="container" id="step">
                <div class="login-inner">
                    <div class="login-left">
                        <!-- <img src="../images/banner/login.png"> -->
                    </div>
                    <div class="login-right">
                        <div class="login-section">
                            <div class="logo"><img src="../images/logo/logo.svg" /></div>

                            <div class="input-field">
                                <label>E-mail Address</label>
                                <input type="text" id="email" placeholder="willie.jennings@example.com" />
                                <h5 id="usercheck" class="empty-field-error"></h5>
                            </div>

                            <div class="input-field">
                                <label>Password</label>
                                <input id="password" type="text" placeholder="********" />
                                <h5 id="passcheck" class="empty-field-error"></h5>
                            </div>

                            <div class="input-field">
                                <label>Referral Code</label>
                                <input type="text" id="referral" placeholder="Enter referral code..." />
                            </div>

                            <button class="red-bttn" id="submitbtn">Next</button>
                            <div class="no-account">s Already have an account? <a href="../login.php">Login</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-login">
                Copyright © 2021 Twiva. All Rights Reserved.
                <a href="https://home.twiva.co.ke/terms-conditions">Terms & Conditions</a>
            </div>
        </div>
    </body>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/custom.js"></script>

    <script>
        $(document).ready(function () {
            $("#usercheck").hide();
            $("#passcheck").hide();

            var user_err = true;
            var pass_err = true;

            $("#email").keyup(function () {
                email_check();
            });

            function email_check() {
                var email_val = $("#email").val();
                if (email_val.length == "") {
                    $("#usercheck").show();
                    $("#usercheck").html("Please fill the email");
                    $("#usercheck").focus();
                    $("#usercheck").css("color", "red");
                    user_err = false;
                    return false;
                } else {
                    $("#usercheck").hide();
                }
            }

            $("#password").keyup(function () {
                pass_check();
            });

            function pass_check() {
                var pass_val = $("#password").val();
                if (pass_val.length == "" && pass_val.length > 8) {
                    $("#passcheck").show();
                    $("#passcheck").html("Password should be minimum 8 characters");
                    $("#passcheck").focus();
                    $("#passcheck").css("color", "red");

                    pass_err = false;
                    return false;
                } else {
                    $("#passcheck").hide();
                }
            }

            $("#submitbtn").click(function () {
                var pass_val = $("#password").val();
                if (pass_val.length > 8) {
                    user_err = true;
                    pass_err = true;
                    email_check();
                    pass_check();
                    if (user_err == true && pass_err == true) {
                        window.location.href = "influencer-dashboard.php";
                        valiateRegister();
                    } else {
                        return false;
                    }
                } else {
                    $("#passcheck").show().css("color", "red").html("Password should be minimum 8 characters");
                    if (email_val.length == "") {
                        $("#emailcheck").show().css("color", "red").html("Please enter your email");
                    }
                }
            });
        });
    </script>
    <script type="text/javascript" src="api/api.js"></script>
    <script type="text/javascript" src="api/login.service.js"></script>
</html>
