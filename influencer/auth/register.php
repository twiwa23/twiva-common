<?php require_once('../../twiva-config.php'); ?>
<?php include INFLUENCER_DIRECTORY."/header/header-auth.php"; ?>
<div class="container-fluid m-0">
    <div class="back-button">
        <button id="back-button"><img src="../../images/icons/chevron-left-white.svg" alt="">Back</button>
    </div>
</div>

<div class="container" id="step">
    <div class="login-inner">
        <div class="login-left">
            <!-- <img src="<?php echo IMAGES_URI_PATH; ?>/banner/login.png"> -->
        </div>
        <div class="login-right">
            <div class="login-section">
                <div class="logo"><img src="<?php echo IMAGES_URI_PATH; ?>/logo/logo.svg" /></div>

                <div class="input-field">
                    <label>E-mail Address</label>
                    <input type="text" id="email" placeholder="willie.jennings@example.com" />
                    <h5 id="usercheck" class="empty-field-error"></h5>
                </div>

                <div class="input-field">
                    <label>Password</label>
                    <input id="password" type="password" placeholder="********" />
                    <div class="closed-eye showPass"></div>
                    <div class="open-eye showPass"></div>                    
                    <h5 id="passcheck" class="empty-field-error"></h5>
                </div>

                <div class="input-field">
                    <label>Referral Code</label>
                    <input type="text" id="referral" placeholder="Enter referral code..." />
                </div>

                <button class="red-bttn" id="submitbtn"><i class="fa fa-spinner fa-spin mr-1 text-white d-none"></i> Next</button>
                <div class="no-account">Already have an account? <a href="<?php echo $base; ?>/login.php">Login</a></div>
            </div>
        </div>
    </div>
</div>
<?php include INFLUENCER_DIRECTORY."/footer/footer-copyright.php"; ?>
<?php include INFLUENCER_DIRECTORY."/footer/footer-auth.php"; ?>
        <script>
        $(document).ready(function(){       
        $('.showPass').on('click', function(){
            var passInput=$("#password");
            if(passInput.attr('type')==='password')
            {
                passInput.attr('type','text');
            }else{
                passInput.attr('type','password');
            }
        })
        })
        </script>
        <script>
            // Show Password 
            $(document).ready(function() {
                $('.closed-eye').on('click', function() {
                    $(this).hide();
                    $('.open-eye').show();
                });

                $('.open-eye').on('click', function() {
                    $(this).hide();
                    $('.closed-eye').show();
                });
            });
        </script>
<script>
    $(document).ready(function () {
        $("#usercheck").hide();
        $("#passcheck").hide();

        var user_err = true;
        var pass_err = true;

        $("#email").keyup(function () {
            email_check();
        });
        function ValidateEmail(mail){
            if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)){
                return true;
            }
            return false
        }
        function email_check() {
            var email_val = $("#email").val();
            if (email_val.length == "") {
                $("#usercheck").show();
                $("#usercheck").html("Please fill the email");
                $("#usercheck").focus();
                $("#usercheck").css("color", "red");
                user_err = false;
                return false;
            } else {
                if(!ValidateEmail(email_val)){
                    $("#usercheck").show();
                    $("#usercheck").html("Please Enter a valid email");
                    $("#usercheck").css("color", "red");
                    $("#usercheck").focus();
                    user_err = false;
                    return false;
                }else{
                    $("#usercheck").hide();
                }
            }
        }

        $("#password").keyup(function () {
            pass_check();
        });

        function pass_check() {
            var pass_val = $("#password").val();
            if (pass_val.length == "" || pass_val.length < 8) {
                $("#passcheck").show();
                $("#passcheck").html("Password should be minimum 8 characters");
                $("#passcheck").focus();
                $("#passcheck").css("color", "red");

                pass_err = false;
                return false;
            } else {
                $("#passcheck").hide();
            }
        }

        $("#submitbtn").click(function () {
            let _this = $(this);
            var pass_val = $("#password").val();
            if (pass_val.length >= 8) {
                user_err = true;
                pass_err = true;
                email_check();
                pass_check();
                if (user_err == true && pass_err == true) {
                    var email = $("#email").val();
                    var password = $("#password").val();

                    localStorage.setItem("email", email);
                    localStorage.setItem("password", password);

                    // Loader Start
                    _this.attr("disabled", true);
                    _this.find("i").removeClass("d-none");
                    //Loader End
                    // alert(email);
                    $.ajax({
                        url: "<?php echo API_URI_PATH ; ?>/email/status",
                        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
                        type: "post",
                        data: { email: email },
                        success: function (data) {
                            _this.attr("disabled", false);
                            _this.find("i").addClass("d-none");
                            if (data.status == true) {
                                window.location.href = "<?php echo INFLUENCER_AUTH_URI_PATH ; ?>/account-creation.php";
                                // valiateRegister();
                            }
                        },
                        error: function (request, status, error) {
                            _this.attr("disabled", false);
                            _this.find("i").addClass("d-none");
                            $("#usercheck").show();
                            $("#usercheck").html(request.responseJSON.message);
                            $("#usercheck").focus();
                            $("#usercheck").css("color", "red");
                        },
                    });
                } else {
                    return false;
                }
            } else {
                $("#passcheck").show().css("color", "red").html("Password should be minimum 8 characters");
                if (email_val.length == "") {
                    $("#emailcheck").show().css("color", "red").html("Please enter your email");
                }
            }
        });
    });
    $(document).ready(function() {
        $("#back-button").click(function() {
            window.location.href = '<?= $global_link; ?>'
        });
    });
</script>
