        <script>
            $(document).ready(function(){
                let eShopLink = '';
                let isShopCreated = localStorage.getItem('_isShopCreated')
                if(isShopCreated == 1){
                    eShopLink = '<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-eshop.php'
                }else{
                    eShopLink = '<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-select-product.php'
                }
                $('.eshop-link').attr('href',eShopLink);
                function setUserInfo(){
                    let user = JSON.parse(localStorage.getItem('_userInfo'));
                    // console.log(user,'user');
                    let token = localStorage.getItem('_userToken');
                    if(!user || !token){
                        window.location.href = "<?php echo $base; ?>/login.php";
                        return false;
                    }
                    if(user.influencer_details && user.influencer_details.profile_image != '' && user.influencer_details.profile_image != null){
                        let picture = `<?php echo $image_base; ?>${user.influencer_details.profile_image}`
                        $('.user-profile-pic').attr('src', picture);
                        // $('.user-profile-pic').attr('src', picture);
                    }
                    if(user.influencer_details == '' || user.influencer_details == null){
                        $('.user-name').text(user.name);
                        // $('.header-company-name-wrapper .company-name').text(user.influencer_details.name);
                        $('.header-company-name-wrapper .email').text(user.email);
                    }else{
                        $('.user-name').text(user.influencer_details.name);
                        $('.header-company-name-wrapper .company-name').text(user.influencer_details.name);
                        $('.header-company-name-wrapper .email').text(user.email);
                    }
                    // if(user.influencer_details && user.influencer_details.name != ''){
                    //     $('.user-name').text(user.influencer_details.name);
                    //     $('.header-company-name-wrapper .company-name').text(user.influencer_details.name);
                    //     $('.header-company-name-wrapper .email').text(user.email);
                    // }else{
                    //     $('.user-name').text(user.name);
                    //     // $('.header-company-name-wrapper .company-name').text(user.influencer_details.name);
                    //     $('.header-company-name-wrapper .email').text(user.email);
                    // }
                }

                function getUnreadCount() {
                    $.ajax({
                        url: `<?php echo API_URI_PATH ; ?>/business/notifications/unread-count`,
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                            Authorization: `Bearer ${localStorage.getItem("_userToken")}`,
                        },
                        type: "get",
                        success: function (data) {
                            if(data.unread_count > 0){
                                $('.notification-bell').append(`<span class="badge bg-yellow">${data.unread_count}</span>`)
                            }
                        },
                        error: function (request, status, error) {
                            console.log("Error: ->", request.responseJSON);
                        },
                    });
                }
                setUserInfo();
                getUnreadCount();
                $('#logout-btn').on('click',function(event){
                    event.preventDefault();
                    localStorage.removeItem('_userInfo');
                    localStorage.removeItem('_userToken');
                    localStorage.removeItem('_isShopCreated')
                    window.location.href = "<?php echo $base; ?>/login.php";
                });
            })
            
        </script>        
    </body>
</html>
