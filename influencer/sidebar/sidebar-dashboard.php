<div class="left_col">
    <div class="scroll-view">
        <div class="clearfix"></div>

        <!-- Sidebar Menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <button class="menu-close-button"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/x.svg" alt="" /></button>
            <a href="javascript:;" class="user-profile " data-toggle="dropdown" aria-expanded="false">
                                    <img class="user-profile-pic" src="<?php echo IMAGES_URI_PATH; ?>/icons/profile.jpeg" alt="" /> <span class="user-name"></span>
                                    <!-- <span class=" fa fa-angle-down"></span> -->
            </a>
            <div class="menu_section">
                <h4>Menu</h4>
                <ul class="nav side-menu">
                    <li>
                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-dashboard.php"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/dashboard.svg" />Dashboard
                        <span class="fa fa-chevron-down"></span>
                        </a>
                    </li>
                    <li class="sidebar-dropdown">
                        <a href="javascript:void(0);"> <img src="<?php echo IMAGES_URI_PATH; ?>/icons/invitation-2.svg" /> My Invites<span class="fa fa-chevron-down"></span></a>
                        <div class="sidebar-submenu">
                            <ul class="account-setting-dropdown-wrapper sidebar-dropdown">
                            
                                <li class="drop-down-itme" id="nav-click-old" data-url="/my-invites/posts/posts">
                                    <a href="#">Posts</a>
                                </li >
                                <li class="drop-down-itme" id="nav-click-old" data-url="/my-invites/twitter-trends/twitter-trends">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/">Twitter Trends</a>
                                </li >
                            </ul>
                        </div>
                    </li>
                    <!-- <li class="sidebar-dropdown">
                        <a href="javascript:void(0);"> <img src="<?php echo IMAGES_URI_PATH; ?>/icons/file-plus.svg" /> Accepted <span class="fa fa-chevron-down"></span></a>
                        <div class="sidebar-submenu">
                            <ul class="account-setting-dropdown-wrapper sidebar-dropdown">
                            
                                <li class="drop-down-itme" id="nav-click-old" data-url="/accepted/posts/posts">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/">Posts</a>
                                </li >
                                <li class="drop-down-itme" id="nav-click-old" data-url="/accepted/twitter-trends/twitter-trends">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/">Twitter Trends</a>
                                </li >
                            </ul>
                        </div>
                    </li> -->
                    <li>
                        <a href="javascript:void(0);" class="eshop-link"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/myEshop.svg" /> My eShop <span class="fa fa-chevron-down"></span></a>
                    </li>
                    <!-- <li class="sidebar-dropdown">
                        <a href="javascript:void(0);"> <img src="<?php echo IMAGES_URI_PATH; ?>/icons/social.svg" /> Social Selling <span class="fa fa-chevron-down"></span></a>
                        <div class="sidebar-submenu">
                            <ul class="account-setting-dropdown-wrapper sidebar-dropdown">
                            
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-post-story.php">Add Post</a>
                                </li >
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-social.php">Social Selling</a>
                                </li >
                            </ul>
                        </div>
                    </li> -->
                    <!-- <li>
                        <a href="javascript:void(0);"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/notification.svg" />Notifications<span class="fa fa-chevron-down"></span></a>
                    </li> -->
                    <li class="sidebar-dropdown">
                        <a href="javascript:void(0);"> <img src="<?php echo IMAGES_URI_PATH; ?>/icons/settings.svg" /> Account Settings<span class="fa fa-chevron-down"></span></a>
                        <div class="sidebar-submenu">
                            <ul class="account-setting-dropdown-wrapper sidebar-dropdown">                            
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-edit-profile.php">Profile</a>
                                </li >
                                <!-- <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/social-media-connections.php">Social Media Connection</a>
                                </li > -->
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-bank-setup.php">Bank Setup</a>
                                </li>
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-my-earning.php">My Earnings</a>
                                </li>
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-payment-history.php">Payment History</a>
                                </li>
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-mobile-verification.php">Phone Verification</a>
                                </li>
                                <li class="drop-down-itme">
                                    <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-change-password.php">Change Password</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo INFLUENCER_DASHBOARD_URI_PATH ; ?>/influencer-refer-earn.php"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/refer.svg" /> Refer & Earn <span class="fa fa-chevron-down"></span></a>
                    </li>
                    <li onclick="logoutFun()">
                        <a href="javascript:void(0);" id="logout-btn"><img src="<?php echo IMAGES_URI_PATH; ?>/icons/logout-sidebar.svg" /> Logout <span class="fa fa-chevron-down"></span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function logoutFun() {
        localStorage.removeItem('_userInfo');
        localStorage.removeItem('_userToken');
        localStorage.removeItem('_isShopCreated')
        window.location.href = "<?php echo $base; ?>/login.php";
    }
</script>
<script>
    $(document).on("click", "#nav-click-old", function(e) {
        var user_id = localStorage.getItem("userId");
        var type = 2;
        var user_token = localStorage.getItem('_userToken');
        var nav_url = $(this).data("url");
        e.preventDefault();
        if (user_id) {
                        $.ajax({
                            //url: "https://twiva.co.ke/api/v1/nav-link",
                            url: "https://home.twiva.co.ke/api/v1/nav-link",
                            //url: "http://127.0.0.1:8000/api/v1/nav-link",
                            headers: {
                                Accept: "application/json",
                                // 'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
                                ["Authorization"]: "Bearer " + user_token,
                            },
                            type: "post",
                            data: {
                                user_id: user_id,
                                nav_url: nav_url,
                                user_token: user_token,
                                type: type,
                            },
                            success: function(data) {
                                window.location.href = data.data.url;
                            },
                            error: function(data) {
                                console.log(data);
                            },
                        });
                    }
    });
</script>