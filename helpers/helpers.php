<?php

if ( !function_exists('dd') ){
	function dd( $arr , $dye = 0){
		$root = debug_backtrace();
		echo "<pre>";
		 print_r($arr);
		echo "</pre>";
		if($dye == 1){
			die(
				'<p><font size="2" face="verdana" color="black"><b>Died on</b></br>Line: </font><code><font face="verdana"size="2" color="red"><u>'
				 . $root[0]['line'] . 
				 '</u></font></code></br><font face="verdana"size="2" color="black"> File : </font><code><font face="verdana"size="2" color="red"><u>' 
				 . str_replace($_SERVER['DOCUMENT_ROOT'],'',$root[0]['file']) . 
				 '</u></font></code></p>'
			   );
		}
	}
}
