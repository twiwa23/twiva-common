function change_password(json){

    __ajax_httpproduct("influencer/change/password", json, headers(), AJAX_CONF.apiType.POST, "", __success_change);      
}

function bank_list(){
   

    __ajaxregister_http("influencer/bank/list", "", headers(), AJAX_CONF.apiType.GET, "", __success_list);

}

function bank_details(){
   

    __ajaxregister_http("influencer/bank/details", "", headers(), AJAX_CONF.apiType.GET, "", __success_bank_details);

}

function __success_bank_details(response)
{
	if (response.status == true) {
        
        $("#bank").val(response.data.bank_id);
		document.getElementById("account").value = response.data.account_number;
		document.getElementById("reaccount").value = response.data.account_number;
        $("#branch_name").val(response.data.branch_name);
        $("#city").val(response.data.city);
		//document.getElementById("code").value = response.data.ifsc_code;
		document.getElementById("name").value = response.data.recipient_name;

	}
	else{
		
	}
	// console.log(response);
}

function _bind_list(bank_id)
{


	$(response.data).each(function(index,key){
        $('#bank').append(`
	<option value="${key.bank_name}" >${key.bank_name}</option>
	`)
	})
}


function bank_setup(json){
   

    __ajaxregister_http("influencer/bank/setup", json, headers(), AJAX_CONF.apiType.POST, "", __success_setup);

}


function bankparams()
{
	var json={
		"bank_id":$("#bank").val(),
		"account_number":$("#account").val(),
		"recipient_name":$("#name").val(),
		"ifsc_code":"NA", //$("#code").val(),
        "branch_name":$("#branch_name").val(),
        "city":$("#city").val(),
	}
	bank_setup(json);

}

function __success_setup(response)
{
	console.log(response.message);
	if (response.status == true) {

	    $('#sucotp').show();
            $('#sucotp').html(response.message);
        }
}

function __success_list(response)
{

	$(response.data).each(function(index,key){


        $('#bank').append(`
	<option value="${key.bank_name}" >${key.bank_name}</option>
	`)
	})
}

function password_param()
{

    var json= {
		"current_password":$("#current").val(),
		"new_password":$("#new").val(),
		"confirm_password":$("#confirm").val(),
	}

    if (json.new_password != json.confirm_password) { 
        $("#error1").css('display', 'block');
        $("#error1").html("New password and confirm password not matched.");
        return false;
     }

    if(validatePassword($("#new"))){

    	change_password(json);

    }

	
}


function validatePassword($this) {

    var minLength = 8;
    var maxLength = 20;
    console.log($($this).val());

    if ($($this).val().length < minLength) {
        $("#error1").css('display', 'block');
        $("#error1").html("Password must be 8 characters long.");
        isError = true;
    } else if ($($this).val().length > maxLength) {
        $("#error1").css('display', 'block');
        $("#error1").html("Password must be between 8 to 20  characters long.");
        isError = true;
    } else {
        $("#error1").css('display', 'none');
        //$("#error1").html("Password must be 8 characters long.");
        isError = false;
    }

    return !isError;
}


function __success_change(response) {
	
	console.log(response);
	if(response.status == false)
	{
		 // $('#error').hide();
	$('#error').show().html(response.message);
	}
	if (response.status == true) 
	{
        
		$('#otpsuc').show().html(response.message);
        document.getElementById("myForm").reset();

	}


}



//validations
    $(document).ready(function(){
    $('#bankerr').hide();
    $('#nameerr').hide();
    $('#accounterr').hide();
    $('#reaccounterr').hide();
    $('#codeerr').hide();
  

    var bankerr = true;
    var nameerr = true;
    var accounterr = true;
    var reaccounterr = true;
    var codeerr = true;
  

    $('#bank').keyup(function(){
        bank_check();
        
    });
//Business name validation

    function bank_check(){
        var bank_val = $('#bank').val();
        if(bank_val.length == ''){
            $('#bankerr').show();
            $('#bankerr').html("Please fill the business name");
            $('#bankerr').focus();
            $('#bankerr').css("color","red");
            bankerr = false;
            return false;



        }
        else{
            $('#bankerr').hide();

        }

    }
    $('#account').keyup(function(){
        account_check();
        
    });

    function account_check(){
        var account_val = $('#account').val();
        if(account_val.length == ''){
            $('#accounterr').show();
            $('#accounterr').html("Please fill the account number");
            $('#accounterr').focus();
            $('#accounterr').css("color","red");
            accounterr = false;
            return false;



        }
        else{
            $('#accounterr').hide();

        }

    }

     $('#reaccount').keyup(function(){
        account_checkre();
        
    });

    function account_checkre(){
        var account_val = $('#reaccount').val();
        if(account_val.length == ''){
            $('#reaccounterr').show();
            $('#reaccounterr').html("Please re enter your account number");
            $('#reaccounterr').focus();
            $('#reaccounterr').css("color","red");
            reaccounterr = false;
            return false;



        }
        else{
            $('#reaccounterr').hide();

        }

    }

    $('#name').keyup(function(){
        name_check();
        
    });

    function name_check(){
        var account_val = $('#name').val();
        if(account_val.length == ''){
            $('#nameerr').show();
            $('#nameerr').html("Please enter your name");
            $('#nameerr').focus();
            $('#nameerr').css("color","red");
            nameerr = false;
            return false;



        }
        else{
            $('#nameerr').hide();

        }

    }

     $('#code').keyup(function(){
        code_check();
        
    });

    function code_check(){
        var account_val = $('#code').val();
        if(account_val.length == ''){
            $('#codeerr').show();
            $('#codeerr').html("Please enter your bank ifsc code");
            $('#codeerr').focus();
            $('#codeerr').css("color","red");
            codeerr = false;
            return false;



        }
        else{
            $('#codeerr').hide();

        }

    }

    function branch_check(){
        var branch_value = $("#branch_name").val();
        if(branch_value.length == ''){
            $('#branch_error').show();
            $('#branch_error').html("Please enter branch name");
            $('#branch_error').focus();
            $('#branch_error').css("color","red");
            branch_error = false;
            return false;
        }
        else{
            $('#branch_error').hide();
        }
    }

    function city_check(){
        var city_value = $("#city").val();
        if(city_value.length == ''){
            $('#city_error').show();
            $('#city_error').html("Please enter city name");
            $('#city_error').focus();
            $('#city_error').css("color","red");
            city_error = false;
            return false;
        }
        else{
            $('#city_error').hide();
        }
    }


    $('#setopsubmit').click(function(){
      
        
            
   
        nameerr = true;
        accounterr = true;
        reaccounterr = true;
        // codeerr = true;
        branch_error = true;
        city_error = true;
      

            // code_check();
            branch_check();
            city_check();
            name_check();
            account_checkre();
            account_check();
         let a = $("#account").val();
         let b = $("#reaccount").val();
            if((bankerr == true) && (nameerr == true) && (accounterr == true) && (accounterr == true) 
            	&& (reaccounterr == true) && (branch_error== true) && (city_error== true)){
            	if (a == b) {
            		bankparams();
            		
            	}
            	else{
            		$('#accounterr').show();
            $('#accounterr').html("account number mistmatch");
            $('#reaccounterr').show();
            $('#reaccounterr').html("account number mistmatch");
            	}
   
            }
            else{
                return false;
            }



    });
    })