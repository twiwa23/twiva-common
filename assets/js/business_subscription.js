function fetch_subscription(){
   

    __ajaxregister_http("subscriptions/listing", "", headers(), AJAX_CONF.apiType.GET, "", __success_subscription);

}
function getsubscription(json)
{
	 __ajaxregister_http("subscriptions/purchase", json, headers(), AJAX_CONF.apiType.POST, "", __success_subscrip);
}
function subscriptiondetails()
{
     __ajaxregister_http("business/dashboard", "", headers(), AJAX_CONF.apiType.GET, "", __success_details);
}

function freesubscription(json){
   

    __ajaxregister_http("subscriptions/purchase", json, headers(), AJAX_CONF.apiType.POST, "", __success_subscriptionfree);

}

function purchasePaidSubscription(json){
    __ajaxregister_http("subscriptions/purchase", json, headers(), AJAX_CONF.apiType.POST, "", __success_paidsubscription);
}

function checkStatus(json){
    __ajaxregister_http("transaction/check-status", json, headers(), AJAX_CONF.apiType.POST, "", __success_transactionStatus);

}

function __success_transactionStatus(response){
    if(response.transaction_status==0){
        var json ={
            "merchant_request_id":merchant_id,
        }
        checkStatus(json);
    }
    if(response.transaction_status==2){
        $("#transaction_error").html('Request cancelled');
    }
    if(response.transaction_status==3){
        $("#transaction_error").html('Request timeout');
    }
    if(response.transaction_status==4){
        $("#transaction_error").html('The balance is insufficient for the transaction');
    }
    if(response.transaction_status==5){
        $("#transaction_error").html('Case Not Detected');
    }
    if(response.transaction_status==1){
        alert('Subscribed successfully');
        setTimeout(function() {
                goto(UI_URL.onboarding4);
            }, 1000);
    }
}



function subsstart()
{
    var json ={
        "subscription_id":"1"
    }
    freesubscription(json);
}



function purchaseSubscription(sub_id,mpesa_number){
    var json ={
        "subscription_id":sub_id,
        "phone_number":mpesa_number,
        
    }
    purchasePaidSubscription(json);
}
var merchant_id = '';
function __success_subscriptionfree(response) {
    setTimeout(function() {
            goto(UI_URL.onboarding4);
          }, 1000);
}

function __success_paidsubscription(response) {
    var json ={
        "merchant_request_id":response.merchant_request_id,
    }
    console.log(response);
    if(response.status==false){
        $("#transaction_error").html('Invalid  MPESA Number');
    }else{
    merchant_id = response.merchant_request_id;
    checkStatus(json);
    }
}

function __success_details(response)
{
    // console.log(response.data.user_subscription);
         $(response.data.user_subscription).each(function(index,key){
            console.log(key);
           var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy + '-' + mm + '-' +dd ;
//diffrenece
      const format3 = "YYYY-MM-DD"
                var enddate = key.end_date;
                var startdate = key.start_date;
                dateTime3 = moment(enddate).format(format3);
            
                const dat1 = new Date(enddate);
                const dat2 = new Date(today);
                const dat3 = new Date(startdate);

                const diffTime = Math.abs(dat1 - dat2);
                const diffTime1 = Math.abs(dat1 - dat3);
                const diffDays1 = Math.ceil(diffTime1 / (1000 * 60 * 60 * 24));                 
const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
 console.log(diffTime + " milliseconds");
var diffdate =diffDays;
console.log(diffdate + "aaaa");
console.log(diffDays1 + "aaaab");

precentage =Math.ceil((diffTime / diffTime1)*100);
precentage = Math.trunc(precentage/10);
precentage = precentage * 10;
            if(key.subscription_details.no_of_days == 30)
            {
      
var percent = diffdate*3;
// var actpercent = percent;
console.log(percent);
         }
          
// alert(dats);
   const format1 = "YYYY MMMM DD"

    var date1= key.start_date; 
    var date2= key.end_date; 

    const format = "DD MMMM YYYY";

dateTime1 = moment(date1).format(format1);
dateTime2 = moment(date2).format(format1);

var remainingDays = dateTime2 - dateTime1;
//alert(remainingDays);
        $('#detailssubs').append(`
        <div class="info-content-wrapper">
                    <div class="title-wrapper">
                        <h4>
                            Subscription
                        </h4>
                    </div>
                    <div class="plan">
                        <p class="plan-month">
                            Active plan: ${key.subscription_details.subscription_title} (1 -${key.subscription_details.max_products} product)
                        <span class="txt ml-2 font-weight-bold" id="expired" style="color:red; display:none;">-- Expired</span>
                        </p>
                        <p class="plan-days">
                            ${moment(date1).format(format)} to ${moment(date2).format(format)}
                        </p>
                    </div>
                </div>
                
                <div class="date-circle-wrapper" id="prog-div">
                    <div class="progress" data-percentage="${precentage}">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                        <div class="progress-value">
                            <div class="day-left">
                                <span>
                                   ${diffDays}
                                </span> <br>
                                <span>Days left</span>
                            </div>
                        </div>
                    </div>
                </div>
        `);
        var end_date = new Date(dateTime2);
        var today = new Date();     
        if(end_date<today){
            $("#prog-div").hide();
            $("#expired").show();   
        }
    });
        



      
}

function __success_subscrip(response){
  $("#successMessage").show().html("<strong >Success!</strong> your subscription has been started successfully");
//     setTimeout(function() {
//     $('#successMessage').fadeOut('slow');
// }, 6000);

          setTimeout(function() {
            goto(UI_URL.onboarding11);
          }, 1000);

}

function subsid(subs){
	 alert(subs);
	var json ={
		"subscription_id":subs
        
	}
	console.log(json);
	getsubscription(json);

}

function __success_subscription(response)

{
	// console.log()
	 $(response.data).each(function(index,key){
        if(key.type>1  && key.type<3){

            if(index==3){
                $('#subscription').append(`
                <a  data-id="${index}" class="imagesubs"> <div class="dashboard-card subscription-card active-card ml-0 mr-2" value="1" id="hello${index}">
                <input type="hidden" id="subsd${index}" value="${key.id}">
                    <div class="top-row ">${key.note} <img id="subsimg${index}" class="img1" style="display:block;" src="../images/icons/checkmark.png"></div>
                 <h1>${key.subscription_title}</h1>
        
                 <p>KSH ${key.price}</p>
                </div></a>
                `)
            }else{
                $('#subscription').append(`
                <a  data-id="${index}" class="imagesubs"> <div class="dashboard-card subscription-card ml-0 mr-2" value="1" id="hello${index}">
                <input type="hidden" id="subsd${index}" value="${key.id}">
                    <div class="top-row ">${key.note} <img id="subsimg${index}" class="img1" style="display:none;" src="../images/icons/checkmark.png"></div>
                    <h1>${key.subscription_title}</h1>
        
                    <p>KSH ${key.price}</p>
                </div></a>
                `)
            }
        
        
}
    if(key.type>2){
        console.log(key)
        if(key.type==4){
            $('#subscriptionannualy').append(`
            <a href="https://home.twiva.co.ke/contact-us" class="imagesubs" data-id="${index}" target="blank"> <div class="dashboard-card subscription-card" value="1" >
            <input type="hidden" id="subsd${index}" value="${key.id}">
                <div class="top-row ">${key.note}</div>
             <h1> ${key.subscription_title} </h1>
            </div></a>
            `);
        }else{
            $('#subscriptionannualy').append(`
            <a  data-id="${index}" class="imagesubs"> <div class="dashboard-card subscription-card" value="1" id="hello${index}">
            <input type="hidden" id="subsd${index}" value="${key.id}">
                <div class="top-row ">${key.note} <img id="subsimg${index}" class="img1" style="display:none;" src="../images/icons/checkmark.png"></div>
             <h1>${key.subscription_title}</h1>
    
             <p>KSH ${key.price}</p>
            </div></a>
            `);
        }
}
    })

        	  


	  $(document).on("click", ".imagesubs", function () {
     var myBookId = $(this).data('id');
     var dd = myBookId - 1 ;
     $('.dashboard-card').removeClass('active-card')
     $('.img1').hide();
     if($('#hello'+myBookId).hasClass("active-card")){
     	$('#hello'+myBookId).attr('class', 'dashboard-card subscription-card');


     
     }
     else{
     	$('#hello'+myBookId).attr('class', 'dashboard-card subscription-card active-card');
     		$('#subsimg'+myBookId).show();	
     }
     });


	$(document).on("click", "#subscriptionbtn", function () {
    var myBookId = $(this).data('id');
    var subs =$('.active-card').children('input').val();
    var mpesa_number = '254'+$('#mpesa_number').val();
    if(subs==undefined || subs ==0){
        alert('Please select plan first');
    }
    if($('#mpesa_number').val()==''){
        $("#transaction_error").html('Please enter MPESA number');
    }
    else{

        console.log( $('.active-card').closest('input').val());
    	$("upgrade-subscription").show();
        purchaseSubscription(subs,mpesa_number);
         
     }
  
        //subsid(subs);

     });
	 
     
     $(document).on("click", "#plan-continue", function () {
        
        var subs=0; 
        subs =$('.active-card').children('input').val()
        if(subs==undefined || subs ==0){
            alert('Please select plan first');
        }
        else{

           // $("#subsriptionPlans").css("display","none");
            //$("#phone").css("display","block");
            
        }
        
        });
        

	  	//  $('#subscriptionbtn').click(function(){
    // 	 	var subs = document.getElementById('subsd'+myBookId).value;
    //  	console.log(subs);
    // 	getsubscription(subs);
    // });
}



