function AddProduct(json){

    __ajax_httpproduct("add-product", json, headers(), AJAX_CONF.apiType.POST, "", __success_product);      
}


function updateProduct(json){

    __ajax_httpproduct("product/"+json.id, json, headers(), AJAX_CONF.apiType.PUT, "", __update_product);      
}

//category

function update_product() {

    document.getElementsByClassName("btntest1")[0].click();
var checkedArray='';
    for(i=0;i<100;i++){               //  $("#size_ids18").prop('checked')
        if($("#size_ids"+i).prop('checked')){
            checkedArray+=$("#size_ids"+i).val()+",";
        }
    }

if (checkedArray.indexOf(',', this.length - ','.length) !== -1) {
    checkedArray = checkedArray.substring(0, checkedArray.length - 1);
    // console.log(checkedArray);
}

var colorarray='';
    for(i=0;i<100;i++){               //  $("#size_ids18").prop('checked')
        if($("#color"+i).prop('checked')){
            colorarray+=$("#color"+i).val()+",";
        }
    }

if (colorarray.indexOf(',', this.length - ','.length) !== -1) {
    colorarray = colorarray.substring(0, colorarray.length - 1);
    // console.log(colorarray);
}

    var json = 
        {"id":STORAGE.get(STORAGE.pid),
        "name":$("#title").val(),
        "price":$("#price").val(),
        "category":$("#category").val(),
        "stock":$("#stock").val(),
        "size_ids":STORAGE.get(STORAGE.productsize),
        "color_ids":STORAGE.get(STORAGE.colors),
        "sku":$("#specification").val(),
        "specification":$("#specification").val(),
        "product_images":[
        {
            "image": STORAGE.get(STORAGE.image),
            "is_cover_pic":1
        }, 
        {
            "image": STORAGE.get(STORAGE.image1),
            "is_cover_pic":0
        },
         {
            "image": STORAGE.get(STORAGE.image2),
            "is_cover_pic":0
        },
         {
            "image": STORAGE.get(STORAGE.image3),
            "is_cover_pic":0
        },
          {
            "image": STORAGE.get(STORAGE.image4),
            "is_cover_pic":0
        }

    ],
        "description":$("#description").val(),
    };
    console.log(json);
        updateProduct(json);
}





function __update_product(response)
{
   alert("product updated successfully")
       goto(UI_URL.onboarding8);

}



var specifications='';

 function addproduct() {
    
var checkedArray='';
    for(i=0;i<100;i++){               //  $("#size_ids18").prop('checked')
        if($("#size_ids"+i).prop('checked')){
            checkedArray+=$("#size_ids"+i).val()+",";
        }
    }

if (checkedArray.indexOf(',', this.length - ','.length) !== -1) {
    checkedArray = checkedArray.substring(0, checkedArray.length - 1);
    // console.log(checkedArray);
}

var colorarray='';
    for(i=0;i<100;i++){               //  $("#size_ids18").prop('checked')
        if($("#color"+i).prop('checked')){
            colorarray+=$("#color"+i).val()+",";
        }
    }

if (colorarray.indexOf(',', this.length - ','.length) !== -1) {
    colorarray = colorarray.substring(0, colorarray.length - 1);
    // console.log(colorarray);
}

if(STORAGE.get(STORAGE.image)==null){
    // alert('Please select atleast main image');
    $("#productAddModal").modal('show');
    return; 
}

if(specifications.length==0){
    specifications='';
}
let sub_category = $("#sub_category").val();
if(sub_category) {
    sub_category = sub_category.toString();
}
    var json = 
        {"name":$("#title").val(),
        "price":$("#price").val(),
        "sku":$("#sku").val(),
        "category":$("#category").val(),
        "sub_category": sub_category,
        "stock":$("#stock").val(),
        "size_ids":STORAGE.get(STORAGE.productsize),
        "color_ids":STORAGE.get(STORAGE.colors),
        // "sku":$("#specification").val(),
        "specification":specifications,
        "product_images":[
        {
            "image": STORAGE.get(STORAGE.image),
            "is_cover_pic":1
        }, 
        {
            "image": STORAGE.get(STORAGE.image1),
            "is_cover_pic":0
        },
         {
            "image": STORAGE.get(STORAGE.image2),
            "is_cover_pic":0
        },
         {
            "image": STORAGE.get(STORAGE.image3),
            "is_cover_pic":0
        },
          {
            "image": STORAGE.get(STORAGE.image4),
            "is_cover_pic":0
        }

    ],
        "description":$("#description").val(),
    };
    console.log(json);
        AddProduct(json);
}


function __success_product(response) {
  console.log(response);
    if (response.status == true) {

        STORAGE.remove(STORAGE.image);
        STORAGE.remove(STORAGE.image1);
        STORAGE.remove(STORAGE.image2);
        STORAGE.remove(STORAGE.image3);
        STORAGE.remove(STORAGE.image4);

           $("#successMessage").show().html("<strong >Success!</strong> Product has been added successfully");
    setTimeout(function() {
    $('#successMessage').fadeOut('fast');
}, 3000);
       
$('html, body').animate({ scrollTop: 0 }, 'fast');
$("successMessage").css("display", "block")
$("#successMessage").show();
$("#successMessage").html("<strong >Success!</strong> Product has been added successfully");
    setTimeout(function() {
    $('#successMessage').fadeOut('fast');
}, 3000);

     console.log("sucesss");

     setTimeout(function() {
          goto(UI_URL.onboarding7);
       }, 1000);
}
}




//validations
$(document).ready(function(){
    $('#titlecheck').hide();
    $('#pricecheck').hide();
    $('#descriptioncheck').hide();
    $('#specificationcheck').hide();

    var title_err = true;
    var proce_err = true;
    var description_err = true;
    var specification_err = true;



    $('#specification').keyup(function(){
        specification_check();
        
    });

    function specification_check(){
        for(i=1; i<=10;i++){
            specification = "specification"+i;
            if($("#"+specification).length){
               
                if($('#'+specification).val().length==''){
                    // $('#specificationcheck').show();
                    // $('#specificationcheck').html("Please fill the specification");
                    // $('#specificationcheck').focus();
                    // $('#specificationcheck').css("color","red");
                  //  specification_err = false;
                   // return false;
                }
                else{
                    $('#specificationcheck').hide();
                    specifications+=$("#"+specification).val()+",";
                    //specifications.push($("#"+specification).val());
        
                }
            }
        }
    }


    $('#description').keyup(function(){
        description_check();
        
    });

    // function description_check(){
    //     var description_val = $('#description').val();
    //     if(description_val.length == ''){
    //         $('#descriptioncheck').show();
    //         $('#descriptioncheck').html("Please fill the description");
    //         $('#descriptioncheck').focus();
    //         $('#descriptioncheck').css("color","red");
    //         description_err = false;
    //         return false;
    //     }
    //     else{
    //         $('#descriptioncheck').hide();

    //     }
    // }

    function description_check(){
        var description_val = $('#description').val();
        if(description_val.length <= 250 && description_val.length >= 20){
            $('#descriptioncheck').hide();
        }
        else{
            $('#descriptioncheck').show();
            $('#descriptioncheck').html("Please fill the description between 20-250 characters limit");
            $('#descriptioncheck').focus();
            $('#descriptioncheck').css("color","red");
            description_err = false;
            return false;
        }
    }

    $('#title').keyup(function(){
        title_check();
        
    });

    // function title_check(){
    //     var title_val = $('#title').val();
    //     if(title_val.length == ''){
    //         $('#titlecheck').show();
    //         $('#titlecheck').html("Please fill the title");
    //         $('#titlecheck').focus();
    //         $('#titlecheck').css("color","red");
    //         title_err = false;
    //         return false;
    //     }
    //     else{
    //         $('#titlecheck').hide();
    //     }
    // }

    function title_check(){
        var title_val = $('#title').val();
        if(title_val.length <= 70 && title_val.length >= 3){
            $('#titlecheck').hide();
        }
        else{
            $('#titlecheck').show();
            $('#titlecheck').html("Please fill the title between 3-70 characters limit");
            $('#titlecheck').focus();
            $('#titlecheck').css("color","red");
            title_err = false;
            return false;
        }
    }

    $('#price').keyup(function(){

        price_check();
        if(isNaN($("#price").val())){
            $('#pricecheck').show();
            $('#pricecheck').html("Please fill the price correctly");
            $('#pricecheck').focus();
            $('#pricecheck').css("color","red");
            price_err = false;
            return false;
        
        }
        
    });

    function price_check(){
        var price_val = $('#price').val();
        if(price_val.length == ''){
            $('#pricecheck').show();
            $('#pricecheck').html("Please fill the price");
            $('#pricecheck').focus();
            $('#pricecheck').css("color","red");
            price_err = false;
            return false;
        


        }
        else{
            $('#pricecheck').hide();

        }

    }

    $('#submitbtn1').click(function(){
         title_err = true;
         price_err = true;
         specification_err = true;
         description_err = true;
        title_check();
        price_check();
        specification_check();
        description_check();
        if((title_err == true) && (price_err == true)  && (specification_err == true)  && (description_err == true)){
            document.getElementsByClassName("btntest1")[0].click();
             addproduct();
        }
        else{
            return false;
        }
    });

    // $('#submitbtn1').click(function(){
        
    //     var title = document.getElementById("title");
    //     if(title.value.length <= 70 && title.value.length >= 3){
    //         // alert("success");
    //         title_err = true;
    //         price_err = true;
    //         specification_err = true;
    //         description_err = true;
    //         title_check();
    //         price_check();
    //         specification_check();
    //         description_check();
    //         if((title_err == true) && (price_err == true)  && (specification_err == true)  && (description_err == true)){
              
    //             document.getElementsByClassName("btntest1")[0].click();
    //              addproduct();
    //         }
    //         else{
    //             return false;
    //         }
    //     }
    //     else{
    //         alert("Input Field Must Be Between 3-70 Characters Long");
    //     }
    // });

});