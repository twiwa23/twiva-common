var country_id = 0;
var state_id = 0;
var city_id = 0;

function profile_details() {
    __ajax_httpproduct("get-business-details", "", headers(), AJAX_CONF.apiType.GET, "", __success_profile);
}

function __success_profile(response) {
    $(response.data).each(function (index, key) {
        console.log(key);
        let logo = STORAGE.set(STORAGE.logo, key.logo);
        let city_name = "-";

        if (key.city && key.city != null) {
            city_name = key.city.name;
        }
        if (!key.area || key.area == null) {
            key.area = "-";
        }
        if (!key.building || key.building == null) {
            key.building = "-";
        }
        $("#profile_details").append(`



        	  <div class="col-md-6 user-info-wrapper">
                        <din class="user-info">
                            <din class="profile-wrapper">
                                <img src="https://api.twiva.co.ke/storage/images/products/${key.logo}" alt="" >
                            </din>
                            <div class="content-wrapper">
                              <div class="company-info">
                                  <h4>
                                    ${key.bussiness_name}
                                  </h4>
                                  <p>
                                    ${key.email}
                                  
                                  </p>
                              </div>
                              <div class="contact-wrapper">
                                  <p>
                                      <span>Contact Person:</span><span>${key.contact_name} </span>
                                  </p>
                              </div>
                               <div class="contact-wrapper">
                                  <p>
                                      <span>Contact Number:</span><span>${key.business_number} </span>
                                  </p>
                              </div>
                              <div class="about-wrapper">
                                  <p>
                                    ${key.description}.
                                  </p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 address-wrapper">
                        <div class="address">
                            <h4>Address</h4>
                            <div class="addres-content">
                               <p><span>Country</span> <span>${key.country.name} </span> </p>
                                <p><span>City</span> <span>${city_name} </span> </p>                
                                <p><span>County</span> <span>${key.state.name} </span> </p>
                                <p><span>Building No.</span> <span>${key.building} </span> </p>
                                <p><span>Area</span> <span>${key.area} </span> </p>
                            </div>
                            <div class="edit-btn-wrapper">
                                <a href="bussiness-edit-profile-information.php">
                                    <span>Edit Information</span>
                                </a>
                            </div>
                        </div>
                    </div>


        	`);

        document.getElementById("area").value = key.area;
        document.getElementById("building").value = key.building;
    });
}

function profile_business() {
    __ajax_http("get-business-details", "", headers(), AJAX_CONF.apiType.GET, "", __success_profile_business, error_profile_business);
}

function error_profile_business(response) {
    console.log(response.responseJSON.message);
    if (response.responseJSON.message == "No company found") {
        let image_path = "<?php echo IMAGES_URI_PATH?>/icons/profile.jpeg";

        $("#image").append(`

        <label class="drop-zone__prompt" for="drop-imageUpload"><img id="blah" src="${image_path}">
               <input type="file" name="myFile" class="drop-zone__input" id="drop-imageUpload"  onchange="readURL(this);">
              
           </label>
           `);
    }
}

function __success_profile_business(response) {
    $(response.data).each(function (index, key) {
        state(key.country.id);
        city(key.state.id);
        document.getElementById("logo_name").value = key.logo;
        document.getElementById("business_name").value = key.bussiness_name;
        document.getElementById("contact_name").value = key.contact_name;
        document.getElementById("comp_email").value = key.email;
        document.getElementById("business_number").value = key.business_number;
        document.getElementById("description").value = key.description;
        document.getElementById("description").value = key.description;

        if(key.logo==null){

            $("#image").append(`
            <label class="drop-zone__prompt" for="drop-imageUpload"><img id="blah" src="../images/icons/profile.jpeg">
                <input type="file" name="myFile" class="drop-zone__input" id="drop-imageUpload"  onchange="readURL(this);">
            </label>
        `);
    }else{
        $("#image").append(`
        <label class="drop-zone__prompt" for="drop-imageUpload"><img id="blah" src="http://api.twiva.co.ke/storage/images/products/${key.logo}">
               <input type="file" name="myFile" class="drop-zone__input" id="drop-imageUpload"  onchange="readURL(this);">
              <!-- uploded image preview  -->
              
           </label>
           `);
    }

        country_id = key.country.id;

        state_id = key.state.id;
        city_id = key.city.id;
    });
}

// update business profile
function update_profile_business(json) {
    __ajax_httpproduct("edit-business-details", json, headers(), AJAX_CONF.apiType.PUT, "", __success_profile_update);
}

function update_profile() {
    json = {
        comp_busi_name: $("#business_name").val(),
        comp_contact_name: $("#contact_name").val(),
        comp_busi_number: $("#business_number").val(),
        comp_description: $("#description").val(),
        comp_logo: $("#logo_name").val(),//STORAGE.get(STORAGE.image),
        comp_country: $("#country").val(),
        comp_state: $("#state").val(),
        comp_city: $("#city").val(),
        comp_area: $("#area").val(),
        comp_building: $("#building").val(),
        comp_email: $("#comp_email").val(),
        comp_pincode: "",
    };
    console.log(json);
    STORAGE.set(STORAGE.name, $("#business_name").val());
    let user_check = STORAGE.get(STORAGE.needToUpdatePlatform);
    if (user_check == 1 || user_check == "1") {
        update_old_user_profile(json);
    } else {
        update_profile_business(json);
    }
}

function update_old_user_profile(json) {
    __ajax_httpproduct("influencer/user/update-profile", json, headers(), AJAX_CONF.apiType.POST, "", __success_profile_update);
}

function __success_profile_update(response) {
    if (response.status == true) {
        alert("Profile Updated Successfully");
        var logo = STORAGE.get(STORAGE.image);
        console.log(logo);
        STORAGE.set(STORAGE.logo, logo);
        STORAGE.set(STORAGE.needToUpdatePlatform, btoa(btoa(0)));
        setTimeout(() => {
            goto(UI_URL.onboarding10);
            goto(UI_URL.onboarding10);
            goto(UI_URL.onboarding10);
            goto(UI_URL.onboarding10);
            goto(UI_URL.onboarding10);
        }, 100);
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blah").attr("src", e.target.result);
            /*.width(150)
                        .height(200);*/
        };
        logoUpload1(input);
        reader.readAsDataURL(input.files[0]);
        $("#user").addClass("hide");
        $("#blah").removeClass("hide");
    }
}

function state(country_id) {
    __ajax_http("state/select?country_id=" + country_id, "", { Accept: "application/json" }, AJAX_CONF.apiType.GET, "", sucessstate);
}

function city(state_id) {
    __ajax_http("city/select?state_id=" + state_id, "", { Accept: "application/json" }, AJAX_CONF.apiType.GET, "POST LOGIN", sucesscity);
}

function country() {
    __ajax_http("country/list", "", { Accept: "application/json" }, AJAX_CONF.apiType.GET, "POST LOGIN", __success_country);
}

function __success_country(response) {
    appendHTML = "";
    response.countries.forEach(function (element) {
        appendHTML += '<option value="' + element.id + '">' + element.name + "</option>";
    });
    $("#country").html(appendHTML);
}

function city1() {
    var state = document.getElementById("state").value;
    var state_id = state;
    city(state_id);
}

function sucesscity(response) {
    $("#city").html("");
    $(response.cities).each(function (index, key) {
        $("#city").append(`
          <option value="${key.id}">${key.name}</option>`);
    });
}

//state fetch

function state1() {
    var country = document.getElementById("country").value;
    var country_id = country;
    state(country_id);
}

function sucessstate(response) {
    $("#state").html("");
    $(response.states).each(function (index, key) {
        $("#state").append(`
          <option value="${key.id}">${key.name}</option>`);
    });
}
