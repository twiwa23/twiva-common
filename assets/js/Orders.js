var image_path ="https://api.twiva.co.ke/storage/images/";
function fetch_orders(){
    __ajax_http("business/order-listing?page=1&limit=10", "", headers(), AJAX_CONF.apiType.GET, "", __success_orders);
}

function fetch_order_detail(orderId){
    __ajax_http("business/order-details/"+orderId, "", headers(), AJAX_CONF.apiType.GET, "", __success_order_details);
}

function __success_orders(response){
    var appendHTML = '';

    if(response.success==true){
        if(response.data.length>0){
            response.data.forEach(function(element) {
                BtnName='';
                if(element.status==1){
                    BtnName="Pending";
                }
                if(element.status==1){
                    BtnName="In progress";
                }
                if(element.status==2){
                   BtnName="Dispatch Order";
               }
               if(element.status==3){
                   BtnName="Delivered";
               }
           
                var date1 = element.created_at;
                date1 = date1.split('T')[0];
                appendHTML+=' <tr>'+
                '<td>  <a href="order-detail.php?order_id='+btoa(btoa(element.order_id))+' ">'+element.order_id+'</a></td>'+
                '<td><a href="order-detail.php?order_id='+btoa(btoa(element.order_id))+' ">'+element.user_info.email+'</a></td> '+
                ' <td>'+date1+'</td>'+
                // ' <td>--</td>'+
                ' <td>'+element.quantity+'</td>'+
                ' <td>'+element.amount+'</td>'+
                ' <td><div class="green-txt">'+BtnName+'</div></td>'+
                ' </tr>';
            });
            $("#order-listing").html(appendHTML);
        }else{
            appendHTML+= '<tr class="product-box-inner p-3" style="border:0;height:250px" id="nf">'+
                '<td class="text-center p-5" colspan="6">'+
                    '<img src="../images/icons/empty.svg" style="width: 80px;height: 100px;">'+
                        '<h3 style="font-size:20px;">'+
                            'You don’t have any orders'+
                        '</h3>'+
                '</td>'+
            '</tr>';
            $("#order-listing").html(appendHTML);
        }
    }
    else{
        $("#order-listing").html(appendHTML);
    }
    
}


window.addEventListener('DOMContentLoaded', (event) => {
    __is_user_logged_in();
    var path = window.location;
    console.log(path.pathname);
    console.log("<br>");
    console.log(UI_URL.orders);

    if(path.pathname==UI_URL.orders){
        fetch_orders();
    }

    if(path.pathname==UI_URL.orderDetail){
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        var order_id = urlParams.get('order_id')
        order_id = atob(atob(order_id));
        fetch_order_detail(order_id);


    }
});

function __success_order_details(response){

    console.log(response);
    total_amount=0;
    amount=0;
    const format1 = "YYYY MMMM DD"
    shipping_charges=0;
    //dateTime1 = moment(response.data.order_details[0].created_at).format(format1);
    dateTime1 = response.data.order_details[0].created_at;
    
    var orderId=0;
    var status =response.data.order_details[0].status;
    if(response.success==true){
     var active='';
     var active1='';
     var active2='';
     var active3 ="";
        if(status==0){
            active="active";
             BtnVal=1;
             BtnName="In progress";
         }
         if(status==1){
            active="active";
            active1="active";
            BtnVal=2;
            BtnName="Dispatch Order";
        }
        if(status==2){
            active="active";
            active1="active";
            active2="active";
            BtnVal=3;
            BtnName="Delivered";
        }
    
        if(status==3){
            active="active";
            active1="active";
            active2="active";
    
            active3="active";
            BtnVal=3;
            BtnName="Delivered";
        }
    

        date1 = dateTime1.split('T')[0];
        $(".order-number").html("Order No. "+ response.data.order_details[0].order_id);
        $(".order-by").html("Order by:  "+ response.data.order_details[0].user_info.email);
        $(".order-date").html( "Order date : "+ date1);
        $(".order-status").html("Order Status : "+ BtnName);
        var appendHTML='';
        response.data.order_details.forEach(function(element) {
            OrderId = element.id;
            status = element.status;
            amount = element.amount * element.quantity;
            appendHTML+='<tr> <td scope="row"> <div class="product-img-nane-wrapper">';
            element.product_images.forEach(function(el){
                if(element.product_id==el.shop_product_id && el.is_cover_pic==1){
                    appendHTML+='<div class="img"><img src="'+image_path + 'products/'+ el.image_path+'" alt=""></div>'
                }

            });
            appendHTML+='<div class="name"><span>'+ element.prduct_details.product_title+'</span></div></div></td>';
            appendHTML+='<td>'+element.amount+'</td>';
            appendHTML+=' <td>'+element.quantity+'</td>';
            appendHTML+='  <td>'+element.amount * element.quantity+'</td>';
            appendHTML+='</tr>';
     });
     $("#order-items").html(appendHTML);
     $(".subTotal-amt").html(amount);
     $(".shipping-fee").html(shipping_charges);
     $(".total").html(amount + shipping_charges);
     if(response.data.address !=null){
     shippingHTML='<p> Name : '+response.data.address.first_name;
     shippingHTML+=' '+ response.data.address.last_name+'</p>';
     shippingHTML+='<p> Apartment : '+response.data.address.apartment +' '+ response.data.address.locality+'</p>';
     shippingHTML+='<p> City : '+response.data.address.city.name +' ,'+ response.data.address.state.name+'</p>';
     shippingHTML+='<p> Country : '+response.data.address.country.name +' .</p>';
     $("#shipping-address").html(shippingHTML);
     }

    
 
    
    $("#progressbar").html('<li class="step0 '+active+' " id="step1">Received</li><li class="step0  '+active1+' " id="step2">In progress</li><li class="step0  '+ active2 +' " id="step3">dispatch</li><li class="step0  '+active3+' " id="step4">Delivered</li>');
    if(status!=3)
     $("#ButtonDiv").html('<button class="white-bttn" >Cancel Order</button> <button class="purple-btn" onclick="changeOrderStatus('+BtnVal+','+OrderId+');">'+BtnName+'</button>');

    }

}

function PrintDiv() 
{  
    var divContents = document.getElementById("print-div").innerHTML;  
    var printWindow = window.open('', '', 'height=200,width=400');  
    printWindow.document.write('<html><head><title>Order receipt</title>');
    printWindow.document.write('<link rel="stylesheet" href="../css/style.css" type="text/css" />');
    printWindow.document.write('</head><body >');  

    printWindow.document.write(divContents);  
    printWindow.document.write('</body></html>');  
    printWindow.document.close();  
    printWindow.print();  
 }  

function changeOrderStatus(Val,OrderId){
    var json={
        "status":Val,
        "order_details_id":OrderId
    }
    __ajax_http("business/orders/change-status", json, headers(), AJAX_CONF.apiType.POST, "", "");
    fetch_order_detail(OrderId);
}




function fetch_payments(page){
    __ajax_http("business/payment-history?page="+page+"&limit=10", "", headers(), AJAX_CONF.apiType.GET, "", __success_payments);
}



function __success_payments(response){
    var appendHTML = '';
    var total_amount=0;
    if(response.success==true){        
        response.data.forEach(function(element) {
            total_amount+=element.amount;
            var date1 = element.created_at;
            date1 = date1.split('T')[0];
            appendHTML+=' <tr>'+
            '<td>  <a href="order-detail.php?order_id='+btoa(btoa(element.order_id))+' ">'+element.order_id+'</a></td>'+
            '<td><a href="order-detail.php?order_id='+btoa(btoa(element.order_id))+' ">'+element.user_info.email+'</a></td> '+
            ' <td>'+date1+'</td>'+
            ' <td>--</td>'+
            ' <td>'+element.quantity+'</td>'+
            ' <td>'+element.amount+'</td>'+
            ' <td><div class="green-txt">Pending</div></td>'+
            ' </tr>';
        });
        $("#earnings").html(appendHTML);
    }
    else{
        $("#earnings").html(appendHTML);
    }
    $("#total_amount").html("KSH " + total_amount);
}

function fetch_earnings(){
    __ajax_http("business/order-listing?page=1&limit=10", "", headers(), AJAX_CONF.apiType.GET, "", __success_payments);
}
