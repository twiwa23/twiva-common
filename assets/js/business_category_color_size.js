
function get_category(){

    __ajax_httpproduct("categories", "", headers(), AJAX_CONF.apiType.Get, "", __success_category);      
}



//color 
function Get_size(id){

    __ajax_httpproduct("size-color?subcategory_id="+id, "", headers(), AJAX_CONF.apiType.Get, "", __success_size);      
}


function Get_sizes()
{
	var id = document.getElementById('sub_category').value;
    Get_size(id);
    Get_Single_SubCategory(id);
}

function GetSubCategory(){
    var id = document.getElementById('category').value;
    get_subcategory(id);   
}
function get_subcategory(id){

    __ajax_httpproduct("sub-categories/"+id, "", headers(), AJAX_CONF.apiType.Get, "", __success_sub_category);      
}

function Get_Single_SubCategory(id){
    __ajax_httpproduct("sub-categories/1?subcategory_id="+id, "", headers(), AJAX_CONF.apiType.Get, "", __success_single_subcategory);    
}

function __success_single_subcategory(response){
    if(response.data.is_color==0){
        document.getElementById("color-field").style.display='none';
    }
    if(response.data.is_color==1){
        document.getElementById("color-field").style.display='block';
    }
    if(response.data.is_size==0){ 
        document.getElementById("sa").style.display='none';
     }
     if(response.data.is_size==1){
        document.getElementById("sa").style.display='block';
     }    
}

function __success_sub_category(response) {
    var options =  '<option value="0"><strong>Select Subcategory</strong></option>';
    response.data.forEach(element => {
        options += `<option value="${element.id}"> ${element.name}</option>`;
    });
    $('#sub_category').html(options); 
    if(response.data.length==0){
        document.getElementById("sca").style.display='none';  
        document.getElementById("color-field").style.display='none';
        document.getElementById("sa").style.display='none';      
    }  
    else{
        document.getElementById("sca").style.display='block';
    }
}

function __success_size(response) {	
    var sizes = '';
	// console.log(response);
 $('#size').html('');
 $('#color').html('');
 

  $(response.sizes).each(function(index,key){
        
        sizes += $('#size').append(`
            

            <option value="${key.id}">${key.name}</option>  
        
       `);
         

    });


  $('#colors li').remove();
  $(response.colors).each(function(index,key){
        
 
    // $('#color').append(`
            

    //    <option value="${key.id}">${key.name}</option>  

    //    `)
    $('#colors').append(`
            

    <li class="color-list-item" title="${key.name}" style="background-color:#${key.hexaval};" data-color="${key.hexaval}" data-id="${key.id}">
                        <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                    </li> 

    `)

    })

    $(".color-list-item").on("click", function(event) {
        event.preventDefault();
        $(this).toggleClass("selected-color");
        $("#color").val($(this).attr("data-id"));
    });
    var colorsids = [];
    $('.color-list .color-list-item').on('click',function(){
            if($(this).hasClass("selected-color")){
                colorsids.push($("#color").val());
                // alert("Color Stored");
            }else{
                var index = colorsids.indexOf($("#color").val());
                // colorsids.remove($("#color").val());
                colorsids.splice(index, 1);
            }
            STORAGE.set(STORAGE.colors, colorsids);
        });
        
}

function __success_category(response) {
  //console.log(response);
  $(response.data).each(function(index,key){
$('#category').append(`<option value="${key.id}"> ${key.name}</option>`)})
}
