var image_path ="https://api.twiva.co.ke/storage/images/";
function fetch_products(){
   

    __ajaxregister_http("fetch-products", "", headers(), AJAX_CONF.apiType.GET, "", __success_products);

}

function fetch_cat(category){
   

    __ajaxregister_http("fetch-products?categories="+category, "", headers(), AJAX_CONF.apiType.GET, "", __success_products);

}
function publish_product(json){
   

    __ajaxregister_http("publish-unpublish", json, headers(), AJAX_CONF.apiType.PUT, "", __success_publish);

}


function delete_products(id){

    __ajaxregister_http("product/"+id, "", headers(), AJAX_CONF.apiType.DELETE, "", __success_delete);
       
}

function product_details(id){

    __ajaxregister_http("product/"+id, "", headers(), AJAX_CONF.apiType.GET, "", __success_details);
       
}



function __success_publish(response)
{
    if(response.success == true){
        document.getElementsByClassName("open-AddBookDialog")[0].click();
        $("#message").html(response.message)
        var value = $("#switch").val();
        if(value == 1) {
            $("#product_status").html('Published')
        } else {
            $("#product_status").html('Unpublished')
        }

    }
    if(response.success == false){
        //$(".label-switch").hide();
        $("#switch").prop('checked', false);
        document.getElementsByClassName("open-AddBookDialog")[0].click();
        $("#message").html(response.message)
   }
}
function category_fetch()
{
  var category=
    $("#category").val();
  fetch_cat(category);

  // console.log(category);  
}

// product details 
function product_edit(id)
{
    STORAGE.set(STORAGE.pid, id);
    goto(UI_URL.onboarding9);
}

function product_detail(id)
{
   STORAGE.set(STORAGE.pid, id);
    goto(UI_URL.onboarding8);
}

function __success_details(response)
{
console.log(response);
//STORAGE.set(STORAGE.cat, response.data.category);
//STORAGE.set(STORAGE.siz, response.data.color_details[0].id);
//STORAGE.set(STORAGE.col, response.data.size_details[0].id);
var name = response.data.name;
var id = response.data.id;
var price = response.data.price;
var stock = response.data.stock;
var sku = response.data.sku;
//var size = response.data.size_details[0].name;
//var color = response.data.color_details[0].name;
var description = response.data.description;
var publishedStatus = response.data.status;
var image = response.data.product_images[0].image;
var rating = response.data.rating;
var description = response.data.description;



if(response.data.specification!=null){
    var specs  =  response.data.specification.split(",");
}
else{
    var specs  = '';
}


specification='';
for (k=0;k<specs.length;k++) {
    if(specs[k]!='')
    specification+='<li>'+ specs[k]+'</li>';
}
var checked='';
var status= "Unpublished";
if(publishedStatus==1){
    checked='checked = "checked"';
    status = "Published";
}
inStock ="In Stock";

if(stock==0 || response.data.stock==0 ){
    inStock="Out of Stock";
}


if(response.data.specification!=null){
    var specs  =  response.data.specification.split(",");
}
else{
    var specs  = '';
}


specification='';
for (k=0;k<specs.length;k++) {
    if(specs[k]!='')
    specification+='<li>'+ specs[k]+'</li>';
}
var checked='';
if(publishedStatus==1){
    checked='checked = "checked"';
}
inStock ="In Stock";

if(stock==0 || response.data.stock==0 ){
    inStock="Out of Stock";
}




$('#product-de').append(`

    <div class="product-detail-cont">
        <div class="product-detail-slider-wrapper">
	        <div class="container ">
                <div class="carousel-container position-relative row">
                    <div id="productCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" id="bigImage">  

                        </div>
                    </div>  
                    <div id="carousel-thumbs" class="carousel slide " data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row mx-0" id="smallImage">

                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>               
                </div>
            </div>
        </div>
<div class="switch-check-box-wrapper">
<span id="product_status">${status}</span>
<input type="checkbox" ${checked}  onchange="publish()" id="switch" value="" />
<label class="label-switch" for="switch">Toggle</label>
</div>
</div>

<div class="product-specification" id="detailsproduct">
<h2>${name}</h2>
<h3><label style="width:73px">Price: </label><b>KSH ${price}</b></h3>

<div class="spec-row">
    <label>Qty:</label>
    <div><h3>${stock} piece</h3> <button>${inStock}</button></div>
</div>


<div class="spec-row" id="size-field">
    <label>Size:</label>
    <div id="size">
    </div>
    </div>
    
    <div class="spec-row align-items-start" id="color-field">
    <label>Color:</label>
    <div id="color" class="flex-wrap">
    </div>
    <div>
       </div>
       </div>
       <div class="prod-desc">
    <label>Description</label>
    <p>${description}</p>
</div>

<div class="prod-spec">
    <label>Specification</label>
    <ul id='specs'>
       ${specification}
    </ul>
</div>

`)
$(response.data.product_images).each(function(index,key){

    if(typeof key.image !== 'object' && key.image !== "null"){         
    $('#bigImage').append(`
        <div class="carousel-item" data-slide-number="${index}">
        <img src="${image_path}products/${key.image}" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/Pn6iimgM-wo/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
        </div>      
`);

if(index==0){
    $('.carousel-item').attr("data-slide-number","0").addClass('active');
}
//////////////////// product slider////////////////////////////////
$(document).ready(function () {
    $('#productCarousel').carousel({
      interval: false
    });
    $('#carousel-thumbs').carousel({
      interval: false
    });
    
    // handles the carousel thumbnails
    // https://stackoverflow.com/questions/25752187/bootstrap-carousel-with-thumbnails-multiple-carousel
    $('[id^=carousel-selector-]').click(function() {
      var id_selector = $(this).attr('id');
      var id = parseInt( id_selector.substr(id_selector.lastIndexOf('-') + 1) );
      $('#productCarousel').carousel(id);
    });
    // Only display 3 items in nav on mobile.
    if ($(window).width() < 575) {
      $('#carousel-thumbs .row div:nth-child(4)').each(function() {
        var rowBoundary = $(this);
        $('<div class="row mx-0">').insertAfter(rowBoundary.parent()).append(rowBoundary.nextAll().addBack());
      });
      $('#carousel-thumbs .carousel-item .row:nth-child(even)').each(function() {
        var boundary = $(this);
        $('<div class="carousel-item">').insertAfter(boundary.parent()).append(boundary.nextAll().addBack());
      });
    }
    // Hide slide arrows if too few items.
    if ($('#carousel-thumbs .carousel-item').length < 2) {
      $('#carousel-thumbs [class^=carousel-control-]').remove();
      $('.machine-carousel-container #carousel-thumbs').css('padding','0 5px');
    }
    // when the carousel slides, auto update
    $('#productCarousel').on('slide.bs.carousel', function(e) {
      var id = parseInt( $(e.relatedTarget).attr('data-slide-number') );
      $('[id^=carousel-selector-]').removeClass('selected');
      $('[id=carousel-selector-'+id+']').addClass('selected');
      
    });
    // when user swipes, go next or previous
    $('#productCarousel').swipe({
      fallbackToMouseEvents: true,
      swipeLeft: function(e) {
        $('#productCarousel').carousel('next');
      },
      swipeRight: function(e) {
        $('#productCarousel').carousel('prev');
      },
      allowPageScroll: 'vertical',
      preventDefaultEvents: false,
      threshold: 75
    });
    $('#productCarousel .carousel-item img').on('click', function(e) {
      var src = $(e.target).attr('data-remote');
      if (src) $(this).ekkoLightbox();
    });
   
    });
$('#smallImage').append(`
    <div id="carousel-selector-${index}" class="thumb" data-target="#myCarousel" data-slide-to="${index}">
    <img src="${image_path}products/${key.image}" class="img-fluid" alt="...">
    </div>
`);

if(index==0){
    $('.thumb').attr("data-slide-to" , "0").addClass('selected');
}


}
})

$(response.data.size_details).each(function(index,key){
    $('#size').append(`


        <span>${key.name}</span>
       
    
`)
})
$(response.data.color_details).each(function(index,key){
    $('#color').append(`


        <div class=" color-box" style="background-color:#${key.hexaval}"></div>
       
    
`)
});

if(response.data.size_details.length==0){
    $("#size-field").hide();
}
if(response.data.color_details.length==0){
    $("#color-field").hide();
}

$('#product-edit').append(`

   <div class="catalog-form">
   <div class="form-field">
       <label>Product Title</label>
       <input id="title" type="text" value="${name}" placeholder="Globex Corporation">
       <h5 id="titlecheck"></h5>
   </div>
    <div class="form-field">
        <label>SKU</label>
        <input value="${sku}" id="sku" type="text" placeholder="">
    </div>

   <div class="form-field">
    <label>Select Category</label>
    <select id="category" onchange="Get_sizes()">
       
    </select>
</div>

<div class="form-field">
    <label>Price</label>
    <h5 id="pricecheck"></h5>
    <input id="price"type="text" value="${price}" placeholder="KSH 400">
</div>

<div class="form-field">
    <label>Stock</label>
    <div class="number">
        <span class="minus">-</span>
        <input id="stock" type="text" value="${stock}">
        <span class="plus">+</span>
    </div>
</div>
</div>

     `)
$('#product-edit2').append(`
     <div class="catalog-form">
<div class="form-field-full">
    <label>Description</label>
     <h5 id="descriptioncheck"></h5>
    <textarea id="description" minlength="20" maxlength="250">${description}</textarea>
</div>

<div class="form-field-full specification-sec"id="specs">
    <div class="field-inner" >
    <label>Specification</label>
     <h5 id="specificationcheck"></h5>
    <input id="specification"type="text" value="${specification}" placeholder="Globex Corporation">
    </div>

    
</div>


<button id="addbtn" onclick="addspecs();">+ Add Specification</button>

<div class="form-field-full">
    <label>Specification</label>
    <div class="browse-box">
        <div class="browse-box-inner">
            <label for="image">
    <input type="file" name="myFile" class="drop-zone__input" id="image"  onchange="readURL(this);">
    <div class="browse-content">
        <img src="../images/icons/image.svg">
        // <p>Drag & drop</p>
        <p> Browse Image</p>
    </div>
    <!-- uploded image preview  -->
    <div class="featured-image-preview-wrapper">
        <img id="blah" src="https://api.twiva.co.ke/storage/images/products/${image}" alt="">
    </div>
    <!-- uploded image preview  -->
    </label>
    
    </div>
    </div>

    <div class="browse-items" id="selectimg">
        
       
        
    </div>
    </div>
<div class="form-field-full">
    <div class="common-button">
    <button onclick="update_product()" class="purple-btn">Save</button>
    </div>

`)
document.getElementById("category").selectedIndex = "2";

console.log('fffffff',size)
 jQuery(document).ready(function () {
        jQuery('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        jQuery('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
    $(document).ready(function(){
    $('#titlecheck').hide();
    $('#pricecheck').hide();
    $('#descriptioncheck').hide();
    $('#sizecheck').hide();
   

    var title_err = true;
    var proce_err = true;
    var description_err = true;
    var specification_err = true;
    var size_err = true;



    $('#specification').keyup(function(){
        specification_check();
        
    });

    function specification_check(){
        var specification_val = $('#specification').val();
        if(specification_val.length == ''){
            $('#specificationcheck').show();
            $('#specificationcheck').html("Please fill the specification");
            $('#specificationcheck').focus();
            $('#specificationcheck').css("color","red");
            specification_err = false;
            return false;



        }
        else{
            $('#specificationcheck').hide();

        }

    }


     $('#description').keyup(function(){
        description_check();
        
    });

    function description_check(){
        var description_val = $('#description').val();
        if(description_val.length == ''){
            $('#descriptioncheck').show();
            $('#descriptioncheck').html("Please fill the description");
            $('#descriptioncheck').focus();
            $('#descriptioncheck').css("color","red");
            description_err = false;
            return false;



        }
        else{
            $('#descriptioncheck').hide();

        }

    }

    $('#title').keyup(function(){
        title_check();
        
    });

    function title_check(){
        var title_val = $('#title').val();
        if(title_val.length == ''){
            $('#titlecheck').show();
            $('#titlecheck').html("Please fill the title");
            $('#titlecheck').focus();
            $('#titlecheck').css("color","red");
            title_err = false;
            return false;



        }
        else{
            $('#titlecheck').hide();

        }

    }

    $('#price').keyup(function(){
        price_check();
        
    });

    function price_check(){
        var price_val = $('#price').val();
        if(price_val.length == ''){
            $('#pricecheck').show();
            $('#pricecheck').html("Please fill the price");
            $('#pricecheck').focus();
            $('#pricecheck').css("color","red");
            price_err = false;
            return false;
        }
        else{
            $('#pricecheck').hide();

        }

    }
      function size_check(){
        var size_val = $('#size').val();
        if(size_val.length == ''){
            $('#sizecheck').show();
            $('#sizecheck').html("Please fill the price");
            $('#sizecheck').focus();
            $('#sizecheck').css("color","red");
              alert(size_val);
             size_err = false;
            return false;
        }
        else{
            $('#sizecheck').hide();
        }
    }

    $('#submitbtn12').click(function(){
         title_err = true;
         price_err = true;
         specification_err = true;
         description_err = true;
         size_err = true;
        title_check();
        price_check();
        specification_check();
        description_check();
        size_check();
        if((title_err == true) && (price_err == true)  && (specification_err == true)  && (description_err == true) && (size_err == true)){
          alert("here");
        }
        else{
            return false;
        }



    })

});
   
$(response.data.product_images).each(function(index,key){
    if(index>0){
        if(typeof key.image !== 'object' && key.image !== "null"){
    $('#selectimg').append(`


         <div class="browse-ss">
            <label for="image${index}">
            <input type="file" id="image${index}" name="image${index}" onchange="image${index}(this)" >
            <span>+</span>
            <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img${index}" src="https://api.twiva.co.ke/storage/images/products/${key.image}" alt="">
            </div>
             <!-- uploded image preview  -->
        </label>
        </div>
       
    
`)
}
}
})



// $(response.data.size_details).each(function(index,key){
   

// console.log(index);
//         $("#size_ids0").prop('checked', true);
       
    

// })
// $(response.data.color_details).each(function(index,key){
//     $('#color').append(`


//         <div class=" color-box" style="background-color:#${key.hexaval}"></div>
       
    
// `)
// })




}


//delete prooducts
function delete_product(){
    var id = 80;
    delete_products(id);
}

function publish()
{
    var pro_id = STORAGE.get(STORAGE.pid);
    var pub =$("#switch").prop('checked');
    if (pub == true) 
    {
        json= {
        "status":"1",
        "product_id":pro_id
    };
    $("#switch").val(1);
    publish_product(json);
    }
    else{
        json= {
            "status":"0",
            "product_id":pro_id
            };
        $("#switch").val(0);

        publish_product(json);
    }
}


function __success_delete(response)
{

  fetch_products();
   $("#successMessage").show().html("<strong >Success!</strong> Product has been deleted successfully");
    setTimeout(function() {
    $('#successMessage').fadeOut('slow');
}, 6000);
   

}

//fetch products

function __success_products(response){
    let appenddata = "";
    $('#products').html('');
    $(response.data).each(function(index,key){
        inStock ="In Stock";
        if(key.stock ==0){
            inStock="Out of Stock";
        }
        status = "Published"
        if(key.status==0) {
            status = "Unpublished"
        }

    if(key.rating == null)
    {
        key.rating = 0;
    }
    appenddata += `<tr> 
                <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)"><img src="https://api.twiva.co.ke/storage/images/products/${key.product_images[0].image}"></a></td>
                <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.name}</a></td>
                <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">`;
                if(key.rating!=0){
                    appenddata += `${key.rating}
                    <img src="../images/icons/star.svg"></img>`;
                }else{
                    appenddata += `NA`;
                }
                appenddata += `
                </a></td>
                <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.stock}</a></td>
                <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">KSH ${key.price}</a></td>
                <td class="green-txt"><button>${inStock}</button></td>
                <td class="green-txt"><button>${status}</button></td>
                <td>
                    <div class="actions">
                     <a href="#addBookDialog" id="${key.id}"  onclick="product_edit(${key.id})"><img src="../images/icons/edit-profile.svg"></a>
                        <a data-toggle="modal" data-id="${key.id}" class="open-AddBookDialog" href="#addBookDialog"><img src="../images/icons/trash-red.svg"></a>
                    </div> 
                </td>
              </tr>

             `;
           
    
      
    })
    $("#products").append(appenddata);

 $(document).on("click", ".open-AddBookDialog", function () {
     var myBookId = $(this).data('id');
     $("#bookId").attr("name", myBookId );
     console.log(myBookId);

     });
 document.getElementsByClassName("c-btn close-btn")[0].click();

    
}


