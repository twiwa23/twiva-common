var image_path = "https://api.twiva.co.ke/storage/images/";

function dashboarddata() {
  __ajaxregister_http(
    "business/dashboard",
    "",
    headers(),
    AJAX_CONF.apiType.GET,
    "",
    __success_dashboard
  );
}

function __success_dashboard(response) {
  console.log(response);
  STORAGE.set(STORAGE.email, response.data.user.email);

  var prod = response.data.products_count;
  $("#totalEarning").html("KSH " + response.data.total_earnings);
  $("#earningThisMonth").html("KSH " + response.data.earning_this_month);
  $("#ProductSoldThisMonth").html(
    response.data.sold_product_this_month + " Product"
  );

  $(response.data.user_subscription).each(function (index, key) {
    var todayDate = new Date();
    var endDate = new Date(key.end_date);
    if (todayDate > endDate) {
      console.log("Expired");
      $("#subsshow").append(`
                    <div class="card-flex">
                        <h3>${key.subscription_details.subscription_title} <span class="txt ml-2" style="color:red">(Expired)</span></h3>
                    </div>
                `);
    } else {
      $("#subsshow").append(`
                    <div class="card-flex">
                        <h3>${key.subscription_details.subscription_title} <span class="txt ml-2">(${key.subscription_details.max_products} Products- ${key.subscription_details.no_of_days} days)</span></h3>
                    </div>
                    `);
    }

    $("#catalog").append(`
        	 <div class="card-flex">
        <h3>${prod} Products</h3>
    </div>
    `);
  });
  $("#selling-products").html("<h2>Top 5 Selling Product</h2>");

  if (response.data.top_five_selling_products.length > 0) {
    response.data.top_five_selling_products.forEach(function (element) {
      var imageUrl = "";
      element.product_images.forEach(function (el) {
        if (el.is_cover_pic == 1) {
          imageUrl = image_path + "products/" + el.image;
        }
      });
      $(
        "#selling-products"
      ).append(`<div class="product-box-inner"><img src="${imageUrl}">
            <div class="box-content"><p>
            ${element.name}</p><h2>KSH${element.price}</h2><span>No of Sale: ${element.total_sale}</span></div></div>`);
    });
  } else {
    $("#selling-products").append(
      `<div class="text-center"> <img src="../images/icons/empty.svg" alt=""> <p>You don’t have any sales at this moment</p></div>`
    );
  }

  $("#recent-orders").html("  <h2>Recent Orders</h2>");

    if (response.data.recent_orders.length > 0) {
        response.data.recent_orders.forEach(function (element) {
            var imageUrl = "";
            element.business_product_detail.product_images.forEach(function (el) {
                if (el.is_cover_pic == 1) {
                    imageUrl = image_path + "products/" + el.image;
                }
            });
            $(
                "#recent-orders"
            ).append(`<div class="product-box-inner"><img src="${imageUrl}">
                        <div class="box-content">
                            <p>${element.business_product_detail.name}</p>
                            <h2>KSH ${element.amount}</h2>
                            <span>No of Sale: ${element.prduct_details.total_sale}</span>
                        </div>
                    </div>`);
        });
    
    } 
    else {
        $("#recent-orders").append(
        `<div class="text-center"> <img src="../images/icons/empty.svg" alt=""> <p>You don’t have any recent orders</p></div>`
        );
    }
}

$(function () {
  dashboarddata();
});
