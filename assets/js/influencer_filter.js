// function category_fetch()
// {
//   var category = $("#category").val();
//   console.log(category);
  
// //   fetch_cat(category);
// }
$("#category").on('change',function(){
    var category = $("#category").val();
    console.log(category);
    $('#sub_category').html("");
    get_subcategory(category);
});

function fetch_cat(category){

    __ajaxregister_http("fetch-products?categories="+category, "", headers(), AJAX_CONF.apiType.GET, "", __success_products);

}

function get_category(){

    __ajax_httpproduct("categories-subcategory", "", headers(), AJAX_CONF.apiType.Get, "", __success_category);      
}

function __success_category(response) {
    console.log(response);
    var options = '';
    $(response.data).each(function(index,key) {
        options += `<option value="${key.id}"> ${key.name}</option>`;
    })
    $('#category').append(options)

}

function get_subcategory(id){

    __ajax_httpproduct("sub-categories/"+id, "", headers(), AJAX_CONF.apiType.Get, "", __success_sub_category);      
}

function __success_sub_category(response) {
    console.log(response);
    var options = '';
    response.data.forEach(element => {
        options += `<option value="${element.id}"> ${element.name}</option>`;
    });
    $('#sub_category').html(options)
}

