var category_id = "";
var sub_category_id = "";
var image_path = "https://api.twiva.co.ke/storage/images/";

function updateProduct(json) {
  __ajax_httpproduct(
    "product/" + json.id,
    json,
    headers(),
    AJAX_CONF.apiType.PUT,
    "",
    __update_product
  );
}

function product_details(id) {
  __ajaxregister_http(
    "product/" + id,
    "",
    headers(),
    AJAX_CONF.apiType.GET,
    "",
    __success_details
  );
}

function update_product() {
  if (!isFormValid()) {
    return false;
  }

  document.getElementsByClassName("btntest1")[0].click();
  var checkedArray = "";
  for (i = 0; i < 100; i++) {
    //  $("#size_ids18").prop('checked')
    if ($("#size_ids" + i).prop("checked")) {
      checkedArray += $("#size_ids" + i).val() + ",";
    }
  }

  if (checkedArray.indexOf(",", this.length - ",".length) !== -1) {
    checkedArray = checkedArray.substring(0, checkedArray.length - 1);
    // console.log(checkedArray);
  }

  var colorarray = "";
  for (i = 0; i < 100; i++) {
    //  $("#size_ids18").prop('checked')
    if ($("#color" + i).prop("checked")) {
      colorarray += $("#color" + i).val() + ",";
    }
  }

  if (colorarray.indexOf(",", this.length - ",".length) !== -1) {
    colorarray = colorarray.substring(0, colorarray.length - 1);
    // console.log(colorarray);
  }

  specification_check();

  if (specifications.length == 0) {
    specifications = "";
  }
  let sub_category = $("#sub_category").val();
  if (sub_category) {
    sub_category = sub_category.toString();
  }
  var json = {
    id: STORAGE.get(STORAGE.pid),
    name: $("#title").val(),
    price: $("#price").val(),
    category: $("#category").val(),
    sub_category: sub_category,
    stock: $("#stock").val(),
    size_ids: STORAGE.get(STORAGE.productsize),
    color_ids: STORAGE.get(STORAGE.colors),
    sku: $("#sku").val(),
    specification: specifications,
    product_images: [
      {
        image: STORAGE.get(STORAGE.image),
        is_cover_pic: 1,
      },
      {
        image: STORAGE.get(STORAGE.image1),
        is_cover_pic: 0,
      },
      {
        image: STORAGE.get(STORAGE.image2),
        is_cover_pic: 0,
      },
      {
        image: STORAGE.get(STORAGE.image3),
        is_cover_pic: 0,
      },
      {
        image: STORAGE.get(STORAGE.image4),
        is_cover_pic: 0,
      },
    ],
    description: $("#description").val(),
  };
  console.log(json);
  updateProduct(json);
}

function isFormValid() {
  let isValid = true;
  var title = $("#title").val();
  var price = $("#product-price").val();
  var description = $("#description").val();
  if (title == "") {
    isValid = false;
    $("#title-error").show();
    $("#title-error").html("Please enter product title");
    $("#title-error").css("color", "red");
  } else if (title.length < 3) {
    isValid = false;
    $("#title-error").show();
    $("#title-error").html(
      "Please fill the title between 3-70 characters limit"
    );
    $("#title-error").css("color", "red");
  } else if (title.length > 70) {
    isValid = false;
    $("#title-error").show();
    $("#title-error").html(
      "Please fill the title between 3-70 characters limit"
    );
    $("#title-error").css("color", "red");
  } else {
    $("#title-error").hide();
  }
  if (price == "") {
    isValid = false;
    $("#price-error").show();
    $("#price-error").html("Please enter product price");
    $("#price-error").css("color", "red");
  } else {
    $("#price-error").hide();
  }
  if (description == "") {
    isValid = false;
    $("#desc-error").show();
    $("#desc-error").html("Please enter product description ");
    $("#desc-error").css("color", "red");
  } else if (description.length < 20) {
    isValid = false;
    $("#desc-error").show();
    $("#desc-error").html(
      "Please fill the description between 20-250 characters limit"
    );
    $("#desc-error").focus();
    $("#desc-error").css("color", "red");
  } else if (description.length > 250) {
    isValid = false;
    $("#desc-error").show();
    $("#desc-error").html(
      "Please fill the description between 20-250 characters limit"
    );
    $("#desc-error").focus();
    $("#desc-error").css("color", "red");
  } else {
    $("#desc-error").hide();
  }

  return isValid;
}

function __update_product(response) {
  $("#productUpdateModal").modal("show");
  //    goto(UI_URL.onboarding8);
}

var specifications = [];

function __success_product(response) {
  console.log(response);
  if (response.status == true) {
    STORAGE.remove(STORAGE.image);
    STORAGE.remove(STORAGE.image1);
    STORAGE.remove(STORAGE.image2);
    STORAGE.remove(STORAGE.image3);
    STORAGE.remove(STORAGE.image4);

    $("#successMessage")
      .show()
      .html("<strong >Success!</strong> Product has been added successfully");
    setTimeout(function () {
      $("#successMessage").fadeOut("fast");
    }, 3000);

    $("html, body").animate({ scrollTop: 0 }, "fast");
    $("successMessage").css("display", "block");
    $("#successMessage").show();
    $("#successMessage").html(
      "<strong >Success!</strong> Product has been added successfully"
    );
    setTimeout(function () {
      $("#successMessage").fadeOut("fast");
    }, 3000);

    console.log("sucesss");

    setTimeout(function () {
      goto(UI_URL.onboarding7);
    }, 1000);
  }
}

function __success_details(response) {
  //console.log(response);
  STORAGE.remove(STORAGE.image);
  STORAGE.remove(STORAGE.image1);
  STORAGE.remove(STORAGE.image2);
  STORAGE.remove(STORAGE.image3);
  STORAGE.remove(STORAGE.image4);
  //STORAGE.set(STORAGE.cat, response.data.category);
  //STORAGE.set(STORAGE.siz, response.data.color_details[0].id);
  //STORAGE.set(STORAGE.col, response.data.size_details[0].id);
  var name = response.data.name;
  var id = response.data.id;
  var price = response.data.price;
  var stock = response.data.stock;
  //var size = response.data.size_details[0].name;
  //var color = response.data.color_details[0].name;
  var description = response.data.description;
  var publishedStatus = response.data.status;
  var image = response.data.product_images[0].image;
  var rating = response.data.rating;
  var description = response.data.description;
  var sku = response.data.sku;
  if (!sku) {
    sku = "";
  }
  category_id = response.data.category_details.id;
  sub_category_id = response.data.subcategory_details.id;
  if (response.data.sub_category) {
    get_sub_category(response.data.category_details.id);
    Get_size(response.data.subcategory_details["0"].id);
  }

  colorArray = [];
  sizeArray = [];
  subCategoryArray = [];
  for (i = 0; i < response.data.color_details.length; i++) {
    colorArray.push(response.data.color_details[i].id);
  }
  for (i = 0; i < response.data.subcategory_details.length; i++) {
    subCategoryArray.push(response.data.subcategory_details[i].id);
  }

  if (response.data.color == null) {
    $("#color-field").hide();
  }
  if (response.data.size == null) {
    $("#size-field").hide();
  }

  for (i = 0; i < response.data.size_details.length; i++) {
    sizeArray.push(response.data.size_details[i].id);
  }

  for (i = 0; i < response.data.product_images.length; i++) {
    if (i == 0) {
      STORAGE.set(STORAGE.image, response.data.product_images[i].image);
    } else {
      STORAGE.set(
        STORAGE.image + "" + i,
        response.data.product_images[i].image
      );
    }
  }

  if (response.data.specification != null) {
    var specs = response.data.specification.split(",");
  } else {
    var specs = "";
  }

  specification = "";
  var specId = 0;
  if (specs.length > 1) {
    for (k = 0; k < specs.length; k++) {
      specId++;
      if (specs[k] != "")
        specification +=
          '<div class="field-inner" ><label>Specification</label> <h5 id="specificationcheck"></h5><input name = "CreateTextbox" id="specification' +
          specId +
          '" type="text" value = "' +
          specs[k] +
          '" /> ' +
          '<input type="button" value="Remove" class="remove" /> </div>';
    }
  } else {
    specification +=
      '<div class="field-inner" ><label>Specification</label> <h5 id="specificationcheck"></h5><input name = "CreateTextbox" id="specification1" type="text" value = "" /> </div>';
  }

  var checked = "";
  if (publishedStatus == 1) {
    checked = 'checked = "checked"';
  }
  inStock = "In Stock";

  if (stock == 0) {
    inStock = "Out of Stock";
  }

  $("#product-de").append(`

    <div class="product-detail-cont">
    <div class="browse-box">
        <img src="${image_path}products/${image}">
    </div>

     <div class="browse-items" id="images">
    
</div>
<div class="switch-check-box-wrapper">
<span>Published</span>
<input type="checkbox" ${checked}  onchange="publish()" id="switch" />
<label class="label-switch" for="switch">Toggle</label>
</div>
</div>

<div class="product-specification" id="detailsproduct">
<h2>${name}</h2>
<h3><label>Price: </label><b>KSH ${price}</b></h3>

<div class="spec-row">
    <label>Qty:</label>
    <div><h3>${stock} piece</h3> <button>${inStock}</button></div>
</div>


<div class="spec-row">
    <label>Size:</label>
    <div>
    <div id="size">
    </div>
    </div>
    </div>
    
    <div class="spec-row">
    <label>Color:</label>
    <div>
    <div id="color">
    </div>
       </div>
       </div>
       <div class="prod-desc">
    <label>Description</label>
    <p>${description}</p>
</div>

`);
  $(response.data.product_images).each(function (index, key) {
    console.log(key);
    if (typeof key.image !== "object" && key.image !== "null") {
      if (index > 0) {
        $("#images").append(`
        <div class="browse-ss">
            <img src="${image_path}products/${key.image}">
        </div>        
`);
      }
    }
  });

  $("#product-edit").append(`

   <div class="catalog-form">
   <div class="form-field">
       <label>Product Title</label>
       <input id="title" type="text" value="${name}" placeholder="">
       <h5 id="title-error"></h5>
   </div>

   <div class="form-field">
       <label>SKU</label>
       <input id="sku" type="text" value="${sku}">
   </div>

   <div class="form-field">
    <label>Select Category</label>
    <select id="category" onchange="categories()">
       
    </select>
</div>

<div class="form-field" id="subcategory-field">
    <label>Select SubCategory</label>
    <select id="sub_category"  class="" onchange="Get_sizess()">
    </select>
</div>

<div class="form-field">
    <label for="price">Product Price</label>
    <h5 id="pricecheck"></h5>
    <div class="input-group-prepend product-price">
        <span class="input-group-text">KSH</span>
        <input type="text"class="form-control" id="price" value="${price}">
    </div>
</div>


<div class="form-field">
    <label>Stock</label>
    <div class="number">
        <span class="minus">-</span>
        <input id="stock" type="text" value="${stock}">
        <span class="plus">+</span>
    </div>
</div>
</div>

     `);
  $("#product-edit2").append(`
     <div class="catalog-form">
<div class="form-field-full">
    <label>Description</label>
    <textarea id="description" minlength="20" maxlength="250">${description}</textarea>
    <h5 id="desc-error"></h5>
</div>

<div class="form-field-full specification-sec"id="specs">
${specification}

    
</div>


<button id="addbtn" onclick="addspecs();">+ Add Specification</button>

<div class="form-field-full">
    <label class="mb-2">Add Images (Minimum image should be 500x500 pixels & size should be less than 2 MB)</label>
    <div class="browse-box">
        <div class="browse-box-inner">
            <label for="image">
    <input type="file" name="myFile" class="drop-zone__input" id="image"  onchange="readURL(this);">
    <div class="browse-content">
        <img src="../images/icons/image.svg">
        // <p>Drag & drop</p>
        <p> Browse  Image</p>
    </div>
    <!-- uploded image preview  -->
    <div class="featured-image-preview-wrapper">
        <img id="blah" src="https://api.twiva.co.ke/storage/images/products/${image}" alt="">
    </div>
    <!-- uploded image preview  -->
    </label>
    
    </div>
    </div>
    <span class="custom_error" id="image_error" style="display: none;
    margin-top: 5px;
    margin-left: 165px;
    color: red;
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 150%;">Image size should not be greater than 2MB</span>
    <div class="browse-items" id="selectimg">
        
       
        
    </div>
    </div>
    <span class="custom_error" id="image_error" style="display:none;
    margin-top: -20px;
    margin-left: 100px;
    color: red;
    font-family: Source Sans Pro;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 150%;">Image size should not be greater than 2MB</span>
<div class="form-field-full">
    <div class="common-button">
    <button id="submitbtn1" onclick="update_product()" class="purple-btn">Update</button>
    </div>

`);

  get_category();
  jQuery(document).ready(function () {
    jQuery(".minus").click(function () {
      var $input = $(this).parent().find("input");
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
    });
    jQuery(".plus").click(function () {
      var $input = $(this).parent().find("input");
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    });
  });
  $(document).ready(function () {
    $("#titlecheck").hide();
    $("#pricecheck").hide();
    $("#descriptioncheck").hide();
    $("#sizecheck").hide();

    var title_err = true;
    var proce_err = true;
    var description_err = true;
    var specification_err = true;
    var size_err = true;

    $("#specification").keyup(function () {
      // specification_check();
    });

    //  $('#description').keyup(function(){
    //     description_check();

    // });

    // function description_check(){
    //     var description_val = $('#description').val();
    //     if(description_val.length == ''){
    //         $('#descriptioncheck').show();
    //         $('#descriptioncheck').html("Please fill the description");
    //         $('#descriptioncheck').focus();
    //         $('#descriptioncheck').css("color","red");
    //         description_err = false;
    //         return false;

    //     }
    //     else{
    //         $('#descriptioncheck').hide();

    //     }

    // }

    // function description_check(){
    //     var description_val = $('#description').val();
    //     if(description_val.length <= 250 && description_val.length >= 20){
    //         $('#descriptioncheck').hide();
    //         $("#submitbtn1").removeAttr('disabled');
    //     }
    //     else{
    //         $('#descriptioncheck').show();
    //         $('#descriptioncheck').html("Please fill the description between 20-250 characters limit");
    //         $('#descriptioncheck').focus();
    //         $('#descriptioncheck').css("color","red");
    //         description_err = false;
    //         $("#submitbtn1").attr("disabled","true");
    //         return false;
    //     }
    // }

    // $('#title').keyup(function(){
    //     title_check();

    // });

    // function title_check(){
    //     var title_val = $('#title').val();
    //     if(title_val.length == ''){
    //         $('#titlecheck').show();
    //         $('#titlecheck').html("Please fill the title");
    //         $('#titlecheck').focus();
    //         $('#titlecheck').css("color","red");
    //         title_err = false;
    //         return false;
    //     }
    //     else{
    //         $('#titlecheck').hide();
    //     }
    // }

    // function title_check(){
    //     var title_val = $('#title').val();
    //     if(title_val.length <= 70 && title_val.length >= 3){
    //         $('#titlecheck').hide();
    //         $("#submitbtn1").removeAttr('disabled');
    //     }
    //     else{
    //         $('#titlecheck').show();
    //         $('#titlecheck').html("Please fill the title between 3-70 characters limit");
    //         $('#titlecheck').focus();
    //         $('#titlecheck').css("color","red");
    //         title_err = false;
    //         $("#submitbtn1").attr("disabled","true");
    //         return false;
    //     }
    // }

    $("#price").keyup(function () {
      price_check();
    });

    function price_check() {
      var price_val = $("#price").val();
      if (price_val.length == "") {
        $("#pricecheck").show();
        $("#pricecheck").html("Please fill the price");
        $("#pricecheck").focus();
        $("#pricecheck").css("color", "red");
        price_err = false;
        return false;
      } else {
        $("#pricecheck").hide();
      }
    }
    function size_check() {
      var size_val = $("#size").val();
      if (size_val.length == "") {
        $("#sizecheck").show();
        $("#sizecheck").html("Please fill the price");
        $("#sizecheck").focus();
        $("#sizecheck").css("color", "red");
        alert(size_val);
        size_err = false;
        return false;
      } else {
        $("#sizecheck").hide();
      }
    }

    $("#submitbtn12").click(function () {
      title_err = true;
      price_err = true;
      specification_err = true;
      description_err = true;
      size_err = true;
      title_check();
      price_check();
      specification_check();
      description_check();
      size_check();
      if (
        title_err == true &&
        price_err == true &&
        specification_err == true &&
        description_err == true &&
        size_err == true
      ) {
        alert("here");
      } else {
        return false;
      }
    });
  });

  $(response.data.product_images).each(function (index, key) {
    if (index > 0) {
      if (typeof key.image !== "object" && key.image !== "null") {
        $("#selectimg").append(`


         <div class="browse-ss position-relative">
            <label for="image${index}">
            <input type="file" id="image${index}" name="image${index}" onchange="image${index}(this)" >
            <span>+</span>
            <!-- uploded image preview  -->
            <div class="product-additional-image">
                <img id="img${index}" src="http://api.twiva.co.ke/storage/images/products/${key.image}" alt="">
            </div>
             <!-- uploded image preview  -->
        </label>
        <span class="image-delete${index}" data-name='image${index}' style="
        width: 16px;
        height: 16px;
        position: absolute;
        right: 4px;
        top: 4px;
        border-radius: 35%;
        z-index: 20;
    "><img src="../images/icons/x.svg" alt=""></span>
        </div>
       
    
`);
      }
    }
  });
  $(document).on("click", ".image-delete1", function () {
    var image = $(this).attr("data-name");
    $("#img1").removeAttr("src");
    STORAGE.remove(STORAGE.image1);
    $(".image-delete1").hide();
  });
  $(document).on("click", ".image-delete2", function () {
    var image = $(this).attr("data-name");
    $("#img2").removeAttr("src");
    STORAGE.remove(STORAGE.image2);
    $(".image-delete2").hide();
  });
  $(document).on("click", ".image-delete3", function () {
    var image = $(this).attr("data-name");
    $("#img3").removeAttr("src");
    STORAGE.remove(STORAGE.image3);
    $(".image-delete3").hide();
  });
  $(document).on("click", ".image-delete4", function () {
    var image = $(this).attr("data-name");
    $("#img4").removeAttr("src");
    STORAGE.remove(STORAGE.image4);
    $(".image-delete4").hide();
  });
  if(response.data.product_images.length==5){
    for (i = 0; i < 5; i++) {
      if (
        typeof response.data.product_images[i].image === "object" ||
        response.data.product_images[i].image == "null"
      ) {
        $("#selectimg").append(`
  
  
           <div class="browse-ss position-relative">
              <label for="image${i}">
              <input type="file" id="image${i}" name="image${i}" onchange="image${i}(this)" >
              <span>+</span>
              <!-- uploded image preview  -->
              <div class="product-additional-image">
                  <img id="img${i}" alt >
              </div>
               <!-- uploded image preview  -->
          </label>
          <span class="image-delete${i}" data-name='image${i}' style="
          display:none;
          width: 16px;
          height: 16px;
          position: absolute;
          right: 4px;
          top: 4px;
          border-radius: 35%;
          z-index: 20;
      "><img src="../images/icons/x.svg" alt=""></span>        
          </div>
         
      
  `);
      }
    }    
  }else{
    i = response.data.product_images.length;
    for (i=i; i < 5; i++) {
        $("#selectimg").append(`
  
  
           <div class="browse-ss position-relative">
              <label for="image${i}">
              <input type="file" id="image${i}" name="image${i}" onchange="image${i}(this)" >
              <span>+</span>
              <!-- uploded image preview  -->
              <div class="product-additional-image">
                  <img id="img${i}" alt >
              </div>
               <!-- uploded image preview  -->
          </label>
          <span class="image-delete${i}" data-name='image${i}' style="
          display:none;
          width: 16px;
          height: 16px;
          position: absolute;
          right: 4px;
          top: 4px;
          border-radius: 35%;
          z-index: 20;
      "><img src="../images/icons/x.svg" alt=""></span>        
          </div>
         
      
  `);
    } 
  }
  if (response.data.sub_category == 0) {
    document.getElementById("subcategory-field").style.display = "none";
    document.getElementById("size-field").style.display = "none";
    document.getElementById("color-field").style.display = "none";
  }
  if (response.data.sub_category == null) {
    document.getElementById("subcategory-field").style.display = "none";
    document.getElementById("size-field").style.display = "none";
    document.getElementById("color-field").style.display = "none";
  }
}

function specification_check() {
  console.log('Hi');
  for (i = 1; i <= 10; i++) {
    specification = "specification" + i;
    if ($("#" + specification).length) {
      if ($("#" + specification).val().length == "") {
        // $('#specificationcheck').show();
        // $('#specificationcheck').html("Please fill the specification");
        // $('#specificationcheck').focus();
        // $('#specificationcheck').css("color","red");
        // specification_err = false;
        // return false;
      } else {
        $("#specificationcheck").hide();
        specifications += $("#" + specification).val() + ",";
        //specifications.push($("#"+specification).val());
      }
    }
  }
}

//validations
$(document).ready(function () {
  $("#titlecheck").hide();
  $("#pricecheck").hide();
  $("#descriptioncheck").hide();
  $("#specificationcheck").hide();

  var title_err = true;
  var proce_err = true;
  var description_err = true;
  var specification_err = true;

  $("#specification").keyup(function () {
    specification_check();
  });

  $("#description").keyup(function () {
    description_check();
  });

  function description_check() {
    var description_val = $("#description").val();
    if (description_val.length == "") {
      $("#descriptioncheck").show();
      $("#descriptioncheck").html("Please fill the description");
      $("#descriptioncheck").focus();
      $("#descriptioncheck").css("color", "red");
      description_err = false;
      return false;
    } else {
      $("#descriptioncheck").hide();
    }
  }

  $("#title").keyup(function () {
    title_check();
  });

  function title_check() {
    var title_val = $("#title").val();
    if (title_val.length == "") {
      $("#titlecheck").show();
      $("#titlecheck").html("Please fill the title");
      $("#titlecheck").focus();
      $("#titlecheck").css("color", "red");
      title_err = false;
      return false;
    } else {
      $("#titlecheck").hide();
    }
  }

  $("#price").keyup(function () {
    price_check();
  });

  function price_check() {
    var price_val = $("#price").val();
    if (price_val.length == "") {
      $("#pricecheck").show();
      $("#pricecheck").html("Please fill the price");
      $("#pricecheck").focus();
      $("#pricecheck").css("color", "red");
      price_err = false;
      return false;
    } else {
      $("#pricecheck").hide();
    }
  }

  $("#submitbtn1").click(function () {
    title_err = true;
    price_err = true;
    specification_err = true;
    description_err = true;
    title_check();
    price_check();
    specification_check();
    description_check();
    if (
      title_err == true &&
      price_err == true &&
      specification_err == true &&
      description_err == true
    ) {
      document.getElementsByClassName("btntest1")[0].click();
      addproduct();
    } else {
      return false;
    }
  });
});

function categories() {
  var category = $("#category").val();
  console.log(category);
  $("#sub_category").html("");
  get_subcategory(category);
}

function get_subcategory(id) {
  __ajax_httpproduct(
    "sub-categories/" + id,
    "",
    headers(),
    AJAX_CONF.apiType.Get,
    "",
    __success_sub_category
  );
}

function __success_sub_category(response) {
  $("#sub_category").append(`<option  value=""> Select Subcategory </option>`);
  if (response.data.length == 0) {
    $("#subcategory-field option:selected").remove();
    $("#size-field option:selected").remove();
    $("#color-field option:selected").remove();
    document.getElementById("subcategory-field").style.display = "none";
    document.getElementById("size-field").style.display = "none";
    document.getElementById("color-field").style.display = "none";
  }
  if (response.data.length > 0) {
    document.getElementById("subcategory-field").style.display = "block";
  }
  $(response.data).each(function (index, key) {
    if (key.id == sub_category_id) {
      $("#sub_category").append(
        `<option selected="selected" value="${key.id}"> ${key.name}</option>`
      );
    } else {
      $("#sub_category").append(
        `<option value="${key.id}"> ${key.name}</option>`
      );
    }
  });
}

function get_category() {
  __ajax_http(
    "categories",
    "",
    headers(),
    AJAX_CONF.apiType.Get,
    "",
    __success_category
  );
}

function __success_category(response) {
  $(response.data).each(function (index, key) {
    if (key.id == category_id) {
      $("#category").append(
        `<option selected="selected" value="${key.id}"> ${key.name}</option>`
      );
    } else {
      $("#category").append(`<option value="${key.id}"> ${key.name}</option>`);
    }
  });
  $(response.data).each(function (index, key) {
    if (key.id == category_id) {
      get_subcategory(category_id);
    }
  });
}

function __success_size(response) {
  $("#size").html("");
  $("#color").html("");
  $(response.sizes).each(function (index, key) {
    checked = false;
    for (i = 0; i < sizeArray.length; i++) {
      if (key.id == sizeArray[i]) {
        checked = true;
      }
    }
    if (checked == true) {
      $("#size").append(
        `<option selected="selected" value="${key.id}">${key.name}</option>`
      );
    } else {
      $("#size").append(`<option value="${key.id}">${key.name}</option>  `);
    }
  });
  $("#colors li").remove();
  STORAGE.set(STORAGE.colors, "");
  $(response.colors).each(function (index, key) {
    checked = false;
    for (i = 0; i < colorArray.length; i++) {
      if (key.id == colorArray[i]) {
        checked = true;
      }
    }
    if (checked == true) {
      $("#colors").append(`
            

    <li class="color-list-item selected-color" title="${key.name}" style="background-color:#${key.hexaval};" data-color="${key.hexaval}" data-id="${key.id}">
                        <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                    </li> 

    `);
      //$('#color').append(`<option selected="selected" value="${key.id}">${key.name}</option>`);
    } else {
      $("#colors").append(`
            

    <li class="color-list-item" title="${key.name}" style="background-color:#${key.hexaval};" data-color="${key.hexaval}" data-id="${key.id}">
                        <a href=""><i class="fa fa-check" aria-hidden="true"></i></a>
                    </li> 

    `);
      //$('#color').append(`<option value="${key.id}">${key.name}</option>`);
    }
  });
  $(".color-list-item").on("click", function (event) {
    event.preventDefault();
    $(this).toggleClass("selected-color");
    $("#color").val($(this).attr("data-id"));
  });

  var colorsids = colorArray;
  $(".color-list .color-list-item").on("click", function () {
    if ($(this).hasClass("selected-color")) {
      colorsids.push(parseInt($("#color").val()));
    } else {
      var index = colorsids.indexOf($("#color").val());
      colorsids.splice(index, 1);
    }
    STORAGE.set(STORAGE.colors, colorsids);
  });
}

function Get_size(id) {
  __ajax_http(
    "size-color?subcategory_id=" + id,
    "",
    headers(),
    AJAX_CONF.apiType.Get,
    "",
    __success_size
  );
}

function get_sub_category(id) {
  __ajax_http(
    "sub-categories/" + id,
    "",
    headers(),
    AJAX_CONF.apiType.Get,
    "",
    __success_get_sub_category
  );
}

function __success_get_sub_category(response) {
  console.log(response);
  $("#sub_category").html("");
  $(response.data).each(function (index, key) {
    checked = false;
    for (i = 0; i < subCategoryArray.length; i++) {
      if (key.id == subCategoryArray[i]) {
        checked = true;
      }
    }
    if (checked == true) {
      $("#sub_category").append(
        `<option selected="selected" value="${key.id}">${key.name}</option>`
      );
    } else {
      $("#sub_category").append(
        `<option value="${key.id}">${key.name}</option>  `
      );
    }
  });
}

function Get_sizess() {
  var id = document.getElementById("sub_category").value;
  Get_size(id);
  Get_Single_SubCategory(id);
}

function Get_Single_SubCategory(id) {
  __ajax_httpproduct(
    "sub-categories/1?subcategory_id=" + id,
    "",
    headers(),
    AJAX_CONF.apiType.Get,
    "",
    __success_single_subcategory
  );
}

function __success_single_subcategory(response) {
  if (response.data.is_color == 0) {
    document.getElementById("color-field").style.display = "none";
  }
  if (response.data.is_color == 1) {
    document.getElementById("color-field").style.display = "block";
  }
  if (response.data.is_size == 0) {
    document.getElementById("size-field").style.display = "none";
  }
  if (response.data.is_size == 1) {
    document.getElementById("size-field").style.display = "block";
  }
}
