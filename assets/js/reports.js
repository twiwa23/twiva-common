//business/reports?type=3
var image_path ="https://api.twiva.co.ke/storage/images/products/";
function fetch_reports(){
    __ajaxregister_http("business/reports?type=1", "", headers(), AJAX_CONF.apiType.GET, "", __success_reports);
}

function fetch_reports1(){
    __ajaxregister_http("business/reports?type=2", "", headers(), AJAX_CONF.apiType.GET, "", __success_reports2);
}

function fetch_reports2(){
    __ajaxregister_http("business/reports?type=3", "", headers(), AJAX_CONF.apiType.GET, "", __success_reports3);
}

function fetch_reports3(){
    __ajaxregister_http("business/reports?type=4", "", headers(), AJAX_CONF.apiType.GET, "", __success_reports4);
}

function product_detail(id)
{
   STORAGE.set(STORAGE.pid, id);
    goto(UI_URL.onboarding8);
}

function __success_reports(response){

    if(bestPerforming.length > 0){
        $('#products').html('');
        $(response.data).each(function(index,key){
            if(key.rating == null)
            {
                key.rating = 0;
            }
            $('#bestPerforming').append(`
            <tr> 
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)"><img width='48px' height='48px' src="https://api.twiva.co.ke/storage/images/products/${key.product_images[0].image}"></a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.name}</a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.rating}<img src="../images/icons/star.svg"></a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.stock}</a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">KSH ${key.price}</a></td>
                    <td class="green-txt"><button>In Stock</button></td>
                    <td>
                        <div class="actions">
                        <a href="#addBookDialog" id="${key.id}"  onclick="product_edit(this.id)"><img src="../images/icons/edit-profile.svg"></a>
                            <a data-toggle="modal" data-id="${key.id}" class="open-AddBookDialog" href="#addBookDialog"><img src="../images/icons/trash-red.svg"></a>
                        </div> 
                    </td>
                </tr>

                `) 
        
        })
    }
    else{
        $('#tab_content1').append(`<div class="card product-box-inner p-3" style="border:0;">
            <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                <img src="../images/icons/empty.svg" alt="">
                    <h3 style="font-size:20px;">
                    You don’t have any sales at this moment 
                    </h3>
                </div>
            </div>`)
    }
    

}

function __success_reports2(response){

    if(leastPerforming.length > 0){
        $(response.data).each(function(index,key){
            if(key.rating == null)
            {
                key.rating = 0;
            }
            $('#leastPerforming').append(`
                <tr> 
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)"><img width='48px' height='48px' src="https://api.twiva.co.ke/storage/images/products/${key.product_images[0].image}"></a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.name}</a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.rating}<img src="../images/icons/star.svg"></a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">${key.stock}</a></td>
                    <td> <a href="#addBookDialog" id="${key.id}"  onclick="product_detail(this.id)">KSH ${key.price}</a></td>
                    <td class="green-txt"><button>In Stock</button></td>
                    <td>
                        <div class="actions">
                            <a href="#addBookDialog" id="${key.id}"  onclick="product_edit(this.id)"><img src="../images/icons/edit-profile.svg"></a>
                            <a data-toggle="modal" data-id="${key.id}" class="open-AddBookDialog" href="#addBookDialog"><img src="../images/icons/trash-red.svg"></a>
                        </div> 
                    </td>
                </tr>
            `)
        })
    }
    else{
        $('#tab_content2').append(`<div class="card product-box-inner p-3" style="border:0;">
            <div class="col-12 col-md-12 col-lg-12 text-center p-5">
                <img src="../images/icons/empty.svg" alt="">
                    <h3 style="font-size:20px;">
                        You don’t have any sales at this moment
                    </h3>
                </div>
            </div>`)
    }

    
}

function __success_reports3(response){
    
}

function __success_reports4(response){
    
}