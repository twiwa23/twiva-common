/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/** ******  left menu  *********************** **/
$(function () {
    // $('#sidebar-menu li ul').slideUp();
    // $('#sidebar-menu li').removeClass('active');

    // $('#sidebar-menu li.sidebar-dropdown').click(function () {
    //     if ($(this).is('.active')) {
    //         $(this).removeClass('active');
    //         $('ul', this).slideUp();
    //         $(this).removeClass('nv');
    //         $(this).addClass('vn');
    //     } else {
    //         $('#sidebar-menu li ul').slideUp();
    //         $(this).removeClass('vn');
    //         $(this).addClass('nv');
    //         $('ul', this).slideDown();
    //         $('#sidebar-menu li.sidebar-dropdown').removeClass('active');
    //         $(this).addClass('active');
    //     }
    // });

    $('#menu_toggle').click(function () {
        if ($('body').hasClass('nav-md')) {
            $('body').removeClass('nav-md');
            $('body').addClass('nav-sm');
            $('.left_col').removeClass('scroll-view');
            $('.left_col').removeAttr('style');
            $('.sidebar-footer').hide();

            if ($('#sidebar-menu li').hasClass('active')) {
                $('#sidebar-menu li.active').addClass('active-sm');
                $('#sidebar-menu li.active').removeClass('active');
            }
        } 
        else {
            $('body').removeClass('nav-sm');
            $('body').addClass('nav-md');
            $('.sidebar-footer').show();

            if ($('#sidebar-menu li').hasClass('active-sm')) {
                $('#sidebar-menu li.active-sm').addClass('active');
                $('#sidebar-menu li.active-sm').removeClass('active-sm');
            }
        }
    });

});

///// Left Menu //////
$(function () {
    var url = window.location;
    $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
    $('#sidebar-menu a').filter(function () {
        return this.href == url;
    }).parent('li').addClass('current-page').parent('ul').parent().addClass('active');
    if ($('.sidebar-submenu ').hasClass('active')){
        $(".sidebar-submenu.active").parent('li').addClass('current-page');
        $(".sidebar-submenu.active").parent('li').find("> a").addClass("current-page");
        $(".sidebar-submenu.active").parent('li').find("> .current-page .fa-chevron-down").addClass("rotate-active");
    }
});
$(document).ready(function(){
    // Sidebar Dropdown Menu //
    $(".sidebar-dropdown > a").on("click", function () {
        $(this).parent().toggleClass("current-page");
        // debugger;
        $(this).siblings().toggleClass('active');
        $(this).find(".fa-chevron-down").toggleClass("rotate-active");
    });
});
///// Left Menu //////



/* Sidebar Menu active class */
// $(function () {
//     var url = window.location;
//     $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
//     $('#sidebar-menu a').filter(function () {
//         return this.href == url;
//     }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');

//     if ($('.sidebar-submenu ').hasClass('active')){
//         $(".sidebar-submenu.active").parent('li').addClass('current-page');
//     }
// });

// $(function () {
//     var url = window.location;
//     $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
//     $('#sidebar-menu a').filter(function () {
//         return this.href == url;
//     }).parent('li').addClass('current-page').parent('ul').parent().addClass('active');

//     if ($('.sidebar-submenu ').hasClass('active')){
//         $(".sidebar-submenu.active").parent('li').addClass('current-page');
//         $(".sidebar-submenu.active").parent('li').find("> a").addClass("current-page");
//         $(".sidebar-submenu.active").parent('li').find("> .current-page .fa-chevron-down").addClass("rotate-active");
//     }
// });

// $(document).ready(function(){
//     // Sidebar Dropdown Menu //
//     $(".sidebar-dropdown > a").on("click", function () {
//         $(this).parent().toggleClass("current-page");
//         $(".sidebar-submenu").addClass("active")
//     });
// });

// $(document).ready(function(){
//     // Sidebar Dropdown Menu //
//     $(".sidebar-dropdown").on("click", function () {
//         $(this).toggleClass("current-page");
//         $(this).find("> a").toggleClass("current-page");
//         $(this).find(".sidebar-submenu").slideToggle();
//         // $(this).find(".sidebar-submenu > ul").toggle();
//         $(this).find(".fa-chevron-down").toggleClass("rotate-active");
//         // if($(this).find('.sidebar-submenu').hasClass('active')){
//         //     $('.sidebar-submenu ul').show();
//         // }
//     });
// });
/** ******  left menu  *********************** **/




/********** Image Modal Start ***********/
// Get the modal
var modal = document.getElementById("ImgModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.querySelectorAll(".Img-List");

var modalImg = document.getElementById("img");

for(var i = 0; i<img.length; i++){
    img[i].onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
    }
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal.style.display = "none";
// }

/********** Image Modal End ***********/

/********* **********/
// $('.earning-list > div').load(function(){
//     if($(this).hasClass('not-found')){
//         $(this).parent.find('.earning-list').css('background-color','white');
//     }
//     else{
//         $(this).parent.find('.earning-list').css('background-color','green');
//     }

// });


$(document).ready(function(){
      
    //Mobile Toggle Menu//
    $(".toggle").click(function(){
        $(".backdrop").show();
        $(".left_col").show();
        $(".left_col").addClass('show-sidebar');
        $("#sidebar-menu").show();
        $("#sidebar-menu").addClass('show-sidebar');
        $("html").toggleClass("model-open");
        $(".filter-wrapper").hide();
    });
    $(".backdrop").click(function(){
        $(this).hide();
        $(".left_col").hide();
        $(".left_col").removeClass('show-sidebar');
        $("#sidebar-menu").hide();
        $("#sidebar-menu").removeClass('show-sidebar');
        $("html").toggleClass("model-open");
    });

    $(".menu-close-button").click(function(){
        $(".left_col").hide();
        // $("#sidebar-menu").hide();
        $(".backdrop").hide();
        $(".left_col").removeClass('show-sidebar');
        $("#sidebar-menu").removeClass('show-sidebar');
        $("html").toggleClass("model-open");
    });

    //Mobile Filter Toggle
    $(".filter-button").click(function(){
        $(".filter-wrapper").toggle();
        $("html").toggleClass("model-open");
    });

    $(".filter-header-wrapper").click(function(){
        $(".filter-wrapper").hide();
        $("html").toggleClass("model-open");
    });

});



//**** Drpdown Checkboxes */
var options = [];

$( '.dropdown-menu a' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
      
   console.log( options );
   return false;
});


/**** Product Box Toggle Menu - influencer-post.html */
$("#scheduled").click(function(){
    $("#scheduled-box").addClass("post-active");
    $("#scheduled").addClass("toggle-link-active");
    $("#history-box").removeClass("post-active");
    $("#history").removeClass("toggle-link-active");
});

$("#history").click(function(){
    $("#history-box").addClass("post-active");
    $("#history").addClass("toggle-link-active");
    $("#scheduled-box").removeClass("post-active");
    $("#scheduled").removeClass("toggle-link-active");
});



/** ******  tooltip  *********************** **/
$(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    /** ******  /tooltip  *********************** **/
    /** ******  progressbar  *********************** **/
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar(); // bootstrap 3
}
/** ******  /progressbar  *********************** **/
/** ******  switchery  *********************** **/
if ($(".js-switch")[0]) {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function (html) {
        var switchery = new Switchery(html, {
            color: '#26B99A'
        });
    });
}
/** ******  /switcher  *********************** **/
/** ******  collapse panel  *********************** **/
// Close ibox function
$('.close-link').click(function () {
    var content = $(this).closest('div.x_panel');
    content.remove();
});

// Collapse ibox function
$('.collapse-link').click(function () {
    var x_panel = $(this).closest('div.x_panel');
    var button = $(this).find('i');
    var content = x_panel.find('div.x_content');
    content.slideToggle(200);
    (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
    (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    setTimeout(function () {
        x_panel.resize();
    }, 50);
});
/** ******  /collapse panel  *********************** **/
/** ******  iswitch  *********************** **/
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
/** ******  /iswitch  *********************** **/
/** ******  star rating  *********************** **/
// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function ($, window) {
    var Starrr;

    Starrr = (function () {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function (e, value) {}
        };

        function Starrr($el, options) {
            var i, _, _ref,
                _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                _ = _ref[i];
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'span', function (e) {
                return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function () {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'span', function (e) {
                return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);
        }

        Starrr.prototype.createStars = function () {
            var _i, _ref, _results;

            _results = [];
            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
            }
            return _results;
        };

        Starrr.prototype.setRating = function (rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function (rating) {
            var i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                }
            }
            if (!rating) {
                return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function () {
            var args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function () {
                var data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

$(function () {
    return $(".starrr").starrr();
});

$(document).ready(function () {

    $('#stars').on('starrr:change', function (e, value) {
        $('#count').html(value);
    });


    $('#stars-existing').on('starrr:change', function (e, value) {
        $('#count-existing').html(value);
    });

});
/** ******  /star rating  *********************** **/
/** ******  table  *********************** **/
$('table input').on('ifChecked', function () {
    check_state = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    check_state = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var check_state = '';
$('.bulk_action input').on('ifChecked', function () {
    check_state = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    check_state = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    check_state = 'check_all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    check_state = 'uncheck_all';
    countChecked();
});

function countChecked() {
        if (check_state == 'check_all') {
            $(".bulk_action input[name='table_records']").iCheck('check');
        }
        if (check_state == 'uncheck_all') {
            $(".bulk_action input[name='table_records']").iCheck('uncheck');
        }
        var n = $(".bulk_action input[name='table_records']:checked").length;
        if (n > 0) {
            $('.column-title').hide();
            $('.bulk-actions').show();
            $('.action-cnt').html(n + ' Records Selected');
        } else {
            $('.column-title').show();
            $('.bulk-actions').hide();
        }
    }
    /** ******  /table  *********************** **/
    /** ******    *********************** **/
    /** ******    *********************** **/
    /** ******    *********************** **/
    /** ******    *********************** **/
    /** ******    *********************** **/
    /** ******    *********************** **/
    /** ******  Accordion  *********************** **/

$(function () {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

/** ******  Accordion  *********************** **/
/** ******  scrollview  *********************** **/
$(document).ready(function () {
  
            $(".scroll-view").niceScroll({
                touchbehavior: true,
                cursorcolor: "rgba(42, 63, 84, 0.35)"
            });

});
/** ******  /scrollview  *********************** **/

// mobile verify/////////////////
$('.digit-group').find('input').each(function() {
	$(this).attr('maxlength', 1);
	$(this).on('keyup', function(e) {
		var parent = $($(this).parent());
		
		if(e.keyCode === 8 || e.keyCode === 37) {
            
			var prev = parent.find('input#' + $(this).data('previous'));
			
			if(prev.length) {
				$(prev).select();
			}
		} else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
			var next = parent.find('input#' + $(this).data('next'));
			
			if(next.length) {
				$(next).select();
			} else {
				if(parent.data('autosubmit')) {
					parent.submit();
				}
			}
		}
	});
});


//////////////////// product slider////////////////////////////////
$(document).ready(function () {
$('#productCarousel').carousel({
  interval: false
});
$('#carousel-thumbs').carousel({
  interval: false
});

// handles the carousel thumbnails
// https://stackoverflow.com/questions/25752187/bootstrap-carousel-with-thumbnails-multiple-carousel
$('[id^=carousel-selector-]').click(function() {
  var id_selector = $(this).attr('id');
  var id = parseInt( id_selector.substr(id_selector.lastIndexOf('-') + 1) );
  $('#productCarousel').carousel(id);
});
// Only display 3 items in nav on mobile.
if ($(window).width() < 575) {
  $('#carousel-thumbs .row div:nth-child(4)').each(function() {
    var rowBoundary = $(this);
    $('<div class="row mx-0">').insertAfter(rowBoundary.parent()).append(rowBoundary.nextAll().addBack());
  });
  $('#carousel-thumbs .carousel-item .row:nth-child(even)').each(function() {
    var boundary = $(this);
    $('<div class="carousel-item">').insertAfter(boundary.parent()).append(boundary.nextAll().addBack());
  });
}
// Hide slide arrows if too few items.
if ($('#carousel-thumbs .carousel-item').length < 2) {
  $('#carousel-thumbs [class^=carousel-control-]').remove();
  $('.machine-carousel-container #carousel-thumbs').css('padding','0 5px');
}
// when the carousel slides, auto update
$('#productCarousel').on('slide.bs.carousel', function(e) {
  var id = parseInt( $(e.relatedTarget).attr('data-slide-number') );
  $('[id^=carousel-selector-]').removeClass('selected');
  $('[id=carousel-selector-'+id+']').addClass('selected');
  
});
// when user swipes, go next or previous
$('#productCarousel').swipe({
  fallbackToMouseEvents: true,
  swipeLeft: function(e) {
    $('#productCarousel').carousel('next');
  },
  swipeRight: function(e) {
    $('#productCarousel').carousel('prev');
  },
  allowPageScroll: 'vertical',
  preventDefaultEvents: false,
  threshold: 75
});
/*
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
*/

$('#productCarousel .carousel-item img').on('click', function(e) {
  var src = $(e.target).attr('data-remote');
  if (src) $(this).ekkoLightbox();
});




});

/********** Image Modal Start ***********/
// Get the modal
var modal = document.getElementById("ImgModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.querySelectorAll(".Img-List");

var modalImg = document.getElementById("img");

for(var i = 0; i<img.length; i++){
    img[i].onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
    }
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

/********** Image Modal End ***********/

$(document).ready(function() {
    getNotifications();
    getNotificationCount();
})

function getNotifications() {
    
    __ajaxregister_http("business/notifications/listing", "", headers(), AJAX_CONF.apiType.GET, "", __success_getNotifications);
}

function __success_getNotifications(response) {
    console.log(response)
    if(response.success == true) {
        let notifications = '';
        
        response.data.forEach(element => {
            const dateTimeAgo = moment(element.created_at).fromNow();
            notifications += 
            `<li id="markAsRead" data-id="${element.id}" data-request_id="${element.request_id}">
                <a>
                    <span class="image">
                    </span>
                    <span>
                            <span class="time">${dateTimeAgo}</span>
                            </span>
                            <span class="message">
                        ${element.description} 
                    </span>
                </a>
            </li>`
        });
        if(response.data.length >= 1 ) {
            notifications += `<li>
                <div class="text-center">
                    <a href="notifications.php">
                        <strong>See All Notifications</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </li>`;
        } else {
            notifications += `<li>
                <div class="text-center">
                    <a href="">
                        <strong>No Notifications</strong>
                    </a>
                </div>
            </li>`;
        }
        $(".msg_list").append(notifications);
    }
}

$(document).on('click', '#markAsRead', function(event){
    event.preventDefault();
    let order_id = $(this).attr('data-request_id');
    var json = {
        notification_id: $(this).attr('data-id')
    }
    markAsRead(json);
    window.location.href = "order-detail.php?order_id="+ btoa(btoa(order_id));
});

function markAsRead(json) {
    __ajaxregister_http("/business/notifications/mark-read", json, headers(), AJAX_CONF.apiType.PUT, "", __success_markAsRead);
}

function __success_markAsRead(response) {
    console.log(response);
}

function getNotificationCount() {
    
    __ajaxregister_http("business/notifications/unread-count", "", headers(), AJAX_CONF.apiType.GET, "", __success_getNotificationCount);
}

function __success_getNotificationCount(response) {
    console.log(response)
    if(response.status == true) {
        $("#notification_count").html(response.unread_count)
    }
}

$(document).on('click', '#nav-click-old', function(e) {
    var user_id = localStorage.getItem('userId');
    var user_token = STORAGE.get(STORAGE.accesstoken);
    var nav_url = $(this).data('url');
    console.log(nav_url);
    e.preventDefault();
    if(user_id) {
        $.ajax({
            url: "https://twiva.co.ke/api/v1/nav-link",
            // url: "http://localhost:8000/api/v1/nav-link",
            headers: {
                'Accept': 'application/json',
                // 'X-CSRF-Token': $('meta[name="_token"]').attr('content'),
                ['Authorization']: "Bearer " + STORAGE.get(STORAGE.accesstoken),
            },
            type: "post",
            data: { user_id: user_id, nav_url : nav_url, user_token : user_token},
            success: function(data){
                console.log(data);
                console.log(data.data.url);
                // window.open(data.data.url);
                // let url = data.data.url.split("?");
                window.location.href = data.data.url;
            },
            error : function(data) {
                console.log(data)
            }
        });
    }
});
$(document).ready(function() {
    var name= STORAGE.get(STORAGE.name);
    var email= STORAGE.get(STORAGE.email);
    var logo ;
    logo = STORAGE.get(STORAGE.logo);
    var image ;
    image = '<?= IMAGES_URI_PATH ?>/icons/profile.jpeg';
    
    if(logo && logo != "null" && logo != null ) {
        image = 'https://api.twiva.co.ke/storage/images/products/'+logo;
    }
    console.log(logo);
    setTimeout(() => {
        document.getElementById("user-image").src = image;
        document.getElementById("user-name").innerHTML = name;
        document.getElementById("nname").innerHTML = name;
        document.getElementById("eemail").innerHTML = email;
    }, 2000);
    
    
    //     $('#userprofile').append(`
    //      <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    //             <img src="${logo}" alt="">  ${name}
    //             <!-- <span class=" fa fa-angle-down"></span> -->
                
    //         </a>
    //     `)
        
    
});

