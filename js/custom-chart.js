
// Line Chart //
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
            data: [18000, 15000, 18000, 35000, 20000, 4000, 22000, 15000, 18000, 35000, 20000, 45000,],
            borderColor: [
                '#2F80ED',
            ],
            borderWidth: 2,
            tension: 0.5,
            pointRadius: 0,
        }]
    },
    options: {
        
        scales: {
            y: {
                beginAtZero: true,
                type:'linear',
            },
            
        },
        plugins: {
            legend: {
                display: false,
                
            }
        }
    }
});
    


// Pie Chart //
var pie = document.getElementById('piechart').getContext('2d');

var piechart = new Chart(pie, {
    type: 'doughnut',
    data: {
        labels: ['Order', 'Stock' ,'Profit', 'Sale'],
        datasets: [{
            data: [25, 25, 25, 25],
            backgroundColor: [
                '#28D094',
                '#6F52ED',
                '#FF4C61',
                '#FFB800',
            ],
            borderWidth: 0,
            radius: 80,
            cutout: 100,
            
            
        }]
    },
    options: {
        scales: {
            y: {
                display:false,
            }
        },
        
        plugins: {
            legend: {
                display: false,
                
            }
        },

        
    }
});
        