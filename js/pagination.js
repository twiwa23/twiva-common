function Pagination(totalRecords, limit, page_no) {
  let totalPage = Math.ceil(totalRecords / limit);
  let output = `<ul class='pagination'><li class="page-item">
  <a class="page-link left-arrow ${page_no == 1 ? 'disabled' : false}" id="prev" href="#"><img src="../../images/icons/left-gray-arr.svg"></a>
</li>`;
  for (let i = 1; i <= totalPage; i++) {
    let active = '';
    if (i == page_no) {
      active = "page-link-active";
    } else {
      active = "";
    }
    output += `<li class='page-item ${active}'><a class='page-link ${i == page_no ? 'disabled' : false}' id='${i}' href='#'>${i}</a></li>`;
  }
  output += `<li class="page-item">
  <a class="page-link left-arrow ${totalPage == page_no ? 'disabled' : false}" id="next" href="#"><img src="../../images/icons/right-gray-arr.svg"></a>
</li></ul>`;
  return output;
}
